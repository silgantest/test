#include "SystemConfigLoader.h"
#include "..\..\3rdParty\pugixml-1.9\src\pugixml.hpp" 


SystemConfigLoader::Holder SystemConfigLoader::instanceHolder = SystemConfigLoader::Holder();
SystemConfig* SystemConfigLoader::theConfig;

SystemConfigLoader::SystemConfigLoader()
{
}

SystemConfigLoader::~SystemConfigLoader()
{
}

SystemConfig* SystemConfigLoader::GetConfig()
{
	return theConfig;
}

SystemConfigLoader::Holder::Holder()
{
	instance = new SystemConfigLoader();
	theConfig = instance->LoadConfig();
}

SystemConfigLoader::Holder::~Holder()
{
	delete instance;
	delete theConfig;
}

SystemConfig* SystemConfigLoader::LoadConfig()
{
	const char* CONFIG = "config";
	const char* SYSTEM = "system";
	const char* REQUIRED_CAMERAS = "requiredCameras";
	const char* MODEL = "model";
	const char* CAMERAS = "cameras";

	const char* NUMBER = "number";
	const char* VENDOR = "vendor";
	const char* TITLE = "title";
	const char* SERIAL = "serial";

	const char* SETTINGS = "settings";
	const char* AOI = "aoi";
	const char* OFFSETX = "offsetX";
	const char* OFFSETY = "offsetY";
	const char* WIDTH = "width";
	const char* HEIGHT = "height";

	int numCams;

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(CONFIG_FILE);

	//Read system node
	pugi::xml_node sysnode = doc.child(CONFIG).child(SYSTEM);
	numCams = sysnode.child(REQUIRED_CAMERAS).text().as_int();

	SystemConfig* config = new SystemConfig(sysnode.child(MODEL).text().as_string(), numCams);

	//For each camera
	pugi::xml_object_range<pugi::xml_node_iterator> it = doc.child(CONFIG).child(CAMERAS).children();
	for (pugi::xml_node_iterator nit = it.begin(); nit != it.end(); ++nit) 
	{
		int num;
		sscanf_s(nit->child(NUMBER).text().as_string(), "%d", &num);

		ExpectedCameraConfig* ec = new ExpectedCameraConfig(num, nit->child(VENDOR).text().as_string(), 
			nit->child(MODEL).text().as_string(),
			nit->child(SERIAL).text().as_string(), 
			nit->child(TITLE).text().as_string());
		config->AddCamera(ec);
		pugi::xml_node aoi = nit->child(SETTINGS);
		aoi = aoi.child(AOI);
			
		ec->AddAOI(aoi.child(OFFSETX).text().as_int(), 
			aoi.child(OFFSETY).text().as_int(),
			aoi.child(WIDTH).text().as_int(), 
			aoi.child(HEIGHT).text().as_int());

	}
	config->SetAppConfig(AppConfig::LoadAppConfig85());
	return config;
}

