#include "Point.h"
#include "LabelIntegrityMethod.h"
#include <string>
#include <vector>
#include "defs.h"
#include "inspection.h"


#ifndef JOB_H
#define JOB_H

#define MAX_JOB_NAME_LENGTH 50
#define MAX_JOB_PATH_LENGTH 150
#define JOB_FULL_PATH "%s\\%s\\job.xml"
#define JOB_DIR "%s\\%s"

#define INSPECTIONS_85 12

class EXPORT Job
{
public:
	enum class ShowStitch
	{
		Light,
		Dark,
		Edges, 
		Final, 
		BW,
		Unknown
	};

	enum class EdgeTransition
	{
		Dark2Light,
		Light2Dark,
		Unknown
	};

	enum class UseStitching
	{
		Light, 
		Dark, 
		Edge
	};

	struct Bounds
	{
		int left;
		int right;
		int top;
		int bottom;
	};

	struct WaistSettings
	{
		Job::Bounds bounds;
		Job::Bounds upperBounds;
		int edgeThreshold;
		int vertSearchLength;
		int horSearchLength;

	};

	struct CamSettings
	{
		Job::WaistSettings waistSettings;
		int index;
		int taughtMiddleRefY;
		int shiftTopLim;
		int shiftBotLim;
		int ySplice;
		int lightLevel;
		int noLabelOffsetX;
		int stitchY;
	};
	

private:

	char name[MAX_JOB_NAME_LENGTH];
	char pathToJob[MAX_JOB_PATH_LENGTH];
	Inspection** inspections; //24*12
	Bounds labelMatchingBounds;
	Bounds teachColorBounds;

	int numCams;
	int vertBarX;
	int midBarY;
	int neckBarY;
	int capColorSize;
	int yTopLimPatt;
	int yBotLimPatt;
	int wSplice;
	int hSplice;
	int hSpliceSmBx;
	int spliceVar;
	int avoidOffX;
	int middImg;
	int noLabelOffset;
	int noLabelOffsetW;
	int noLabelOffsetH;
	int rotOffset;
	int bottleStyle;
	int boxYt;
	int boxWt;
	int boxHt;
	int boxYb;
	int boxHb;
	int shiftHorIniLim;
	int shiftHorEndLim;
	int shiftWidth;
	int sensEdge;
	int labelSen;
	int nolabelSen;
	int shiftedLabSen;
	int edgeLabSen;
	int ellipseSen;
	int senMidd;
	int stitchWidth;
	int stitchHeight;
	int stLighSen;
	int stDarkSen;
	int stEdgeSen;
	int sportPos;
	int sportSen;
	int ldist;
	int targetColorRed;
	int targetColorGreen;
	int targetColorBlue;
	int neckStrength;
	int capStrength;
	int capStrength2;
	int orienArea;
	int fillOffset;
	int capColorOffset;
	int motPos;
	int sensMethod3;
	int heightMethod3;
	int widthMethod3;
	int heightMethod5;
	int widthMethod5;
	int boltOffset;
	int boltOffset2;

	ShowStitch showStitch;
	LabelIntegrityMethod labelIntegrityMethod;
	EdgeTransition edgeTransition;
	ImageFilters filterSelPatt;
	ImageFilters filterSelPattCap;
	ImageFilters filterSelNoLabel;
	ImageFilters filterSelShifted;
	ImageFilters filterSelSplice;
	UseStitching useStitching;
	ImageFilters stLighImg;
	ImageFilters stDarkImg;
	ImageFilters stEdgeImg;

	CamSettings* camSettings;

	bool displayable;
	bool showLabelMatching;
	bool c5InPictures;
	bool middles;
	bool edges;
	bool showInsp5;
	bool showInsp7;
	bool showInsp8;
	bool taught;
	bool findMiddles;
	bool enabledSportCap;
	bool useRing;
	bool wehaveTempX;
	bool useStLigh;
	bool useStDark;
	bool useStEdge;
	bool isFloat;
	Job(int cams);

public:
	static Job* Create(int cams);
	virtual ~Job();

	const char* GetName();

	//Cap Inspection
	int GetLDist();
	int GetColorTargR(); 
	int GetColorTargG(); 
	int GetColorTargB(); 
	int GetNeckStrength(); 
	int GetCapStrength();
	int GetCapStrength2();
	int GetOrienArea();
	int GetBottleStyle();
	int GetSportSen();
	int GetSportPos();
	bool SportCap();
	int GetNeckBarY();
	int GetCapColorOffset();

	//Bottle Inspection
	int GetMiddImg();
	int GetSenMidd();
	int GetHeigMet3();
	int GetWidtMet3();
	int GetSensMet3();
	int GetWidtMet5();
	int GetHeigMet5();
	int GetEdgeLabSen();
	int GetShiftedLabSen();
	int GetSensEdge();
	int GetBoxWt();
	int GetBoxHt();
	int GetBoxYt();
	int GetBoxHb();
	int GetBoxYb();
	int GetYTopLimPatt();
	int GetYBotLimPatt();
	int GetTaughtMiddleRefY(int cameraIndex);
	int GetShiftTopLim(int camIndex);
	int GetShiftBottomLim(int camIndex);
	int GetShiftHorIniLim();
	int GetShiftHorEndLim();
	int GetFindMiddles();
	int GetShiftWidth();
	ImageFilters GetStLightImg();
	ImageFilters GetStDarkImg();
	ImageFilters GetStEdgeImg();
	int GetStLightSen();
	int GetStDarkSen();
	int GetStEdgeSen();
	int GetUseStLight();
	int GetUseStDark();
	int GetUseStEdge();
	int GetAvoidOffsetX();

	ImageFilters GetFilterSelShifted();
	ImageFilters GetFilterSelPattCap();
	ImageFilters GetFilterSelPatt();
	ImageFilters GetFilterSelNoLabel();
	ImageFilters GetFilterSelSplice();
	LabelIntegrityMethod GetLabelIntegrityMethod();
	Job::EdgeTransition GetEdgeTransition();
	int GetNoLabelSens();
	int GetEllipseSens();
	int GetHSplice();
	int GetWSplice();
	int GetHSpliceSmBx();
	int GetYSplice(int index);
	int GetSpliceVar();

	int GetWaistEdgeThreshold(int camIndex);
	int GetWaistVerticalSearchLength(int camIndex);
	int GetWaistHorizontalSearchLength(int camIndex);
	int GetWaistMeasurementTopBound(int camIndex);
	int GetWaistMeasurementBottomBound(int camIndex);
	int GetWaistMeasurementLeftBound(int camIndex);
	int GetWaistMeasurementRightBound(int camIndex);
	int GetWaistMeasurementUpperTopBound(int camIndex);
	int GetWaistMeasurementUpperBottomBound(int camIndex);
	int GetWaistMeasurementUpperLeftBound(int camIndex);
	int GetWaistMeasurementUpperRightBound(int camIndex);

	char* GetPath();

	int GetNoLabelOffset();
	int GetNoLabelOffsetX(int nCam);
	int GetNoLabelOffsetW();
	int GetNoLabelOffsetH();

	void SetName(const char* jobname, const char* path);
	void SetDisplayable(bool val);
	void SetLabelMatching(bool show, Bounds bounds, Bounds teachColor);
	void SetBars(int vertBarX, int midBarY, int neckBarY);
	void SetShowStitch(ShowStitch value);
	void SetCapColorSize(int size);
	void SetShow(bool c5InPictures, bool middles, bool edges, bool showInsp5, bool showInsp7, bool showInsp8);
	void SetCameraWaist(int index, Bounds bounds, Bounds upperBounds,
		int edgeThres, int vertSearchLength, int horSearchLength);
	void SetCameraInsp(int index, int taughtMiddleRefY, int shiftTopLim, int shiftBotLim, 
		int ySplice, int noLabelOffsetX, int stitchY);
	void SetTaught(bool taught);
	void SetLimPatt(int yTopLimPatt, int yBotLimPatt);
	void SetSplice(int wSplice, int hSplice, int hSpliceSmBx, int spliceVar);
	void SetLabelIntegrityMethod(LabelIntegrityMethod labelIngMtd);
	void SetEdgeTransition(EdgeTransition et);
	void SetAvoidOffsetX(int avoidOffX);
	void SetMiddImg(int midImg);
	void SetFindMiddles(bool findMid);
	void SetNoLabelOffset(int offset);
	void SetNoLabelOffsetW(int offset);
	void SetNoLabelOffsetH(int offset);
	void SetRotOffset(int rotOff);
	void SetNeckBar(int value);
	void SetNeckStrength(int value);
	void SetCapFilter(ImageFilters filter);
	void SetVertXBar(int value);
	void SetMidYBar(int value);
	void SetCapStrength(int value);
	void SetCapStrength2(int value);
	void SetCapColorOffset(int value);
	void SetUseSportCap(bool value);
	void SetLDist(int value);
	void SetSportSen(int value);
	void SetSportPos(int value);

	int GetCapColorSize();
	int GetVertXBar();
	int GetMidYBar();

	int GetRotOffset();
	int GetStitchY(int index);
	int GetStitchW();
	int GetStitchH();
	Job::Bounds GetLabelMatchingBounds();
	Job::Bounds GetTeachColorBounds();
	bool GetShowLabelMatching();
	Job::ShowStitch GetShowStitch();

	bool GetShowInsp8();
	bool GetShowInsp7();
	bool GetShowInsp5();
	bool GetShowEdges();
	bool GetShowMiddles();
	bool GetShowC5InPictures();
	int GetMotPos();
	bool GetUseRing();

	int GetLightLevel(int index);

	void SetNumberCams(int cams);
	int GetNumberCams();

	void SetBottleStyle(int bottStyle);
	void SetFilter(ImageFilters filterSelPatt, ImageFilters filterSelPattCap, 
		ImageFilters filterSelNoLabel, ImageFilters filterSelShifted, ImageFilters filterSelSplice);
	void SetBox(int boxYt, int boxWt, int boxHt, int boxYb, int boxHb);
	void SetShift(int shiftHorIniLim, int shiftHorEndLim, int shiftWidth);
	void SetSensors(int sensEdge, int labelSen, int nolabelSen, int shiftedLabSen,
		int edgeLabSen, int ellipseSen, int senMidd);
	void SetStitching(UseStitching use, int stitchWidth, int stitchHeight,
		int stLighSen, int stDarkSen, int stEdgeSen,
		int stLighImg, int stDarkImg, int stEdgeImg);
	void SetSport(bool enabled, int sportPos, int sportSen);
	void SetColor(int targetColorRed, int targetColorGreen, int targetColorBlue);

	void SetCap(int ldist, int targetColorRed, int targetColorGreen, int targetColorBlue,
		int neckStrength, int capStrength, int capStrength2, int orienArea, int fillOffset,
		int capColorOffset);
	void SetLightLevel(int index, int lightLevel);
	void SetUseRing(bool useRing);
	void SetMotPos(int motPos);
	void SetMethods(int sensMethod3, int heightMethod3, int widthMethod3, int heightMethod5, int widthMethod5);

	int GetBoltOffset();
	int GetBoltOffset2();
	void SetBoltOffset(int offset);
	void SetBoltOffset2(int offset);

	bool GetWeHaveTempX();
	void SetWeHaveTempX(bool set);

	void SetInspection(int index, bool enabled, float lmin, float lmax, float lminB, float lmaxC, bool isFloat);
	Inspection* GetInspection(int index);
	float GetInspectionMax(int index);

	Job::Bounds GetWaistMeasurementBounds(int camIndex);
	Job::Bounds GetWaistMeasurementUpperBounds(int camIndex);
	int GetWaistLabelOffsetX(int camIndex);
	int GetWaistStitchY(int camIndex);
	bool GetTaught();
	int GetLabelSens();
	int GetFillOffset();
};

#endif