//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 4.0.2
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------


public class UIImageSettings : global::System.IDisposable {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal UIImageSettings(global::System.IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(UIImageSettings obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  ~UIImageSettings() {
    Dispose(false);
  }

  public void Dispose() {
    Dispose(true);
    global::System.GC.SuppressFinalize(this);
  }

  protected virtual void Dispose(bool disposing) {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          SvfLibPINVOKE.delete_UIImageSettings(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
    }
  }

  public UIImageSettings(int width, int height, int camIndex) : this(SvfLibPINVOKE.new_UIImageSettings(width, height, camIndex), true) {
  }

  public int GetWidth() {
    int ret = SvfLibPINVOKE.UIImageSettings_GetWidth(swigCPtr);
    return ret;
  }

  public int GetHeight() {
    int ret = SvfLibPINVOKE.UIImageSettings_GetHeight(swigCPtr);
    return ret;
  }

  public int GetCamIndex() {
    int ret = SvfLibPINVOKE.UIImageSettings_GetCamIndex(swigCPtr);
    return ret;
  }

}
