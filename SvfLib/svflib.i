/* File : example.i */
%module SvfLib

%{
#define EXPORT 
#include "AppConfig.h"
#include "Directories.h"
#include "IPC.h"
#include "Inspection.h"
#include "Job.h"
#include "JobLoader.h"
#include "LabelIntegrityMethod.h"
#include "Point.h"
#include "SystemConfig.h"
#include "SystemConfigLoader.h"
#include "IPCCommands.h"
#include "CapResultDetailsIndices.h"
%}

 %define EXPORT 
 %enddef
 %include "AppConfig.h"
 %include "Directories.h"
 %include "IPC.h"
 %include "Inspection.h" 
 %include "Job.h"
 %include "JobLoader.h"
 %include "LabelIntegrityMethod.h"
 %include "Point.h"
 %include "SystemConfig.h"
 %include "SystemConfigLoader.h"
 %include "IPCCommands.h"
 %include "CapResultDetailsIndices.h"
