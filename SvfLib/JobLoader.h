#include "job.h"
#include "directories.h"
#include "..\..\3rdParty\pugixml-1.9\src\pugixml.hpp" 
#include "defs.h"

#include <unordered_map>

#ifndef JOB_LOADER
#define JOB_LOADER


class EXPORT JobLoader
{

private:

	char str[50];
	std::unordered_map<std::string, Job::ShowStitch> showStitchTable;
	std::unordered_map<Job::ShowStitch, std::string> showStitchTableString;
	std::unordered_map<std::string, Job::EdgeTransition> showEdgeTransTable;
	std::unordered_map<Job::EdgeTransition, std::string> showEdgeTransTableString;
	std::unordered_map<std::string, Job::UseStitching> stitchingUseTable;
	std::unordered_map<Job::UseStitching, std::string> stitchingUseTableString;
	std::unordered_map<int, LabelIntegrityMethod> methodTable;
	std::unordered_map<LabelIntegrityMethod, int> methodTableInt;
	std::unordered_map<int, ImageFilters> filtersTable;
	std::unordered_map<ImageFilters, int> filtersTableInt;

	const char* JOB = "job";
	const char* JOBS = "jobs";
	const char* NUM_CAMS = "numCams";
	const char* DISPLAYABLE = "displayable";
	const char* DRAW_SETTINGS = "drawSettings";
	const char* INSPECTION_SETTINGS = "inspectionSettings";
	const char* CAMERA_SETTINGS = "cameraSettings";
	const char* PLC_SETTINGS = "plcSettings";

	const char* LEFT_BOUND = "leftBound";
	const char* RIGHT_BOUND = "rightBound";
	const char* TOP_BOUND = "topBound";
	const char* BOTTOM_BOUND = "bottomBound";


	const char* LABEL_MATCHING = "labelMatching";
	const char* SHOW_LABEL_MATCHING = "show";
	const char* BOUNDS = "bounds";
	const char* TEACH_COLOR = "teachColor";

	const char* VERT_BAR_X = "vertBarX";
	const char* MID_BAR_Y = "midBarY";
	const char* NECK_BAR_Y = "neckBarY";
	const char* SHOW_ST = "showSt";
	const char* CAP_COLOR_SIZE = "capColorSize";
	const char* SHOW_C5_IN_PICTURES = "showC5inPictures";
	const char* SHOW_MIDDLES = "showMiddles";
	const char* SHOW_EDGES = "showEdges";
	const char* SHOW_INSP = "showInsp";
	const char* SHOW_INSP_5 = "show5";
	const char* SHOW_INSP_7 = "show7";
	const char* SHOW_INSP_8 = "show8";

	const char* INDEX = "index";
	const char* WAIST_MEASUREMENT = "waistMeasurement";
	const char* UPPER_BOUNDS = "upperBounds";
	const char* EDGE_THRESHOLD = "edgeThreshold";
	const char* SEARCH_LENGTH = "searchLength";
	const char* VERTICAL = "vertical";
	const char* HORIZONTAL = "horizontal";
	const char* TAUGHT_MIDDLE_REF_Y = "taughtMiddleRefY";
	const char* SHIFT_TOP_LIM = "shiftTopLim";
	const char* SHIFT_BOT_LIM = "shiftBotLim";
	const char* Y_SPLICE = "ysplice";
	const char* NO_LABEL_OFFSET_X = "nolabeloffsetx";
	const char* STITCH_Y = "stitchY";
	const char* TAUGHT = "taught";
	const char* Y_TOP_LIM_PATT = "ytopLimPatt";
	const char* Y_BOT_LIM_PATT = "ybotLimPatt";
	const char* W_SPLICE = "wSplice";
	const char* H_SPLICE = "hSplice";
	const char* H_SPLICE_SM_BX = "hSpliceSmBx";
	const char* SPLICE_VAR = "spliceVar";
	const char* LAB_INTEGRITY_METHOD = "labelIntegrityMethod";
	const char* EDGE_TRANSITION = "edgeTransition";
	const char* AVOID_OFF_X = "avoidOffX";
	const char* MIDD_IMG = "middImg";
	const char* FIND_MIDDLES = "findMiddles";
	const char* NO_LABEL_OFFSET_W = "nolabeloffsetW";
	const char* NO_LABEL_OFFSET_H = "nolabeloffsetH";
	const char* NO_LABEL_OFFSET = "nolabeloffset";
	const char* ROT_OFFSET = "rotoffset";
	const char* BOTTLE_STYLE = "bottleStyle";
	const char* TEMP_X = "tempX";
	const char* FILTER = "filter";
	const char* FILTER_SEL_PATT = "filterSelPatt";
	const char* FILTER_SEL_PATT_CAP = "filterSelPattCap";
	const char* FILTER_SEL_NO_LABEL = "filterSelNoLabel";
	const char* FILTER_SEL_SHIFTED = "filterSelShifted";
	const char* FILTER_SEL_SPLICE = "filterSelSplice";

	const char* BOX = "box";
	const char* BOX_YT = "boxYt";
	const char* BOX_WT = "boxWt";
	const char* BOX_HT = "boxHt";
	const char* BOX_YB = "boxYb";
	const char* BOX_HB = "boxHb";

	const char* SHIFT = "shift";
	const char* SHIFT_HOR_INI_LIM = "shiftHorIniLim";
	const char* SHIFT_HOR_END_LIM = "shiftHorEndLim";
	const char* SHIFT_WIDTH = "shiftWidth";

	const char* SENSORS = "sensors";
	const char* SENS_EDGE = "sensEdge";
	const char* LABEL_SENS = "labelSen";
	const char* NO_LABEL_SENS = "nolabelSen";
	const char* SHIFTED_LABEL_SENS = "shiftedLabSen";
	const char* EDGE_LABEL_SENS = "edgeLabSen";
	const char* ELLIPSE_SENS = "ellipseSen";
	const char* MIDD_SENS = "senMidd";

	const char* STITCHING = "stitching";
	const char* USE = "use";
	const char* STITCH_WIDTH = "stitchWidth";
	const char* STITCH_HEIGHT = "stitchHeight";
	const char* STITCH_LIGHT_SENSOR = "stLighSen";
	const char* STITCH_DARK_SENSOR = "stDarkSen";
	const char* STITCH_EDGE_SENSOR = "stEdgeSen";
	const char* STITCH_LIGHT_IMG = "stLighImg";
	const char* STITCH_DARK_IMG = "stDarkSen";
	const char* STITCH_EDGE_IMG = "stEdgeSen";

	const char* SPORT = "sport";
	const char* ENABLED = "enabled";
	const char* SPORT_POSITION = "sportPos";
	const char* SPORT_SENSOR = "sportSen";

	const char* CAP = "cap";
	const char* L_DIST = "ldist";
	const char* COLOR_TARGET = "colorTarget";
	const char* RED = "red";
	const char* GREEN = "green";
	const char* BLUE = "blue";

	const char* NECK_STRENGTH = "neckStrength";
	const char* CAP_STRENGTH = "capStrength";
	const char* CAP_STRENGTH2 = "capStrength2";
	const char* ORIEN_AREA = "orienArea";
	const char* FILL_OFFSET = "fillOffset";
	const char* CAP_COLOR_OFFSET = "capColorOffset";

	const char* METHODS = "methods";
	const char* SENS_METHOD_3 = "sensMet3";
	const char* WIDTH_METHOD_3 = "heigMet3";
	const char* HEIGHT_METHOD_3 = "widtMet3";
	const char* WIDTH_METHOD_5 = "heigMet5";
	const char* HEIGHT_METHOD_5 = "widtMet5";

	const char* INSPECTIONS = "inspections";
	const char* L_MIN = "lMin";
	const char* L_MAX = "lMax";
	const char* L_MAX_C = "lMaxC";
	const char* L_MIN_B = "lMinB";
	const char* IS_FLOAT = "isFloat";

	const char* USE_RING = "useRing";
	const char* MOT_POS = "motPos";
	const char* LIGHT_LEVEL = "lightLevel";

	void WriteFloat(float value, pugi::xml_node node);
	void WriteInt(int value, pugi::xml_node node);
	void WriteBool(bool value, pugi::xml_node node);

public:
	JobLoader();
	~JobLoader();
	Job* LoadJob(const char* jobName);
	Job* LoadDefaultJob();

	void SaveJob(Job* loadedJob);

private:

	Job* doLoadJob(pugi::xml_node node, const  char* jobName, char* jobdir);
	Job::Bounds LoadBounds(pugi::xml_node node);
	void LoadDrawSettingsNode(Job* j, pugi::xml_node drawnode);
	void LoadInspectionSettingsNode(Job* j, pugi::xml_node inspnode);
	void LoadPLCSettingsNode(Job* j, pugi::xml_node plcnode);
	void LoadCameraSettingsNode(Job* j, pugi::xml_node settingsnode);

	void SaveDrawSettingsNode(Job* loadedJob, pugi::xml_node drawnode);
	void SaveInspectionSettingsNode(Job* loadedJob, pugi::xml_node inspnode);
	void SaveCameraSettingsNode(Job* loadedJob, pugi::xml_node  camsettnode);
	void SavePLCSettingsNode(Job* loadedJob, pugi::xml_node plcnode);
	void SetBounds(pugi::xml_node node, Job::Bounds bounds);

};

#endif
