#include "Job.h"

Job::Job(int cams) :numCams(cams), displayable(false), showLabelMatching(false), vertBarX(0), midBarY(0), neckBarY(0), capColorSize(0), c5InPictures(0), middles(0), edges(0), showInsp5(0), showInsp7(0), showInsp8(0),
taught(0), yTopLimPatt(0), yBotLimPatt(0), wSplice(0), hSplice(0), hSpliceSmBx(0), spliceVar(0), labelIntegrityMethod(LabelIntegrityMethod::Unknown), avoidOffX(0), middImg(0), findMiddles(false), noLabelOffset(0), noLabelOffsetW(0), 
noLabelOffsetH(0), rotOffset(0), bottleStyle(0), edgeTransition(EdgeTransition::Unknown), filterSelPatt(ImageFilters::Unknown), filterSelPattCap(ImageFilters::Unknown), filterSelNoLabel(ImageFilters::Unknown),
filterSelShifted(ImageFilters::Unknown), filterSelSplice(ImageFilters::Unknown), boxYt(0), boxWt(0), boxHt(0), boxYb(0), boxHb(0), shiftHorIniLim(0), shiftHorEndLim(0), shiftWidth(0), sensEdge(0), labelSen(0), nolabelSen(0),
shiftedLabSen(0), edgeLabSen(0), ellipseSen(0), senMidd(0), useStitching(UseStitching::Dark), stitchWidth(0), stitchHeight(0), stLighSen(0), stDarkSen(0), stEdgeSen(0), stLighImg(ImageFilters::Unknown), stDarkImg(ImageFilters::Unknown), 
stEdgeImg(ImageFilters::Unknown), sportSen(0), sportPos(0), enabledSportCap(false), ldist(0), targetColorRed(0), targetColorGreen(0), targetColorBlue(0), neckStrength(0), capStrength(0), capStrength2(0), orienArea(0), fillOffset(0), capColorOffset(0),
useRing(false), motPos(0), sensMethod3(0), heightMethod3(0), widthMethod3(0), heightMethod5(0), widthMethod5(0), wehaveTempX(false), boltOffset(0), boltOffset2(0), useStLigh(false), useStDark(false), useStEdge(false), showStitch(ShowStitch::Unknown)
{
	camSettings = new CamSettings[cams];
	inspections = new Inspection*[INSPECTIONS_85];

	for (int i = 0; i < INSPECTIONS_85; i++)
		inspections[i] = new Inspection();
}

Job* Job::Create(int cams)
{
	return new Job(cams);
}


Job::~Job()
{
	delete[] camSettings;

	for (int i = 0; i < INSPECTIONS_85; i++)
		delete inspections[i];
	delete[] inspections;
}

const char* Job::GetName()
{
	return name;
}

int Job::GetLDist() { return this->ldist; }
int Job::GetColorTargR() { return this->targetColorRed; }
int Job::GetColorTargG() { return this->targetColorGreen; }
int Job::GetColorTargB() { return this->targetColorBlue; }
int Job::GetNeckStrength() { return this->neckStrength; }
int Job::GetCapStrength() { return this->capStrength; }
int Job::GetCapStrength2() { return this->capStrength2; }
int Job::GetOrienArea() { return this->orienArea; }
int Job::GetFillOffset() { return this->fillOffset; }

int Job::GetBottleStyle() { return this->bottleStyle; }
int Job::GetSportSen() { return this->sportSen; }
int Job::GetSportPos() { return this->sportPos; }
bool Job::SportCap() { return this->enabledSportCap; }
int Job::GetNeckBarY() { return this->neckBarY; }
int Job::GetCapColorOffset() { return this->capColorOffset; }
int Job::GetMiddImg() { return this->middImg; }
int Job::GetSenMidd() { return this->senMidd; }
int Job::GetHeigMet3() { return this->heightMethod3; }
int Job::GetWidtMet3() { return this->widthMethod3; }
int Job::GetSensMet3() { return this->sensMethod3; }
int Job::GetHeigMet5() { return this->heightMethod5; }
int Job::GetWidtMet5() { return this->widthMethod5; }
int Job::GetEdgeLabSen() { return this->edgeLabSen; }
int Job::GetShiftedLabSen() { return this->shiftedLabSen; }
int Job::GetSensEdge() { return this->sensEdge; }
int Job::GetBoxWt() { return this->boxWt; }
int Job::GetBoxHt() { return this->boxHt; }
int Job::GetBoxYt() { return this->boxYt; }
int Job::GetBoxHb() { return this->boxHb; }
int Job::GetBoxYb() { return this->boxYb; }
int Job::GetYTopLimPatt() { return this->yTopLimPatt; }
int Job::GetYBotLimPatt() { return this->yBotLimPatt; }
int Job::GetTaughtMiddleRefY(int cameraIndex) { return this->camSettings[cameraIndex].taughtMiddleRefY; }
int Job::GetShiftTopLim(int camIndex) { return this->camSettings[camIndex].shiftTopLim; }
int Job::GetShiftBottomLim(int camIndex) { return this->camSettings[camIndex].shiftBotLim; }
int Job::GetYSplice(int camIndex) { return this->camSettings[camIndex].ySplice; }
int Job::GetShiftHorIniLim() { return this->shiftHorIniLim; }
int Job::GetShiftHorEndLim() { return this->shiftHorEndLim; }
int Job::GetFindMiddles() { return this->findMiddles; }
int Job::GetShiftWidth() { return this->shiftWidth; }
ImageFilters Job::GetFilterSelShifted() { return this->filterSelShifted; }
ImageFilters Job::GetFilterSelPattCap() { return this->filterSelPattCap; }
ImageFilters Job::GetFilterSelPatt() { return this->filterSelPatt; }
ImageFilters Job::GetFilterSelNoLabel() { return this->filterSelNoLabel; }
LabelIntegrityMethod Job::GetLabelIntegrityMethod() { return this->labelIntegrityMethod; }
Job::EdgeTransition Job::GetEdgeTransition() { return this->edgeTransition; }
int Job::GetWaistEdgeThreshold(int camIndex) { return this->camSettings[camIndex].waistSettings.edgeThreshold; }
int Job::GetWaistVerticalSearchLength(int camIndex) { return this->camSettings[camIndex].waistSettings.vertSearchLength; }
int Job::GetWaistHorizontalSearchLength(int camIndex) { return this->camSettings[camIndex].waistSettings.horSearchLength; }
Job::Bounds Job::GetWaistMeasurementBounds(int camIndex) { return this->camSettings[camIndex].waistSettings.bounds; }
Job::Bounds Job::GetWaistMeasurementUpperBounds(int camIndex) { return this->camSettings[camIndex].waistSettings.upperBounds; }
int Job::GetWaistLabelOffsetX(int camIndex) { return this->camSettings[camIndex].noLabelOffsetX; }
int Job::GetWaistStitchY(int camIndex) { return this->camSettings[camIndex].stitchY; }

int Job::GetWaistMeasurementTopBound(int camIndex) { return this->camSettings[camIndex].waistSettings.upperBounds.top; }
int Job::GetWaistMeasurementBottomBound(int camIndex) { return this->camSettings[camIndex].waistSettings.upperBounds.bottom; }
int Job::GetWaistMeasurementLeftBound(int camIndex) { return this->camSettings[camIndex].waistSettings.upperBounds.left; }
int Job::GetWaistMeasurementRightBound(int camIndex) { return this->camSettings[camIndex].waistSettings.bounds.right; }
int Job::GetWaistMeasurementUpperTopBound(int camIndex) { return this->camSettings[camIndex].waistSettings.upperBounds.top; }
int Job::GetWaistMeasurementUpperBottomBound(int camIndex) { return this->camSettings[camIndex].waistSettings.upperBounds.bottom; }
int Job::GetWaistMeasurementUpperLeftBound(int camIndex) { return this->camSettings[camIndex].waistSettings.upperBounds.left; }
int Job::GetWaistMeasurementUpperRightBound(int camIndex) { return this->camSettings[camIndex].waistSettings.upperBounds.right; }

char* Job::GetPath() { return this->pathToJob; }

void Job::SetName(const char* jobname, const char* path)
{
	strncpy_s(this->name, jobname, _TRUNCATE);
	strncpy_s(this->pathToJob, path, _TRUNCATE);
}

void Job::SetDisplayable(bool val)
{
	this->displayable = val;
}

void Job::SetLabelMatching(bool show, Bounds bounds, Bounds teachColor)
{
	this->showLabelMatching = show;
	this->labelMatchingBounds = bounds;
	this->teachColorBounds = teachColor;
}
void Job::SetBars(int vertBarX, int midBarY, int neckBarY)
{
	this->vertBarX = vertBarX;
	this->midBarY = midBarY;
	this->neckBarY = neckBarY;
}

Job::Bounds Job::GetLabelMatchingBounds()
{
	return this->labelMatchingBounds;
}

Job::Bounds Job::GetTeachColorBounds()
{
	return this->teachColorBounds;
}

bool Job::GetShowLabelMatching()
{
	return this->showLabelMatching;
}

int Job::GetMidYBar()
{
	return midBarY;
}

int Job::GetVertXBar()
{
	return vertBarX;
}

int Job::GetCapColorSize()
{
	return capColorSize;
}




void Job::SetNeckBar(int value)
{
	this->neckBarY = value;
}

void Job::SetNeckStrength(int value)
{
	this->neckStrength = value;
}

void Job::SetCapFilter(ImageFilters filter)
{
	this->filterSelPattCap = filter;
}


Job::ShowStitch Job::GetShowStitch()
{
	return this->showStitch;
}


void Job::SetShowStitch(Job::ShowStitch value)
{
	this->showStitch = value;
}

void Job::SetCapColorSize(int size)
{
	this->capColorSize = size;
}

void Job::SetShow(bool c5InPictures, bool middles, bool edges, bool showInsp5, bool showInsp7, bool showInsp8)
{
	this->c5InPictures = c5InPictures;
	this->middles = middles;
	this->edges = edges;
	this->showInsp5 = showInsp5;
	this->showInsp7 = showInsp7;
	this->showInsp8 = showInsp8;
}

bool Job::GetShowC5InPictures()
{
	return this->c5InPictures;
}

bool Job::GetShowMiddles()
{
	return this->middles;
}

bool Job::GetShowEdges()
{
	return this->edges;
}

bool Job::GetShowInsp5()
{
	return this->showInsp5;
}

bool Job::GetShowInsp7()
{
	return this->showInsp7;
}

bool Job::GetShowInsp8()
{
	return this->showInsp8;
}

bool Job::GetUseRing()
{
	return this->useRing;
}

int Job::GetMotPos()
{
	return this->motPos;
}

void Job::SetNumberCams(int cams)
{
	this->numCams = cams;
}

int Job::GetNumberCams()
{
	return this->numCams;
}

int Job::GetLightLevel(int index)
{
	return this->camSettings[index].lightLevel;
}

void Job::SetCameraWaist(int index, Job::Bounds bounds, Job::Bounds upperBounds,
	int edgeThres, int vertSearchLength, int horSearchLength)
{
	camSettings[index].index = index;
	camSettings[index].waistSettings.bounds = bounds;
	camSettings[index].waistSettings.upperBounds = upperBounds;
	camSettings[index].waistSettings.edgeThreshold = edgeThres;
	camSettings[index].waistSettings.vertSearchLength = vertSearchLength;
	camSettings[index].waistSettings.horSearchLength = horSearchLength;
}

void Job::SetCameraInsp(int index, int taughtMiddleRefY, int shiftTopLim, 
	int shiftBotLim, int ySplice, int noLabelOffsetX, int stitchY)
{
	camSettings[index].index = index;
	camSettings[index].taughtMiddleRefY = taughtMiddleRefY;
	camSettings[index].shiftTopLim = shiftTopLim;
	camSettings[index].shiftBotLim = shiftBotLim;
	camSettings[index].ySplice = ySplice;
	camSettings[index].noLabelOffsetX = noLabelOffsetX;
	camSettings[index].stitchY = stitchY;
}

void Job::SetTaught(bool taught)
{
	this->taught = taught;
}

bool Job::GetTaught()
{
	return this->taught;
}

void Job::SetLimPatt(int yTopLimPatt, int yBotLimPatt)
{
	this->yTopLimPatt = yTopLimPatt;
	this->yBotLimPatt = yBotLimPatt;
}

void Job::SetSplice(int wSplice, int hSplice, int hSpliceSmBx, int spliceVar)
{
	this->wSplice = wSplice;
	this->hSplice = hSplice;
	this->hSpliceSmBx = hSpliceSmBx;
	this->spliceVar = spliceVar;
}

int Job::GetStitchY(int index)
{
	return camSettings[index].stitchY;
}

int Job::GetStitchW()
{
	return this->stitchWidth;
}

int Job::GetStitchH()
{
	return this->stitchHeight;
}

int Job::GetAvoidOffsetX()
{
	return this->avoidOffX;
}


int Job::GetWSplice()
{
	return this->wSplice;
}

int Job::GetHSplice()
{
	return this->hSplice;
}

int Job::GetHSpliceSmBx()
{
	return this->hSpliceSmBx;
}

int Job::GetSpliceVar()
{
	return this->spliceVar;
}

void Job::SetLabelIntegrityMethod(LabelIntegrityMethod labelIngMtd)
{
	this->labelIntegrityMethod = labelIngMtd;
}

void Job::SetEdgeTransition(EdgeTransition et)
{
	edgeTransition = et;
}

void Job::SetAvoidOffsetX(int avoidOffX)
{
	this->avoidOffX = avoidOffX;
}

void Job::SetMiddImg(int midImg)
{
	this->middImg = midImg;
}

void Job::SetFindMiddles(bool findMid)
{
	this->findMiddles = findMid;
}

void Job::SetNoLabelOffset(int offset)
{
	this->noLabelOffset = offset;
}

void Job::SetNoLabelOffsetW(int offset)
{
	this->noLabelOffsetW = offset;
}

void Job::SetNoLabelOffsetH(int offset)
{
	this->noLabelOffsetH = offset;
}


int Job::GetNoLabelOffset()
{
	return this->noLabelOffset;
}

int Job::GetNoLabelOffsetX(int nCam)
{
	return this->camSettings[nCam].noLabelOffsetX;
}


void Job::SetRotOffset(int rotOff)
{
	this->rotOffset = rotOff;
}

int Job::GetRotOffset()
{
	return this->rotOffset;
}

void Job::SetBottleStyle(int bottStyle)
{
	this->bottleStyle = bottStyle;
}

void Job::SetFilter(ImageFilters filterSelPatt, ImageFilters filterSelPattCap, ImageFilters filterSelNoLabel,
	ImageFilters filterSelShifted, ImageFilters filterSelSplice)
{
	this->filterSelPatt = filterSelPatt;
	this->filterSelPattCap = filterSelPattCap;
	this->filterSelNoLabel = filterSelNoLabel;
	this->filterSelShifted = filterSelShifted;
	this->filterSelSplice = filterSelSplice;
}

ImageFilters Job::GetFilterSelSplice()
{
	return this->filterSelSplice;
}

void Job::SetBox(int boxYt, int boxWt, int boxHt, int boxYb, int boxHb)
{
	this->boxYt = boxYt;
	this->boxWt = boxWt;
	this->boxHt = boxHt;
	this->boxYb = boxYb;
	this->boxHb = boxHb;

}

void Job::SetShift(int shiftHorIniLim, int shiftHorEndLim, int shiftWidth)
{
	this->shiftHorIniLim = shiftHorIniLim;
	this->shiftHorEndLim = shiftHorEndLim;
	this->shiftWidth = shiftWidth;
}

void Job::SetSensors(int sensEdge, int labelSen, int nolabelSen, int shiftedLabSen,
	int edgeLabSen, int ellipseSen, int senMidd)
{
	this->sensEdge = sensEdge;
	this->labelSen = labelSen;
	this->nolabelSen = nolabelSen;
	this->shiftedLabSen = shiftedLabSen;
	this->edgeLabSen = edgeLabSen;
	this->ellipseSen = ellipseSen;
	this->senMidd = senMidd;
}

int Job::GetNoLabelSens()
{
	return this->nolabelSen;
}

int Job::GetLabelSens()
{
	return this->labelSen;
}

int Job::GetEllipseSens()
{
	return this->ellipseSen;
}

void Job::SetStitching(UseStitching use, int stitchWidth, int stitchHeight,
	int stLighSen, int stDarkSen, int stEdgeSen,
	int stLighImg, int stDarkImg, int stEdgeImg)
{
	this->useStitching = use;
	this->stitchWidth = stitchWidth;
	this->stitchHeight = stitchHeight;
	this->stLighSen = stLighSen;
	this->stDarkSen = stDarkSen;
	this->stEdgeSen = stEdgeSen;
	this->stLighImg = (ImageFilters)stLighImg;
	this->stDarkImg = (ImageFilters)stDarkImg;
	this->stEdgeImg = (ImageFilters)stEdgeImg;

	this->useStLigh = use == UseStitching::Light;
	this->useStDark = use == UseStitching::Dark;
	this->useStEdge = use == UseStitching::Edge;
}

ImageFilters Job::GetStLightImg()
{
	return this->stLighImg;
}

ImageFilters Job::GetStDarkImg()
{
	return this->stLighImg;
}

ImageFilters Job::GetStEdgeImg()
{
	return this->stLighImg;
}

void Job::SetSport(bool enabled, int sportPos, int sportSen)
{
	this->enabledSportCap = enabled;
	this->sportPos = sportPos;
	this->sportSen = sportSen;
}

void Job::SetColor(int targetColorRed, int targetColorGreen, int targetColorBlue)
{
	this->targetColorRed = targetColorRed;
	this->targetColorGreen = targetColorGreen;
	this->targetColorBlue = targetColorBlue;
}

void Job::SetCap(int ldist, int targetColorRed, int targetColorGreen, int targetColorBlue,
	int neckStrength, int capStrength, int capStrength2, int orienArea, int fillOffset,
	int capColorOffset)
{
	this->ldist = ldist;
	this->targetColorRed = targetColorRed;
	this->targetColorGreen = targetColorGreen;
	this->targetColorBlue = targetColorBlue;
	this->neckStrength = neckStrength;
	this->capStrength = capStrength;
	this->capStrength2 = capStrength2;
	this->orienArea = orienArea;
	this->fillOffset = fillOffset;
	this->capColorOffset = capColorOffset;
}

void Job::SetLightLevel(int index, int lightLevel)
{
	this->camSettings->index = index;
	this->camSettings->lightLevel = lightLevel;
}

void Job::SetUseRing(bool useRing)
{
	this->useRing = useRing;
}

void Job::SetMotPos(int motPos)
{
	this->motPos = motPos;
}

void Job::SetMethods(int sensMethod3, int heightMethod3, int widthMethod3, int heightMethod5, int widthMethod5)
{
	this->sensMethod3 = sensMethod3;
	this->heightMethod3 = heightMethod3;
	this->widthMethod3 = widthMethod3;
	this->heightMethod5 = heightMethod5;
	this->widthMethod5 = widthMethod5;
}

bool Job::GetWeHaveTempX()
{
	return wehaveTempX;
}

void Job::SetWeHaveTempX(bool set)
{
	wehaveTempX = set;
}

void Job::SetInspection(int index, bool enabled, float lmin, float lmax, float lminB, float lmaxC, bool isFloat)
{
	inspections[index]->SetValues(index, enabled, lmin, lmax, lminB, lmaxC, isFloat);
}

int Job::GetNoLabelOffsetW()
{
	return this->noLabelOffsetW;
}

int Job::GetNoLabelOffsetH()
{
	return this->noLabelOffsetH;
}

int Job::GetBoltOffset()
{
	return this->boltOffset;
}

int Job::GetBoltOffset2()
{
	return this->boltOffset2;
}

void Job::SetBoltOffset(int offset)
{
	this->boltOffset = offset;
}

void Job::SetBoltOffset2(int offset)
{
	this->boltOffset2 = offset;
}

int Job::GetStLightSen()
{
	return this->stLighSen;
}

int Job::GetStDarkSen()
{
	return this->stDarkSen;
}

int Job::GetStEdgeSen()
{
	return this->stEdgeSen;
}

int Job::GetUseStLight()
{
	return this->useStLigh;
}

int Job::GetUseStDark()
{
	return this->useStDark;
}

int Job::GetUseStEdge()
{
	return this->useStEdge;
}

Inspection* Job::GetInspection(int index)
{
	return this->inspections[index];
}

void Job::SetMidYBar(int value)
{
	midBarY = value;
}

void Job::SetCapStrength(int value)
{
	capStrength = value;
}

void Job::SetCapStrength2(int value)
{
	capStrength2 = value;
}

void Job::SetCapColorOffset(int value)
{
	capColorOffset = value;
}

void Job::SetUseSportCap(bool value)
{
	enabledSportCap = value;
}

void Job::SetLDist(int value)
{
	ldist = value;
}

void Job::SetVertXBar(int value)
{
	vertBarX = value;
}

void Job::SetSportSen(int value)
{
	sportSen = value;
}

void Job::SetSportPos(int value)
{
	sportPos = value;
}

float Job::GetInspectionMax(int index)
{
	return inspections[index]->GetMax();
}
