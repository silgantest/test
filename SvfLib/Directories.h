#ifndef DIRECTORIES_H
#define DIRECTORIES_H

const char* const SILGAN_DATA_DIR = "c:\\SilganData_svf";
const char* const SILGAN_JOBS_DIR = "c:\\SilganData_svf\\Jobs";

const char* const CONFIG_FILE = "c:\\SilganData_svf\\config.xml";
const char* const APP_CONFIG_FILE = "c:\\SilganData_svf\\appconfig.xml";

const char* const SIM_DIR = "c:\\SilganData_svf\\Sim";
const char* const SIM_DIR_CAM = "c:\\SilganData_svf\\Sim\\Cam_";

#endif