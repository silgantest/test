#include "SystemConfig.h"

SystemConfig::SystemConfig(std::string model, int requiredCameras) 
{ 
	this->requiredCameras = requiredCameras;
	this->model = model;
	setSysType();

	cameras = new std::list<ExpectedCameraConfig*>();
}

SystemConfig::SystemConfig()
{

}


void SystemConfig::setSysType()
{
	if (model.find("85") != std::string::npos)
		this->type = SystemType::SVF85;
}


SystemConfig::SystemType SystemConfig::GetSystemType()
{
	return this->type;
}

int SystemConfig::GetNumberInspections()
{
	if (this->type == SystemType::SVF85)
	{
		return 13;
	}
}


SystemConfig::~SystemConfig()
{
	std::list<ExpectedCameraConfig*>::iterator it;

	for (it = cameras->begin(); it != cameras->end(); ++it)
	{
		delete *it;
	}
	delete cameras;
	delete appConfig;
}

std::string SystemConfig::GetModel()
{
	return model;
}

int SystemConfig::GetRequiredCameras()
{
	return requiredCameras;
}

void SystemConfig::AddCamera(ExpectedCameraConfig* cam)
{
	cameras->push_back(cam);
}


std::list<ExpectedCameraConfig*>* SystemConfig::GetCameras()
{
	return cameras;
}

ExpectedCameraConfig::ExpectedCameraConfig(int number, std::string vendor, std::string model, std::string serial, std::string title)
	: number(number), vendor(vendor), model(model), serial(serial), title(title)
{

}
ExpectedCameraConfig::~ExpectedCameraConfig()
{

}

std::string ExpectedCameraConfig::GetVendor()
{
	return vendor;
}

std::string ExpectedCameraConfig::GetModel()
{
	return model;
}

std::string ExpectedCameraConfig::GetSerial()
{
	return serial;
}

std::string ExpectedCameraConfig::GetTitle()
{
	return title;
}

int ExpectedCameraConfig::GetNumber()
{
	return number;
}

void ExpectedCameraConfig::AddAOI(int oX, int oY, int wid, int hei)
{
	offsetX = oX;
	offsetY = oY;
	width = wid;
	height = hei;
}

int ExpectedCameraConfig::GetOffsetX()
{
	return offsetX;
}

int ExpectedCameraConfig::GetOffsetY()
{
	return offsetY;
}

int ExpectedCameraConfig::GetWidth()
{
	return width;
}

int ExpectedCameraConfig::GetHeight()
{
	return height;
}

void SystemConfig::SetAppConfig(AppConfig* appConfig)
{
	this->appConfig = appConfig;
}

AppConfig* SystemConfig::GetAppConfig()
{
	return this->appConfig;
}
