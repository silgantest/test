#ifndef IPC_H
#define IPC_H

const char* const SHARED_MEMORY_IMAGES = "Local\\SharedMemImages";
const char* const SHARED_MEMORY_IMAGES_MTX = "SharedMemImagesMtx";
const char* const SHARED_MEMORY_IMAGES_SEM = "SharedMemImagesSem";

const char* const COMMAND_BUFFER = "Local\\CommandBuffer";
const char* const SHARED_MEMORY_COMMAND_BUFFER_MTX = "SharedMemCommandBufferMtx";
const char* const SHARED_MEMORY_COMMAND_BUFFER_SEM = "SharedMemCommandBufferSem";

const char* const STATUS_BUFFER = "Local\\StatusBuffer";
const char* const SHARED_MEMORY_STATUS_BUFFER_MTX = "SharedMemStatusBufferMtx";
const char* const SHARED_MEMORY_STATUS_BUFFER_SEM = "SharedMemStatusBufferSem";


const int IMAGE_SIZE = 341 * 136 * 4;
const int CAP_IMAGE_SIZE = 512 * 204 * 4;
const int CAP_FILTER_SIZE = 512 * 204;
const int MAX_RESULTS_SIZE = 13 * 32;
const int MAX_DETAILS_BUFFER = 32;

const int SHARED_MEM_BUFFER_SIZE = IMAGE_SIZE * 4 + CAP_IMAGE_SIZE + MAX_RESULTS_SIZE + CAP_FILTER_SIZE * 4 + MAX_DETAILS_BUFFER*5;
const int COMMAND_BUFFER_SIZE = 256;
const int STATUS_BUFFER_SIZE = 1024;

#endif
