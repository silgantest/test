#include "AppConfig.h"
#include "..\..\3rdParty\pugixml-1.9\src\pugixml.hpp" 
#include "Directories.h"



AppConfig::AppConfig() : camHeight(0), camWidth(0), mainDisplayImageHeight(0), mainDisplayImageWidth(0), mainDisplayImageXOffset(0), mainDisplayImageYOffset(0),
croppedCameraWidth(0), croppedCameraHeight(0), croppedCameraOffsetX(0), croppedCameraOffsetY(0), sizePatternBy3_2W(0), sizePatternBy3_2H(0), sizePatternBy6(0), sizePatternBy12(0),
scaleFactor(0), bytesPerPixel(0), wasitAvailable(0), alignAvailable(0)
{

}

AppConfig::~AppConfig()
{
}


int AppConfig::GetMainDisplayImageHeight() { return this->mainDisplayImageHeight; }
int AppConfig::GetMainDisplayImageWidth() { return this->mainDisplayImageWidth; }

int AppConfig::GetMainCameraDisplayXOffset() { return this->mainDisplayImageXOffset; }
int AppConfig::GetMainCameraDisplayYOffset() { return this->mainDisplayImageYOffset; }

int AppConfig::GetCroppedCameraWidth() { return this->croppedCameraWidth; }
int AppConfig::GetCroppedCameraHeight() { return this->croppedCameraHeight; }
int AppConfig::GetCroppedCameraOffsetX() { return this->croppedCameraOffsetX; }
int AppConfig::GetCroppedCameraOffsetY() { return this->croppedCameraOffsetY; }

int AppConfig::GetSizePattBy3_2W() { return this->sizePatternBy3_2W; }
int AppConfig::GetSizePattBy3_2H() { return this->sizePatternBy3_2H; }
int AppConfig::GetSizePattBy6() { return this->sizePatternBy6; }
int AppConfig::GetSizePattBy12() { return this->sizePatternBy12; }
int AppConfig::GetScaleFactor() { return this->scaleFactor; }
int AppConfig::GetBytesPerPixel() { return this->bytesPerPixel; }

void AppConfig::SetMainDisplayImageHeight(int val) { this->mainDisplayImageHeight = val; }
void AppConfig::SetMainDisplayImageWidth(int val) { this->mainDisplayImageWidth = val; }
void AppConfig::SetMainCameraDisplayXOffset(int val) { this->mainDisplayImageXOffset = val; }
void AppConfig::SetMainCameraDisplayYOffset(int val) { this->mainDisplayImageYOffset = val; }
void AppConfig::SetCroppedCameraWidth(int val) { this->croppedCameraWidth = val; }
void AppConfig::SetCroppedCameraHeight(int val) { this->croppedCameraHeight = val; }
void AppConfig::SetCroppedCameraOffsetX(int val) { this->croppedCameraOffsetX = val; }
void AppConfig::SetCroppedCameraOffsetY(int val) { this->croppedCameraOffsetY = val; }
void AppConfig::SetSizePattBy3_2W(int val) { this->sizePatternBy3_2W = val; }
void AppConfig::SetSizePattBy3_2H(int val) { this->sizePatternBy3_2H = val; }
void AppConfig::SetSizePattBy6(int val) { this->sizePatternBy6 = val; }
void AppConfig::SetSizePattBy12(int val) { this->sizePatternBy12 = val; }

void AppConfig::SetScaleFactor(int val) { this->scaleFactor = val; }
void AppConfig::SetBytesPerPixel(int val) { this->bytesPerPixel = val; }


bool AppConfig::GetWaistAvailable() { return this->wasitAvailable; }
int AppConfig::GetCamWidth() { return this->camWidth; }
int AppConfig::GetCamHeight() { return this->camHeight; }

void AppConfig::SetWaistAvailable(bool val) { this->wasitAvailable = val; }
void AppConfig::SetCamWidth(int val) { this->camWidth = val; }
void AppConfig::SetCamHeight(int val) { this->camHeight = val; }

bool AppConfig::GetAlignAvailable() { return this->alignAvailable; }
void AppConfig::SetAlignAvailable(bool val) { this->alignAvailable = val; }

int AppConfig::GetLabelUICameraWidth() { return labelUICameraWidth; }
int AppConfig::GetLabelUICameraHeight() { return labelUICameraHeight; }
int AppConfig::GetCapUICameraWidth() { return capUICameraWidth; }
int AppConfig::GetCapUICameraHeight() { return capUICameraHeight; }
void AppConfig::SetLabelUICameraWidth(int val) { labelUICameraWidth = val; }
void AppConfig::SetLabelUICameraHeight(int val) { labelUICameraHeight = val; }
void AppConfig::SetCapUICameraWidth(int val) { capUICameraWidth = val; }
void AppConfig::SetCapUICameraHeight(int val) { capUICameraHeight = val; }

int AppConfig::GetUIWidth(int index)
{
	if (index == 4)
		return capUICameraWidth;
	return labelUICameraWidth;
}
int AppConfig::GetUIHeight(int index)
{
	if (index == 4)
		return capUICameraHeight;
	return labelUICameraHeight;
}

AppConfig* AppConfig::LoadAppConfig85()
{
	const char* INSPECTIONSETTINGS84 = "inspectionSettings84";
	const char* CONFIG = "config";
	const char* SYSTEM = "system";
	const char* CAMERA = "camera";
	const char* WIDTH = "width";
	const char* HEIGHT = "height";
	const char* MAINDISPLAY = "mainDisplay";
	const char* SCALEFACTOR = "scaleFactor";
	const char* BYTESPERPIXEL = "bytesPerPixel";
	const char* OFFSETX = "offsetX";
	const char* OFFSETY = "offsetY";
	const char* CROPPED = "cropped";
	const char* SIZEPATTERNS = "sizePatterns";
	const char* SIZEPATTERNSBY3_2W = "sizePatternBy3_2W";
	const char* SIZEPATTERNSBY3_2H = "sizePatternBy3_2H";
	const char* SIZEPATTERNSBY6 = "sizePatternBy6";
	const char* SIZEPATTERNSBY12 = "sizePatternBy12";
	const char* UI_SIZES = "uiSizes";
	const char* CAP = "cap";
	const char* LABEL = "label";

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(APP_CONFIG_FILE);

	//Read system node
	pugi::xml_node inspnode = doc.child(CONFIG).child(SYSTEM).child(INSPECTIONSETTINGS84);
	pugi::xml_node camnode = inspnode.child(CAMERA);

	AppConfig* app = new AppConfig();
	app->SetCamWidth(camnode.child(WIDTH).text().as_int());
	app->SetCamHeight(camnode.child(HEIGHT).text().as_int());
	pugi::xml_node dispnode = inspnode.child(MAINDISPLAY);

	app->SetMainDisplayImageWidth(dispnode.child(WIDTH).text().as_int());
	app->SetMainDisplayImageHeight(dispnode.child(HEIGHT).text().as_int());
	app->SetMainCameraDisplayXOffset(dispnode.child(OFFSETX).text().as_int());
	app->SetMainCameraDisplayYOffset(dispnode.child(OFFSETY).text().as_int());

	app->SetScaleFactor(dispnode.child(SCALEFACTOR).text().as_int());
	app->SetBytesPerPixel(dispnode.child(BYTESPERPIXEL).text().as_int());

	pugi::xml_node croppednode = inspnode.child(CROPPED);

	app->SetCroppedCameraWidth(croppednode.child(WIDTH).text().as_int());
	app->SetCroppedCameraHeight(croppednode.child(HEIGHT).text().as_int());
	app->SetCroppedCameraOffsetX(dispnode.child(OFFSETX).text().as_int());
	app->SetCroppedCameraOffsetY(dispnode.child(OFFSETY).text().as_int());

	pugi::xml_node sizenode = inspnode.child(SIZEPATTERNS);
	app->SetSizePattBy3_2W(sizenode.child(SIZEPATTERNSBY3_2W).text().as_int());
	app->SetSizePattBy3_2H(sizenode.child(SIZEPATTERNSBY3_2H).text().as_int());
	app->SetSizePattBy6(sizenode.child(SIZEPATTERNSBY6).text().as_int());
	app->SetSizePattBy12(sizenode.child(SIZEPATTERNSBY12).text().as_int());

	pugi::xml_node uisizenode = inspnode.child(UI_SIZES);
	pugi::xml_node capsizenode = uisizenode.child(CAP);
	pugi::xml_node labelsizenode = uisizenode.child(LABEL);
	app->SetCapUICameraWidth(capsizenode.child(WIDTH).text().as_int());
	app->SetCapUICameraHeight(capsizenode.child(HEIGHT).text().as_int());
	app->SetLabelUICameraWidth(labelsizenode.child(WIDTH).text().as_int());
	app->SetLabelUICameraHeight(labelsizenode.child(HEIGHT).text().as_int());

	return app;
}