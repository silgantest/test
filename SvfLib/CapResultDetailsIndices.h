#ifndef CAP_RESULTS_DETAILS_INDICES
#define CAP_RESULTS_DETAILS_INDICES

	const int CapResults_LeftSide = 0;
	const int CapResults_RightSide = 2;
	const int CapResults_LeftTop = 4;
	const int CapResults_RightTop = 6;
	const int CapResults_LeftLip = 8;
	const int CapResults_RightLip = 11;
	const int CapResults_MidTop = 14;
	const int CapResults_MidBot = 16;
	const int CapResults_SportBand = 20;
	const int CapResults_Red = 24;
	const int CapResults_Green = 25;
	const int CapResults_Blue = 26;


#endif
