//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 4.0.2
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------


public class Inspection : global::System.IDisposable {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal Inspection(global::System.IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(Inspection obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  ~Inspection() {
    Dispose(false);
  }

  public void Dispose() {
    Dispose(true);
    global::System.GC.SuppressFinalize(this);
  }

  protected virtual void Dispose(bool disposing) {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          SvfLibPINVOKE.delete_Inspection(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
    }
  }

  public void SetValues(int index, bool enabled, float lmin, float lmax, float lminB, float lmaxC, bool isFloat) {
    SvfLibPINVOKE.Inspection_SetValues(swigCPtr, index, enabled, lmin, lmax, lminB, lmaxC, isFloat);
  }

  public void SetMin(float value) {
    SvfLibPINVOKE.Inspection_SetMin(swigCPtr, value);
  }

  public float GetMin() {
    float ret = SvfLibPINVOKE.Inspection_GetMin(swigCPtr);
    return ret;
  }

  public void SetMax(float value) {
    SvfLibPINVOKE.Inspection_SetMax(swigCPtr, value);
  }

  public float GetMax() {
    float ret = SvfLibPINVOKE.Inspection_GetMax(swigCPtr);
    return ret;
  }

  public void SetMinB(float value) {
    SvfLibPINVOKE.Inspection_SetMinB(swigCPtr, value);
  }

  public float GetMinB() {
    float ret = SvfLibPINVOKE.Inspection_GetMinB(swigCPtr);
    return ret;
  }

  public void SetMaxC(float value) {
    SvfLibPINVOKE.Inspection_SetMaxC(swigCPtr, value);
  }

  public float GetMaxC() {
    float ret = SvfLibPINVOKE.Inspection_GetMaxC(swigCPtr);
    return ret;
  }

  public bool IsFloat() {
    bool ret = SvfLibPINVOKE.Inspection_IsFloat(swigCPtr);
    return ret;
  }

  public bool IsEnabled() {
    bool ret = SvfLibPINVOKE.Inspection_IsEnabled(swigCPtr);
    return ret;
  }

  public void Enable(bool value) {
    SvfLibPINVOKE.Inspection_Enable(swigCPtr, value);
  }

  public Inspection() : this(SvfLibPINVOKE.new_Inspection(), true) {
  }

}
