#include "JobLoader.h"

JobLoader::JobLoader()
{
	showStitchTable = {
		{"Light",Job::ShowStitch::Light},
		{"Dark",Job::ShowStitch::Dark},
		{"BW",Job::ShowStitch::BW},
		{"Edges",Job::ShowStitch::Edges},
		{"Final",Job::ShowStitch::Final}
	};

	showStitchTableString = {
	{Job::ShowStitch::Light, "Light"},
	{Job::ShowStitch::Dark, "Dark"},
	{Job::ShowStitch::BW, "BW"},
	{Job::ShowStitch::Edges, "Edges"},
	{Job::ShowStitch::Final, "Final"}
	};

	showEdgeTransTable = {
		{"Dark2Light", Job::EdgeTransition::Dark2Light},
		{"Light2Dark", Job::EdgeTransition::Light2Dark},
	};

	showEdgeTransTableString = {
		{Job::EdgeTransition::Dark2Light, "Dark2Light"},
		{Job::EdgeTransition::Light2Dark, "Light2Dark"},
	};

	stitchingUseTable = { 
		{"Light", Job::UseStitching::Light},
		{"Dark", Job::UseStitching::Dark}, 
		{"Edge", Job::UseStitching::Edge}
	};

	stitchingUseTableString = {
	{Job::UseStitching::Light, "Light"},
	{Job::UseStitching::Dark, "Dark"},
	{Job::UseStitching::Edge, "Edge"}
	};

	methodTable = {
		{1, LabelIntegrityMethod::Method1},
		{2, LabelIntegrityMethod::Method2},
		{3, LabelIntegrityMethod::Method3},
		{4, LabelIntegrityMethod::Method4},
		{5, LabelIntegrityMethod::Method5},
		{6, LabelIntegrityMethod::Method6}
	};

	methodTableInt = {
	{LabelIntegrityMethod::Method1, 1},
	{LabelIntegrityMethod::Method2, 2},
	{LabelIntegrityMethod::Method3, 3},
	{LabelIntegrityMethod::Method4, 4},
	{LabelIntegrityMethod::Method5, 5},
	{LabelIntegrityMethod::Method6, 6}
	};

	filtersTable = {
	{0, ImageFilters::Unknown},
	{1, ImageFilters::BlackAndWhite},
	{2, ImageFilters::Saturation},
	{3, ImageFilters::Magenta},
	{4, ImageFilters::Yellow},
	{5, ImageFilters::Cyan},
	{6, ImageFilters::Red},
	{7, ImageFilters::Green},
	{8, ImageFilters::Blue}
	};

	filtersTableInt = {
	{ImageFilters::Unknown, 0},
	{ImageFilters::BlackAndWhite, 1},
	{ImageFilters::Saturation, 2},
	{ImageFilters::Magenta, 3},
	{ImageFilters::Yellow, 4},
	{ImageFilters::Cyan, 5},
	{ImageFilters::Red, 6},
	{ImageFilters::Green, 7},
	{ImageFilters::Blue, 8}
	};
}

JobLoader::~JobLoader()
{
}

void JobLoader::SaveJob(Job* loadedJob)
{
	
	char jobfullpath[MAX_JOB_PATH_LENGTH];
	char jobdir[MAX_JOB_PATH_LENGTH];
	sprintf_s(jobfullpath, JOB_FULL_PATH, SILGAN_JOBS_DIR, loadedJob->GetName());
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(jobfullpath);

	pugi::xml_node jobnode = doc.child(JOB);
	jobnode.child(DISPLAYABLE).text().set(true);
	pugi::xml_node drawnode = jobnode.child(DRAW_SETTINGS);
	pugi::xml_node inspnode = jobnode.child(INSPECTION_SETTINGS);
	pugi::xml_node camsettnode = jobnode.child(CAMERA_SETTINGS);
	pugi::xml_node plcnode = jobnode.child(PLC_SETTINGS);
	
	SaveDrawSettingsNode(loadedJob, drawnode);


	SaveInspectionSettingsNode(loadedJob, inspnode);
	SaveCameraSettingsNode(loadedJob, camsettnode);
	SavePLCSettingsNode(loadedJob, plcnode);
	doc.save_file(jobfullpath);
}

void JobLoader::WriteInt(int value, pugi::xml_node node)
{
	memset(str, 0, 50);
	sprintf_s(str, "%d", value);
	node.first_child().set_value(str);
}

void JobLoader::WriteFloat(float value, pugi::xml_node node)
{
	memset(str, 0, 50);
	sprintf_s(str, "%f", value);
	node.first_child().set_value(str);
}

void JobLoader::WriteBool(bool value, pugi::xml_node node)
{
	if (value)
		node.first_child().set_value("True");
	else
		node.first_child().set_value("False");
}

void JobLoader::SaveDrawSettingsNode(Job* loadedJob, pugi::xml_node drawnode)
{
	pugi::xml_node labmatnode = drawnode.child(LABEL_MATCHING);
	WriteBool(loadedJob->GetShowLabelMatching(), labmatnode.child(SHOW_LABEL_MATCHING));
	SetBounds(labmatnode.child(BOUNDS), loadedJob->GetLabelMatchingBounds());
	SetBounds(labmatnode.child(BOUNDS), loadedJob->GetTeachColorBounds());

	WriteInt(loadedJob->GetVertXBar(), drawnode.child(VERT_BAR_X));
	WriteInt(loadedJob->GetMidYBar(), drawnode.child(MID_BAR_Y));
	WriteInt(loadedJob->GetNeckBarY(), drawnode.child(NECK_BAR_Y));
	drawnode.child(SHOW_ST).first_child().set_value(this->showStitchTableString[loadedJob->GetShowStitch()].c_str());
	WriteInt(loadedJob->GetCapColorSize(), drawnode.child(CAP_COLOR_SIZE));

	WriteBool(loadedJob->GetShowLabelMatching(), labmatnode.child(SHOW_C5_IN_PICTURES));
	WriteBool(loadedJob->GetShowMiddles(), labmatnode.child(SHOW_MIDDLES));
	WriteBool(loadedJob->GetShowEdges(), labmatnode.child(SHOW_EDGES));
	WriteBool(loadedJob->GetShowInsp5(), drawnode.child(SHOW_INSP).child(SHOW_INSP_5));
	WriteBool(loadedJob->GetShowInsp7(), drawnode.child(SHOW_INSP).child(SHOW_INSP_7));
	WriteBool(loadedJob->GetShowInsp8(), drawnode.child(SHOW_INSP).child(SHOW_INSP_8));

}

void JobLoader::SetBounds(pugi::xml_node node, Job::Bounds bounds)
{
	WriteInt(bounds.left, node.child(LEFT_BOUND));
	WriteInt(bounds.right, node.child(RIGHT_BOUND));
	WriteInt(bounds.top, node.child(TOP_BOUND));
	WriteInt(bounds.bottom, node.child(BOTTOM_BOUND));
}

void JobLoader::SaveInspectionSettingsNode(Job* loadedJob, pugi::xml_node inspnode)
{

	pugi::xml_node camsettnode = inspnode.child(CAMERA_SETTINGS);
	pugi::xml_object_range<pugi::xml_node_iterator> it = camsettnode.children();
	pugi::xml_node waistnode;
	int index;
	for (pugi::xml_node_iterator nit = it.begin(); nit != it.end(); ++nit)
	{
		sscanf_s(nit->attribute(INDEX).value(), "%d", &index);
		waistnode = nit->child(WAIST_MEASUREMENT);
		SetBounds(waistnode.child(BOUNDS), loadedJob->GetWaistMeasurementBounds(index));
		SetBounds(waistnode.child(UPPER_BOUNDS), loadedJob->GetWaistMeasurementUpperBounds(index));

		WriteInt(loadedJob->GetWaistEdgeThreshold(index), waistnode.child(EDGE_THRESHOLD));
		WriteInt(loadedJob->GetWaistVerticalSearchLength(index), waistnode.child(SEARCH_LENGTH).child(VERTICAL));
		WriteInt(loadedJob->GetWaistHorizontalSearchLength(index), waistnode.child(SEARCH_LENGTH).child(HORIZONTAL));
		WriteInt(loadedJob->GetTaughtMiddleRefY(index), waistnode.child(TAUGHT_MIDDLE_REF_Y));
		WriteInt(loadedJob->GetShiftTopLim(index), waistnode.child(SHIFT_TOP_LIM));
		WriteInt(loadedJob->GetShiftBottomLim(index), waistnode.child(SHIFT_BOT_LIM));
		WriteInt(loadedJob->GetYSplice(index), waistnode.child(Y_SPLICE));
		WriteInt(loadedJob->GetWaistLabelOffsetX(index), waistnode.child(NO_LABEL_OFFSET_X));
		WriteInt(loadedJob->GetStitchY(index), waistnode.child(STITCH_Y));
	}

	WriteBool(loadedJob->GetTaught(), inspnode.child(TAUGHT));
	WriteInt(loadedJob->GetYTopLimPatt(), inspnode.child(Y_TOP_LIM_PATT));
	WriteInt(loadedJob->GetWSplice(), inspnode.child(W_SPLICE));
	WriteInt(loadedJob->GetHSplice(), inspnode.child(H_SPLICE));
	WriteInt(loadedJob->GetHSpliceSmBx(), inspnode.child(H_SPLICE_SM_BX));
	WriteInt(loadedJob->GetSpliceVar(), inspnode.child(SPLICE_VAR));

	WriteInt(this->methodTableInt[loadedJob->GetLabelIntegrityMethod()], inspnode.child(LAB_INTEGRITY_METHOD));
	inspnode.child(EDGE_TRANSITION).first_child().set_value(this->showEdgeTransTableString[loadedJob->GetEdgeTransition()].c_str());
	WriteInt(loadedJob->GetAvoidOffsetX(), inspnode.child(AVOID_OFF_X));
	WriteInt(loadedJob->GetMiddImg(), inspnode.child(MIDD_IMG));
	WriteBool(loadedJob->GetFindMiddles(), inspnode.child(FIND_MIDDLES));
	WriteInt(loadedJob->GetNoLabelOffset(), inspnode.child(NO_LABEL_OFFSET));
	WriteInt(loadedJob->GetNoLabelOffsetW(), inspnode.child(NO_LABEL_OFFSET));
	WriteInt(loadedJob->GetNoLabelOffsetH(), inspnode.child(NO_LABEL_OFFSET));
	WriteInt(loadedJob->GetRotOffset(), inspnode.child(ROT_OFFSET));
	WriteInt(loadedJob->GetBottleStyle(), inspnode.child(BOTTLE_STYLE));
	WriteBool(loadedJob->GetWeHaveTempX(), inspnode.child(TEMP_X));

	WriteInt(this->filtersTableInt[loadedJob->GetFilterSelPatt()], inspnode.child(FILTER).child(FILTER_SEL_PATT));
	WriteInt(this->filtersTableInt[loadedJob->GetFilterSelPattCap()], inspnode.child(FILTER).child(FILTER_SEL_PATT_CAP));
	WriteInt(this->filtersTableInt[loadedJob->GetFilterSelNoLabel()], inspnode.child(FILTER).child(FILTER_SEL_NO_LABEL));
	WriteInt(this->filtersTableInt[loadedJob->GetFilterSelShifted()], inspnode.child(FILTER).child(FILTER_SEL_SHIFTED));
	WriteInt(this->filtersTableInt[loadedJob->GetFilterSelSplice()], inspnode.child(FILTER).child(FILTER_SEL_SPLICE));

	WriteInt(loadedJob->GetBoxYt(), inspnode.child(BOX).child(BOX_YT));
	WriteInt(loadedJob->GetBoxWt(), inspnode.child(BOX).child(BOX_WT));
	WriteInt(loadedJob->GetBoxHt(), inspnode.child(BOX).child(BOX_HT));
	WriteInt(loadedJob->GetBoxYb(), inspnode.child(BOX).child(BOX_YB));
	WriteInt(loadedJob->GetBoxHb(), inspnode.child(BOX).child(BOX_HB));

	WriteInt(loadedJob->GetSensEdge(), inspnode.child(SENSORS).child(SENS_EDGE));
	WriteInt(loadedJob->GetLabelSens(), inspnode.child(SENSORS).child(LABEL_SENS));
	WriteInt(loadedJob->GetNoLabelSens(), inspnode.child(SENSORS).child(NO_LABEL_SENS));
	WriteInt(loadedJob->GetShiftedLabSen(), inspnode.child(SENSORS).child(SHIFTED_LABEL_SENS));
	WriteInt(loadedJob->GetEdgeLabSen(), inspnode.child(SENSORS).child(EDGE_LABEL_SENS));
	WriteInt(loadedJob->GetEllipseSens(), inspnode.child(SENSORS).child(ELLIPSE_SENS));
	WriteInt(loadedJob->GetSenMidd(), inspnode.child(SENSORS).child(MIDD_SENS));

	WriteBool(loadedJob->SportCap(), inspnode.child(SPORT).child(ENABLED));
	WriteInt(loadedJob->GetSportPos(), inspnode.child(SPORT).child(SPORT_POSITION));
	WriteInt(loadedJob->GetSportSen(), inspnode.child(SPORT).child(SPORT_SENSOR));

	WriteInt(loadedJob->GetLDist(), inspnode.child(CAP).child(L_DIST));
	WriteInt(loadedJob->GetColorTargR(), inspnode.child(CAP).child(COLOR_TARGET).child(RED));
	WriteInt(loadedJob->GetColorTargG(), inspnode.child(CAP).child(COLOR_TARGET).child(GREEN));
	WriteInt(loadedJob->GetColorTargB(), inspnode.child(CAP).child(COLOR_TARGET).child(BLUE));
	WriteInt(loadedJob->GetNeckStrength(), inspnode.child(CAP).child(NECK_STRENGTH));
	WriteInt(loadedJob->GetCapStrength(), inspnode.child(CAP).child(CAP_STRENGTH));
	WriteInt(loadedJob->GetCapStrength2(), inspnode.child(CAP).child(CAP_STRENGTH2));
	WriteInt(loadedJob->GetOrienArea(), inspnode.child(CAP).child(ORIEN_AREA));
	WriteInt(loadedJob->GetFillOffset(), inspnode.child(CAP).child(FILL_OFFSET));
	WriteInt(loadedJob->GetCapColorOffset(), inspnode.child(CAP).child(CAP_COLOR_OFFSET));

	WriteInt(loadedJob->GetSensMet3(), inspnode.child(METHODS).child(SENS_METHOD_3));
	WriteInt(loadedJob->GetHeigMet3(), inspnode.child(METHODS).child(HEIGHT_METHOD_3));
	WriteInt(loadedJob->GetWidtMet3(), inspnode.child(METHODS).child(WIDTH_METHOD_3));
	WriteInt(loadedJob->GetHeigMet5(), inspnode.child(METHODS).child(HEIGHT_METHOD_5));
	WriteInt(loadedJob->GetWidtMet5(), inspnode.child(METHODS).child(WIDTH_METHOD_5));

	pugi::xml_node nIS = inspnode.child(INSPECTIONS);
	it = nIS.children();
	for (pugi::xml_node_iterator nit = it.begin(); nit != it.end(); ++nit)
	{
		sscanf_s(nit->attribute(INDEX).value(), "%d", &index);
		Inspection* insp = loadedJob->GetInspection(index);
		WriteBool(insp->IsEnabled(), nit->child(ENABLED));
		WriteFloat(insp->GetMin(), nit->child(L_MIN));
		WriteFloat(insp->GetMinB(), nit->child(L_MIN_B));
		WriteFloat(insp->GetMax(), nit->child(L_MAX));
		WriteFloat(insp->GetMaxC(), nit->child(L_MAX_C));
		WriteBool(insp->IsFloat(), nit->child(IS_FLOAT));

	}
}

void JobLoader::SaveCameraSettingsNode(Job* loadedJob, pugi::xml_node camsettnode)
{
	pugi::xml_object_range<pugi::xml_node_iterator> it = camsettnode.children();
	int index;
	for (pugi::xml_node_iterator nit = it.begin(); nit != it.end(); ++nit)
	{
		sscanf_s(nit->attribute(INDEX).value(), "%d", &index);
		memset(str, 0, 50);
		sprintf_s(str, "%d", loadedJob->GetLightLevel(index));
		nit->child(LIGHT_LEVEL).first_child().set_value(str);
	}
}

void JobLoader::SavePLCSettingsNode(Job* loadedJob, pugi::xml_node plcnode)
{
	WriteBool(loadedJob->GetUseRing(), plcnode.child(USE_RING));
	memset(str, 0, 50);
	sprintf_s(str, "%d", loadedJob->GetMotPos());
	plcnode.child(MOT_POS).first_child().set_value(str);

}

Job* JobLoader::LoadDefaultJob()
{
	char jobfullpath[MAX_JOB_PATH_LENGTH];
	char jobdir[MAX_JOB_PATH_LENGTH];
	char jobName[20];
	sprintf_s(jobName, "DefaultJob");
	sprintf_s(jobfullpath, JOB_FULL_PATH, SILGAN_DATA_DIR, jobName);
	sprintf_s(jobdir, JOB_DIR, SILGAN_DATA_DIR, jobName);

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(jobfullpath);

	pugi::xml_node jobnode = doc.child(JOB);
	Job* j = doLoadJob(jobnode, jobName, jobdir);
	return j;
}

Job* JobLoader::LoadJob(const char* jobName)
{
	char jobfullpath[MAX_JOB_PATH_LENGTH];
	char jobdir[MAX_JOB_PATH_LENGTH];
	sprintf_s(jobfullpath, JOB_FULL_PATH, SILGAN_JOBS_DIR, jobName);
	sprintf_s(jobdir, JOB_DIR, SILGAN_JOBS_DIR, jobName);

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(jobfullpath);

	pugi::xml_node jobnode = doc.child(JOB);
	Job* job = doLoadJob(jobnode, jobName, jobdir);
	return job;

}

Job* JobLoader::doLoadJob(pugi::xml_node jobnode, const char* jobName, char* jobdir)
{
	Job* loadedJob = Job::Create(jobnode.child(NUM_CAMS).text().as_int());

	loadedJob->SetName(jobName, jobdir);
	loadedJob->SetDisplayable(jobnode.child(DISPLAYABLE).text().as_bool());
	pugi::xml_node drawnode = jobnode.child(DRAW_SETTINGS);
	pugi::xml_node inspnode = jobnode.child(INSPECTION_SETTINGS);
	pugi::xml_node camsettnode = jobnode.child(CAMERA_SETTINGS);
	pugi::xml_node plcnode = jobnode.child(PLC_SETTINGS);
	LoadDrawSettingsNode(loadedJob, drawnode);
	LoadInspectionSettingsNode(loadedJob, inspnode);
	LoadCameraSettingsNode(loadedJob, camsettnode);
	LoadPLCSettingsNode(loadedJob, plcnode);
	return loadedJob;

};

Job::Bounds JobLoader::LoadBounds(pugi::xml_node node)
{

	Job::Bounds ret;
	ret.left = node.child(LEFT_BOUND).text().as_int();
	ret.right = node.child(RIGHT_BOUND).text().as_int();
	ret.top = node.child(TOP_BOUND).text().as_int();
	ret.bottom = node.child(BOTTOM_BOUND).text().as_int();

	return ret;
}

void JobLoader::LoadDrawSettingsNode(Job* j, pugi::xml_node drawnode)
{
	pugi::xml_node labmatnode = drawnode.child(LABEL_MATCHING);
	j->SetLabelMatching(labmatnode.child(SHOW_LABEL_MATCHING).text().as_bool(),
		LoadBounds(labmatnode.child(BOUNDS)),
		LoadBounds(labmatnode.child(TEACH_COLOR)));

	j->SetBars(drawnode.child(VERT_BAR_X).text().as_int(), drawnode.child(MID_BAR_Y).text().as_int(), 
		drawnode.child(NECK_BAR_Y).text().as_int());
	
	j->SetShowStitch(this->showStitchTable[drawnode.child(SHOW_ST).text().as_string()]);

	j->SetCapColorSize(drawnode.child(CAP_COLOR_SIZE).text().as_int());
	j->SetShow(drawnode.child(SHOW_C5_IN_PICTURES).text().as_bool(), drawnode.child(SHOW_MIDDLES).text().as_bool(),
		drawnode.child(SHOW_EDGES).text().as_bool(), drawnode.child(SHOW_INSP).child(SHOW_INSP_5).text().as_bool(),
		drawnode.child(SHOW_INSP).child(SHOW_INSP_7).text().as_bool(), drawnode.child(SHOW_INSP).child(SHOW_INSP_8).text().as_bool());

}

void JobLoader::LoadInspectionSettingsNode(Job* j, pugi::xml_node inspnode)
{
	pugi::xml_node camsettnode = inspnode.child(CAMERA_SETTINGS);
	pugi::xml_object_range<pugi::xml_node_iterator> it = camsettnode.children();
	int index, edgeThres, vertSearchLength, horSearchLength, stitchY;
	int taughtMiddleRefY, shiftTopLim, shiftBotLim, ySplice, noLabelOffsetX;
	pugi::xml_node waistnode;
	for (pugi::xml_node_iterator nit = it.begin(); nit != it.end(); ++nit)
	{
		sscanf_s(nit->attribute(INDEX).value(), "%d", &index);
		waistnode = nit->child(WAIST_MEASUREMENT);
		Job::Bounds bounds = LoadBounds(waistnode.child(BOUNDS));
		Job::Bounds upperBounds = LoadBounds(waistnode.child(UPPER_BOUNDS));
		edgeThres = waistnode.child(EDGE_THRESHOLD).text().as_int();

		vertSearchLength = waistnode.child(SEARCH_LENGTH).child(VERTICAL).text().as_int();
		horSearchLength = waistnode.child(SEARCH_LENGTH).child(HORIZONTAL).text().as_int();

		taughtMiddleRefY = nit->child(TAUGHT_MIDDLE_REF_Y).text().as_int();
		shiftTopLim = nit->child(SHIFT_TOP_LIM).text().as_int();
		shiftBotLim = nit->child(SHIFT_BOT_LIM).text().as_int();
		ySplice = nit->child(Y_SPLICE).text().as_int();
		noLabelOffsetX = nit->child(NO_LABEL_OFFSET_X).text().as_int();
		stitchY = nit->child(STITCH_Y).text().as_int();

		j->SetCameraWaist(index, bounds, upperBounds, edgeThres, vertSearchLength, horSearchLength);
		j->SetCameraInsp(index, taughtMiddleRefY, shiftTopLim, shiftBotLim, ySplice, noLabelOffsetX, stitchY);
	}

	j->SetTaught(inspnode.child(TAUGHT).text().as_bool());
	j->SetLimPatt(inspnode.child(Y_TOP_LIM_PATT).text().as_int(), inspnode.child(Y_BOT_LIM_PATT).text().as_int());
	j->SetSplice(inspnode.child(W_SPLICE).text().as_int(), inspnode.child(H_SPLICE).text().as_int(),
		inspnode.child(H_SPLICE_SM_BX).text().as_int(), inspnode.child(SPLICE_VAR).text().as_int());

	j->SetLabelIntegrityMethod(this->methodTable[inspnode.child(LAB_INTEGRITY_METHOD).text().as_int()]);
	j->SetEdgeTransition(this->showEdgeTransTable[inspnode.child(EDGE_TRANSITION).text().as_string()]);
	j->SetAvoidOffsetX(inspnode.child(AVOID_OFF_X).text().as_int());
	j->SetMiddImg(inspnode.child(MIDD_IMG).text().as_int());
	j->SetFindMiddles(inspnode.child(FIND_MIDDLES).text().as_bool());

	j->SetNoLabelOffset(inspnode.child(NO_LABEL_OFFSET).text().as_int());
	j->SetNoLabelOffsetW(inspnode.child(NO_LABEL_OFFSET_W).text().as_int());
	j->SetNoLabelOffsetH(inspnode.child(NO_LABEL_OFFSET_H).text().as_int());
	j->SetRotOffset(inspnode.child(ROT_OFFSET).text().as_int());
	j->SetBottleStyle(inspnode.child(BOTTLE_STYLE).text().as_int());
	j->SetWeHaveTempX(inspnode.child(TEMP_X).text().as_bool());

	j->SetFilter(this->filtersTable[inspnode.child(FILTER).child(FILTER_SEL_PATT).text().as_int()],
		this->filtersTable[inspnode.child(FILTER).child(FILTER_SEL_PATT_CAP).text().as_int()],
		this->filtersTable[inspnode.child(FILTER).child(FILTER_SEL_NO_LABEL).text().as_int()],
		this->filtersTable[inspnode.child(FILTER).child(FILTER_SEL_SHIFTED).text().as_int()],
		this->filtersTable[inspnode.child(FILTER).child(FILTER_SEL_SPLICE).text().as_int()]);

	j->SetBox(inspnode.child(BOX).child(BOX_YT).text().as_int(),
		inspnode.child(BOX).child(BOX_WT).text().as_int(),
		inspnode.child(BOX).child(BOX_HT).text().as_int(),
		inspnode.child(BOX).child(BOX_YB).text().as_int(),
		inspnode.child(BOX).child(BOX_HB).text().as_int());

	j->SetShift(inspnode.child(SHIFT).child(SHIFT_HOR_INI_LIM).text().as_int(),
		inspnode.child(SHIFT).child(SHIFT_HOR_END_LIM).text().as_int(),
		inspnode.child(SHIFT).child(SHIFT_WIDTH).text().as_int());

	j->SetSensors(inspnode.child(SENSORS).child(SENS_EDGE).text().as_int(),
		inspnode.child(SENSORS).child(LABEL_SENS).text().as_int(),
		inspnode.child(SENSORS).child(NO_LABEL_SENS).text().as_int(),
		inspnode.child(SENSORS).child(SHIFTED_LABEL_SENS).text().as_int(),
		inspnode.child(SENSORS).child(EDGE_LABEL_SENS).text().as_int(),
		inspnode.child(SENSORS).child(ELLIPSE_SENS).text().as_int(),
		inspnode.child(SENSORS).child(MIDD_SENS).text().as_int());

	j->SetStitching(
		this->stitchingUseTable[inspnode.child(STITCHING).child(USE).text().as_string()],
		inspnode.child(STITCHING).child(STITCH_WIDTH).text().as_int(),
		inspnode.child(STITCHING).child(STITCH_HEIGHT).text().as_int(),
		inspnode.child(STITCHING).child(STITCH_LIGHT_SENSOR).text().as_int(),
		inspnode.child(STITCHING).child(STITCH_DARK_SENSOR).text().as_int(),
		inspnode.child(STITCHING).child(STITCH_EDGE_SENSOR).text().as_int(),
		inspnode.child(STITCHING).child(STITCH_LIGHT_IMG).text().as_int(),
		inspnode.child(STITCHING).child(STITCH_DARK_IMG).text().as_int(),
		inspnode.child(STITCHING).child(STITCH_EDGE_IMG).text().as_int());

	j->SetSport(
		inspnode.child(SPORT).child(ENABLED).text().as_bool(),
		inspnode.child(SPORT).child(SPORT_POSITION).text().as_int(),
		inspnode.child(SPORT).child(SPORT_SENSOR).text().as_int());

	j->SetCap(
		inspnode.child(CAP).child(L_DIST).text().as_int(),
		inspnode.child(CAP).child(COLOR_TARGET).child(RED).text().as_int(),
		inspnode.child(CAP).child(COLOR_TARGET).child(GREEN).text().as_int(),
		inspnode.child(CAP).child(COLOR_TARGET).child(BLUE).text().as_int(),
		inspnode.child(CAP).child(NECK_STRENGTH).text().as_int(),
		inspnode.child(CAP).child(CAP_STRENGTH).text().as_int(),
		inspnode.child(CAP).child(CAP_STRENGTH2).text().as_int(),
		inspnode.child(CAP).child(ORIEN_AREA).text().as_int(),
		inspnode.child(CAP).child(FILL_OFFSET).text().as_int(),
		inspnode.child(CAP).child(CAP_COLOR_OFFSET).text().as_int());

	j->SetMethods(inspnode.child(METHODS).child(SENS_METHOD_3).text().as_int(),
		inspnode.child(METHODS).child(HEIGHT_METHOD_3).text().as_int(),
		inspnode.child(METHODS).child(WIDTH_METHOD_3).text().as_int(),
		inspnode.child(METHODS).child(HEIGHT_METHOD_5).text().as_int(),
		inspnode.child(METHODS).child(WIDTH_METHOD_5).text().as_int());


	pugi::xml_node nIS = inspnode.child(INSPECTIONS);
	it = nIS.children();
	for (pugi::xml_node_iterator nit = it.begin(); nit != it.end(); ++nit)
	{
		j->SetInspection(nit->child(INDEX).text().as_int(), nit->child(ENABLED).text().as_bool(),
			nit->child(L_MIN).text().as_float(), nit->child(L_MAX).text().as_float(),
			nit->child(L_MIN_B).text().as_float(), nit->child(L_MAX_C).text().as_float(), nit->child(IS_FLOAT).text().as_bool());
	}
}

void JobLoader::LoadPLCSettingsNode(Job* j, pugi::xml_node plcnode)
{
	j->SetUseRing(plcnode.child(USE_RING).text().as_bool());
	j->SetMotPos(plcnode.child(MOT_POS).text().as_int());
}

void JobLoader::LoadCameraSettingsNode(Job* j, pugi::xml_node settingsnode)
{
	pugi::xml_object_range<pugi::xml_node_iterator> it = settingsnode.children();
	int index, count =0;
	for (pugi::xml_node_iterator nit = it.begin(); nit != it.end(); ++nit)
	{
		sscanf_s(nit->attribute(INDEX).value(), "%d", &index);
		j->SetLightLevel(index, nit->child(LIGHT_LEVEL).text().as_int());
		count++;
	}
	j->SetNumberCams(count);
}