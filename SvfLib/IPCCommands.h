#include "defs.h"

#ifndef COMMAND_ENUM_H
#define COMMAND_ENUM_H

class EXPORT IPCCommands
{
public:

    enum EngCommand
    {
        ReloadJob = 1,
        Shutdown = 2,
        ReconnectCams = 3,
        ChangeCamSetting = 4,
        PLCPassthrough = 5,
        TimedTrigger = 6,
    };

    enum EXPORT CamSettings
    {
    };

    enum class PLCCommand
    {
        Online,
        Offline,
        ManualTrigger,

    };

};
#endif