#include "inspection.h"

void Inspection::SetMin(float value)
{
	min = value;
}

float Inspection::GetMin()
{
	return min;
}

void Inspection::SetMax(float value)
{
	max = value;
}

float Inspection::GetMax()
{
	return max;
}
void Inspection::SetMinB(float value)
{
	minB = value;
}

float Inspection::GetMinB()
{
	return minB;
}

void Inspection::SetMaxC(float value)
{
	maxC = value;
}

float Inspection::GetMaxC()
{
	return maxC;
}

bool Inspection::IsFloat()
{
	return isFloat;
}

bool Inspection::IsEnabled()
{
	return enabled;
}

void Inspection::Enable(bool value)
{
	enabled = value;
}