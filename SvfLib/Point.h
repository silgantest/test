#include "defs.h"

#ifndef POINT_H
#define POINT_H


class EXPORT Point
{
public:
	int x;
	int y;
	int c;
};

class EXPORT SpecialPoint
{
public:
	int x;
	int y;
	int c;
};


class EXPORT TwoPoint
{
public:
	int x;
	int y;
	int x1;
	int y1;
};


class EXPORT SinglePoint
{
public:
	int x;
	int y;
};

struct EXPORT SingPtXAsc
{
	bool operator()(const SinglePoint & a, const SinglePoint & b)const
	{
		return a.x < b.x;
	}
};
#endif //POINT_H