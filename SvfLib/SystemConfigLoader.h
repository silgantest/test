#include "SystemConfig.h"
#include "AppConfig.h"
#include "directories.h"
#include "defs.h"

#ifndef SYSTEM_CONFIG_LOADER_H
#define SYSTEM_CONFIG_LOADER_H

class EXPORT SystemConfigLoader
{
private:
			
	class Holder
	{
	public:
		SystemConfigLoader* instance;
		Holder();
		~Holder();

	};
	static Holder instanceHolder;
	static SystemConfig* theConfig;


public:
	SystemConfigLoader();
	virtual ~SystemConfigLoader();

	static SystemConfig* GetConfig();

	SystemConfig* LoadConfig();

};

#endif // SYSTEM_CONFIG_LOADER_H
