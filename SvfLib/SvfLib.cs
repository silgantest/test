//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 4.0.2
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------


public class SvfLib {
  public static string SILGAN_DATA_DIR {
    get {
      string ret = SvfLibPINVOKE.SILGAN_DATA_DIR_get();
      return ret;
    } 
  }

  public static string SILGAN_JOBS_DIR {
    get {
      string ret = SvfLibPINVOKE.SILGAN_JOBS_DIR_get();
      return ret;
    } 
  }

  public static string CONFIG_FILE {
    get {
      string ret = SvfLibPINVOKE.CONFIG_FILE_get();
      return ret;
    } 
  }

  public static string APP_CONFIG_FILE {
    get {
      string ret = SvfLibPINVOKE.APP_CONFIG_FILE_get();
      return ret;
    } 
  }

  public static string SIM_DIR {
    get {
      string ret = SvfLibPINVOKE.SIM_DIR_get();
      return ret;
    } 
  }

  public static string SIM_DIR_CAM {
    get {
      string ret = SvfLibPINVOKE.SIM_DIR_CAM_get();
      return ret;
    } 
  }

  public static string SHARED_MEMORY_IMAGES {
    get {
      string ret = SvfLibPINVOKE.SHARED_MEMORY_IMAGES_get();
      return ret;
    } 
  }

  public static string SHARED_MEMORY_IMAGES_MTX {
    get {
      string ret = SvfLibPINVOKE.SHARED_MEMORY_IMAGES_MTX_get();
      return ret;
    } 
  }

  public static string SHARED_MEMORY_IMAGES_SEM {
    get {
      string ret = SvfLibPINVOKE.SHARED_MEMORY_IMAGES_SEM_get();
      return ret;
    } 
  }

  public static string COMMAND_BUFFER {
    get {
      string ret = SvfLibPINVOKE.COMMAND_BUFFER_get();
      return ret;
    } 
  }

  public static string SHARED_MEMORY_COMMAND_BUFFER_MTX {
    get {
      string ret = SvfLibPINVOKE.SHARED_MEMORY_COMMAND_BUFFER_MTX_get();
      return ret;
    } 
  }

  public static string SHARED_MEMORY_COMMAND_BUFFER_SEM {
    get {
      string ret = SvfLibPINVOKE.SHARED_MEMORY_COMMAND_BUFFER_SEM_get();
      return ret;
    } 
  }

  public static string STATUS_BUFFER {
    get {
      string ret = SvfLibPINVOKE.STATUS_BUFFER_get();
      return ret;
    } 
  }

  public static string SHARED_MEMORY_STATUS_BUFFER_MTX {
    get {
      string ret = SvfLibPINVOKE.SHARED_MEMORY_STATUS_BUFFER_MTX_get();
      return ret;
    } 
  }

  public static string SHARED_MEMORY_STATUS_BUFFER_SEM {
    get {
      string ret = SvfLibPINVOKE.SHARED_MEMORY_STATUS_BUFFER_SEM_get();
      return ret;
    } 
  }

  public static int IMAGE_SIZE {
    get {
      int ret = SvfLibPINVOKE.IMAGE_SIZE_get();
      return ret;
    } 
  }

  public static int CAP_IMAGE_SIZE {
    get {
      int ret = SvfLibPINVOKE.CAP_IMAGE_SIZE_get();
      return ret;
    } 
  }

  public static int CAP_FILTER_SIZE {
    get {
      int ret = SvfLibPINVOKE.CAP_FILTER_SIZE_get();
      return ret;
    } 
  }

  public static int MAX_RESULTS_SIZE {
    get {
      int ret = SvfLibPINVOKE.MAX_RESULTS_SIZE_get();
      return ret;
    } 
  }

  public static int MAX_DETAILS_BUFFER {
    get {
      int ret = SvfLibPINVOKE.MAX_DETAILS_BUFFER_get();
      return ret;
    } 
  }

  public static int SHARED_MEM_BUFFER_SIZE {
    get {
      int ret = SvfLibPINVOKE.SHARED_MEM_BUFFER_SIZE_get();
      return ret;
    } 
  }

  public static int COMMAND_BUFFER_SIZE {
    get {
      int ret = SvfLibPINVOKE.COMMAND_BUFFER_SIZE_get();
      return ret;
    } 
  }

  public static int STATUS_BUFFER_SIZE {
    get {
      int ret = SvfLibPINVOKE.STATUS_BUFFER_SIZE_get();
      return ret;
    } 
  }

  public static int CapResults_LeftSide {
    get {
      int ret = SvfLibPINVOKE.CapResults_LeftSide_get();
      return ret;
    } 
  }

  public static int CapResults_RightSide {
    get {
      int ret = SvfLibPINVOKE.CapResults_RightSide_get();
      return ret;
    } 
  }

  public static int CapResults_LeftTop {
    get {
      int ret = SvfLibPINVOKE.CapResults_LeftTop_get();
      return ret;
    } 
  }

  public static int CapResults_RightTop {
    get {
      int ret = SvfLibPINVOKE.CapResults_RightTop_get();
      return ret;
    } 
  }

  public static int CapResults_LeftLip {
    get {
      int ret = SvfLibPINVOKE.CapResults_LeftLip_get();
      return ret;
    } 
  }

  public static int CapResults_RightLip {
    get {
      int ret = SvfLibPINVOKE.CapResults_RightLip_get();
      return ret;
    } 
  }

  public static int CapResults_MidTop {
    get {
      int ret = SvfLibPINVOKE.CapResults_MidTop_get();
      return ret;
    } 
  }

  public static int CapResults_MidBot {
    get {
      int ret = SvfLibPINVOKE.CapResults_MidBot_get();
      return ret;
    } 
  }

  public static int CapResults_SportBand {
    get {
      int ret = SvfLibPINVOKE.CapResults_SportBand_get();
      return ret;
    } 
  }

  public static int CapResults_Red {
    get {
      int ret = SvfLibPINVOKE.CapResults_Red_get();
      return ret;
    } 
  }

  public static int CapResults_Green {
    get {
      int ret = SvfLibPINVOKE.CapResults_Green_get();
      return ret;
    } 
  }

  public static int CapResults_Blue {
    get {
      int ret = SvfLibPINVOKE.CapResults_Blue_get();
      return ret;
    } 
  }

  public static readonly int MAX_JOB_NAME_LENGTH = SvfLibPINVOKE.MAX_JOB_NAME_LENGTH_get();
  public static readonly int MAX_JOB_PATH_LENGTH = SvfLibPINVOKE.MAX_JOB_PATH_LENGTH_get();
  public static readonly string JOB_FULL_PATH = SvfLibPINVOKE.JOB_FULL_PATH_get();
  public static readonly string JOB_DIR = SvfLibPINVOKE.JOB_DIR_get();
  public static readonly int INSPECTIONS_85 = SvfLibPINVOKE.INSPECTIONS_85_get();
}
