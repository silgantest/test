#include <string>
#include <list>
#include "AppConfig.h"
#include "defs.h"

#ifndef SYSTEM_CONFIG_H
#define SYSTEM_CONFIG_H

class EXPORT ExpectedCameraConfig
{
private:
	int number;
	std::string vendor;
	std::string model;
	std::string serial;
	std::string title;

	int offsetX;
	int offsetY;
	int width;
	int height;

public:
	ExpectedCameraConfig(int number, std::string vendor, std::string model, std::string serial, std::string title);
	virtual ~ExpectedCameraConfig();

	std::string GetVendor();
	std::string GetModel();
	std::string GetSerial();
	std::string GetTitle();
	int GetNumber();
	int GetOffsetX();
	int GetOffsetY();
	int GetWidth();
	int GetHeight();

	void AddAOI(int oX, int oY, int wid, int hei);
};

class EXPORT  SystemConfig
{
public:
	enum SystemType
	{
		SVF85 = 85
	};


private:
	int requiredCameras;
	std::string model;
	std::list<ExpectedCameraConfig*>* cameras;
	
	SystemType type;
	AppConfig* appConfig;

private:
	void setSysType();
public:
	SystemConfig(std::string model, int requiredCameras);
	SystemConfig();
	virtual ~SystemConfig();

	std::string GetModel();
	int GetRequiredCameras();

	void AddCamera(ExpectedCameraConfig* cam);

	std::list<ExpectedCameraConfig*>* GetCameras();

	void SetAppConfig(AppConfig* appConfig);
	AppConfig* GetAppConfig();
	SystemType GetSystemType();
	int GetNumberInspections();

};



#endif // SYSTEM_CONFIG_H
