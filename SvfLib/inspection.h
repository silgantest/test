#include "defs.h"
#include <iostream>

#ifndef INSPECTION_H
#define INSPECTION_H

class EXPORT Inspection
{
private:
	float min;
	float max;
	float maxC;
	float minB;
	int index;
	bool enabled;
	bool isFloat;

public:
	void SetValues(int index, bool enabled, float lmin, float lmax, float lminB, float lmaxC, bool isFloat)
	{
		this->index = index;
		this->enabled = enabled;
		this->min = lmin;
		this->max = lmax;
		this->minB = lminB;
		this->maxC = lmaxC;
		this->isFloat = isFloat;
	}

	void SetMin(float value);
	float GetMin();
	void SetMax(float value);
	float GetMax();
	void SetMinB(float value);
	float GetMinB();
	void SetMaxC(float value);
	float GetMaxC();
	bool IsFloat();
	bool IsEnabled();
	void Enable(bool value);

};
#endif