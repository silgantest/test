#include "defs.h"

#ifndef LABEL_INTEGRITY_METHOD
#define LABEL_INTEGRITY_METHOD


//TODO -- Figure out more meaningful labels
enum class EXPORT LabelIntegrityMethod
{
    Method1,
    Method2,
    Method3,  //Edge Search
    Method4,
    Method5,
    Method6,
    Unknown
};

enum class EXPORT ImageFilters
{
    Unknown = 0,
    BlackAndWhite = 1,
    Saturation = 2,
    Magenta = 3, 
    Yellow = 4,
    Cyan = 5,
    Red = 6,
    Green = 7,
    Blue = 8
};
#endif

