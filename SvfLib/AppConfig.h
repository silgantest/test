#include "defs.h"
#include <list>

#ifndef APP_CONFIG
#define APP_CONFIG



class EXPORT AppConfig
{

private:
	int camHeight;
	int camWidth;

	int mainDisplayImageHeight;
	int mainDisplayImageWidth;
	int mainDisplayImageXOffset;
	int mainDisplayImageYOffset;

	int croppedCameraWidth;
	int croppedCameraHeight;
	int croppedCameraOffsetX;
	int croppedCameraOffsetY;

	int capUICameraWidth;
	int capUICameraHeight;
	int labelUICameraWidth;
	int labelUICameraHeight;

	int sizePatternBy3_2W; //20
	int sizePatternBy3_2H; //20
	int sizePatternBy6;  //12
	int sizePatternBy12;  //12

	int scaleFactor;
	int bytesPerPixel;

	bool wasitAvailable;
	bool alignAvailable;
	AppConfig();

public:
	virtual ~AppConfig();

	int GetMainDisplayImageHeight();
	int GetMainDisplayImageWidth();

	int GetMainCameraDisplayXOffset();
	int GetMainCameraDisplayYOffset();

	int GetCroppedCameraWidth();
	int GetCroppedCameraHeight();
	int GetCroppedCameraOffsetX();
	int GetCroppedCameraOffsetY();

	int GetSizePattBy3_2W();
	int GetSizePattBy3_2H(); 
	int GetSizePattBy6();
	int GetSizePattBy12();
	int GetScaleFactor();
	int GetBytesPerPixel();

	int GetLabelUICameraWidth();
	int GetLabelUICameraHeight();
	int GetCapUICameraWidth();
	int GetCapUICameraHeight();

	void SetMainDisplayImageHeight(int val);
	void SetMainDisplayImageWidth(int val); 
	void SetMainCameraDisplayXOffset(int val);
	void SetMainCameraDisplayYOffset(int val);
	void SetCroppedCameraWidth(int val);
	void SetCroppedCameraHeight(int val);
	void SetCroppedCameraOffsetX(int val);
	void SetCroppedCameraOffsetY(int val);
	void SetSizePattBy3_2W(int val);
	void SetSizePattBy3_2H(int val); 
	void SetSizePattBy6(int val); 
	void SetSizePattBy12(int val);

	void SetScaleFactor(int val);
	void SetBytesPerPixel(int val);


	bool GetWaistAvailable();
	bool GetAlignAvailable();
	int GetCamWidth();
	int GetCamHeight();

	void SetWaistAvailable(bool val);
	void SetAlignAvailable(bool val);
	void SetCamWidth(int val);
	void SetCamHeight(int val);


	void SetLabelUICameraWidth(int val);
	void SetLabelUICameraHeight(int val);
	void SetCapUICameraWidth(int val);
	void SetCapUICameraHeight(int val);

	int GetUIWidth(int index);
	int GetUIHeight(int index);
	static AppConfig* LoadAppConfig85();

};

#endif 