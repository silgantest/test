//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 4.0.2
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------


public enum ImageFilters {
  Unknown = 0,
  BlackAndWhite = 1,
  Saturation = 2,
  Magenta = 3,
  Yellow = 4,
  Cyan = 5,
  Red = 6,
  Green = 7,
  Blue = 8
}
