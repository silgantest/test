#include <string.h>

#ifndef STATUS_H
#define STATUS_H

#define MAX_INSPECTIONS 12
#define CIRCULAR_BUFFER_SIZE 64

enum OperationMode
{
	Online = 1,
	Offline = 2,
};

class Status
{
private:
	OperationMode mode;
	int inspected;
	int rejected;
	int ReasonForFailure[MAX_INSPECTIONS];
	bool Trends [MAX_INSPECTIONS][CIRCULAR_BUFFER_SIZE];

	int currentIndices[MAX_INSPECTIONS];
	static Status* currentStatus;
	Status()
	{
		memset(&ReasonForFailure, 0, MAX_INSPECTIONS * sizeof(int));
		memset(&currentIndices, 0, MAX_INSPECTIONS * sizeof(int));
		memset(&Trends, 0, CIRCULAR_BUFFER_SIZE + MAX_INSPECTIONS * sizeof(bool));
	}
public:

	

	virtual ~Status()
	{
	}

	static void Shutdown()
	{
		delete currentStatus;
	}

	static void Initialize()
	{
		currentStatus = new Status();
	}

	static Status* GetStatus()
	{
		return currentStatus;
	}

	void UpdateFromResultSet(int index, bool pass)
	{
		inspected++;
		if (!pass)
		{
			ReasonForFailure[index]++;
			rejected++;
		}
		Trends[index][currentIndices[index]] = pass;
		currentIndices[index]++;
		currentIndices[index] = currentIndices[index] & (CIRCULAR_BUFFER_SIZE-1);
	}


	void UpdateFromResultSet(Results* )
	{
		inspected++;
		if (!pass)
		{
			ReasonForFailure[index]++;
			rejected++;
		}
		Trends[index][currentIndices[index]] = pass;
		currentIndices[index]++;
		currentIndices[index] = currentIndices[index] & (CIRCULAR_BUFFER_SIZE - 1);
	}

	void GetStatusArray(unsigned char* buffer)
	{
		memcpy(buffer, ReasonForFailure, MAX_INSPECTIONS * sizeof(int));
		int index = MAX_INSPECTIONS * sizeof(int) - 1;
		memcpy(&buffer[index], Trends, MAX_INSPECTIONS * CIRCULAR_BUFFER_SIZE * sizeof(bool));
		index += MAX_INSPECTIONS * CIRCULAR_BUFFER_SIZE * sizeof(bool);
		memcpy(&buffer[index], (void*)inspected, sizeof(int));
		memcpy(&buffer[index], (void*)rejected, sizeof(int));
		memcpy(&buffer[index], (void*)mode, sizeof(int));
	}
};

#endif