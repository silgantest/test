﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using SilganVision.Models;
using SilganVision.IPC;

namespace SilganVision
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        InspectionUpdator updator;
        EngineCommandThread commandThread;

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            updator.Stop();
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            LatestUnit unit = LatestUnit.GetLatestUnit();
            updator = new InspectionUpdator(unit, SilganVision.FSI.ConfigManager.GetCurrentConfig().SysConfig);
            updator.Start();
            ImageDrawablesFactory.Initialize(SilganVision.FSI.ConfigManager.GetCurrentConfig().SysConfig.GetRequiredCameras());
        }
    }
}
