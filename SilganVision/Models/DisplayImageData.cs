﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace SilganVision.Models
{

    public class DisplayImageData : INotifyPropertyChanged
    {
        WriteableBitmap notDrawable;
        Int32Rect rect;

        public DisplayImageData()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Int32Rect Rect
        {
            get
            {
                return rect;
            }
        }


        public WriteableBitmap NotDrawable
        {
            get
            {
                return notDrawable;
            }
        }


        public void SetBitmapData(byte[] data, int linewidth, int imageWidth, int imageHeight, bool gray = false)
        {
            if (notDrawable == null)
            {
                BitmapPalette myPalette;
                if (gray)
                {
                    myPalette = BitmapPalettes.Gray256;
                    notDrawable = new WriteableBitmap(imageWidth, imageHeight, 96, 96, PixelFormats.Gray8, myPalette);
                }
                else
                { 
                    List<System.Windows.Media.Color> colors = new List<System.Windows.Media.Color>();
                    colors.Add(System.Windows.Media.Colors.Red);
                    colors.Add(System.Windows.Media.Colors.Blue);
                    colors.Add(System.Windows.Media.Colors.Green);
                    myPalette = new BitmapPalette(colors);
                    notDrawable = new WriteableBitmap(imageWidth, imageHeight, 96, 96, PixelFormats.Bgra32, myPalette);
                }
                rect = new Int32Rect(0, 0, imageWidth, imageHeight);
            }
            notDrawable.WritePixels(rect, data, linewidth, 0);
            NotifyDisplayChanged();
         }

        public void NotifyDisplayChanged()
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("NotDrawable"));

        }

        public int Index
        {
            get;
            set;
        }

    
    }
   


    



}
