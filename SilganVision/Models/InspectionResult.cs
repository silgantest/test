﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SilganVision.Models
{
    public delegate void InspectionUpdatedEventHandler();

    public enum InspectionPassFail
    { 
        Pass,
        Fail,
        NotRun
    }
    public enum DataType4Byte
    {
        FLOAT,
        INT,
        UNKNOWN
    }

    public class InspectionResult 
    {
        public DataType4Byte DataType
        {
            get;
            set;
        }

        public int Index
        {
            get;
            set;
        }

        public string Min
        {
            get;
            set;
        }

        public string Max
        {
            get;
            set;
        }

        public string CurrentResultValue
        {
            get;
            set;
        }

        public InspectionPassFail CurrentResult
        {
            get;
            set;
        }

        public event InspectionUpdatedEventHandler InspectionUpdated;

        public void SendUpdates()
        {
            if (InspectionUpdated != null)
                InspectionUpdated();
        }

    }
}
