﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilganVision.Models
{   class LoadedConfig
    {
        private SystemConfig sysConfig;
        private UsageConfig usrConfig;
        private Job currentJob;

        public LoadedConfig(Job job, SystemConfig sysCfg)
        {
            this.currentJob = job;
            this.sysConfig = sysCfg;
        }

        public Job CurrentJob
        {
            get
            {
                return currentJob;
            }
            set
            {
                currentJob = value;
            }
        }

        public SystemConfig SysConfig
        {
            get
            {
                return sysConfig;
            }
        }

        public AppConfig AppConfig
        {
            get
            {
                return sysConfig.GetAppConfig();
            }
        }

        public int DisplayableHeight
        {
            get
            {
                return sysConfig.GetAppConfig().GetMainDisplayImageHeight() / sysConfig.GetAppConfig().GetScaleFactor();
            }
        
        }

        public int DisplayableWidth
        {
            get
            {
                return sysConfig.GetAppConfig().GetMainDisplayImageWidth() / sysConfig.GetAppConfig().GetScaleFactor();
            }

        }
    }
}
