﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilganVision.Models
{
    public class LatestUnit
    {
        private DisplayImageDataGroup imageData;
        private List<InspectionResult> list;
        private Dictionary<string, InspectionResult> hash;
        protected static LatestUnit theObject = null;

        protected LatestUnit(int numInspections)
        {
            imageData = new DisplayImageDataGroup();
            list = new List<InspectionResult>();
            for (int i = 0; i < numInspections; i++)
                list.Add(new InspectionResult());
        }

        public static LatestUnit GetLatestUnit()
        {
            if (theObject == null)
                theObject = new LatestUnit(SilganVision.FSI.ConfigManager.GetCurrentConfig().SysConfig.GetNumberInspections());
            return theObject;
        }


        public byte[][] ResultDetails
        {
            get;
            set;
        }

        public DisplayImageDataGroup ImageData
        {
            get
            {
                return imageData;
            }
        }

        public InspectionResult GetInspection(int index)
        {
            return list[index];
        }

        public int GetNumInspections()
        {
            return list.Count;
        }


    }
}
