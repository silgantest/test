﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SilganVision.Models
{
    public class JobSetupInfo : INotifyPropertyChanged
    {
        public JobSetupInfo()
        {
            tempJobName = "";
        }

        string tempJobName;
        public string TempJobName
        {
            get
            {
                return tempJobName;
            }
            set
            {
                tempJobName = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("TempJobName"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
