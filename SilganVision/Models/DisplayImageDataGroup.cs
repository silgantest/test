﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SilganVision.FSI;

namespace SilganVision.Models
{
    public class DisplayImageDataGroup 
    {
        DisplayImageData[] images;
        DisplayImageData[] capFilters;
        public DisplayImageDataGroup()
        {
            int numOfCams = SilganVision.FSI.ConfigManager.GetCurrentConfig().SysConfig.GetRequiredCameras();
            images = new DisplayImageData[numOfCams];
            for (int i = 0; i < numOfCams; i++)
            {
                images[i] = new DisplayImageData();
                images[i].Index = i;
            }
            if (numOfCams == 5)
            {
                capFilters = new DisplayImageData[4];
                for (int i = 0; i < 4; i++)
                { 
                    capFilters[i] = new DisplayImageData();
                    capFilters[i].Index = 4;
                }
            }

        }

        public void SetBufferGroup(MemoryBufferGroup group)
        {
            for (int i = 0; i < group.NumCams; i++)
            {
                images[i].SetBitmapData(group.Images[i], group.GetLineLength(i), group.Width(i), group.Height(i));
            }
            if(group.NumCams == 5)
            {
                for (int i = 0; i < 4; i++)
                {
                    capFilters[i].SetBitmapData(group.CapFilters[i], group.GetLineLength(4)/4, group.Width(4), group.Height(4), true);
                }
            }
        }

        public DisplayImageData GetImage(int index)
        {
            return images[index];
        }

        public DisplayImageData GetCapFilters(int index)
        {
            return capFilters[index];
        }

     }
}
