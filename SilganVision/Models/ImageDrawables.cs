﻿using System.Collections.Generic;
using System;
using System.Windows.Media;

namespace SilganVision.Models
{
    public class Drawable
    {
        public System.Windows.Media.Color Color;
    }


    public class BoxDrawable : Drawable
    {
        public System.Windows.Point TopLeft = new System.Windows.Point();
        public System.Windows.Point BottomRight = new System.Windows.Point();
        public int Width;
        public int Height;
    }

    public enum Axis
    {
        X, 
        Y,
    }

    public class LineDrawable : Drawable
    {
        public Axis axis;
        public int location;
        private BoxDrawable box = new BoxDrawable();

        public BoxDrawable GetPoints(System.Windows.Int32Rect rect)
        {
            if(axis == Axis.X)
            {
                box.BottomRight.X = location;
                box.BottomRight.Y = rect.Height;
                box.TopLeft.X = location;
                box.TopLeft.Y = 0;
            }
            else
            {
                box.BottomRight.Y = location;
                box.BottomRight.X = rect.Width;
                box.TopLeft.Y = location;
                box.TopLeft.X = 0;

            }
            return box;
        }
    }


    public class ImageDrawables
    {
        public List<LineDrawable> Lines = new List<LineDrawable>();
        public List<BoxDrawable> Boxes = new List<BoxDrawable>();
        public List<string> Text = new List<string>();

        public void Clear()
        {
            Lines.Clear();
            Boxes.Clear();
            Text.Clear();
        }
    }
    
    public static class ImageDrawablesFactory
    {
        public static ImageDrawables[] jobDrawables;

        public static void Initialize(int maxCams)
        {
            jobDrawables = new ImageDrawables[5];
            for(int i=0; i< maxCams; i++)
                jobDrawables[i] = new ImageDrawables();

        }


        public static ImageDrawables GetImageDrawablesFromJob(int index)
        {
            jobDrawables[index].Clear();
            Job job = SilganVision.FSI.ConfigManager.GetCurrentConfig().CurrentJob;
            switch (index)
            {
                case 4:
                    LineDrawable l = new LineDrawable();
                    l.axis = Axis.X;
                    l.location = job.GetVertXBar();
                    l.Color = System.Windows.Media.Colors.SlateGray;
                    jobDrawables[index].Lines.Add(l);
                    l = new LineDrawable();
                    l.axis = Axis.Y;
                    l.location = job.GetMidYBar();
                    l.Color = System.Windows.Media.Colors.SlateGray;
                    jobDrawables[index].Lines.Add(l);
                    l = new LineDrawable();
                    l.axis = Axis.Y;
                    l.location = job.GetNeckBarY();
                    l.Color = System.Windows.Media.Colors.SlateGray;
                    jobDrawables[index].Lines.Add(l);

                    BoxDrawable b = new BoxDrawable();

                    break;
            }
            return jobDrawables[index];
        }
   
        public static ImageDrawables GetDrawablesFromInspections(int index, byte[] resultdetails)
        {
            ImageDrawables drawables = new ImageDrawables();
            Job job = SilganVision.FSI.ConfigManager.GetCurrentConfig().CurrentJob;

            if (index == 4)
            {
                //if (job.GetInspection(0).IsEnabled())
                //{
                System.Windows.Point topL = GetPointFromBytes(resultdetails, SvfLib.CapResults_LeftTop);
                System.Windows.Point topR = GetPointFromBytes(resultdetails, SvfLib.CapResults_RightTop);

                drawables.Boxes.Add(GetBoxAtPoint(GetPointFromBytes(resultdetails, SvfLib.CapResults_LeftSide), 4, 4, System.Windows.Media.Colors.Green));
                drawables.Boxes.Add(GetBoxAtPoint(GetPointFromBytes(resultdetails, SvfLib.CapResults_RightSide), 4, 4, System.Windows.Media.Colors.Green));
                drawables.Boxes.Add(GetBoxAtPoint(topL, 4, 4, System.Windows.Media.Colors.Green));
                drawables.Boxes.Add(GetBoxAtPoint(topR, 4, 4, System.Windows.Media.Colors.Green));
                //}

                if (job.SportCap())
                {
                    int sportPosY = job.GetSportPos();
                    System.Windows.Point boxStart = new System.Windows.Point();
                    boxStart.X = topL.X - 5;
                    boxStart.Y = sportPosY - 5;

                    drawables.Boxes.Add(GetBoxAtPoint(boxStart, (int)(topR.X - boxStart.X), 
                        10, System.Windows.Media.Colors.Green));

                    drawables.Boxes.Add(GetBoxAtPoint(new System.Windows.Point(
                        BitConverter.ToInt32(resultdetails, SvfLib.CapResults_SportBand), sportPosY),
                        3, 6, System.Windows.Media.Colors.Green));
                    drawables.Boxes.Add(GetBoxAtPoint(new System.Windows.Point(
                        BitConverter.ToInt32(resultdetails, SvfLib.CapResults_SportBand+2), sportPosY),
                        3, 6, System.Windows.Media.Colors.Green));
                }
                else
                {
                    drawables.Boxes.Add(GetBoxAtPoint(GetPointFromBytes(resultdetails, SvfLib.CapResults_MidTop), 
                     4, 4, System.Windows.Media.Colors.Green));

                }

                System.Windows.Point leftLip = GetPointFromBytes(resultdetails, SvfLib.CapResults_LeftLip);
                System.Windows.Point rightLip = GetPointFromBytes(resultdetails, SvfLib.CapResults_RightLip);
                drawables.Boxes.Add(GetBoxAtPoint(new System.Windows.Point(
                    leftLip.X + 80,
                    leftLip.Y - job.GetCapColorOffset()),
                    60, job.GetCapColorSize(), System.Windows.Media.Colors.Green));


                int leftc = BitConverter.ToInt32(resultdetails, SvfLib.CapResults_LeftLip + 2);
                int rightc = BitConverter.ToInt32(resultdetails, SvfLib.CapResults_RightLip + 2);
                Color right, left;
                if (rightc > leftc)
                {
                    right = System.Windows.Media.Colors.Red;
                    left = System.Windows.Media.Colors.Green;
                }
                else
                {
                    left = System.Windows.Media.Colors.Red;
                    right = System.Windows.Media.Colors.Green;
                }

                leftLip.X -= 5;
                rightLip.X -= 5;
                drawables.Boxes.Add(GetBoxAtPoint(leftLip, 20, 3, left));
                drawables.Boxes.Add(GetBoxAtPoint(rightLip, 20, 3, right));
            }

     
            return drawables;
        }

        private static BoxDrawable GetBoxAtPoint(System.Windows.Point point, int width, int height, Color col)
        {
            BoxDrawable b = new BoxDrawable();
            b.TopLeft = point;
            b.Width = width;
            b.Height = height;
            b.BottomRight = new System.Windows.Point(point.X + width, point.Y + height);
            b.Color = col;
            return b;
        }

        public static System.Windows.Point GetPointFromBytes(byte[] pt, int offset)
        {
            return new System.Windows.Point(BitConverter.ToInt32(pt, offset + 0), BitConverter.ToInt32(pt, offset + 4));
        }
    }
}
