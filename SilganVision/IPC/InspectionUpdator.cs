﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using SilganVision.Models;
using SilganVision.ViewModels;
using System.Windows.Threading;

namespace SilganVision.IPC
{
    public class InspectionUpdator
    {
        MemoryBufferGroupFactory factory;
        InspectionBufferQueue queue;
        SharedMemoryThread imThread;
        LatestUnit unit;
        Thread thread;
        Dispatcher d;

        public InspectionUpdator(LatestUnit latestUnit, SystemConfig cfg)
        {
            unit = latestUnit;
            factory = new MemoryBufferGroupFactory();
            factory.InitializeFactory(cfg);
            queue = new InspectionBufferQueue();
            imThread = new SharedMemoryThread();
            imThread.Initialize(factory, queue, cfg.GetRequiredCameras());
            imThread.StartReadingImages();
            d = Dispatcher.CurrentDispatcher;
        }

        internal void Stop()
        {
            imThread.StopReadingImages();
            queue.Shutdown();
        }

        public void Start()
        {

            ThreadStart ts = MemoryThread;
            thread = new Thread(ts);
            thread.IsBackground = true;
            thread.Start();

        }

        private void Getbytes(byte[] outBytes, byte[] sourcebytes, int position)
        {
            for (int i = 0; i < 4; i++)
                outBytes[i] = sourcebytes[position + i];
            
        }

        public string ConvertInt(byte[] outBytes, byte[] sourcebytes, int position)
        {
            Getbytes(outBytes, sourcebytes, position);
            return Convert.ToInt32(outBytes).ToString();
        }

        public string ConvertFloat(byte[] outBytes, byte[] sourcebytes, int position)
        {
            return Math.Round(BitConverter.ToSingle(sourcebytes, position), 3).ToString();
        }

        public void MemoryThread()
        {
            MemoryBufferGroup group;
            byte[] temp = new byte[4];
            while ((group = queue.GetNextImage()) != null)
            {
                d.Invoke(() =>
                {
                    unit.ImageData.SetBufferGroup(group);
                    unit.ResultDetails = group.InspectionDetails;

                    for (int i = 0; i < unit.GetNumInspections(); i++)
                    {
                        InspectionResult insp = unit.GetInspection(i);
                        if (insp.DataType == DataType4Byte.INT)
                        {
                            insp.Min = ConvertInt(temp, group.Results[i], (int)InspectionDataPositions.MinLimit);
                            insp.Max = ConvertInt(temp, group.Results[i], (int)InspectionDataPositions.MaxLimit);
                            insp.CurrentResultValue = ConvertInt(temp, group.Results[i], (int)InspectionDataPositions.Value);
                        }
                        else
                        {
                            insp.Min = ConvertFloat(temp, group.Results[i], (int)InspectionDataPositions.MinLimit);
                            insp.Max = ConvertFloat(temp, group.Results[i], (int)InspectionDataPositions.MaxLimit);
                            insp.CurrentResultValue = ConvertFloat(temp, group.Results[i], (int)InspectionDataPositions.Value);
                        }
                        insp.CurrentResult = (InspectionPassFail)group.Results[i][(int)InspectionDataPositions.Result];
                        insp.SendUpdates();
                    }

                });
                factory.Return(group);
            }
        }

    }
}
