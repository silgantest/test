﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilganVision.IPC
{
    public enum InspectionDataPositions
    {
        MinLimit = 0x00,
        MaxLimit = 0x04,
        Value = 0x08,
        Result = 0xC,
    }
}
