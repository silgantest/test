﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using SilganVision.Models;

namespace SilganVision.IPC
{
    public class InspectionBufferQueue
    {
        BlockingCollection<MemoryBufferGroup> groups;
        System.Threading.CancellationTokenSource cancelToken= new System.Threading.CancellationTokenSource();

        public InspectionBufferQueue()
        {
            groups = new BlockingCollection<MemoryBufferGroup>();
        }

        public void AddByteGroup(MemoryBufferGroup group)
        {
            groups.Add(group);
        }

        public MemoryBufferGroup GetNextImage()
        {

            try
            {
                return groups.Take(cancelToken.Token);

            }catch(OperationCanceledException)
            {

            }
            finally
            {
            }
            return null;
        }

        internal void Shutdown()
        {
            cancelToken.Cancel();
        }
    }
}
