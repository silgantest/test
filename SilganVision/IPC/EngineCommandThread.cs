﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO.MemoryMappedFiles;
using System.Collections.Concurrent;


namespace SilganVision.IPC
{

    public class EngineCommand
    {

        public IPCCommands.EngCommand cmd;
        public byte[] value;

        public EngineCommand(IPCCommands.EngCommand c, byte[] data) :this(c)
        {
            value = data;
        }

        public EngineCommand(IPCCommands.EngCommand c)
        {
            cmd = c;
            value = null;
        }


        public void GetCommandInBytes(byte[] buffer)
        {
            buffer[0] = (byte)cmd;
            if(value != null)
                value.CopyTo(buffer, 1);
        }
    }

    public static class EngineCommandFactory
    {
        public static void SendReloadJobCmd()
        {
            EngineCommandThread.GetInstance().AddEngineCommand(new EngineCommand(IPCCommands.EngCommand.ReloadJob, 
                Encoding.ASCII.GetBytes(SilganVision.FSI.ConfigManager.GetCurrentConfig().CurrentJob.GetName()))); 
        }
    }

    public class EngineCommandThread
    {
        BlockingCollection<EngineCommand> queue;
        System.Threading.CancellationTokenSource cancelToken = new System.Threading.CancellationTokenSource();
        Semaphore sem;
        Thread thread;
        Mutex mutex;
        volatile bool running;
        static EngineCommandThread instance;
        MemoryMappedFile mappedFile;
        protected EngineCommandThread()
        {
            queue = new BlockingCollection<EngineCommand>();
        }

        public static EngineCommandThread GetInstance()
        {
            return instance;
        }

        public void AddEngineCommand(EngineCommand cmd)
        {
            queue.Add(cmd);
        }

        public static void Initialize()
        {
            instance = new EngineCommandThread();
        }

        protected void SendCommands()
        {
            
            int bufSize = SvfLib.COMMAND_BUFFER_SIZE;
            using (var accessor = mappedFile.CreateViewAccessor(0, bufSize))
            {
                EngineCommand cmd;
                byte[] buffer = new byte[bufSize];
                //read the stream here.
                while ((cmd = queue.Take()) != null)
                {
                    try
                    {
                        mutex.WaitOne();
                        cmd.GetCommandInBytes(buffer);
                        accessor.WriteArray<byte>(0, buffer, 0, bufSize);
                        sem.Release();
                    }
                    finally
                    {
                        mutex.ReleaseMutex();
                    }
                }
            }
        }

        public void SetupBuffers()
        {
            mappedFile = MemoryMappedFile.CreateOrOpen(SvfLib.COMMAND_BUFFER, SvfLib.COMMAND_BUFFER_SIZE, MemoryMappedFileAccess.ReadWriteExecute);
           
            sem = new Semaphore(0, 1, SvfLib.SHARED_MEMORY_COMMAND_BUFFER_SEM);
            mutex = new Mutex(false, SvfLib.SHARED_MEMORY_COMMAND_BUFFER_MTX);

        }

        public void StartCommandHandler()
        {

            ThreadStart ts = SendCommands;
            thread = new Thread(ts);
            thread.Start();
        }

        public void StopCommandHandler()
        {
            cancelToken.Cancel();
            Thread.Sleep(200);
            sem.Release();
            thread.Join();
        }


    }
}
