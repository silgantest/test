﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SilganVision.Models
{
    public abstract class ReusablePool
    {
        const int GROWTH_RATE = 5;
        protected List<Reusable> available;
        protected List<Reusable> inuse;

        public ReusablePool()
        {
            available = new List<Reusable>();
            inuse = new List<Reusable>();
        }

        protected void grow()
        {
            for (int i = 0; i < GROWTH_RATE; i++)
                available.Add(newObject());
        }

        protected abstract Reusable newObject();

        public Reusable Get()
        {
            lock (available)
            {
                if (available.Count == 0)
                    grow();

                Reusable r = available[0];
                available.RemoveAt(0);
                inuse.Add(r);
                return r;
            }
        }

        public void Return(Reusable r)
        {
            lock (available)
            {
                if (inuse.Contains(r))
                {
                    inuse.Remove(r);
                }
                r.Reset();
                available.Add(r);
            }
        }
    }

}
