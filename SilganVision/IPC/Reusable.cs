﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilganVision.Models
{
    public interface Reusable
    {
        void Reset();
    }
}
