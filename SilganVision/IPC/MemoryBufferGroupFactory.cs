﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilganVision.Models
{

    public class UIImageSettings
    {
	    private int width;
        private int height;
        private int pixelSize;

        public UIImageSettings(int width, int height, int pixelSize) 
        {
            this.width = width;
            this.height = height;
            this.pixelSize = pixelSize;

        }
        public int GetWidth() { return width; }
        public int GetHeight() { return height; }
        public int GetPixelSize() { return pixelSize; }
    }


    public class MemoryBufferGroupFactory : ReusablePool
    {
        int numberOfImages;
        int numberOfInspection;
        List<UIImageSettings> imageSettings;

        public void InitializeFactory(SystemConfig config)
        {
            numberOfImages = config.GetRequiredCameras();
            numberOfInspection = config.GetNumberInspections();
            imageSettings = new List<UIImageSettings>();
            for (int i=0; i<numberOfImages; i++)
            {
                imageSettings.Add(new UIImageSettings(config.GetAppConfig().GetUIWidth(i), config.GetAppConfig().GetUIHeight(i), config.GetAppConfig().GetBytesPerPixel()));
            }

        }

        protected override Reusable newObject()
        {
            return new MemoryBufferGroup(imageSettings, numberOfInspection);
        }

        public int GetNumberOfImages()
        {
            return numberOfImages;
        }
        public int GetNumberOfInspections()
        {
            return numberOfInspection;
        }
        

    }
}
