﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO.MemoryMappedFiles;
using System.IO;
using System.Diagnostics;
using System.ServiceProcess;

using SilganVision.Models;

namespace SilganVision.IPC
{
    class SharedMemoryThread
    {
        Thread thread;
        Mutex mutex;
        MemoryBufferGroupFactory bufferGroupFactory;
        InspectionBufferQueue queue;
        volatile bool running;
        Semaphore sem;
        int numCameras;

        const string SERVICE_NAME = "nsi";
        const string ENGINE_APP_PATH = "silganengine.exe";
        const string ENGINE_PROC_NAME = "silganengine";

        public void Initialize(MemoryBufferGroupFactory factory, InspectionBufferQueue queue, int numCams)
        {
            bufferGroupFactory = factory;
            this.queue = queue;
            numCameras = numCams;

        }

        public bool CheckIntegrity(MemoryBufferGroup group)
        {
            if(group.Images[0][0] == 0 &&
                group.Images[1][0] == 0 &&
                group.Images[3][0] == 0)
            {
                return false;
            }
            return true;
        }


        protected void ReadSharedMemory()
        {
            MemoryMappedFile mappedFile =  MemoryMappedFile.OpenExisting(SvfLib.SHARED_MEMORY_IMAGES);

            using (var accessor = mappedFile.CreateViewAccessor(0, SvfLib.SHARED_MEM_BUFFER_SIZE))
            {

                //read the stream here.
                while (sem.WaitOne() && running)
                {
                    try
                    {
                        MemoryBufferGroup group = (MemoryBufferGroup)bufferGroupFactory.Get();
                        do
                        {
                            int position = 0;
                            try
                            {
                                mutex.WaitOne();
                                for (int i = 0; i < bufferGroupFactory.GetNumberOfImages(); i++)
                                {
                                    accessor.ReadArray<byte>(position, group.Images[i], 0, group.GetImageSize(i));
                                    position += group.GetImageSize(i);
                                }
                                for (int i = 0; i < 4; i++)
                                {
                                    accessor.ReadArray<byte>(position, group.CapFilters[i], 0, group.GetImageSize(4)/4);
                                }
                                for (int i = 0; i < bufferGroupFactory.GetNumberOfInspections(); i++)
                                {
                                    accessor.ReadArray<byte>(position, group.Results[i], 0, 32);
                                    position += 32;
                                }
                                for (int i = 0; i < bufferGroupFactory.GetNumberOfImages(); i++)
                                {
                                    accessor.ReadArray<byte>(position, group.InspectionDetails[i], 0, SvfLib.MAX_DETAILS_BUFFER * 4);
                                    position += SvfLib.MAX_DETAILS_BUFFER * 4;
                                }

                            }
                            finally
                            {
                                mutex.ReleaseMutex();
                            }
                        } while (!CheckIntegrity(group));
                        queue.AddByteGroup(group);

                    }
                    finally
                    {
                    }
                }
            }
        }

        public void StartReadingImages()
        {
            EngineCommandThread.Initialize();
            EngineCommandThread.GetInstance().SetupBuffers();
            KillAnyRunningEngines();
            ServiceController sc = new ServiceController(SERVICE_NAME);
            while (sc.Status != ServiceControllerStatus.Running)
                System.Threading.Thread.Sleep(100);

            sem = new Semaphore(0, 1, SvfLib.SHARED_MEMORY_IMAGES_SEM);
            mutex = new Mutex(false, SvfLib.SHARED_MEMORY_IMAGES_MTX);

            ThreadStart ts = ReadSharedMemory;
            thread = new Thread(ts);


            Process.Start(ENGINE_APP_PATH);
            sem.WaitOne();
            running = true;
            thread.Start();
            EngineCommandThread.GetInstance().StartCommandHandler();

        }

        public void KillAnyRunningEngines()
        {
            Process[] p = Process.GetProcessesByName(ENGINE_PROC_NAME);
            for (int i = 0; i < p.Length; i++)
            {
                p[0].Kill();
                Thread.Sleep(100);
            }
        }

        public void StopReadingImages()
        {
            EngineCommandThread.GetInstance().StopCommandHandler();
            running = false;
            KillAnyRunningEngines();
            Thread.Sleep(200);
            sem.Release();
            thread.Join();
        }


    }
}
