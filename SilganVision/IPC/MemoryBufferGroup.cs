﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilganVision.Models
{
    public class MemoryBufferGroup : Reusable
    {
        List<UIImageSettings> settings;

        byte[][] images;
        byte[][] results;
        byte[][] inspectiondetails;

        byte[][] capImageFilters;

        public MemoryBufferGroup(List<UIImageSettings> settings, int numInspections)
        {
            this.settings = settings;
            images = new byte[this.settings.Count][];
            inspectiondetails = new byte[this.settings.Count][];

            for (int i= 0; i< this.settings.Count; i++)
            {
                images[i] = new byte[settings[i].GetWidth() * settings[i].GetHeight() * settings[i].GetPixelSize()];
                inspectiondetails[i] = new byte[SvfLib.MAX_DETAILS_BUFFER * 4];
            }

            if (this.settings.Count == 5)  // if cap used
            {
                capImageFilters = new byte[4][];
                for (int i = 0; i < 4; i++)
                {
                    capImageFilters[i] = new byte[settings[4].GetWidth() * settings[4].GetHeight()];
                }
            }

            results = new byte[numInspections][];
            for (int i = 0; i < numInspections; i++)
            {
                results[i] = new byte[32];
            }

        }

        public int Height(int index)
        {
            return settings[index].GetHeight(); ;
        }

        public int Width(int index)
        {
            return settings[index].GetWidth(); ;
        }

      
        public int NumCams
        { 
            get 
            { 
                return settings.Count; 
            } 
        }

        public int GetImageSize(int index)
        {
            return settings[index].GetWidth() * settings[index].GetHeight() * settings[index].GetPixelSize();
        }

        public int GetLineLength(int index)
        {
            return settings[index].GetWidth() * settings[index].GetPixelSize();
        }

        public void Reset()
        {
            for(int i=0; i< settings.Count; i++)
                Array.Clear(images[i], 0, GetImageSize(i));
        }

        public byte[][] Images
        {
            get
            {
                return images;
            }
        }

        public byte[][] Results
        {
            get
            {
                return results;
            }
        }

        public byte[][] CapFilters
        {
            get
            {
                return capImageFilters;
            }
        }

        public byte[][] InspectionDetails
        {
            get
            {
                return inspectiondetails;
            }
        }
    }
}
