﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using SilganVision.ViewModels;

namespace SilganVision.Views
{
    /// <summary>
    /// Interaction logic for SelectJob.xaml
    /// </summary>
    public partial class SelectJob : Window
    {
        SelectJobViewModel vm;
        public SelectJob()
        {
            InitializeComponent();
            vm = new SelectJobViewModel(new ViewWindowFactory());
            DataContext = vm;
            vm.RequestClose += (_, __) => this.Close();

        }
    }
}
