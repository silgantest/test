﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using SilganVision.ViewModels;

namespace SilganVision.Views
{
    /// <summary>
    /// Interaction logic for CameraImageControl.xaml
    /// </summary>
    public partial class CameraImageControl : UserControl
    {
        CameraImageControlViewModel vm;
        public CameraImageControl()
        {
            InitializeComponent();
            vm = new CameraImageControlViewModel(Index);
            DataContext = vm;
            vm.PropertyChanged += Vm_PropertyChanged;
        }

        private void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Drawable"))
            {
                try
                {
                    DrawableItems.Children.Clear();

                }
                catch(Exception ex)
                {

                }
                foreach (Shape gmrtDwg in vm.Drawable)
                {
                    DrawableItems.Children.Add(gmrtDwg);
                    
                }
            }
        }

        public static readonly DependencyProperty IndexProperty =
           DependencyProperty.Register("Index", typeof(int),typeof(CameraImageControl), new PropertyMetadata(-1, OnIndexPropertyChanged));

        public int Index
        {
            get { return (int)GetValue(IndexProperty); }
            set {
                SetValue(IndexProperty, value); 
            }
        }

        private static void OnIndexPropertyChanged(
           DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            CameraImageControl cic = (CameraImageControl)sender;
            cic.vm.SetIndex((int)e.NewValue);
        }
    }

}
