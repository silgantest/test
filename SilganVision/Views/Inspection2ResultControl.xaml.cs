﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SilganVision.Views
{
    /// <summary>
    /// Interaction logic for Inspection2Results.xaml
    /// </summary>
    /// 
    using SilganVision.ViewModels;
    public partial class Inspection2ResultsControl : UserControl
    {
        Inspection2ResultsControlViewModel vm;
        public Inspection2ResultsControl()
        {
            InitializeComponent();
            vm = new Inspection2ResultsControlViewModel();
            DataContext = vm;
        }

        public static readonly DependencyProperty InspectionNumberLeftProperty =
                DependencyProperty.Register("InspectionNumberLeft", typeof(int), typeof(Inspection2ResultsControl), 
                    new PropertyMetadata(-1, OnInspectionNumberLeftPropertyChanged));
        public static readonly DependencyProperty InspectionNumberRightProperty =
                DependencyProperty.Register("InspectionNumberRight", typeof(int), typeof(Inspection2ResultsControl), 
                    new PropertyMetadata(-1, OnInspectionNumberRightPropertyChanged));
        public static readonly DependencyProperty InspectionNameProperty =
                DependencyProperty.Register("InspectionName", typeof(String), typeof(Inspection2ResultsControl),
                    new PropertyMetadata("", OnInspectionNamePropertyChanged));

        public int InspectionNumberLeft
        {
            get { return (int)GetValue(InspectionNumberLeftProperty); }
            set
            {
                SetValue(InspectionNumberLeftProperty, value);
                vm.SetInspectionNumberLeft(value);
            }
        }

        public int InspectionNumberRight
        {
            get { return (int)GetValue(InspectionNumberRightProperty); }
            set
            {
                SetValue(InspectionNumberRightProperty, value);
                vm.SetInspectionNumberRight(value);
            }
        }

        public String InspectionName
        {
            get { return (String)GetValue(InspectionNameProperty); }
            set
            {
                SetValue(InspectionNameProperty, value);
                vm.InspectionName = value;
            }
        }

        private static void OnInspectionNumberRightPropertyChanged(
           DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Inspection2ResultsControl irc = (Inspection2ResultsControl)sender;
            irc.vm.SetInspectionNumberRight((int)e.NewValue);

        }

        private static void OnInspectionNumberLeftPropertyChanged(
           DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Inspection2ResultsControl irc = (Inspection2ResultsControl)sender;
            irc.vm.SetInspectionNumberLeft((int)e.NewValue);
        }

        private static void OnInspectionNamePropertyChanged(
           DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Inspection2ResultsControl irc = (Inspection2ResultsControl)sender;
            irc.vm.InspectionName = (String)e.NewValue;

        }
    }
}