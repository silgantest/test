﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using SilganVision.ViewModels;

namespace SilganVision.Views
{
    /// <summary>
    /// Interaction logic for InspectionResultControl.xaml
    /// </summary>
    public partial class InspectionResultControl : UserControl
    {
        InspectionResultControlViewModel vm;
        public InspectionResultControl()
        {
            InitializeComponent();
            vm = new InspectionResultControlViewModel();
            DataContext = vm;
        }

        public static readonly DependencyProperty InspectionNumberProperty =
          DependencyProperty.Register("InspectionNumber", typeof(int), typeof(InspectionResultControl), new PropertyMetadata(-1, OnInspectionNumberPropertyChanged));

        public int InspectionNumber
        {
            get { return (int)GetValue(InspectionNumberProperty); }
            set
            {
                SetValue(InspectionNumberProperty, value);
                vm.SetInspectionNumber(value);
            }
        }

        private static void OnInspectionNumberPropertyChanged(
           DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            InspectionResultControl irc = (InspectionResultControl)sender;
            irc.vm.SetInspectionNumber((int)e.NewValue);
        }

        public static readonly DependencyProperty InspectionNameProperty =
                DependencyProperty.Register("InspectionName", typeof(string), typeof(InspectionResultControl), new PropertyMetadata("", OnInspectionNamePropertyChanged));

        public String InspectionName
        {
            get { return (String)GetValue(InspectionNameProperty); }
            set
            {
                SetValue(InspectionNameProperty, value);
                vm.InspectionName = value;
            }
        }

        private static void OnInspectionNamePropertyChanged(
           DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            InspectionResultControl irc = (InspectionResultControl)sender;
            irc.vm.InspectionName = (String)e.NewValue;

        }
    }
}
