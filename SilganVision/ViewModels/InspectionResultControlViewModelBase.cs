﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SilganVision.ViewModels
{
    class InspectionResultControlViewModelBase
    {
        protected ICommand configureCommand;

        public ICommand ConfigureCommand
        {
            get
            {
                if (configureCommand == null)
                {
                    configureCommand = new RelayCommand(param => this.Configure());
                }

                return configureCommand;
            }
            set
            {
                configureCommand = value;
            }
        }
        public void Configure()
        {
            MediatorFactory.GetInspectionConfigMediator().Notify(this);
        }
        
        public string InspectionName
        {
            get;
            set;
        }

    }
}
