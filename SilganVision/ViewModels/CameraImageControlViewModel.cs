﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows;

using SilganVision.Models;
using SilganVision.FSI;
using System.Windows.Shapes;

namespace SilganVision.ViewModels
{
    public class CameraImageControlViewModel : INotifyPropertyChanged, IJobUpdate
    {
        ImageDrawables jobDrawables;
        ImageDrawables inspectionDrawables;
        int index;
        public CameraImageControlViewModel(int index)
        {
            ImageData = null;
            drawingGroup = new List<Shape>();
            JobUpdateManager.AddUpdator(this);
            this.index = index;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private List<Shape> drawingGroup;

        public void SetIndex(int index)
        {
            if (index == -1)
                return;
            ImageData = LatestUnit.GetLatestUnit().ImageData.GetImage(index);
            ImageData.PropertyChanged += ImageData_PropertyChanged;
            JobUpdated();
            this.index = index;
        }

        private void ImageData_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
            inspectionDrawables = ImageDrawablesFactory.GetDrawablesFromInspections(index, LatestUnit.GetLatestUnit().ResultDetails[index]);
            Redraw();
        }

        public DisplayImageData ImageData
        {
            get;
            set;
        }

        public ImageSource NotDrawable
        {
            get
            {
                return FilterDisplay.GetFilteredImage(LatestUnit.GetLatestUnit().ImageData, index).NotDrawable;
            }
        }

        public List<Shape> Drawable
        {
            get
            {
                return drawingGroup;
            }
        }


        private Shape GetGeometry(Drawable drawable)
        {
            if (drawable is LineDrawable)
            {
                BoxDrawable b = ((LineDrawable)drawable).GetPoints(ImageData.Rect);
                Line line = new Line();
                line.X1 = b.BottomRight.X;
                line.X2 = b.TopLeft.X;
                line.Y1 = b.BottomRight.Y;
                line.Y2 = b.TopLeft.Y;
                line.Stroke = new SolidColorBrush(drawable.Color);
                return line;
            }
            else if (drawable is BoxDrawable)
            {
                System.Windows.Shapes.Rectangle rect = new Rectangle();
                rect.Width = ((BoxDrawable)drawable).Width;
                rect.Height = ((BoxDrawable)drawable).Height;
                rect.Stroke = new SolidColorBrush(drawable.Color);

                return rect;
            }
            return null;
        }

        public void JobUpdated()
        {
            jobDrawables = ImageDrawablesFactory.GetImageDrawablesFromJob(ImageData.Index);
            Redraw();
        }
        public void Redraw()
        {
            this.drawingGroup.Clear();
            for (int i = 0; i < jobDrawables.Lines.Count; i++)
                this.drawingGroup.Add(GetGeometry(jobDrawables.Lines[i]));

            for (int i = 0; i < jobDrawables.Boxes.Count; i++)
                this.drawingGroup.Add(GetGeometry(jobDrawables.Boxes[i]));
            //            for (int i = 0; i < dr.Text.Count; i++)
            //                gg.Children.Add(new FormattedText(dr.Text[i]));
            if (inspectionDrawables != null)
            {
            }
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("Drawable"));

        }

        public void AddDrawables(ImageDrawables id)
        {
            for (int i = 0; i < id.Lines.Count; i++)
                this.drawingGroup.Add(GetGeometry(id.Lines[i]));

            for (int i = 0; i < id.Boxes.Count; i++)
            {
                Shape bd = GetGeometry(id.Boxes[i]);
                this.drawingGroup.Add(bd);
                
            }
        }
    }
}
