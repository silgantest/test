﻿using System;
using System.Windows;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Windows.Media;

using SilganVision.FSI;
using SilganVision.Models;

namespace SilganVision.ViewModels
{
    class CapConfigControlViewModel: INotifyPropertyChanged, IJobUpdate
    {

        public event PropertyChangedEventHandler PropertyChanged;
        protected int adj = 1;
        protected int imageHeight;
        protected int imageWidth;
        private Dispatcher d;

        public CapConfigControlViewModel()
        {
            imageWidth = LatestUnit.GetLatestUnit().ImageData.GetImage(4).Rect.Width;
            imageHeight = LatestUnit.GetLatestUnit().ImageData.GetImage(4).Rect.Height;
            CloseCommand = new RelayCommand(param => this.Close());
            CapCornerLeftCommand = new RelayCommand(param => this.MoveCapCorner(-3 * adj));
            CapCornerRightCommand = new RelayCommand(param => this.MoveCapCorner(3 * adj));
            MidCapUpCommand = new RelayCommand(param => this.MoveMidCap(-3 * adj));
            MidCapDownCommand = new RelayCommand(param => this.MoveMidCap(3 * adj));
            BelowLipUpCommand = new RelayCommand(param => this.MoveBelowLip(-3 * adj));
            BelowLipDownCommand = new RelayCommand(param => this.MoveBelowLip(3 * adj));

            TopLRPlusCommand = new RelayCommand(param => this.MoveTopLRSens(3 * adj));
            TopLRMinusCommand = new RelayCommand(param => this.MoveTopLRSens(-3 * adj));
            TopMidPlusCommand = new RelayCommand(param => this.MoveTopMidSens(3 * adj));
            TopMidMinusCommand = new RelayCommand(param => this.MoveTopMidSens(-3 * adj));
            NeckSensPlusCommand = new RelayCommand(param => this.MoveNeckSens(3 * adj));
            NeckSensMinusCommand = new RelayCommand(param => this.MoveNeckSens(-3 * adj));
            CapColorSizePlusCommand = new RelayCommand(param => this.MoveCapColorSize(3 * adj));
            CapColorSizeMinusCommand = new RelayCommand(param => this.MoveCapColorSize(-3 * adj));
            CapPosUpCommand = new RelayCommand(param => this.MoveCapColorPos(3 * adj));
            CapPosDownCommand = new RelayCommand(param => this.MoveCapColorPos(-3 * adj));
            SportSensPlusCommand = new RelayCommand(param => this.MoveSportSens(3 * adj));
            SportSensMinusCommand = new RelayCommand(param => this.MoveSportSens(-3 * adj));
            SportPosPlusCommand = new RelayCommand(param => this.MoveSportPos(3 * adj));
            SportPosMinusCommand = new RelayCommand(param => this.MoveSportPos(-3 * adj));

            IncrementCapLRMaxCommand = new RelayCommand(param => this.MoveCapLRMax(3 * adj));
            DecrementCapLRMaxCommand = new RelayCommand(param => this.MoveCapLRMax(-3 * adj));
            IncrementCapLRMinCommand = new RelayCommand(param => this.MoveCapLRMin(3 * adj));
            DecrementCapLRMinCommand = new RelayCommand(param => this.MoveCapLRMin(-3 * adj));
            IncrementCapMidMaxCommand = new RelayCommand(param => this.MoveCapMidMax(3 * adj));
            DecrementCapMidMaxCommand = new RelayCommand(param => this.MoveCapMidMax(-3 * adj));
            IncrementCapMidMinCommand = new RelayCommand(param => this.MoveCapMidMin(3 * adj));
            DecrementCapMidMinCommand = new RelayCommand(param => this.MoveCapMidMin(-3 * adj));
            IncrementCapColorMaxCommand = new RelayCommand(param => this.MoveCapColorMax(3 * adj));
            DecrementCapColorMaxCommand = new RelayCommand(param => this.MoveCapColorMax(-3 * adj));
            IncrementCapColorMinCommand = new RelayCommand(param => this.MoveCapColorMin(3 * adj));
            DecrementCapColorMinCommand = new RelayCommand(param => this.MoveCapColorMin(-3 * adj));
            IncrementLipPatternMaxCommand = new RelayCommand(param => this.MoveLipPatternMax(3 * adj));
            DecrementLipPatternMaxCommand = new RelayCommand(param => this.MoveLipPatternMax(-3 * adj));
            IncrementLipPatternMinCommand = new RelayCommand(param => this.MoveLipPatternMin(3 * adj));
            DecrementLipPatternMinCommand = new RelayCommand(param => this.MoveLipPatternMin(-3 * adj));

            TeachPatternCommand = new RelayCommand(param => this.TeachPattern());
            TeachColorCommand = new RelayCommand(param => this.TeachColor ());
            JobUpdateManager.AddUpdator(this);
            d = Dispatcher.CurrentDispatcher;
            BitmapPalette myPalette = BitmapPalettes.Gray256;
            LeftLipPattern = new WriteableBitmap(28, 28, 96, 96, PixelFormats.Gray8, myPalette);
            RightLipPattern = new WriteableBitmap(28, 28, 96, 96, PixelFormats.Gray8, myPalette);
            TargetColor = new SolidColorBrush(Color.FromRgb(100, 100, 100));

        }

        public ICommand TopLRPlusCommand
        {
            get;
            set;
        }
        public ICommand TopLRMinusCommand
        {
            get;
            set;
        }
        public ICommand TopMidPlusCommand
        {
            get;
            set;
        }
        public ICommand TopMidMinusCommand
        {
            get;
            set;
        }
        public ICommand NeckSensPlusCommand
        {
            get;
            set;
        }
        public ICommand NeckSensMinusCommand
        {
            get;
            set;
        }

        public ICommand CapColorSizePlusCommand
        {
            get;
            set;
        }
        public ICommand CapColorSizeMinusCommand
        {
            get;
            set;
        }
        public ICommand CapPosUpCommand
        {
            get;
            set;
        }
        public ICommand CapPosDownCommand
        {
            get;
            set;
        }
        public ICommand SportSensPlusCommand
        {
            get;
            set;
        }
        public ICommand SportSensMinusCommand
        {
            get;
            set;
        }

        public ICommand SportPosPlusCommand
        {
            get;
            set;
        }
        public ICommand SportPosMinusCommand
        {
            get;
            set;
        }



        public ICommand CapCornerLeftCommand
        {
            get;
            set;
        }

        public ICommand CapCornerRightCommand
        {
            get;
            set;
        }

        public ICommand MidCapUpCommand
        {
            get;
            set;
        }

        public ICommand MidCapDownCommand
        {
            get;
            set;
        }

        public ICommand BelowLipUpCommand
        {
            get;
            set;
        }

        public ICommand BelowLipDownCommand
        {
            get;
            set;
        }

        public ICommand IncrementCapLRMaxCommand
        {
            get;
            set;
        }

        public ICommand IncrementCapLRMinCommand
        {
            get;
            set;
        }

        public ICommand DecrementCapLRMaxCommand
        {
            get;
            set;
        }

        public ICommand DecrementCapLRMinCommand
        {
            get;
            set;
        }
        
        public ICommand IncrementCapMidMaxCommand
        {
            get;
            set;
        }

        public ICommand DecrementCapMidMaxCommand
        {
            get;
            set;
        }
        public ICommand IncrementCapMidMinCommand
        {
            get;
            set;
        }

        public ICommand DecrementCapMidMinCommand
        {
            get;
            set;
        }

        public ICommand IncrementCapColorMaxCommand
        {
            get;
            set;
        }

        public ICommand DecrementCapColorMaxCommand
        {
            get;
            set;
        }
        public ICommand IncrementCapColorMinCommand
        {
            get;
            set;
        }

        public ICommand DecrementCapColorMinCommand
        {
            get;
            set;
        }

        public ICommand IncrementLipPatternMaxCommand
        {
            get;
            set;
        }

        public ICommand DecrementLipPatternMaxCommand
        {
            get;
            set;
        }
        public ICommand IncrementLipPatternMinCommand
        {
            get;
            set;
        }

        public ICommand DecrementLipPatternMinCommand
        {
            get;
            set;
        }

        

        private void MoveCapCorner(int move)
        {
            int m = ConfigManager.GetCurrentConfig().CurrentJob.GetVertXBar();
            m += move;
            if (m < 1)
                m = 1;
            else if (m >= imageWidth - 1)
                m = imageWidth - 1;
            ConfigManager.GetCurrentConfig().CurrentJob.SetVertXBar(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("CapCorner"));
        }

        private void MoveMidCap(int move)
        {
            int m = ConfigManager.GetCurrentConfig().CurrentJob.GetMidYBar();
            m += move;
            if (m < 1)
                m = 1;
            else if (m >= imageHeight - 1)
                m = imageHeight - 1;
            ConfigManager.GetCurrentConfig().CurrentJob.SetMidYBar(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("MidCap"));
        }

        private void MoveBelowLip(int move)
        {
            int m = ConfigManager.GetCurrentConfig().CurrentJob.GetNeckBarY();
            m += move;
            if (m < 1)
                m = 1;
            else if (m >= imageHeight - 1)
                m = imageHeight - 1;
            ConfigManager.GetCurrentConfig().CurrentJob.SetNeckBar(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("BelowLip"));
        }


        private void MoveTopLRSens(int move)
        {
            int m = ConfigManager.GetCurrentConfig().CurrentJob.GetCapStrength();
            m += move;
            if (m < 1)
                m = 1;
            ConfigManager.GetCurrentConfig().CurrentJob.SetCapStrength(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("TopLRSens"));
        }

        private void MoveTopMidSens(int move)
        {
            int m = ConfigManager.GetCurrentConfig().CurrentJob.GetCapStrength2();
            m += move;
            if (m < 1)
                m = 1;
            ConfigManager.GetCurrentConfig().CurrentJob.SetCapStrength2(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("TopMidSens"));
        }

        private void MoveNeckSens(int move)
        {
            int m = ConfigManager.GetCurrentConfig().CurrentJob.GetNeckStrength();
            m += move;
            if (m < 1)
                m = 1;
            ConfigManager.GetCurrentConfig().CurrentJob.SetNeckStrength(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("NeckSens"));
        }

        private void MoveSportSens(int move)
        {
            int m = ConfigManager.GetCurrentConfig().CurrentJob.GetSportSen();
            m += move;
            if (m < 1)
                m = 1;
            ConfigManager.GetCurrentConfig().CurrentJob.SetSportSen(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("SportSen"));
        }

        private void MoveSportPos(int move)
        {
            int m = ConfigManager.GetCurrentConfig().CurrentJob.GetSportPos();
            m += move;
            if (m < 1)
                m = 1;
            ConfigManager.GetCurrentConfig().CurrentJob.SetSportPos(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("SportPos"));
        }


        private void MoveCapColorSize(int move)
        {
            int m = ConfigManager.GetCurrentConfig().CurrentJob.GetCapColorSize();
            m += move;
            if (m < 1)
                m = 1;
            ConfigManager.GetCurrentConfig().CurrentJob.SetCapColorSize(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("ColorSize"));
        }


        private void MoveCapColorPos(int move)
        {
            int m = ConfigManager.GetCurrentConfig().CurrentJob.GetCapColorOffset();
            m += move;
            if (m < 1)
                m = 1;
            ConfigManager.GetCurrentConfig().CurrentJob.SetCapColorOffset(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("ColorPos"));
        }

        private void MoveCapLRMax(int move)
        {
            float m = ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(0).GetMax();
            m += move;
            if (m > 10000.0f)
                m = 10000.0f;
            else if (m < 1.0f)
                m = 1.0f;
            ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(0).SetMax(m);
            ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(1).SetMax(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("MaxLRHeight"));
        }
        private void MoveCapLRMin(int move)
        {
            float m = ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(0).GetMin();
            m += move;
            if (m > 10000.0f)
                m = 10000.0f;
            else if (m < 1.0f)
                m = 1.0f;
            ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(0).SetMin(m);
            ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(1).SetMin(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("MinLRHeight"));
        }

        private void MoveCapMidMax(int move)
        {
            float m = ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(2).GetMax();
            m += move;
            if (m > 10000.0f)
                m = 10000.0f;
            else if (m < 1.0f)
                m = 1.0f;
            ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(2).SetMax(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("MaxMidHeight"));
        }
        private void MoveCapMidMin(int move)
        {
            float m = ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(2).GetMin();
            m += move;
            if (m > 10000.0f)
                m = 10000.0f;
            else if (m < 1.0f)
                m = 1.0f;
            ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(2).SetMin(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("MinMidHeight"));
        }

        private void MoveCapColorMax(int move)
        {
            float m = ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(3).GetMax();
            m += move;
            if (m > 10000.0f)
                m = 10000.0f;
            else if (m < 1.0f)
                m = 1.0f;
            ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(3).SetMax(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("MaxColor"));
        }
        private void MoveCapColorMin(int move)
        {
            float m = ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(3).GetMin();
            m += move;
            if (m > 255.0f)
                m = 255.0f;
            else if (m < 1.0f)
                m = 1.0f;
            ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(3).SetMin(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("MinColor"));
        }


        private void MoveLipPatternMax(int move)
        {
            float m = ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(4).GetMax();
            m += move;
            if (m > 10000.0f)
                m = 10000.0f;
            else if (m < 1.0f)
                m = 1.0f;
            ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(4).SetMax(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("MaxLipPattern"));
        }

        private void MoveLipPatternMin(int move)
        {
            float m = ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(4).GetMin();
            m += move;
            if (m > 10000.0f)
                m = 10000.0f;
            else if (m < 1.0f)
                m = 1.0f;
            ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(4).SetMin(m);
            SaveJob();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("MinLipPattern"));
        }

        public ICommand CloseCommand
        {
            get;
            set;

        }

        private void Close()
        {
            MediatorFactory.GetInspectionConfigMediator().Notify(this);
        }

        public ICommand TeachPatternCommand
        {
            get;
            set;
        }

        public ICommand TeachColorCommand
        {
            get;
            set;
        }

        void TeachColor()
        {
            byte r = LatestUnit.GetLatestUnit().ResultDetails[4][SvfLib.CapResults_Red];
            byte g = LatestUnit.GetLatestUnit().ResultDetails[4][SvfLib.CapResults_Green];
            byte b = LatestUnit.GetLatestUnit().ResultDetails[4][SvfLib.CapResults_Blue];

            ConfigManager.GetCurrentConfig().CurrentJob.SetColor(r, g, b);
            SaveJob();
            TargetColor = new SolidColorBrush(Color.FromRgb(r, g, b));
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("TargetColor"));
        }

        public SolidColorBrush TargetColor
        {
            get;
            set;
        }

        void TeachPattern()
        {
            int CapLSideX = BitConverter.ToInt32(LatestUnit.GetLatestUnit().ResultDetails[4], SvfLib.CapResults_LeftSide);
            int CapLSideY = BitConverter.ToInt32(LatestUnit.GetLatestUnit().ResultDetails[4], SvfLib.CapResults_LeftSide + 4);
            int CapRSideX = BitConverter.ToInt32(LatestUnit.GetLatestUnit().ResultDetails[4], SvfLib.CapResults_RightSide);
            int CapRSideY = BitConverter.ToInt32(LatestUnit.GetLatestUnit().ResultDetails[4], SvfLib.CapResults_RightSide+4);
            int lDist = ConfigManager.GetCurrentConfig().CurrentJob.GetVertXBar() - CapLSideX;

            if (lDist >= 40)
            { 
                lDist = 40;
            }

            if (lDist <= 0)
            { 
                lDist = 0; 
            }

            ConfigManager.GetCurrentConfig().CurrentJob.SetLDist(lDist);

            DisplayImageData id = FilterDisplay.GetFilteredImage(LatestUnit.GetLatestUnit().ImageData, 4);
            LearnPattern(id, CapLSideX, CapLSideY, true);
            LearnPattern(id, CapRSideX, CapRSideY, false);

            ConfigManager.GetCurrentConfig().CurrentJob.SetTaught(true);
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("LeftLipPattern"));
                PropertyChanged(this, new PropertyChangedEventArgs("RightLipPattern"));
            }
        }

        public System.Windows.Media.Imaging.WriteableBitmap LeftLipPattern
        {
            get;
            set;
        }

        public System.Windows.Media.Imaging.WriteableBitmap RightLipPattern
        {
            get;
            set;
        }


        void LearnPattern(DisplayImageData id, int sideX, int sideY, bool left)
        {
            WriteableBitmap wb = id.NotDrawable;
            if (sideX > id.Rect.Width)
                sideX = 100;
                WriteableBitmap ct = left ? LeftLipPattern : RightLipPattern;
            

            int cnt = 0;

            int neckBarY = ConfigManager.GetCurrentConfig().CurrentJob.GetNeckBarY();

            Int32Rect rc2 = new Int32Rect(0, 0, 28, 28);

            try
            {

                Int32Rect rc;
                rc = left ? new Int32Rect(sideX - 24, neckBarY - 28, 28, 28) :
                    new Int32Rect(sideX - 4, neckBarY - 28, 28, 28);

                CopyPixelsTo(wb, rc, ct, rc2);


            }
            catch(Exception e)
            {

            }
            SavePatternCap(left, ct);
         
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("LeftLipPattern"));
                PropertyChanged(this, new PropertyChangedEventArgs("RightLipPattern"));
            }

        }

        void CopyPixelsTo(BitmapSource sourceImage, Int32Rect sourceRoi, WriteableBitmap destinationImage, Int32Rect destinationRoi)
        {
            var croppedBitmap = new CroppedBitmap(sourceImage, sourceRoi);
            int stride = croppedBitmap.PixelWidth * (croppedBitmap.Format.BitsPerPixel / 8);
            var data = new byte[stride * croppedBitmap.PixelHeight];
            croppedBitmap.CopyPixels(data, stride, 0);
            destinationImage.WritePixels(destinationRoi, data, stride, 0);
        }

        void SavePatternCap(bool left, BitmapSource image5)
        {
            string filename = left ? "capimagel.bmp" : "capimager.bmp";
            string patdir = SvfLib.SILGAN_JOBS_DIR + "\\" + ConfigManager.GetCurrentConfig().CurrentJob.GetName() + "\\pat\\";
            filename = patdir + filename;
            using (System.IO.FileStream stream5 = new System.IO.FileStream(filename, System.IO.FileMode.Create))
            {
                PngBitmapEncoder encoder5 = new PngBitmapEncoder();
                encoder5.Frames.Add(BitmapFrame.Create(image5));
                encoder5.Save(stream5);
            }
        }

        public void JobUpdated()
        {
            TargetColor = new SolidColorBrush(Color.FromRgb(
                (byte)ConfigManager.GetCurrentConfig().CurrentJob.GetColorTargR(),
                (byte)ConfigManager.GetCurrentConfig().CurrentJob.GetColorTargG(),
                (byte)ConfigManager.GetCurrentConfig().CurrentJob.GetColorTargB()));

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("BelowLip"));
                PropertyChanged(this, new PropertyChangedEventArgs("CapCorner"));
                PropertyChanged(this, new PropertyChangedEventArgs("MidCap"));
                PropertyChanged(this, new PropertyChangedEventArgs("TopLRSens"));
                PropertyChanged(this, new PropertyChangedEventArgs("TopMidSens"));
                PropertyChanged(this, new PropertyChangedEventArgs("NeckSens"));
                PropertyChanged(this, new PropertyChangedEventArgs("ColorSize"));
                PropertyChanged(this, new PropertyChangedEventArgs("ColorPos"));
                PropertyChanged(this, new PropertyChangedEventArgs("UseSportCap"));
                PropertyChanged(this, new PropertyChangedEventArgs("BWFilterChecked"));
                PropertyChanged(this, new PropertyChangedEventArgs("RedFilterChecked"));
                PropertyChanged(this, new PropertyChangedEventArgs("GreenFilterChecked"));
                PropertyChanged(this, new PropertyChangedEventArgs("BlueFilterChecked"));
                PropertyChanged(this, new PropertyChangedEventArgs("SportSens"));
                PropertyChanged(this, new PropertyChangedEventArgs("SportPos"));
                PropertyChanged(this, new PropertyChangedEventArgs("EnableLRHeight"));
                PropertyChanged(this, new PropertyChangedEventArgs("EnableMidHeight"));
                PropertyChanged(this, new PropertyChangedEventArgs("EnableColor"));
                PropertyChanged(this, new PropertyChangedEventArgs("EnableLipPattern"));

                PropertyChanged(this, new PropertyChangedEventArgs("MaxLRHeight"));
                PropertyChanged(this, new PropertyChangedEventArgs("MinLRHeight"));
                PropertyChanged(this, new PropertyChangedEventArgs("MaxMidHeight"));
                PropertyChanged(this, new PropertyChangedEventArgs("MinMidHeight"));

                PropertyChanged(this, new PropertyChangedEventArgs("MaxColor"));
                PropertyChanged(this, new PropertyChangedEventArgs("MinColor"));
                PropertyChanged(this, new PropertyChangedEventArgs("MaxLipPattern"));
                PropertyChanged(this, new PropertyChangedEventArgs("MinLipPattern"));
                PropertyChanged(this, new PropertyChangedEventArgs("LeftLipPattern"));
                PropertyChanged(this, new PropertyChangedEventArgs("RightLipPattern"));
                PropertyChanged(this, new PropertyChangedEventArgs("TargetColor"));

            }
        }

        public string MaxLRHeight
        {
            get
            {
               return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(0).GetMax().ToString();
            }
            set
            {
                int max = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(0).SetMax(max);
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(1).SetMax(max);
                SaveJob();
            }
        }

        public bool EnableLRHeight
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(0).IsEnabled() &&
                    ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(1).IsEnabled();
            }
            set
            {
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(1).Enable(value);
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(0).Enable(value);
                SaveJob();
            }
        }

        public bool EnableMidHeight
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(2).IsEnabled();
            }
            set
            {
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(2).Enable(value);
                SaveJob();
            }
        }

        public bool EnableColor
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(3).IsEnabled();
            }
            set
            {
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(3).Enable(value);
                SaveJob();
            }
        }

        public bool EnableLipPattern
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(4).IsEnabled();
            }
            set
            {
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(4).Enable(value);
                SaveJob();
            }
        }

        public string MinLRHeight
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(0).GetMin().ToString();
            }
            set
            {
                int min = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(0).SetMin(min);
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(1).SetMin(min);
                SaveJob();
            }
        }

        public string MaxMidHeight
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(2).GetMax().ToString();
            }
            set
            {
                int max = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(2).SetMax(max);
                SaveJob();
            }
        }

        public string MinMidHeight
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(2).GetMin().ToString(); 
            }
            set
            {
                int min = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(2).SetMin(min);
                SaveJob();
            }
        }

        public string MaxColor
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(3).GetMax().ToString();
            }
            set
            {
                int max = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(3).SetMax(max);
                SaveJob();
            }
        }

        public string MinColor
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(3).GetMin().ToString();
            }
            set
            {
                int min = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(3).SetMin(min);
                SaveJob();
            }
        }

        public string MaxLipPattern
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(4).GetMax().ToString();
            }
            set
            {
                int max = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(4).SetMax(max);
                SaveJob();
            }
        }

        public string MinLipPattern
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(4).GetMin().ToString();
            }
            set
            {
                int min = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.GetInspection(4).SetMin(min);
                SaveJob();
            }
        }

        public string BelowLip
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetNeckBarY().ToString();
            }
            set
            {
                int neckBarY = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.SetNeckBar(neckBarY);
                SaveJob();
            }
        }

        public string CapCorner
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetVertXBar().ToString();
            }
            set
            {
                int bar = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.SetVertXBar(bar);
                SaveJob();
            }
        }

        public string MidCap
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetMidYBar().ToString();
            }
            set
            {
                int bar = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.SetMidYBar(bar);
                SaveJob();
            }
        }

        public string TopLRSens
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetCapStrength().ToString();
            }
            set
            {
                int sens = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.SetCapStrength(sens);
                SaveJob();
            }
        }

        public string TopMidSens
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetCapStrength2().ToString();
            }
            set
            {
                int sens = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.SetCapStrength2(sens);
                SaveJob();
            }
        }

        public string NeckSens
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetNeckStrength().ToString();
            }
            set
            {
                int neck = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.SetNeckStrength(neck);
                SaveJob();
            }
        }

        public string ColorSize
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetCapColorSize().ToString();
            }
            set
            {
                int size = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.SetCapColorSize(size);
                SaveJob();
            }
        }

        public string ColorPos
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetCapColorOffset().ToString();
            }
            set
            {
                int pos = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.SetCapColorOffset(pos);
                SaveJob();
            }
        }

        public bool UseSportCap
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.SportCap();
            }
            set
            {
                ConfigManager.GetCurrentConfig().CurrentJob.SetUseSportCap(value);
                SaveJob();
            }
        }

        public bool DisplayCapFilter
        {
            get
            {
                return FilterDisplay.DisplayFilterIndexCap != -1;
            }
            set
            {
                if(value)
                {
                    FilterDisplay.SetCapFilter();
                }
                else
                {
                    FilterDisplay.DisplayFilterIndexCap = -1;
                }
                LatestUnit.GetLatestUnit().ImageData.GetImage(4).NotifyDisplayChanged();
            }
        }

        public bool BWFilterChecked
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetFilterSelPattCap() == ImageFilters.BlackAndWhite;
            }
            set
            {
                if (value)
                {
                    ConfigManager.GetCurrentConfig().CurrentJob.SetCapFilter(ImageFilters.BlackAndWhite);
                    SaveJob();
                }
            }
        }

        public bool RedFilterChecked
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetFilterSelPattCap() == ImageFilters.Red;
            }
            set
            {
                if (value)
                {
                    ConfigManager.GetCurrentConfig().CurrentJob.SetCapFilter(ImageFilters.Red);
                    SaveJob();
                }
            }
        }

        public bool GreenFilterChecked
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetFilterSelPattCap() == ImageFilters.Green;
            }
            set
            {
                if (value)
                {
                    ConfigManager.GetCurrentConfig().CurrentJob.SetCapFilter(ImageFilters.Green);
                    SaveJob();
                }
            }
        }

        public bool BlueFilterChecked
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetFilterSelPattCap() == ImageFilters.Blue;
            }
            set
            {
                if (value)
                {
                    ConfigManager.GetCurrentConfig().CurrentJob.SetCapFilter(ImageFilters.Blue);
                    SaveJob();
                }
            }
        }

        public string SportSen
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetSportSen().ToString();
            }
            set
            {
                int sens = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.SetSportSen(sens);
                SaveJob();
            }
        }
        
        public string SportPos
        {
            get
            {
                return ConfigManager.GetCurrentConfig().CurrentJob.GetSportPos().ToString();
            }
            set
            {
                int pos = int.Parse(value);
                ConfigManager.GetCurrentConfig().CurrentJob.SetSportPos(pos);
                SaveJob();
            }
        }
        private DateTime whenSaveCall;
        volatile bool runningSave;

        public void SaveJob()
        {

            if (!runningSave)
            {
                System.Diagnostics.Debug.WriteLine("Set save true");
                runningSave = true;
                lock (this)
                {
                    whenSaveCall = DateTime.Now.AddMilliseconds(500.0);
                }
                var waitTask = Task.Run(async () =>
                {
                    while (SaveWaitCondition()) await Task.Delay(100);

                    lock (this)
                    {
                        System.Diagnostics.Debug.WriteLine(string.Format("Saving {0}, {1}",
                          ConfigManager.GetCurrentConfig().CurrentJob.GetVertXBar(), whenSaveCall));
                        d.Invoke(() =>
                        {
                            ConfigManager.SaveJob();
                            System.Diagnostics.Debug.WriteLine("Set save false");
                            runningSave = false;
                        });
                    }
                });
            }
            else
            {
                lock (this)
                {
                    System.Diagnostics.Debug.WriteLine("Resetting Save");
                    whenSaveCall = DateTime.Now.AddMilliseconds(500.0);
                }
                if (!runningSave)
                    SaveJob();
            }
        }

        public bool SaveWaitCondition()
        {
            lock(this)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("Not time {0} {1}",
                           ConfigManager.GetCurrentConfig().CurrentJob.GetVertXBar(), whenSaveCall));
                return DateTime.Now <= whenSaveCall;
            }
        }
    }
}
