﻿using System;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows;

namespace SilganVision.ViewModels
{
    public class KeyCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public KeyCommand()
        {
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            HandleKeyPress((string)parameter);
        }

        protected virtual void HandleKeyPress(string val)
        {

        }
    }

    public class JobNameKeyCommand : KeyCommand
    {
        protected override void HandleKeyPress(string val)
        {
            base.HandleKeyPress(val);
            if (val.CompareTo("Backspace") == 0)
                SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName =
                SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName.Remove(SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName.Length - 1);
            else if (val.CompareTo("Clear") == 0)
                SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName = "";
            else
                SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName =
                    SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName + val;

        }
    }


}
