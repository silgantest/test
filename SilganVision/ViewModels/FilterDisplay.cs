﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SilganVision.FSI;
using SilganVision.Models;

namespace SilganVision.ViewModels
{
    public static class FilterDisplay
    {
        public static  int DisplayFilterIndexBottle = -1;
        public static int DisplayFilterIndexCap = -1;

        public static void SetCapFilter()
        {
            switch (ConfigManager.GetCurrentConfig().CurrentJob.GetFilterSelPattCap())
            {
                case ImageFilters.Red:
                    FilterDisplay.DisplayFilterIndexCap = 1;
                    break;
                case ImageFilters.Green:
                    FilterDisplay.DisplayFilterIndexCap = 2;
                    break;
                case ImageFilters.Blue:
                    FilterDisplay.DisplayFilterIndexCap = 3;
                    break;
                case ImageFilters.BlackAndWhite:
                default:
                    FilterDisplay.DisplayFilterIndexCap = 0;
                    break;
            }

        }


        public static DisplayImageData GetFilteredImage(DisplayImageDataGroup group, int index)
        {
            if (index < 4)
            {
                return group.GetImage(index);
            }
            else if (index == 4)
            {
                if (FilterDisplay.DisplayFilterIndexCap == -1)
                {
                    return group.GetImage(index);
                }
                else
                {
                    return group.GetCapFilters(FilterDisplay.DisplayFilterIndexCap);
                }
            }
            else
            {
                return null;
            }
        }

    }
}
