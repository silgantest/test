﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;

namespace SilganVision.ViewModels
{
    class NewJobNameViewModel : INotifyPropertyChanged, ICloseable
    {
        KeyBoardControlViewModel keyboardVM;
        ICommand okCommand;
        public NewJobNameViewModel()
        {
            keyboardVM = new KeyBoardControlViewModel(new JobNameKeyCommand());
            SilganVision.FSI.ConfigManager.JobSetupInfo.PropertyChanged += JobSetupInfo_PropertyChanged;
            DuplicateName = Visibility.Hidden;
            ValidName = Visibility.Hidden;
            okCommand = new RelayCommand(param => this.OK());
            JobNameChanged = false;

        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<EventArgs> RequestClose;

        public KeyBoardControlViewModel KeyboardVM
        {
            get
            {
                return keyboardVM;
            }
        }

        private void JobSetupInfo_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("JobName"));
            NameExists();
            JobNameValid();
        }

        public string JobName
        {
            get
            {
                return SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName;
            }
            set
            {
                SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName = value;
            }
        }

        public ICommand OKCommand
        {
            get
            {
                return okCommand;
            }
            set
            {
                okCommand = value;
            }
        }

        public bool JobNameChanged
        {
            get;
            set;
        }

        public void OK()
        {
            if (ValidateJobName())
            {
                JobNameChanged = true;
                if (RequestClose != null)
                    RequestClose(this, new EventArgs());
            }
        }

        private bool ValidateJobName()
        {
            return !NameExists() && JobNameValid();
        }

        private bool JobNameValid()
        {
            if(JobName.Length > 0)
                ValidName = Visibility.Hidden;
            else
                ValidName = Visibility.Visible;
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("ValidName"));

            return JobName.Length > 0;
        }

        public bool NameExists()
        {
            bool ret = SilganVision.FSI.ConfigManager.JobList.Exists(x => x.CompareTo(SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName) == 0);
            if(ret && SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName.Length > 0)
                DuplicateName = Visibility.Visible;
            else
                DuplicateName = Visibility.Hidden;

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("DuplicateName"));
            return ret;
        }

        public Visibility ValidName
        {
            get;
            set;
        }

        public Visibility DuplicateName
        {
            get;
            set;
        }
    }
}
