﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Threading.Tasks;

namespace SilganVision.ViewModels
{
    public class KeyBoardControlViewModel
    {
        private ICommand keyCommand;
        public KeyBoardControlViewModel(KeyCommand cmd)
        {
            KeyPressCommand = cmd;
        }
        public ICommand KeyPressCommand
        {
            get
            {
                return keyCommand;
            }
            set
            {
                keyCommand = value;
            }
        }
    }

    public class QWERTY
    {
        private Dictionary<string, KeyboardKey> keys;

        public QWERTY()
        {
            keys = new Dictionary<string, KeyboardKey>();

            keys.Add("1", new KeyboardKey(Key.D1, "1"));
            keys.Add("2", new KeyboardKey(Key.D2, "2"));
            keys.Add("3", new KeyboardKey(Key.D3, "3"));
            keys.Add("4", new KeyboardKey(Key.D4, "4"));
            keys.Add("5", new KeyboardKey(Key.D5, "5"));
            keys.Add("6", new KeyboardKey(Key.D6, "6"));
            keys.Add("7", new KeyboardKey(Key.D7, "7"));
            keys.Add("8", new KeyboardKey(Key.D8, "8"));
            keys.Add("9", new KeyboardKey(Key.D9, "9"));
            keys.Add("0", new KeyboardKey(Key.D0, "0"));
            keys.Add("Q", new KeyboardKey(Key.Q, "Q"));
            keys.Add("W", new KeyboardKey(Key.W, "W"));
            keys.Add("E", new KeyboardKey(Key.E, "E"));
            keys.Add("R", new KeyboardKey(Key.R, "R"));
            keys.Add("T", new KeyboardKey(Key.T, "T"));
            keys.Add("Y", new KeyboardKey(Key.Y, "Y"));
            keys.Add("U", new KeyboardKey(Key.U, "U"));
            keys.Add("I", new KeyboardKey(Key.I, "I"));
            keys.Add("O", new KeyboardKey(Key.O, "O"));
            keys.Add("P", new KeyboardKey(Key.P, "P"));
            keys.Add("A", new KeyboardKey(Key.A, "A"));
            keys.Add("S", new KeyboardKey(Key.S, "S"));
            keys.Add("D", new KeyboardKey(Key.D, "D"));
            keys.Add("F", new KeyboardKey(Key.F, "F"));
            keys.Add("G", new KeyboardKey(Key.G, "G"));
            keys.Add("H", new KeyboardKey(Key.H, "H"));
            keys.Add("J", new KeyboardKey(Key.J, "J"));
            keys.Add("K", new KeyboardKey(Key.K, "K"));
            keys.Add("L", new KeyboardKey(Key.L, "L"));
            keys.Add("Z", new KeyboardKey(Key.Z, "Z"));
            keys.Add("X", new KeyboardKey(Key.X, "X"));
            keys.Add("C", new KeyboardKey(Key.C, "C"));
            keys.Add("V", new KeyboardKey(Key.V, "V"));
            keys.Add("B", new KeyboardKey(Key.B, "B"));
            keys.Add("N", new KeyboardKey(Key.N, "N"));
            keys.Add("M", new KeyboardKey(Key.M, "M"));
            keys.Add("Backspace", new KeyboardKey(Key.Back, "Backspace"));
            keys.Add("Clear", new KeyboardKey(Key.Capital, "Clear"));
        }

        public KeyboardKey A
        {
            get
            {
                return keys["A"];
            }
        }


    }



    public class KeyboardKey
    {
        public KeyboardKey(Key k, string text)
        {
            Key = k;
            Text = text;
        }

        public Key Key
        {
            get;
            set;
        }

        public String Text
        {
            get;
            set;
        }

    }

}
