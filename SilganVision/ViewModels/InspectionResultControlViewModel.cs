﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Input;

using SilganVision.Models;


namespace SilganVision.ViewModels
{
    class InspectionResultControlViewModel : InspectionResultControlViewModelBase, INotifyPropertyChanged
    {
        protected InspectionResult insp;
        protected SolidColorBrush greenBrush;
        protected SolidColorBrush redBrush;
        protected SolidColorBrush grayBrush;

        public InspectionResultControlViewModel()
        {
            greenBrush = new SolidColorBrush(Colors.Green);
            redBrush = new SolidColorBrush(Colors.Firebrick);
            grayBrush = new SolidColorBrush(Colors.Gray);
        }

        public InspectionResult Inspection
        {
            get
            {
                return insp;
            }
        }

        public string Min
        {
            get
            {
                if (insp != null)
                    return insp.Min;
                else
                    return "";

            }
        }

        public string Max
        {
            get
            {
                if (insp != null)
                    return insp.Max;
                else
                    return "";
            }
        }

       
        public string ResultValue
        {
            get
            {
                if (insp != null)
                    return insp.CurrentResultValue;
                else
                    return "";
            }
        }

        public SolidColorBrush InspectionPassFail
        {
            get
            {
                if (insp == null)
                    return grayBrush;
                else if (insp.CurrentResult == Models.InspectionPassFail.Pass)
                    return greenBrush;
                else
                    return redBrush;
            }
        }


        public void SetInspectionNumber(int inspNumber)
        {
            insp = LatestUnit.GetLatestUnit().GetInspection(inspNumber);
            insp.InspectionUpdated += Insp_InspectionUpdated;
            Insp_InspectionUpdated();
        }

        private void Insp_InspectionUpdated()
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("ResultValue"));
                PropertyChanged(this, new PropertyChangedEventArgs("Max"));
                PropertyChanged(this, new PropertyChangedEventArgs("Min"));
                PropertyChanged(this, new PropertyChangedEventArgs("InspectionPassFail"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
