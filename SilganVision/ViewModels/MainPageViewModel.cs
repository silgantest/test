﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows;
using System.ComponentModel;
using System.Threading.Tasks;

using SilganVision.Models;

namespace SilganVision.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        private LoadedConfig cfg;
        private ICommand openNewWindow;
        private readonly IWindowFactory windowFactory;
        Visibility capVis;
        Visibility labelVis;

        public event PropertyChangedEventHandler PropertyChanged;
        protected ICommand selectJobCommand;

  
        public MainPageViewModel(IWindowFactory winFactory)
        {
            windowFactory = winFactory;
            openNewWindow = null;
            SelectJob();
            cfg = SilganVision.FSI.ConfigManager.GetCurrentConfig();
            CapVisibility = Visibility.Hidden;
            LabelVisibility = Visibility.Hidden;
            MediatorFactory.InitMediators(this);
        }

        public void SelectJob()
        {
            windowFactory.CreateSelectJobWindow();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("JobName"));
        }

        public ICommand SelectJobCommand
        {
            get
            {
                if (selectJobCommand == null)
                {
                    selectJobCommand = new RelayCommand(param => this.SelectJob());
                }

                return selectJobCommand;
            }
            set
            {
                selectJobCommand = value;
            }
        }

        public ICommand OpenNewWindow { get { return openNewWindow; } }
        public string JobName
        {
            get
            {
                return cfg.CurrentJob.GetName();
            }
        }

        public Visibility CapVisibility
        {
            get
            {
                return capVis;
            }
            set
            {
                capVis = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("CapVisibility"));
            }
        }

        public Visibility LabelVisibility
        {
            get
            {
                return labelVis;
            }
            set
            {
                labelVis = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("LabelVisibility"));
            }
        }

        public void ConfigureCap(bool config)
        {
            if (config)
                CapVisibility = Visibility.Visible;
            else
                CapVisibility = Visibility.Hidden;
        }

        public void ConfigureLabel(bool config)
        {
            if (config)
                LabelVisibility = Visibility.Visible;
            else
                LabelVisibility = Visibility.Hidden;

        }


    }

}
