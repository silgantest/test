﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.ComponentModel;
using SilganVision.Models;

namespace SilganVision.ViewModels
{
    class Inspection2ResultsControlViewModel : InspectionResultControlViewModelBase, INotifyPropertyChanged
    {
        protected InspectionResult inspl;
        protected InspectionResult inspr;
        protected SolidColorBrush greenBrush;
        protected SolidColorBrush redBrush;
        protected SolidColorBrush grayBrush;

        public Inspection2ResultsControlViewModel()
        {
            greenBrush = new SolidColorBrush(Colors.Green);
            redBrush = new SolidColorBrush(Colors.Firebrick);
            grayBrush = new SolidColorBrush(Colors.Gray);
        }

        public InspectionResult InspectionRight
        {
            get
            {
                return inspr;
            }
        }

        public InspectionResult InspectionLeft
        {
            get
            {
                return inspl;
            }
        }
        public string Min
        {
            get
            {
                if (inspr != null && inspl != null &&
                    inspr.Min != null && inspl.Min != null)
                {
                    int min1 = 0, min2 = 0;
                    int.TryParse(inspr.Min, out min1);
                    int.TryParse(inspl.Min, out min2);
                    return Math.Max(min1, min2).ToString();

                }
                else
                    return "";
            }
        }

        public string Max
        {
            get
            {
                if (inspr != null && inspl != null
                    && inspr.Max != null && inspl.Max != null)
                {
                    int max1 = 0, max2 = 0;
                    int.TryParse(inspr.Max, out max1);
                    int.TryParse(inspl.Max, out max2);
                    return Math.Min(max1, max2).ToString();

                }
                else
                    return "";
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected void Insp_InspectionUpdated()
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("ResultValue"));
                PropertyChanged(this, new PropertyChangedEventArgs("Max"));
                PropertyChanged(this, new PropertyChangedEventArgs("Min"));
                PropertyChanged(this, new PropertyChangedEventArgs("InspectionPassFail"));
            }
        }

        public void SetInspectionNumberLeft(int inspNum)
        {
            inspl = LatestUnit.GetLatestUnit().GetInspection(inspNum);
            inspl.InspectionUpdated += Insp_InspectionUpdated;
            Insp_InspectionUpdated();
        }

        public void SetInspectionNumberRight(int inspNum)
        {
            inspr = LatestUnit.GetLatestUnit().GetInspection(inspNum);
            inspr.InspectionUpdated += Insp_InspectionUpdated;
            Insp_InspectionUpdated();
        }
        
        public SolidColorBrush InspectionPassFail
        {
            get
            {
                if (inspl == null || inspr == null)
                    return grayBrush;
                else if (inspl.CurrentResult == Models.InspectionPassFail.Pass &&
                        inspr.CurrentResult == Models.InspectionPassFail.Pass)
                    return greenBrush;
                else
                    return redBrush;
            }
        }
        public string ResultRightValue
        {
            get
            {
                if (inspr != null)
                    return inspr.CurrentResultValue;
                else
                    return "";

            }
        }

        public string ResultLeftValue
        {
            get
            {
                if (inspl != null)
                    return inspl.CurrentResultValue;
                else
                    return "";

            }
        }
    }
}
