﻿using System;
using System.Windows.Input;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilganVision.ViewModels
{
    public class RelayCommand : ICommand
    {

        readonly Action<object> execute;
        readonly Predicate<object> canExecute;


        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");
            this.execute = execute;
            this.canExecute = canExecute;
        }


        public event EventHandler CanExecuteChanged
        {
            add
            {

                if (this.canExecute != null)
                    CommandManager.RequerySuggested += value;
            }

            remove
            {

                if (this.canExecute != null)
                    CommandManager.RequerySuggested -= value;
            }
        }


 
        public bool CanExecute(object parameters)
        {
            return canExecute == null ? true : canExecute(parameters);
        }


        public void Execute(object parameters)
        {
            execute(parameters);
        }
    }
}
