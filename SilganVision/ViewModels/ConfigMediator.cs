﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SilganVision.Models;

namespace SilganVision.ViewModels
{
    public interface IConfigMediator
    {
        void Notify(object sender);
    }

    public static class MediatorFactory
    {
        static IConfigMediator inspmediator;
        public static IConfigMediator GetInspectionConfigMediator()
        {
            return inspmediator;
        }
        public static void InitMediators(MainPageViewModel mpvm)
        {
            if(SilganVision.FSI.ConfigManager.GetCurrentConfig().SysConfig.GetSystemType() == SystemConfig.SystemType.SVF85)
            {
                Config85FormMediator.Initialize(mpvm);
                inspmediator = Config85FormMediator.GetInstance();
            }
        }
    }

    internal class Config85FormMediator : IConfigMediator
    {
        MainPageViewModel mainpagevm;
        static Config85FormMediator instance;
        protected Config85FormMediator(MainPageViewModel mpvm)
        {
            mainpagevm = mpvm;
        }

        public static void Initialize(MainPageViewModel mpvm)
        {
            instance = new Config85FormMediator(mpvm);
        }

        public static Config85FormMediator GetInstance()
        {
            return instance;
        }

        public void Notify(object sender)
        {
            if(sender is InspectionResultControlViewModel)
            {
                InspectionResultControlViewModel vm = (InspectionResultControlViewModel)sender;
                ConfigWindow(vm.Inspection);
            }
            else if (sender is Inspection2ResultsControlViewModel)
            {
                Inspection2ResultsControlViewModel vm = (Inspection2ResultsControlViewModel)sender;
                ConfigWindow(vm.InspectionLeft);
            }
            else if (sender is CapConfigControlViewModel)
            {
                mainpagevm.ConfigureCap(false);
            }

        }


        public void ConfigWindow(InspectionResult insp)
        {
            if(insp.Index <= 4)
            {
                mainpagevm.ConfigureCap(true);
            }
            else
            {
                mainpagevm.ConfigureLabel(false);
            }

        }
    }

}
