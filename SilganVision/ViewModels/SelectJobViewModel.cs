﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SilganVision.ViewModels
{
    class SelectJobViewModel : INotifyPropertyChanged, ICloseable
    {
        private IWindowFactory windowFactory;
        private ICommand loadJobCommand;
        private ICommand newJobCommand;
        private ICommand copyJobCommand;
        private string selectedJob;

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<EventArgs> RequestClose;

        public SelectJobViewModel(IWindowFactory winFactory)
        {
            selectedJob = "";
            windowFactory = winFactory;
            SilganVision.FSI.ConfigManager.ReloadJobList();
        }


        public string SelectedJob
        {
            get
            {
                return selectedJob;
            }
            set
            {
                selectedJob = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("IsJobSelected"));

            }
        }

        public bool IsJobSelected
        {
            get
            {
                return SelectedJob.Length > 0;
            }
        }

        public List<string> JobList
        {
            get
            {
                return SilganVision.FSI.ConfigManager.JobList;
            }
        }

        public ICommand LoadJobCommand
        {
            get
            {
                if (loadJobCommand == null)
                    loadJobCommand = new RelayCommand(param => this.LoadJob());
                return loadJobCommand;
            }
        }


       public ICommand NewJobCommand
        {
            get
            {
                if (newJobCommand == null)
                {
                    newJobCommand = new RelayCommand(param => this.NewJob());

                }
                return newJobCommand;
            }
        }

        public ICommand CopyJobCommand
        {
            get
            {
                if (copyJobCommand == null)
                    copyJobCommand = new RelayCommand(param => this.CopyJob());
                return copyJobCommand;
            }
        }

        public void CopyJob()
        {
            string jobName = windowFactory.NewJobNameWindow();
            if (jobName.Length > 0)
            {
                SilganVision.FSI.ConfigManager.NewJob(SelectedJob);
                UpdateJobs(jobName);
            }
        }
        public void NewJob()
        {
            string jobName = windowFactory.NewJobNameWindow();
            if (jobName.Length > 0)
            {
                SilganVision.FSI.ConfigManager.NewJob();
                UpdateJobs(jobName);
            }
        }


        public void LoadJob()
        {
            if (SelectedJob.Length > 0)
            {
                SilganVision.FSI.ConfigManager.LoadJob(SelectedJob);
                if (RequestClose != null)
                    RequestClose(this, new EventArgs());
            }
        }

        public void UpdateJobs(string jobName)
        {
            SilganVision.FSI.ConfigManager.ReloadJobList();
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("JobList"));
            SelectedJob = jobName;
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("SelectedJob"));
                PropertyChanged(this, new PropertyChangedEventArgs("IsJobSelected"));
            }
        }
    }
}
