﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SilganVision.Views;

namespace SilganVision.ViewModels
{
    public interface IWindowFactory
    {
        void CreateSelectJobWindow();
        string NewJobNameWindow();
    }
    public class ViewWindowFactory : IWindowFactory
    {

        #region Implementation of INewWindowFactory

        public void CreateSelectJobWindow()
        {
            SelectJob window = new SelectJob();
            window.ShowDialog();
        }

        public string NewJobNameWindow()
        {
            NewJobName njn = new NewJobName();
            njn.ShowDialog();
            if (!((NewJobNameViewModel)njn.DataContext).JobNameChanged)
                SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName = "";
            return SilganVision.FSI.ConfigManager.JobSetupInfo.TempJobName;
        }

        #endregion
    }
}
