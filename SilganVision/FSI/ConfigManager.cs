﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using SilganVision.Models;
using System.Xml;

namespace SilganVision.FSI
{
    static class ConfigManager
    {

        static LoadedConfig cfg = null;
        static JobLoader jl = null;
        static bool loaded = false;
        static JobSetupInfo jobSetupInfo;

        static List<string> jobList;

        static public LoadedConfig GetInitialConfig()
        {
            if (!loaded)
            {
                /////Load Usage Data
                //XmlDocument doc = new XmlDocument();
                //doc.Load(SvfLib.SILGAN_DATA_DIR + @"\usageConfig.xml");
                //string lastJob = doc.FirstChild.FirstChild.InnerText;

                //string str = string.Format("{0}\\{1}\\{2}.xml", SvfLib.SILGAN_DATA_DIR, lastJob, lastJob);
                jl = new JobLoader();

                cfg = new LoadedConfig(jl.LoadJob(@"DefaultJob"), SystemConfigLoader.GetConfig());
                loaded = true;
            }
            return cfg;

        }
        public static JobSetupInfo JobSetupInfo
        {
            get
            {
                if (jobSetupInfo == null)
                    jobSetupInfo = new JobSetupInfo(); 
                return jobSetupInfo;
            }
        }

        public static void ReloadJobList()
        {
            jobList = new List<string>();

            string[] files = System.IO.Directory.GetDirectories(SvfLib.SILGAN_JOBS_DIR);
            foreach (string s in files)
            {
                string s1 = s.Replace(SvfLib.SILGAN_JOBS_DIR, "").Replace("\\", "");
                jobList.Add(s1);
            }

        }

        public static List<string> JobList
        {
            get
            {

                if (jobList == null)
                    ReloadJobList();
                return jobList;
            }
        }

        public static void SaveJob()
        {
            if (jl == null)
                jl = new JobLoader();
            jl.SaveJob(GetCurrentConfig().CurrentJob);
            SilganVision.IPC.EngineCommandFactory.SendReloadJobCmd();
            JobUpdateManager.JobUpdated();
        }

        static public LoadedConfig GetCurrentConfig()
        {
            if (!loaded)
                cfg = GetInitialConfig();
            return cfg;

        }

        internal static void LoadJob(string job)
        {
            if(jl == null)
                jl = new JobLoader();
            cfg.CurrentJob = jl.LoadJob(job);
            SilganVision.IPC.EngineCommandFactory.SendReloadJobCmd();
            JobUpdateManager.JobUpdated();
        }

        internal static void NewJob()
        {
            if (jobSetupInfo.TempJobName.Trim().Length == 0)
                return;
            DirectoryCopy(SvfLib.SILGAN_DATA_DIR + "\\DefaultJob", SvfLib.SILGAN_JOBS_DIR + "\\" + jobSetupInfo.TempJobName, true);
            ReloadJobList();
            jobSetupInfo.TempJobName = "";
        }

        internal static void NewJob(string existingJobName)
        {
            if (jobSetupInfo.TempJobName.Trim().Length == 0)
                return;
            DirectoryCopy(SvfLib.JOB_DIR + "\\" + existingJobName, SvfLib.JOB_DIR + "\\" + jobSetupInfo.TempJobName, true);
            ReloadJobList();
            jobSetupInfo.TempJobName = "";
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.       
            Directory.CreateDirectory(destDirName);

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string tempPath = Path.Combine(destDirName, file.Name);
                file.CopyTo(tempPath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string tempPath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, tempPath, copySubDirs);
                }
            }
        }
    }
}
