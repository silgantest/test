﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilganVision.FSI
{
    public interface IJobUpdate
    {
        void JobUpdated();
    }

    public static class JobUpdateManager
    {
        private static List<IJobUpdate> list;

        public static void JobUpdated()
        {
            if(list == null)
                list = new List<IJobUpdate>();
            foreach (IJobUpdate update in list)
                update.JobUpdated();
        }

        public static void AddUpdator(IJobUpdate update)
        {
            if (list == null)
                list = new List<IJobUpdate>();
            list.Add(update);
        }
    }
}
