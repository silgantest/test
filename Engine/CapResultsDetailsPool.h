#include "ResultDetails.h"
#include "ReusablePool.h"

#ifndef CAP_RESULTS_DETAILS_POOL_H
#define CAP_RESULTS_DETAILS_POOL_H

class CapResultsDetailsPool : ReusablePool
{
private:
	static CapResultsDetailsPool* instance;

protected:
	CapResultsDetailsPool();

	virtual Reusable* create() override;

	void deleteObject(Reusable* obj) override;

public:
	CapResultsDetailsPool(CapResultsDetailsPool&) = delete;
	CapResultsDetailsPool& operator=(CapResultsDetailsPool& rhs) = delete;

	virtual ~CapResultsDetailsPool();

	static CapResultDetails* GetDetails();
	static void DeleteDetails(CapResultDetails* obj);
	static void Initialize();
	static void Shutdown();
};

#endif
