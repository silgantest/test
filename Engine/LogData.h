#include <iostream>
#include <fstream>
#include <mutex>

#ifndef LOG_DATA_H
#define LOG_DATA_H

class LogData
{
private:
	std::ofstream fout;
	std::mutex mtx;
	static LogData* instance;

public:

	LogData()
	{
		fout.open("c:\\out\\mainlog.txt");

	}
	virtual ~LogData()
	{
		fout.close();

	}
	static void Initialize()
	{
		instance = new LogData();
	}

	static void Shutdown()
	{
		delete instance;
	}

	static void Write(const char* str)
	{
		std::lock_guard<std::mutex> lk(instance->mtx);
		instance->fout.write(str, strlen(str));
		std::cout << str;
	}

};

#endif