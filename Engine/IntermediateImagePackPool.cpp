#include "SystemConfigLoader.h"
#include "IntermediateImagePackPool.h"

//static methods
IntermediateImagePackPool* IntermediateImagePackPool::instance = nullptr;
bool IntermediateImagePackPool::loadedApp = false;

IntermediateImagePack* IntermediateImagePackPool::GetImage(CapturedImage* image)
{
	IntermediateImagePack* b = (IntermediateImagePack*)instance->Get();
	b->Set(image->GetImageNumber(), image->GetCameraIndex());

	return b;
}

void IntermediateImagePackPool::DeleteImages(IntermediateImagePack* obj)
{
	instance->Delete(obj);
}

void IntermediateImagePackPool::Initialize()
{
	instance = new IntermediateImagePackPool();
	instance->grow();
}

void IntermediateImagePackPool::Shutdown()
{
	delete instance;
}

IntermediateImagePackPool::IntermediateImagePackPool() 
{
	sprintf_s(name, "IntermediateImagePackPool");
}

IntermediateImagePackPool::~IntermediateImagePackPool()
{
	deleteEverything();
}

void IntermediateImagePackPool::deleteObject(Reusable* obj)
{
	delete (IntermediateImagePack*)obj;
}

Reusable* IntermediateImagePackPool::create()
{
	
	SystemConfig* cfg = SystemConfigLoader::GetConfig();
	AppConfig* acfg = cfg->GetAppConfig();
	int dispHeight = acfg->GetMainDisplayImageHeight();
	int dispWidth = acfg->GetMainDisplayImageWidth();
	int scaleFactor = acfg->GetScaleFactor();
	int bytesPerPixel = acfg->GetBytesPerPixel();

	return new IntermediateImagePack(
		dispHeight, dispWidth, dispHeight / scaleFactor, dispWidth / scaleFactor, bytesPerPixel);
};

void IntermediateImagePackPool::loadApp()
{

}
