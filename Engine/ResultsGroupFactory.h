#include "ResultsGroup.h"
#include "ResultsGroupPool.h"

#ifndef RESULTS_GROUP_FACTORY_H
#define RESULTS_GROUP_FACTORY_H

class ResultsGroupFactory
{
private:
	ResultsGroupFactory();
	static ResultsGroupPool* poolInstance;

public:
	static ResultsGroup* GetResultGroup(int lastResultSent, int requiredInspections, int numCameras);
};

#endif

