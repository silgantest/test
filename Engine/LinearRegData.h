#ifndef LINEAR_REG_DATA_H
#define LINEAR_REG_DATA_H

typedef struct linearRegData {
	linearRegData() :avgx(0), avgy(0), incl(0), slop(0),
		angIncRad(0), angIncDeg(0), angSloRad(0), angSloDeg(0)
	{}
	float avgx;
	float avgy;
	float incl;
	float slop;
	float angIncRad; float angIncDeg;
	float angSloRad; float angSloDeg;
}linearRegData;

#endif