#include "InspectionResult.h"

#ifndef RANGE_RESULT_H
#define RANGE_RESULT_H

class DecimalRangeResult : public InspectionResult
{
public:
	DecimalRangeResult() {};
	virtual ~DecimalRangeResult() {};

	void SetResultLeft(int res) {};
	void SetResultRight(int res) {};
	void SetResult(int res) {};
	void Reset() { }
	virtual void Unload() {};

};

#endif //RANGE_RESULT_H