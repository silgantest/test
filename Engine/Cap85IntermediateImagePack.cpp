#include "Cap85IntermediateImagePack.h"
#include "Cap85IntermediateImagePackPool.h"
#include "ImageFileUtilities.h"

Cap85IntermediateImagePack::Cap85IntermediateImagePack(size_t height, size_t width, size_t bytesPerPixel)
{

	this->scaledHeight = height/2;
	this->scaledWidth = width/2;
	this->bytesPerPixel = bytesPerPixel;


	this->reduced = new unsigned char[this->scaledHeight * this->scaledWidth * bytesPerPixel];
	this->bw = new unsigned char[this->scaledWidth * this->scaledHeight];
	this->bluebw = new unsigned char[this->scaledWidth * this->scaledHeight];
	this->greenbw = new unsigned char[this->scaledWidth * this->scaledHeight];
	this->redbw = new unsigned char[this->scaledWidth * this->scaledHeight];
	this->filterForPattMatchDiv3 = nullptr;
}

void Cap85IntermediateImagePack::ReduceCapCam(unsigned char* Image, int mainDisWidth, int mainDisHeight)
{

	int nCam = 5;
	int pix1, pix2, pix3, pix4, pix5, pix6;
	int pix1x, pix2x, pix3x, pix4x, pix5x, pix6x;
	int resultr, resultg, resultb;

	int camWidth = mainDisHeight;
	int camHeight = mainDisWidth;

	int LineLength = camWidth * bytesPerPixel;
	int LineLength2 = (bytesPerPixel * camWidth) / 2;
	int LineLength3 = camWidth / 2;

	int startx = 0;
	int starty = 0;
	int endx = bytesPerPixel * (camWidth - 8);
	int endy = camHeight;

	int j = 0;
	int l = 0;
	int r = 0;
	int m = 0;

	for (int i = starty; i < endy - 2;)
	{
		for (int k = startx; k < endx;)
		{
			pix1 = *((Image + k + LineLength * (i)));
			pix2 = *((Image + k + 1 + LineLength * (i)));
			pix3 = *((Image + k + 2 + LineLength * (i)));
			pix4 = *((Image + k + 4 + LineLength * (i)));
			pix5 = *((Image + k + 5 + LineLength * (i)));
			pix6 = *((Image + k + 6 + LineLength * (i)));

			pix1x = *((Image + k + LineLength * (i + 1)));
			pix2x = *((Image + k + 1 + LineLength * (i + 1)));
			pix3x = *((Image + k + 2 + LineLength * (i + 1)));
			pix4x = *((Image + k + 4 + LineLength * (i + 1)));
			pix5x = *((Image + k + 5 + LineLength * (i + 1)));
			pix6x = *((Image + k + 6 + LineLength * (i + 1)));

			resultb = (pix1 + pix4 + pix1x + pix4x) / 4;
			resultg = (pix2 + pix5 + pix2x + pix5x) / 4;
			resultr = (pix3 + pix6 + pix3x + pix6x) / 4;

			*(reduced + j + LineLength2 * l) = resultb;
			*(reduced + j + 1 + LineLength2 * l) = resultg;
			*(reduced + j + 2 + LineLength2 * l) = resultr;

			*(bw + m + LineLength3 * l) = (resultr + resultg + resultb) / 3;

			//////////////////

			*(bluebw + m + LineLength3 * l) = resultb;
			*(greenbw + m + LineLength3 * l) = resultg;
			*(redbw + m + LineLength3 * l) = resultr;

			//////////////////

			k += 8; j += 4; m += 1;
		}

		i += 2; j = 0; l += 1; m = 0;
	}
}

void Cap85IntermediateImagePack::Reset()
{
	imageNumber = -1;
}

void Cap85IntermediateImagePack::Unload()
{
	Cap85IntermediateImagePackPool::DeleteImages(this);
}

void  Cap85IntermediateImagePack::SetFilterForCap(ImageFilters filter)
{
	if (filter == ImageFilters::Red) { filterForPattMatchDiv3 = redbw; }
	else if (filter == ImageFilters::Green) { filterForPattMatchDiv3 = greenbw; }
	else if (filter == ImageFilters::Blue) { filterForPattMatchDiv3 = bluebw; }
	else { filterForPattMatchDiv3 = bw; }
}

void Cap85IntermediateImagePack::Set(int imNum)
{
	imageNumber = imNum;
}
