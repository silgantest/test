#include "Status.h"
#include "Mode.h"
#include <mutex>

#ifndef MODE_STATE_H
#define MODE_STATE_H

class ModeState
{
private:
	OnlineMode* online;
	OfflineMode* offline;
	Mode* current;
	ModeState();
	virtual ~ModeState();
	static ModeState* instance;
	bool OKToTrigger;
	std::condition_variable cvTrigger;
	std::mutex triggerMutex;


public:
	static void Initialize();
	static void Shutdown(); 
	static Status* GetCurrentStatus();
	static void PauseTrigger();
	static void ResumeTrigger();
	static void GoOnline();
	static void GoOffline();
	static bool WaitForOKtoTrigger(int mill);

};

#endif