#include "ReusablePool.h"
#include "LabelIntegrityMethod.h"

#ifndef CAP_85_INTERMEDIATE_IMAGE_PACK_H
#define CAP_85_INTERMEDIATE_IMAGE_PACK_H

/// TODO: Refactor with IntermediateImagePack 

class Cap85IntermediateImagePack : public Reusable
{
public:
	int imageNumber;
	size_t scaledHeight;
	size_t scaledWidth;
	size_t bytesPerPixel;

	unsigned char* reduced;
	unsigned char* bw;
	unsigned char* bluebw;
	unsigned char* greenbw;
	unsigned char* redbw;
	unsigned char* filterForPattMatchDiv3;

	Cap85IntermediateImagePack(size_t height, size_t width, size_t bytesPerPixel);

	void ReduceCapCam(unsigned char* Image, int mainDisWidth, int mainDisHeight);
	void SetFilterForCap(ImageFilters method);
	void Set(int imNum);

	virtual void Reset() override;
	virtual void Unload() override;
};

#endif