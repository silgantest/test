#include "ResultsGroupFactory.h"
#include "SystemConfigLoader.h"

ResultsGroupPool* ResultsGroupFactory::poolInstance = nullptr;

ResultsGroup* ResultsGroupFactory::GetResultGroup(int lastResultSent, int requiredInspections, int numCameras)
{
	if (!poolInstance)
	{
		if (SystemConfigLoader::GetConfig()->GetSystemType() == SystemConfig::SystemType::SVF85)
		{
			poolInstance = Results85GroupPool::GetPool();
		}
	}
	if(poolInstance)
		return poolInstance->GetResultGroup(lastResultSent, requiredInspections, numCameras);
	return nullptr;
}
