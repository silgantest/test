#include <vector>
#include <memory>
#include "Camera.h"

#ifndef CAMERA_FACTORY_H
#define CAMERA_FACTORY_H


class CameraFactory
{
public:
	CameraFactory();
	virtual ~CameraFactory();

	static std::list<Camera*>* GetCameras();
};

#endif