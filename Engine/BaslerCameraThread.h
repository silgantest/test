#include "BaslerCamera.h"
#include "CounterLocks.h"

#ifndef BASLER_CAMERA_THREAD_H
#define BASLER_CAMERA_THREAD_H

class BaslerCameraThread
{
public:
	BaslerCameraThread() = default;
	~BaslerCameraThread() = default;
	BaslerCameraThread& operator=(BaslerCameraThread&) = delete;
	BaslerCameraThread(BaslerCameraThread&) = delete;

	void operator()(BaslerCamera* cam, ImageProcessor* processor, CounterLocks* lk) const;

};

#endif

