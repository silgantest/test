#include "PLCcomm.h"

#ifndef DUMMY_PLC_COMM_H
#define DUMMY_PLC_COMM_H

class DummyPLCcomm : virtual public PLCcomm
{
public:
	DummyPLCcomm() {};
	virtual ~DummyPLCcomm() {};

	virtual bool ChangeSetting()
	{
		return true;
	}
	virtual bool SendPassFail(bool pass)
	{
		return true;
	}

	virtual bool SendPassFail(char* code)
	{
		return true;
	}

	virtual bool PauseTrigger()
	{
		
		return true;
	}

	virtual bool ResumeTrigger()
	{

		return true;
	}


};

#endif //DUMMY_PLC_COMM_H
