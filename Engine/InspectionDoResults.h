#include "ResultsGroup.h"
#include "JobLoader.h"
#include "SystemConfigLoader.h"
#include "IntermediateImagePack.h"
#include "Inspection85Results.h"
#include "BottleResultJobSettings.h"

#ifndef INSPECTION_DO_RESULTS
#define INSPECTION_DO_RESULTS


class InspectionDoResults
{
public:

	virtual void DoResults(ResultsGroup* group) = 0;
	virtual void UpdateJob(Job* newJob) = 0;

};

class Bottle84DoResults : public InspectionDoResults
{
private:
	std::mutex mtx;

protected:
	BottleResultJobSettings* jobSettings;
	AppConfig* config;
	Results* results;
	int camLeft;
	TwoPoint labelBox;
	int labelPosY;
	int resBest;

	//No Label parameters
	int xNoLbl[BOTTLE_CAMS];
	int yNoLbl[BOTTLE_CAMS];
	int wNoLbl[BOTTLE_CAMS];
	int hNoLbl[BOTTLE_CAMS];
	int	avgNoLabel[BOTTLE_CAMS];
	int	bottleXspliceIni[BOTTLE_CAMS];
	int	bottleYspliceIni[BOTTLE_CAMS];
	int	minSpliceAvg[BOTTLE_CAMS];
	int varianceSplice[BOTTLE_CAMS];
	int	avgPixInRow[1000];
	int	yposSplice[BOTTLE_CAMS];
	int sdeviatiSplice[BOTTLE_CAMS];
	int shiftedMidd[BOTTLE_CAMS];
	int shiftedMiddWPoints[BOTTLE_CAMS];
	int stitchX[BOTTLE_CAMS];
	int stitchY[BOTTLE_CAMS];
	int xAvoidIni[BOTTLE_CAMS];
	int xAvoidEnd[BOTTLE_CAMS];
	TwoPoint NoLbl[BOTTLE_CAMS];
	SinglePoint	ledgePoint[50];

	int ScaleSize;

	int	outh0s, outh1s, outh2s, outh3s, outh4s;
	int	outh5s, outh6s, outh7s;
	int outh1ms, outh2ms, outh3ms, outh4ms;
	int	outh5ms, outh6ms, outh7ms;
	int	outh0, outh1, outh2, outh3;
	int	outh4, outh5, outh6, outh7;
	int	outh1m, outh2m, outh3m, outh4m;
	int	outh5m, outh6m, outh7m;

	unsigned char* im_match3;
	unsigned char* im_mat3;

	void calcTearCoordinates(Inspection85Results* specGroup);
	int Stitch4ImgX(Inspection85Results* specGroup, int camPatt, TwoPoint pattPos,
		bool takecam1, bool takecam2, bool takecam3, bool takecam4, int fact);

	unsigned char* imgStitchedLigh;
	unsigned char* imgStitchedDark;
	unsigned char* imgStitchedEdg;

	unsigned char* imgStitchedFilLigh;
	unsigned char* imgStitchedFilDark;
	unsigned char* imgStitchedFilEdge;

	unsigned char* imgStitchedFilFinal;
	unsigned char* imgStitchedBW;

public:
	Bottle84DoResults(AppConfig* config);
	virtual ~Bottle84DoResults();
	virtual void DoResults(ResultsGroup* group) override;
	void DoLipton(Inspection85Results* specGroup);
	float Result9(Inspection85Results* specGroup);
	void Stitch(int camTocheck, Inspection85Results* specGroup, TwoPoint labelBox);
	void DoWolverine(int cam, Inspection85Results* specGroup);
	void DoNotWolverine();
	TwoPoint CheckLabelNatural(IntermediateImagePack* pack, int fact);
	TwoPoint FindLedge3(unsigned char* image, TwoPoint Bolt, int cam);
	TwoPoint FindPattern3(unsigned char* im_srcIn);
	void CopyStitchedImages();

	void Result11();
	void SetAll();
	void CreateResultObj();
	void SendEverywhere();
	void Reset();

	void findSplice(int nCam, int fact,
		int xSplice, int ySplice, int wSplice,
		int hSplice, int hsmSplice, IntermediateImagePack* pack);
	void SetupMins();
	virtual void UpdateJob(Job* newJob) override;

};

class BottleCap85DoResults : public Bottle84DoResults
{
public:
	BottleCap85DoResults(AppConfig* config);
	virtual void DoResults(ResultsGroup* group) override;

};

class Cap56DoResults : public InspectionDoResults
{

public:
	void DoResults(ResultsGroup* group) override
	{

	}
};

class DoResultsFactory
{
private: 

	DoResultsFactory();
public:
	static InspectionDoResults* GetDoResults(AppConfig* config)
	{

		//TODO:
		//Load based on Application Type
		return new BottleCap85DoResults(config);
	}

};

#endif 