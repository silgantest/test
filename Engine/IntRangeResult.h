#include "InspectionResult.h"

#ifndef INT_RANGE_RESULT
#define INT_RANGE_RESULT

class IntRangeResult : public InspectionResult
{
private:
	int x;

public:
	IntRangeResult();
	virtual ~IntRangeResult();


	void SetResult(int res);

	void Reset();
	virtual void Unload();

};

#endif //FLOAT_RANGE_RESULT