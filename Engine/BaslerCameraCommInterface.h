#include <pylon/PylonIncludes.h>
#include <pylon/gige/BaslerGigEInstantCamera.h>
#include <pylon/usb/BaslerUsbInstantCamera.h>

#include <string>

#ifndef BASLER_CAMERA_COMM_INTERFACE_H
#define BASLER_CAMERA_COMM_INTERFACE_H

using namespace std;
using namespace Pylon;

class CameraCommInterface
{
protected:
	CameraCommInterface() {}
public:
	virtual ~CameraCommInterface() {}
	virtual IPylonDevice* GetDeviceInfo() = 0;

};

class BaslerInterface : public virtual CameraCommInterface
{
public:

	virtual void BusSpecific(CDeviceInfo bdi)
	{

	}

	virtual Pylon::String_t GetDeviceClass() = 0;

	virtual IPylonDevice* GetDeviceInfo() {

		CTlFactory& TlFactory = CTlFactory::GetInstance();
		ITransportLayer *pTI = TlFactory.CreateTl(GetDeviceClass());
		CBaslerGigEDeviceInfo bdi(pTI->CreateDeviceInfo());
		Pylon::String_t s = bdi.GetSerialNumber();
		// Don't configure
		BusSpecific(bdi);
		return pTI->CreateDevice(bdi);
	}
};

class GigEBaslerInterface : public BaslerInterface
{
private:
	const char* ipAddress;
public:

	GigEBaslerInterface(const char* ip) 
	{
		ipAddress = ip;
	}
	virtual ~GigEBaslerInterface() {}
	
	virtual void BusSpecific(CDeviceInfo bdi)
	{
		((CBaslerGigEDeviceInfo) bdi).SetIpAddress(ipAddress);
	}

	virtual Pylon::String_t GetDeviceClass()
	{
		return CBaslerGigEInstantCamera::DeviceClass();
	}
};

class UsbBaslerInterface : public BaslerInterface
{
public:
	UsbBaslerInterface() {}
	virtual ~UsbBaslerInterface() {}

	virtual void BusSpecific(CDeviceInfo bdi)
	{
	//Downcast to CBaslerUsbDeviceInfo 
	}

	virtual Pylon::String_t GetDeviceClass()
	{
		return CBaslerUsbInstantCamera::DeviceClass();
	}


};

#endif //CAMERA_COMM_INTERFACE_H