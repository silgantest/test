#include "Inspector.h"
#include "Point.h"
#include "AppConfig.h"
#include "DoUserCommand.h"
#include "IntRangeResult.h"
#include "LinearRegData.h"
#include "IntermediateImagePack.h"
#include "Patterns.h"
#include <deque>

#ifndef BOTTLE_INSPECTOR_H
#define BOTTLE_INSPECTOR_H



class BottleInspector : public Inspector 
{
public:
	BottleInspector(int camIndex);
	virtual ~BottleInspector();

	virtual void Inspect(CapturedImage* capImg, ResultsHandler* handler) override;
	virtual void ResetForJob() override;
	virtual void SetAppConfig(AppConfig* config) override;

private:

	void edgeImgCreation(IntermediateImagePack* pack, int fact);
	void edgeImgBin(IntermediateImagePack* pack, int fact);
	TwoPoint getLabelPos(IntermediateImagePack* pack);
	void findBottleInPic(IntermediateImagePack* pack, int fact);
	TwoPoint findPatternX1(unsigned char* im_srcIn);
	SpecialPoint findPattA1(unsigned char* imgIn, unsigned char* imgPatt, int factor, int pattTyp);
	SpecialPoint findPattHigherRes(unsigned char* imgIn, unsigned char* imgPatt, int factor, SpecialPoint refPt);
	TwoPoint findLabelEdge_Method5(unsigned char* imgIn, int fact, int limYt, int limYb, int limX, int limW);
	SpecialPoint findLabelEdge_b(unsigned char* imgIn, int factor, SpecialPoint refPt);
	SpecialPoint findWhiteBand(IntermediateImagePack* pack, unsigned char* imgIn, unsigned char* imgPatt, int factor, SpecialPoint refPt);
	void createHistAndBinarize(IntermediateImagePack* pack, unsigned char* imgIn1, int tX, int tY, int tH, int bX, int bY, int bH, int W, int fact);
	void findShiftedEdges7(IntermediateImagePack* pack, int fact);
	SpecialPoint checkNoLabel1(int fac, unsigned char* imgIn, int x0, int y0, int x1, int h0, int w1, int h1);
	void composeWaistInspection(IntermediateImagePack* pack);
	void verAxisPtsRemOutliers(std::vector<SinglePoint>& v);
	void InitializeSinglePointCoordinates(int startY, int endY, int defualtX, int* outSinglePointCoordinates, int& outSize);
	
	int nCam;

	//App
	int FullImageScaledRotatedWidth;
	int FullImageScaledRotatedHeight;
	int CroppedImageYOffset;
	int mainLineLength;
	int mainHeight;
	int croppedHeight;
	int croppedWidth;
	int sizepattBy12;
	int sizepattBy6;
	int sizepattBy3_2W;
	int sizepattBy3_2H;
	int MainDisplayImageHeight;
	int MainDisplayImageWidth;
	int MainCameraDisplayYOffset;
	int MainCameraDisplayXOffset;

//job
	int sensEdge;
	ImageFilters filterPattern;
	int camHeight;
	int camWidth;
	bool waistAvailable;
	bool findMiddles;
	LabelIntegrityMethod method;
	int zerobot;
	Job::EdgeTransition edgeTransition;
	int heigMet3;
	int widtMet3;
	int ytopLimPatt;
	int ybotLimPatt;
	int senTaught;
	int boxWt;
	int boxHt;
	int boxYt;
	int boxHb;
	int boxYb;
	int shiftTopLim;
	int shiftBotLim;
	int widthMet5;
	int heigMet5;
	int bottleStyle;
	int senMidd;
	int midImg;
	int taughtMiddleRefY;
	ImageFilters filterSelShifted;
	int shiftedLabSen;
	int shiftWidth;
	int shiftHorEndLim;
	int shiftHorIniLim;
	int edgeLabSen;
	int waistVerticalSearchLength;
	int waistHorizontalSearchLength;
	int waistEdgeThreshold;
	int waistMeasurementTopBound;
	int waistMeasurementBottomBound;
	int waistMeasurementLeftBound;
	int waistMeasurementRightBound;
	int waistMeasurementUpperTopBound;
	int waistMeasurementUpperBottomBound;
	int waistMeasurementUpperLeftBound;
	int waistMeasurementUpperRightBound;

	//Local
	int bottleXL;
	int bottleXR;
	int bottleYL;
	int bottleYR;
	int xMidd;
	int bxMethod5_Xt;
	int bxMethod5_Xb;
	int histMeth5[260];
	int xShift;
	int yShift;
	int hShift;
	std::vector<SinglePoint> xLShiftPts;
	std::vector<SinglePoint> xRShiftPts;
	int qtyShiftLeft;
	int qtyShiftRigh;
	float inclinL;
	float angIncDegL;
	float inclinR;
	float angIncDegR;
	float xLBorder;
	float xRBorder;
	float middlShiftWPoints;
	int* coordinatesOfContainerLeftTopEdgePointsRaw;
	int* coordinatesOfContainerLeftTopEdgePointsFilled;

	int* coordinatesOfContainerLeftWaistEdgePointsRaw;
	int* coordinatesOfContainerLeftWaistEdgePointsFilled;
	int vectorSize;
	std::deque<SinglePoint>* coordQueue;

	int middlShift;
	float resultwaist;

	TwoPoint labelPos;
	SpecialPoint pattLeftPosBy6;
	SpecialPoint pattLeftPosBy3;
	SpecialPoint pattLeftPosBy6_method6_1;
	SpecialPoint pattLeftPosBy3_method6_1;
	SpecialPoint pattLeftPosBy3_method6;
	SpecialPoint pattLeftPosBy6_method6;
	SpecialPoint pattLeftPosBy3_method6_num3;
	SpecialPoint pattLeftPosBy6_method6_num3;

	//TODO
	//Needs Review
	bool wehaveTempX;

	//Saved patterns
	int sizeWaistArray;
	ImagePattern* bytePattBy6_method6_1;
	ImagePattern* bytePattBy3_method6_1;
	ImagePattern* bytePattBy3;
	ImagePattern* bytePattBy6_method6;
	ImagePattern* bytePattBy3_method6;
	ImagePattern* bytePattBy6_method6_num3;
	ImagePattern* pattern2;


};

#endif //BOTTLE_INSPECTOR_H