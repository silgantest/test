#include "InspectionResult.h"
#include "point.h"
#include "IntermediateImagePackPool.h"

#ifndef LABEL_RESULTS_H
#define LABEL_RESULTS_H

class LabelResults : public InspectionResult
{
private:
	int nCam;
	TwoPoint labelPos;
	float resultwaist;
	float angIncDegL;
	float angIncDegR;
	int xMidd;
	int middlShift;
	int middlShiftWPoints;
	SpecialPoint pattLeftPosBy6;

public:
	LabelResults();
	virtual ~LabelResults();

	int GetCam();
	TwoPoint GetLabelPos();
	float GetWaistResult();
	int GetXMiddle();
	int GetMiddleShift();
	int GetMiddleShiftWPoints();
	float GetAngIncDegL();
	float GetAngIncDegR();
	SpecialPoint GetPosBy6();

	void SetCam(int nCam);
	void SetLabelPos(TwoPoint pos);
	void SetWaistResult(float resultwaist);
	void SetXMiddle(int xMidd);
	void SetMiddleShift(int middlShift);
	void SetMiddleShiftWPoints(int middlShiftWPoints);
	void SetAngIncDeg(float angIncDegL, float angIncDegR);
	void SetPosBy6(SpecialPoint pattLeftPosBy6);

	
	void Reset();
	virtual void Unload();

};

#endif