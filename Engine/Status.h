#include <string.h>
#include "resultsstruct.h"

#ifndef STATUS_H
#define STATUS_H

#define CIRCULAR_BUFFER_SIZE 64

enum OperationMode
{
	Online = 1,
	Offline = 2,
};

class Status
{
private:
	OperationMode mode;
	int inspected;
	int rejected;
	int ReasonForFailure[MAX_INSPECTIONS];
	bool Trends [MAX_INSPECTIONS][CIRCULAR_BUFFER_SIZE];

	int currentIndices[MAX_INSPECTIONS];
	static Status* currentStatus;
	Status();
public:

	virtual ~Status();
	static void Shutdown();
	static void Initialize();
	static Status* GetStatus();
	void UpdateFromResultSet(int index, bool pass);
	void UpdateFromResultSet(Results* results);
	void GetStatusArray(unsigned char* buffer);
};

#endif