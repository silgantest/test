#include "SimCamera.h"
#include "CounterLocks.h"

#ifndef SIM_CAMERA_THREAD_H
#define SIM_CAMERA_THREAD_H

class SimCameraThread
{
public:
	SimCameraThread() = default;
	~SimCameraThread() = default;
	SimCameraThread& operator=(SimCameraThread&) = delete;
	SimCameraThread(SimCameraThread&) = delete;

	void operator()(SimCamera* cam, CounterLocks* lk) const;
};

#endif