#include "InspectionTrigger.h"

InspectionTrigger InspectionTrigger::instance;
int InspectionTrigger::mask = 0xFF;

InspectionTrigger::InspectionTrigger():lastCount(255)
{

}

void InspectionTrigger::Wait(unsigned int count)
{
	std::unique_lock<std::mutex> lk(instance.inspMtx);
	instance.inspectionsComplete.wait(lk, [&]
		{
			bool ret = (instance.lastCount + 2 & mask) == count ||
				(instance.lastCount + 1 & mask) == count;;

			return (instance.lastCount + 2 & mask) == count ||
				(instance.lastCount + 1 & mask) == count;
		});
}
void InspectionTrigger::Notify()
{
	instance.inspectionsComplete.notify_all();
}

void InspectionTrigger::SetLastCount(int count)
{
	std::lock_guard<std::mutex> lk(instance.inspMtx);
	instance.lastCount = count;
}