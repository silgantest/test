#include "SystemConfigLoader.h"
#include "CameraFactory.h"
#include "Camera.h"
#include "ImageProcessor.h"
#include "Inspector.h"
#include "Patterns.h"

#pragma once

class SvfEngineRunner
{
public:
	atomic<bool> running = false;
	SystemConfig* config;
	Patterns* patt;
	ResultsHandler* resultsHandler;
	std::list<ImageProcessor*>* ips;
	std::list<Camera*>* cams;

	void Run();

	void InitObjectPools();
	void CleanUpObjectPools();

	void InitOther();
	void InitCams();

	void writefile(std::list<Camera*>* cams, std::list<ImageProcessor*>* ips);
	void clearStats(std::list<Camera*>* cams, std::list<ImageProcessor*>* ips);
	void setupcam(ExpectedCameraConfig* excam, Camera* disccam, ResultsHandler* resultsHandler, ImageProcessor*& ip, Patterns* patt);
	Camera* setupsimcam(ExpectedCameraConfig* excam, ResultsHandler* resultsHandler, Patterns* patt);


};

