#include "ResultsGroupBuffer.h"

ResultsGroupBuffer::ResultsGroupBuffer() : group(nullptr), hasData(false)
{

}

void ResultsGroupBuffer::AddGroup(ResultsGroup* grp)
{
	std::lock_guard<std::mutex> lk(mtx);
	group = grp;
	hasData = true;
	var.notify_one();
}

ResultsGroup* ResultsGroupBuffer::GetGroup()
{
	std::lock_guard<std::mutex> lk(mtx);
	ResultsGroup* res = group;
	group = nullptr;
	hasData = false;
	return res;
}

void ResultsGroupBuffer::WaitForData()
{
	std::unique_lock<std::mutex> l(cdMtx);
	var.wait(l, [&]{return hasData; });
}

void ResultsGroupBuffer::Shutdown()
{
	hasData = true;
	var.notify_one();

}