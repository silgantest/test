#include <windows.h>
#include <algorithm>
#include <memory>

///PLATFORM SPECIFIC

#ifndef IMAGE_FILE_UTILITIES_H
#define IMAGE_FILE_UTILITIES_H


class ImageFileUtilities
{
public:
	static unsigned char* ReadBMP(const char* filename, bool gray, int* width, int* height, int* size);
	static void  SaveBitmapToFile(unsigned char* bits, int lWidth, int lHeight, const char* cFileName);
	static void  SaveGrayBitmapToFile(unsigned char* bits, int lWidth, int lHeight, const char* cFileName);
	static void  SaveAlphaBitmapToFile(unsigned char* bits, int lWidth, int lHeight, const char* cFileName);

	static void Reduce(unsigned char* bits, int lWidth, int lHeight);
	static void ExportBytes(BYTE* image, int width, int height, int depth);
private:
	static std::unique_ptr<BYTE[]> CreateNewBuffer(unsigned long& padding, unsigned char* pmatrix, const int& width, const int& height, bool gray);

};

#endif