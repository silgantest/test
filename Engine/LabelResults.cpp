#include "LabelResults.h"
#include "LabelResultsPool.h"

LabelResults::LabelResults()
{
	
}

LabelResults::~LabelResults()
{

}

int LabelResults::GetCam()
{
	return this->nCam;
}

TwoPoint LabelResults::GetLabelPos()
{
	return this->labelPos;
}

float LabelResults::GetWaistResult()
{
	return this->resultwaist;
}

int LabelResults::GetXMiddle()
{
	return this->xMidd;
}

int LabelResults::GetMiddleShift()
{
	return this->middlShift;
}

int LabelResults::GetMiddleShiftWPoints()
{
	return this->middlShiftWPoints;
}

float LabelResults::GetAngIncDegL()
{
	return this->angIncDegL;
}

float LabelResults::GetAngIncDegR()
{
	return this->angIncDegR;
}

SpecialPoint LabelResults::GetPosBy6()
{
	return this->pattLeftPosBy6;
}

void LabelResults::SetCam(int nCam)
{
	this->nCam = nCam;
}

void LabelResults::SetLabelPos(TwoPoint pos)
{
	this->labelPos = pos;
}

void LabelResults::SetWaistResult(float resultwaist)
{
	this->resultwaist = resultwaist;
}
void LabelResults::SetXMiddle(int xMidd)
{
	this->xMidd = xMidd;
}

void LabelResults::SetMiddleShift(int middlShift)
{
	this->middlShift = middlShift;
}

void LabelResults::SetMiddleShiftWPoints(int middlShiftWPoints)
{
	this->middlShiftWPoints = middlShiftWPoints;
}

void LabelResults::SetAngIncDeg(float angIncDegL, float angIncDegR)
{
	this->angIncDegL = angIncDegL;
	this->angIncDegR = angIncDegR;
}

void LabelResults::SetPosBy6(SpecialPoint pattLeftPosBy6)
{
	this->pattLeftPosBy6 = pattLeftPosBy6;
}

void LabelResults::Reset()
{
	nCam = 0;
	labelPos.x = labelPos.x1 = labelPos.y = labelPos.y1 = 0;
	resultwaist = angIncDegL = angIncDegR = 0.0f;
	xMidd = middlShift = middlShiftWPoints = 0;
	pattLeftPosBy6.c = pattLeftPosBy6.x = pattLeftPosBy6.y = 0;
}

void LabelResults::Unload()
{
	LabelResultsPool::DeleteResultObj(this);
}
