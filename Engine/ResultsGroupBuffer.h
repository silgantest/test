#include "ResultsGroup.h"

#ifndef  RESULT_GROUP_BUFFER 
#define RESULT_GROUP_BUFFER 


class ResultsGroupBuffer
{
private:
//Keep exactly one buffer.  If there is more than one... we are too slow

	ResultsGroup* group;
	std::mutex mtx;

	std::condition_variable var;
	std::mutex cdMtx;
	bool hasData;

public:
	ResultsGroupBuffer();
	void AddGroup(ResultsGroup* grp);
	ResultsGroup* GetGroup();
	void WaitForData();
	void Shutdown();
};

#endif //  RESULT_GROUP_BUFFER 
