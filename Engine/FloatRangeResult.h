
#ifndef FLOAT_RANGE_RESULT
#define FLOAT_RANGE_RESULT

#include "InspectionResult.h"

class FloatRangeResult : public InspectionResult
{
private:
	float x;
	float xLeft;
	float xRight;
	int index;

public:
	FloatRangeResult();
	virtual ~FloatRangeResult();

	void SetResultLeft(float res);
	void SetResultRight(float res);
	void SetResult(float res);
	float GetResult();
	float GetLeftResult();
	float GetRightResult();
	int GetIndex();

	void Reset();
	virtual void Unload();

};

#endif //FLOAT_RANGE_RESULT