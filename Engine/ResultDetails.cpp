#include "ResultDetails.h"
#include "CapResultsDetailsPool.h"
#include "BottleResultsDetailsPool.h"
#include "CapResultDetailsIndices.h"

void CapResultDetails::AddColorInfo(int r, int g, int b)
{
	Values[CapResults_Red] = r;
	Values[CapResults_Green] = g;
	Values[CapResults_Blue] = b;
}

void CapResultDetails::AddCapLeftSide(Point side)
{
	Values[CapResults_LeftSide] = side.x;
	Values[CapResults_LeftSide + 1] = side.y;
}

void CapResultDetails::AddCapRightSide(Point side)
{
	Values[CapResults_RightSide] = side.x;
	Values[CapResults_RightSide + 1] = side.y;
}

void CapResultDetails::AddCapLeftTop(Point top)
{
	Values[CapResults_LeftTop] = top.x;
	Values[CapResults_LeftTop + 1] = top.y;
}

void CapResultDetails::AddCapRightTop(Point top)
{
	Values[CapResults_RightTop] = top.x;
	Values[CapResults_RightTop + 1] = top.y;
}

void CapResultDetails::AddNeckLeftLip(SpecialPoint lip)
{
	Values[CapResults_LeftLip] = lip.x;
	Values[CapResults_LeftLip + 1] = lip.y;
	Values[CapResults_LeftLip + 2] = lip.c;
}

void CapResultDetails::AddNeckRightLip(SpecialPoint lip)
{
	Values[CapResults_RightLip] = lip.x;
	Values[CapResults_RightLip + 1] = lip.y;
	Values[CapResults_RightLip + 2] = lip.c;
}

void CapResultDetails::AddSportBand(TwoPoint sport)
{
	Values[CapResults_SportBand] = sport.x;
	Values[CapResults_SportBand + 1] = sport.y;
	Values[CapResults_SportBand + 2] = sport.x1;
	Values[CapResults_SportBand + 3] = sport.y1;

}

void CapResultDetails::AddCapMidTop(Point mid)
{
	Values[CapResults_MidTop] = mid.x;
	Values[CapResults_MidTop + 1] = mid.y;
}

void CapResultDetails::AddCapMidBot(Point mid)
{
	Values[CapResults_MidBot] = mid.x;
	Values[CapResults_MidBot+1] = mid.y;
}


void CapResultDetails::Unload()
{
	CapResultsDetailsPool::DeleteDetails(this);
}

CapResultDetails::CapResultDetails()
{
	Reset();
}
CapResultDetails::~CapResultDetails()
{

}

void ResultDetails::Reset()
{
	memset(Values, 0, MAX_DETAILS_BUFFER * sizeof(int));
}
void BottleResultDetails::Unload()
{
	BottleResultsDetailsPool::DeleteDetails(this);
}

BottleResultDetails::BottleResultDetails()
{
	Reset();
}
BottleResultDetails::~BottleResultDetails()
{

}