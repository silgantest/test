#include "Reusable.h"

#ifndef CAPTURED_IMAGE_H
#define CAPTURED_IMAGE_H

class CapturedImage : public Reusable
{
protected:
	int cameraIndex;
	int imageNumber;
	CapturedImage():cameraIndex(-1), imageNumber(-1){};
	virtual void Set(int imNum, int camNum)
	{
		imageNumber = imNum;
		cameraIndex = camNum;
	};

public:

	virtual ~CapturedImage() {};
	virtual void* GetBuffer() = 0;
	virtual int GetWidth() = 0;
	virtual int GetHeight() = 0;
	virtual int GetCameraIndex() { return cameraIndex; }
	virtual int GetImageNumber() { return imageNumber; }
};

class StopCapturedImage : public CapturedImage
{
public:
	StopCapturedImage() { Set(-5, -5); }
	virtual void* GetBuffer() { return 0; };
	virtual int GetWidth() { return 0; };
	virtual int GetHeight() { return 0; };

	virtual void Reset() override { Set(-5, -5); };
	virtual void Unload() {};

};

#endif //CAPTURED_IMAGE_H
