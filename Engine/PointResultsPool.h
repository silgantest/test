#include "PointResult.h"
#include "ReusablePool.h"

#ifndef POINT_RESULTS_POOL_H
#define POINT_RESULTS_POOL_H

class TwoPointResultsPool : public ReusablePool
{
private:

	static TwoPointResultsPool* instance;


protected:
	TwoPointResultsPool();

	virtual Reusable* create() override;
	void deleteObject(Reusable* obj) override;

public:
	TwoPointResultsPool(TwoPointResultsPool&) = delete;
	TwoPointResultsPool& operator=(TwoPointResultsPool& rhs) = delete;
	virtual ~TwoPointResultsPool();

	static TwoPointResult* GetResultObj(TwoPoint pos);
	static void DeleteResultObj(TwoPointResult* obj);
	static void Initialize();
	static void Shutdown();


};

class SpecialPointResultsPool : public ReusablePool
{
private:

	static SpecialPointResultsPool* instance;


protected:
	SpecialPointResultsPool();

	virtual Reusable* create() override;
	void deleteObject(Reusable* obj) override;

public:
	SpecialPointResultsPool(SpecialPointResultsPool&) = delete;
	SpecialPointResultsPool& operator=(SpecialPointResultsPool& rhs) = delete;
	virtual ~SpecialPointResultsPool();

	static SpecialPointResult* GetResultObj(SpecialPoint pos);
	static void DeleteResultObj(SpecialPointResult* obj);
	static void Initialize();
	static void Shutdown();


};

#endif