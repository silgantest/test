#include "SvfEngineRunner.h"

#ifndef SOFTWARE_TRIGGER_THREAD_H
#define SOFTWARE_TRIGGER_THREAD_H

class SoftwareTriggerThread
{
public:
	SoftwareTriggerThread() = default;
	~SoftwareTriggerThread() = default;
	SoftwareTriggerThread& operator=(SoftwareTriggerThread&) = delete;
	SoftwareTriggerThread(SoftwareTriggerThread&) = delete;

	void operator()(SvfEngineRunner* runner) const;

};

#endif