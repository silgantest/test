#include "BaslerCamera.h"
#include "BaslerCameraThread.h"

using namespace Pylon;
using namespace Basler_GigECameraParams;
using namespace Basler_UsbCameraParams;

bool BaslerCamera::beenInit;

BaslerCamera::BaslerCamera(CInstantCamera* cam, CameraSpecificConfiguration* config)
	:Camera()
{
	camera = cam;
	camConfig = config;
	grabThread = nullptr;
}

void BaslerCamera::CameraInit()
{
	if (!beenInit)
	{
		PylonInitialize();
		beenInit = true;
	}
}

void BaslerCamera::Shutdown()
{
	PylonTerminate();
}

std::list<BaslerCamera*>* BaslerCamera::ScanAllCameras()
{
	const uint32_t GroupKey = 0x112233;
	CameraInit();
	std::list<BaslerCamera*>* list = new std::list<BaslerCamera*>();

	CTlFactory& tlFactory = CTlFactory::GetInstance();

	DeviceInfoList_t devices;
	if (tlFactory.EnumerateDevices(devices) == 0)
	{
		return list;
	}

	// Create and attach all Pylon Devices.
	for (size_t i = 0; i < devices.size(); ++i)
	{
		IPylonDevice* pd = tlFactory.CreateDevice(devices[i]);
		if (devices[i].IsIpAddressAvailable())
		{
			CBaslerGigEInstantCamera* c = new  CBaslerGigEInstantCamera(pd, Cleanup_Delete);
			c->RegisterConfiguration(new CSoftwareTriggerConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);

			BaslerCamera* cam = new BaslerCamera(c, new BaslerGigeSpecificConfiguration(c));
			list->push_back(cam);

		}
		else if(devices[i].IsDeviceGUIDAvailable())
		{
			CBaslerUsbInstantCamera* c = new  CBaslerUsbInstantCamera(pd, Cleanup_Delete);
			BaslerCamera* usbcam = new BaslerCamera(c, new BaslerUsbSpecificConfiguration(c));
			c->RegisterConfiguration(new CSoftwareTriggerConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);
			list->push_back(usbcam);
		}
	}

	return list;
}

BaslerCamera::~BaslerCamera()
{
	delete camConfig;
	delete camera;
	running = false;
	if (grabThread)
	{
		grabThread->join();
		delete grabThread;
	}
}

void BaslerCamera::LoadSetAllParameters()
{

}

void BaslerCamera::SaveAllParameters()
{

}

void BaslerCamera::SetParameter(CameraParameter* param) 
{
}

CameraParameter* BaslerCamera::ReadParameter()
{
	return nullptr;
}


void BaslerCamera::ResetImageNumber()
{
	this->imageNumber = 0;
}

void BaslerCamera::Start(CounterLocks* lk)
{
	camera->Open();
	camConfig->SetAOI(xAOI, yAOI, widthAOI, heightAOI);
	camConfig->SetupTrigger(false);
	camera->StartGrabbing(GrabStrategy_LatestImageOnly);
	running = true;
	BaslerCameraThread bct;
	std::thread bcThread(std::ref(bct), this, this->processor, lk);
	bcThread.detach();

}

void BaslerCamera::GetNextImage(CGrabResultPtr& ptrGrabResult)
{
	camera->RetrieveResult(INFINITE, ptrGrabResult, TimeoutHandling_ThrowException);
}


void BaslerCamera::Stop()
{
	running = false;
	{
		std::lock_guard<std::mutex> lk(startStop);
		camera->StopGrabbing();
		camera->Close();
		grabThread->join();
		delete grabThread;
		grabThread = nullptr;
	}
	processor->DoneProcessing();

}

const std::string BaslerCamera::GetModel()
{
	std::string str(camera->GetDeviceInfo().GetModelName());
	return str;
}

const std::string BaslerCamera::GetSerial()
{
	CDeviceInfo di = camera->GetDeviceInfo();
	return di.GetSerialNumber().c_str();
}

bool BaslerCamera::IsRunning()
{
	return running;
}

void BaslerCamera::FireSoftwareTrigger()
{
	if (camera->CanWaitForFrameTriggerReady())
//		camera->WaitForFrameTriggerReady(1000, TimeoutHandling_ThrowException);
	camera->ExecuteSoftwareTrigger();
}

