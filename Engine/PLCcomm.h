#ifndef PLC_COMM
#define PLC_COMM

class PLCcomm
{
protected:
	PLCcomm() = default;
public:
	virtual ~PLCcomm() = default;

	virtual bool SendPassFail(bool pass) = 0;
	virtual bool SendPassFail(char* code) = 0;

	virtual bool PauseTrigger() = 0;
	virtual bool ResumeTrigger() = 0;

};

#define PLC_CODE_GoodBottle L"%000000000000790"
#define PLC_CODE_BadBottle L"%000000000000420"

#endif