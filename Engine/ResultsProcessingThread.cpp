#include "ResultsProcessingThread.h"
#include "ClientIPC.h"
#include "ThreadCounter.h"
#include <iostream>
#include "ModeState.h"

void ResultsProcessingThread::operator()(ResultsGroupBuffer* buffer, InspectionDoResults* doResults, CounterLocks* lk) const
{
	ThreadCounter TC(lk);  //Track if thread is still open
	ResultsGroup* node;
	do
	{
		
		buffer->WaitForData();
		node = buffer->GetGroup();
		if (node)
		{
			doResults->DoResults(node);
			ClientIPC::PushToClient(node);
			ModeState::GetCurrentStatus()->UpdateFromResultSet(node->GetResultsSet());
			node->Unload();
		}
	} while (node != nullptr);
}
