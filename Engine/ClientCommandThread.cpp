#include "ClientCommandThread.h"
#include <windows.h>
#include <iostream>
#include "IPC.h"
#include "IPCCommands.h"
#include "JobManager.h"
#include "ModeState.h"

void ClientCommandThread::operator()(ClientCommandStatus* status)
{
	status->Running = true;
	char str[100];
	HANDLE semSharedDataReady = OpenSemaphore(EVENT_ALL_ACCESS, TRUE, SHARED_MEMORY_COMMAND_BUFFER_SEM);
	HANDLE mtxSharedDataAccess = OpenMutex(MUTEX_ALL_ACCESS, TRUE, SHARED_MEMORY_COMMAND_BUFFER_MTX);

	if (!semSharedDataReady || !mtxSharedDataAccess)
		return;

	HANDLE hMapFile = OpenFileMapping(PAGE_READWRITE, false, COMMAND_BUFFER);
	BYTE* pBuf = (BYTE*)MapViewOfFile(hMapFile, // handle to map object
		FILE_MAP_READ,  // read/write permission
		0,
		0,
		COMMAND_BUFFER_SIZE);

	char strData[240];

	while (status->Running)
	{
		WaitForSingleObject(semSharedDataReady, INFINITE);

		if (pBuf[0] == (unsigned char)IPCCommands::EngCommand::ReloadJob)
		{
			memcpy(strData, &pBuf[1], 240);
			ModeState::PauseTrigger();
			JobManager::ChangeJob(strData);
			ModeState::ResumeTrigger();

			//Reload job
			//Notify inspectors
		}
	}
	UnmapViewOfFile(pBuf);
	CloseHandle(hMapFile);
}
