#include "SvfEngineRunner.h"
#include "PLCcommProxy.h"
#include <iostream>
#include <fstream>
#include "SimCamera.h"
#include "BottleInspector.h"
#include "CapInspector.h"
#include "JobManager.h"

//Startup/Init
#include "IntermediateImagePackPool.h"
#include "Cap85IntermediateImagePackPool.h"
#ifndef MEM_EVAL
#include "BaslerCapturedImagePool.h"
#endif
#include "IntInspectionResultsPool.h"
#include "SimCapturedImagePool.h"
#include "ResultsGroupPool.h"
#include "LabelResultsPool.h"
#include "CapResultsDetailsPool.h"
#include "BottleResultsDetailsPool.h"

//threading

#include "ImageProcessingThread.h"
#include "ClientCommandThread.h"
#include "ThreadTracker.h"
#include "CounterLocks.h"
#include <future>

#include "SoftwareTriggerThread.h"
#include "ModeState.h"
#include "LogData.h"

using namespace std;

void SvfEngineRunner::Run()
{
	LogData::Initialize();
	InitObjectPools();
	InitOther();
	InitCams();

	CounterLocks* lk = new CounterLocks();  //For safe thread shutdown


	//Start results handler
	resultsHandler->Start(lk);

	//Start image processors
	std::list <ImageProcessor*>::iterator itips;
	for (itips = ips->begin(); itips != ips->end(); ++itips)
	{
		ImageProcessingThread ipt;
		std::thread ipThread(std::ref(ipt), (ImageProcessor*)*itips, lk);
		ipThread.detach();
	}

	//start Cams
	std::list <Camera*>::iterator itcams;
	for (itcams = cams->begin(); itcams != cams->end(); ++itcams)
	{
		((Camera*)*itcams)->Start(lk);

	}

	ClientCommandThread ct;
	ClientCommandStatus* status = new ClientCommandStatus();
	status->Running = true;
	std::thread cmdThread(std::ref(ct), status);

	ClientIPC::Create();

	//start timed trigger
	running = true;

	SoftwareTriggerThread sct;
	std::thread timedTriggerThread(std::ref(sct), this);

	//Remove?
	PLCcommProxy::GetPLCcomm()->SendPassFail(true);

	//Keep running...
	char s;
	char disp[100];
	sprintf_s(disp, "Press any key to stop...\n");
	std::cout.write(disp, strlen(disp));
	std::cin.read(&s, 1);

	//Stopping...
	running = false;
	timedTriggerThread.join();

	//stop Cams
	for (itcams = cams->begin(); itcams != cams->end(); ++itcams)
	{
		((Camera*)*itcams)->Stop();
	}

	//stop image processors
	for (itips = ips->begin(); itips != ips->end(); ++itips)
	{
		((ImageProcessor*)*itips)->DoneProcessing();
	}
	resultsHandler->Stop();

	//Wait for thread shutdown
	lk->ThreadCountWait();

	//Clean up
	clearStats(cams, ips);
	for (itips = ips->begin(); itips != ips->end(); ++itips)
	{
		delete* itips;
	}
	delete ips;
	for (itcams = cams->begin(); itcams != cams->end(); ++itcams)
	{
		delete* itcams;
	}
	status->Running = false;
	cmdThread.join();
	delete status;

	delete cams;
	delete patt;
	delete lk;
	ModeState::Shutdown();

	JobManager::Shutdown();
	CleanUpObjectPools();
	ClientIPC::CloseBuffer();
	LogData::Shutdown();

}

void SvfEngineRunner::InitOther()
{
	config = SystemConfigLoader::GetConfig();
	JobManager::Initialize();
	ModeState::Initialize();
	resultsHandler = new ResultsHandler(4, 5, config->GetAppConfig());
	ClientIPC::Create();
	ips = new std::list<ImageProcessor*>();
}

void SvfEngineRunner::InitObjectPools()
{
	IntermediateImagePackPool::Initialize();
	Cap85IntermediateImagePackPool::Initialize();
	SystemConfig* config = SystemConfigLoader::GetConfig();
	FloatingInspectionResultsPool::Initialize();
#ifndef MEM_EVAL
	BaslerCapturedImagePool::Initialize();
#endif
	SimCapturedImagePool::Initialize();
	IntInspectionResultsPool::Initialize();
	Results85GroupPool::Initialize();
	LabelResultsPool::Initialize();
	CapResultsDetailsPool::Initialize();
	BottleResultsDetailsPool::Initialize();
}

void SvfEngineRunner::CleanUpObjectPools()
{

//	SystemConfig* config = SystemConfigLoader::GetConfig();
	Results85GroupPool::Shutdown();
	LabelResultsPool::Shutdown();
	FloatingInspectionResultsPool::Shutdown();
	IntermediateImagePackPool::Shutdown();
	Cap85IntermediateImagePackPool::Shutdown();
#ifndef MEM_EVAL
	BaslerCapturedImagePool::Shutdown();
#endif
	SimCapturedImagePool::Shutdown();
	IntInspectionResultsPool::Shutdown();
	CapResultsDetailsPool::Shutdown();
	BottleResultsDetailsPool::Shutdown();

}

void SvfEngineRunner::InitCams()
{
	char disp[36];
	std::list<ExpectedCameraConfig*>* expectedcams = config->GetCameras();
	cams = CameraFactory::GetCameras();
	std::list <ExpectedCameraConfig*>::iterator it;
	std::list <Camera*>::iterator itcams;
	bool foundcam;
	std::list<Camera*>* simcams = new std::list<Camera*>();

	for (it = expectedcams->begin(); it != expectedcams->end(); ++it)
	{
		foundcam = false;
		for (itcams = cams->begin(); itcams != cams->end(); ++itcams)
		{
			if (((Camera*)*itcams)->GetSerial().compare(((ExpectedCameraConfig*)*it)->GetSerial()) == 0)
			{
				foundcam = true;
				ImageProcessor* ip;
				setupcam(((ExpectedCameraConfig*)*it), ((Camera*)*itcams), resultsHandler, ip, patt);
				((Camera*)*itcams)->SetProcessor(ip);
				sprintf_s(disp, "Start camera %d\n", ((Camera*)*itcams)->GetIndex());
				std::cout.write(disp, strlen(disp));
				break;
			}
		}
		if (!foundcam)
		{
			simcams->push_back(setupsimcam(((ExpectedCameraConfig*)*it), resultsHandler, patt));
		}
	}

	for (itcams = simcams->begin(); itcams != simcams->end(); ++itcams)
	{
		cams->push_back(((Camera*)*itcams));
	}
}

Camera* SvfEngineRunner::setupsimcam(ExpectedCameraConfig* excam, ResultsHandler* resultsHandler, Patterns* patt)
{
	Camera* simcam = new SimCamera();
	ImageProcessor* ip;
	setupcam(excam, simcam, resultsHandler, ip, patt);
	simcam->SetProcessor(ip);
	return simcam;
}

void SvfEngineRunner::setupcam(ExpectedCameraConfig* excam, Camera* disccam, ResultsHandler* resultsHandler, ImageProcessor*& ip, Patterns* patt)
{
	int index = excam->GetNumber();
	disccam->SetIndex(index - 1);
	ip = new ImageProcessor(resultsHandler, excam->GetNumber() - 1);
	ips->push_back(ip);
	Inspector* insp;

	//TODO: 5 cameras are always 85 -- is this OK?
	if (index < 5)
		insp = new BottleInspector(index - 1);
	else if (index == 5)
		insp = new CapInspector();
	else
		insp = nullptr;
	insp->SetAppConfig(SystemConfigLoader::GetConfig()->GetAppConfig());
	insp->ResetForJob();
	resultsHandler->JobUpdated(JobManager::GetCurrentJob());
	disccam->SetAOI(excam->GetOffsetX(), excam->GetOffsetY(), excam->GetWidth(), excam->GetHeight());
	ip->SetInspection(insp);
}

/******************************************************************************
Handle Software Trigger and Record Times
Not used in production
******************************************************************************/
void SvfEngineRunner::clearStats(std::list<Camera*>* cams, std::list<ImageProcessor*>* ips)
{
#ifdef MEASURETIMES
	std::list <Camera*>::iterator itcams;
	for (itcams = cams->begin(); itcams != cams->end(); ++itcams)
	{
		((Camera*)*itcams)->clearStats();
	}

	std::list <ImageProcessor*>::iterator itips;
	for (itips = ips->begin(); itips != ips->end(); ++itips)
	{
		{
			std::lock_guard<std::mutex> lk(((ImageProcessor*)*itips)->timesMtx);
			for (size_t j = 0; j < ((ImageProcessor*)*itips)->times->size(); j++)
			{
				delete ((ImageProcessor*)*itips)->times->at(j);
			}
			((ImageProcessor*)*itips)->times->clear();
		}
	}
#endif
}

void SvfEngineRunner::writefile(std::list<Camera*>* cams, std::list<ImageProcessor*>* ips)
{
#ifdef MEASURETIMES

	std::list<Stats*>::const_iterator st;

	std::list <Camera*>::iterator itcams;
	for (itcams = cams->begin(); itcams != cams->end(); ++itcams)
	{
		char fileName[100];
		sprintf_s(fileName, "C:\\out\\times%d.csv", ((Camera*)*itcams)->GetIndex());
		std::ofstream myfile;
		myfile.open(fileName, std::ofstream::out | std::ofstream::app);
		char buffer[250];
		memset(buffer, 0, 250);
		sprintf_s(buffer, 250, "Cam,Image,StartGrab,EndGrab,StartProc,EndProc,DiffGrab,DiffProc,Diff\n");
		myfile.write(buffer, strlen(buffer));

		((Camera*)*itcams)->WriteTimes(myfile);

	}
#endif
}