#ifndef INSPECTION_RESULT_H
#define INSPECTION_RESULT_H

#include "Reusable.h"

enum InspectionResultType
{
	//generic
	IntRange,
	FloatRange,
	BadPixelCount,

	//84/85
	BottleLabelPos,
};

enum class InspectionCode
{
	Dummy = 0,
	Label= 1,
	Cap85  = 2,
	Cap56= 3,

};


class InspectionResult : public Reusable
{
protected:
	InspectionResult(): imageNumber(-1), camIndex(-1), inspNum(-1), code(InspectionCode::Dummy)
	{
	}
	int imageNumber;
	int camIndex;
	int inspNum;
	InspectionCode code;

public:
	virtual ~InspectionResult()
	{
	}

	void SetIndex(int inspIndex)
	{
		inspNum = inspIndex;
	}

	int GetIndex()
	{
		return inspNum;
	}

	void Set(InspectionCode code, int camIndex, int imageNum)
	{
		this->code = code; 
		this->camIndex = camIndex;
		this->imageNumber = imageNum;
	}

	int GetImageNumber() { return this->imageNumber; };
	int GetCamIndex() { return this->camIndex; };
	InspectionCode GetCode() { return this->code; };
	void SetImageNumber(int imageNumber) {this->imageNumber = imageNumber;}
	void SetCode(InspectionCode code) { this->code = code; };

	virtual void Unload() = 0;

};


#endif //INSPECTION_RESULT_H