#include "SimCapturedImagePool.h"

//static method
SimCapturedImagePool* SimCapturedImagePool::instance;
void SimCapturedImagePool::Initialize()
{
	instance = new SimCapturedImagePool();
	instance->grow();
}

void SimCapturedImagePool::Shutdown()
{
	delete instance;
}

SimCapturedImage* SimCapturedImagePool::GetSimImage(unsigned char* image, int imNum, int camNum, int width, int height)
{
	SimCapturedImage* b = (SimCapturedImage*)instance->Get();
	b->Set(image, imNum, camNum, width, height);
	return b;
}

void SimCapturedImagePool::DeleteCapturedImage(SimCapturedImage* obj)
{
	instance->Delete(obj);
}

SimCapturedImagePool::SimCapturedImagePool()
{
	sprintf_s(name, "SimCapturedImagePool");

}


SimCapturedImagePool::~SimCapturedImagePool()
{
	deleteEverything();
}

void SimCapturedImagePool::deleteObject(Reusable* obj)
{
	delete (SimCapturedImage*)obj;
}

Reusable* SimCapturedImagePool::create()
{
	return new SimCapturedImage();
};

