#pragma once
#include <mutex>
#include "CounterLocks.h"

class ThreadCounter
{
public:

	ThreadCounter(CounterLocks* l) :lk(l)
	{

		std::lock_guard<std::mutex> lock(mtx);
		count++;
	}

	~ThreadCounter()
	{
		std::lock_guard<std::mutex> lock(mtx);
		count--;

		if (count == 0)
		{
			lk->ThreadCountNotify();
		}
	}

	static int count;
	static std::mutex mtx;
	CounterLocks* lk;
};

