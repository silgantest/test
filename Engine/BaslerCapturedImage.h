#include"CapturedImage.h"
#include <pylon/PylonIncludes.h>

#ifndef BASLER_CAPTURED_IMAGE_H
#define BASLER_CAPTURED_IMAGE_H
using namespace Pylon;

class BaslerCapturedImage : public CapturedImage
{
friend class BaslerCapturedImagePool;

protected:
	CGrabResultPtr captured;
	void Set(CGrabResultPtr capPtr, int imNum, int camNum);
	BaslerCapturedImage();
public:

	static BaslerCapturedImage* CreateBaslerCapImg(CGrabResultPtr capPtr, int imNum, int camNum);

	virtual ~BaslerCapturedImage();
	virtual void* GetBuffer();
	virtual int GetWidth();
	virtual int GetHeight();
	virtual void Reset() override;
	virtual void Unload() override;


};
#endif
