#include "ResultsGroupBuffer.h"
#include "CounterLocks.h"
#include "InspectionDoResults.h"

#ifndef RESULTS_PROCESSING_THREAD
#define RESULTS_PROCESSING_THREAD

class ResultsProcessingThread
{
public:
	ResultsProcessingThread() = default;
	~ResultsProcessingThread() = default;
	ResultsProcessingThread& operator=(ResultsProcessingThread&) = delete;
	ResultsProcessingThread(ResultsProcessingThread&) = delete;

	void operator()(ResultsGroupBuffer* buffer, InspectionDoResults* doResults, CounterLocks * lk) const;
};
#endif

