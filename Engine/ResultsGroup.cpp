#include "ResultsGroup.h"
#include "FloatingInspectionResultsPool.h"
#include "ResultsGroupPool.h"
#include "CapturedImage.h"

ResultsGroup::ResultsGroup(int num, int reqInsp, int numCameras) : ResultsGroup()
{
	Set(num, reqInsp, numCameras);
	results = new Results();
}

ResultsGroup::ResultsGroup(): totalInspections(0),  numberInspections(0), imageNumber(0)
{
	inspRes = new std::list<InspectionResult*>();
	for(int i=0; i< TOTAL_CAMS; i++)
		images[i] = nullptr;
	results = new Results();
}

std::list<InspectionResult*>* ResultsGroup::GetResultsList()
{
	return inspRes;
}

ResultsGroup::~ResultsGroup()
{
	delete inspRes;
	delete results;
}

void ResultsGroup::AddInspectionResults(InspectionResult* res, CapturedImage* im, IntermediateImagePack* pack)
{
	inspRes->push_back(res);
	totalInspections++;
	images[im->GetImageNumber() - 1] = im;
	packs[im->GetImageNumber() - 1] = pack;
}

void ResultsGroup::AddDetails(ResultDetails* details, int index)
{
	resultdetails[index] = details;
}


void ResultsGroup::AddInspectionResults(InspectionResult* res[], int count, CapturedImage* im, Cap85IntermediateImagePack* pack)
{
	images[im->GetCameraIndex()] = im;
	capPack = pack;

	for (int i = 0; i < count; i++)
	{
		totalInspections++;
		inspRes->push_back(res[i]);
	}
}


int ResultsGroup::GetImageNumber()
{
	return imageNumber;
}

bool ResultsGroup::Complete()
{
	return (numberInspections == totalInspections); 
}

Results* ResultsGroup::GetResultsSet()
{
	return results;
}

void ResultsGroup::Reset()
{
	Set(-1, -1, -1);
	for (int i = 0; i< TOTAL_CAMS; i++)
		if(images[i])
			images[i]->Unload();
	memset(images, sizeof(CapturedImage*) * TOTAL_CAMS, 0);
	while (inspRes->size() > 0)
	{
		inspRes->front()->Unload();
		inspRes->pop_front();
	}
	inspRes->clear();
}

void ResultsGroup::Set(int num, int reqInsp, int numCameras)
{
	imageNumber = num;
	numberInspections = reqInsp * numCameras;

	totalInspections = 0;
	
}

void ResultsGroup::Unload()
{
	std::list<InspectionResult*>* list = this->GetResultsList();
	std::list<InspectionResult*>::const_iterator iterator;
	for (iterator = list->begin(); iterator != list->end(); ++iterator)
	{
		(*iterator)->Unload();
	}
	list->clear();

	for (int i = 0; i < TOTAL_CAMS; i++)
	{
		if (this->images[i])
			this->images[i]->Unload();
		if (this->resultdetails[i])
			this->resultdetails[i]->Unload();
	}

	for (int i = 0; i < BOTTLE_CAMS; i++)
	{
		if(this->packs[i])
			this->packs[i]->Unload();
	}

	this->capPack->Unload();
}

/*  Should be called after all inpsections complete.  So there should be no contention and no need to lock*/
CapturedImage** ResultsGroup::GetImages()
{
	return images;
}

/*  Should be called after all inpsections complete.  So there should be no contention and no need to lock*/
IntermediateImagePack** ResultsGroup::GetPacks()
{
	return packs;
}
/*  Should be called after all inpsections complete.  So there should be no contention and no need to lock*/
Cap85IntermediateImagePack* ResultsGroup::GetCapPack()
{
	return capPack;
}

ResultDetails* ResultsGroup::GetDetails(int index)
{
	return resultdetails[index];
}
