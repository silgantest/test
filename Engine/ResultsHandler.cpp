#include "ResultsHandler.h"
#include "ResultsGroupFactory.h"
#include "InspectionTrigger.h"
#include "IntermediateImagePack.h"
#include <assert.h>
#include <iostream>
#include "ResultsProcessingThread.h"

ResultsHandler::ResultsHandler(int reqIns, int numCams, AppConfig* config)
{
	requiredInspections = reqIns;
	numCameras = numCams;
	doResults = DoResultsFactory::GetDoResults(config);
	entryCount = 0;
	resultsGroup[0] = resultsGroup[1] = nullptr;
	buffer = new ResultsGroupBuffer();

}

void ResultsHandler::Start(CounterLocks* lk)
{
	ResultsProcessingThread rpt;
	std::thread t(std::ref(rpt), buffer, doResults, lk);
	t.detach();
}
void ResultsHandler::Stop()
{
	buffer->Shutdown();
}


ResultsHandler::~ResultsHandler()
{
	delete doResults;
}

void ResultsHandler::AddResult(InspectionResult* ir, CapturedImage* im, IntermediateImagePack* pack, ResultDetails* details)
{
	if (im->GetImageNumber() == -1)
		assert(false);

	std::lock_guard<std::mutex> lk(lock);
	ResultsGroup* node = getGroup(im->GetImageNumber());
	node->AddInspectionResults(ir, im, pack);
	node->AddDetails(details, im->GetCameraIndex());
	handleComplete(node);
}

void ResultsHandler::JobUpdated(Job* newJob)
{
	doResults->UpdateJob(newJob);
}


void ResultsHandler::handleComplete(ResultsGroup* node)
{
	if (node->Complete())
	{
		InspectionTrigger::SetLastCount(node->GetImageNumber());
		InspectionTrigger::Notify();

		int imNum = node->GetImageNumber();
		buffer->AddGroup(node);

		for (int i = 0; i < 2; i++)
		{
			if (resultsGroup[i])
			{
				if (resultsGroup[i]->GetImageNumber() == imNum)
				{
					resultsGroup[i] = nullptr;
				}
			}
		}

	}

}

ResultsGroup* ResultsHandler::getGroup(int imageNum)
{
	entryCount++;
	ResultsGroup* node = nullptr;
	for (int i = 0; i < 2; i++)
	{
		if (resultsGroup[i])
		{
			if (resultsGroup[i]->GetImageNumber() == imageNum)
			{
				node = resultsGroup[i];
				break;
			}
		}
	}
	if (!node)
	{
		bool found = false;
		node = ResultsGroupFactory::GetResultGroup(imageNum, requiredInspections, numCameras);
		for (int i = 0; i < 2; i++)
		{
			if (resultsGroup[i] == nullptr)
			{
				resultsGroup[i] = node;
				found = true;
				break;
			}
		}
		if (!found)
			assert(false); ///this is broken
	}
	return node;
}


void ResultsHandler::AddResults(InspectionResult* res[], int count, CapturedImage* im, Cap85IntermediateImagePack* pack, ResultDetails* details)
{
	std::lock_guard<std::mutex> lk(lock);
	ResultsGroup* node = getGroup(im->GetImageNumber());
	node->AddInspectionResults(res, count, im, pack);
	node->AddDetails(details, im->GetCameraIndex());
	handleComplete(node);
}
