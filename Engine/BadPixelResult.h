#include <list>
#include "Point.h"
#include "InspectionResult.h"

#ifndef BAD_PIXEL_RESULT
#define BAD_PIXEL_RESULT

class BadPixelResult : public InspectionResult
{
public:
	BadPixelResult() { pointList = new std::list<Point>(); };
	virtual ~BadPixelResult() { delete pointList; };
	void Reset() { }
	virtual void Unload() {}

private:
	std::list<Point>* pointList;
};
#endif //BAD_PIXEL_RESULT
