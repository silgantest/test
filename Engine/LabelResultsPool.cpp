#include "LabelResultsPool.h"

//static methods
LabelResultsPool* LabelResultsPool::instance;

LabelResults* LabelResultsPool::GetResultObj(int camIndex, int imageNumber)
{
	LabelResults* res = (LabelResults*)instance->Get();
	res->Set(InspectionCode::Label, camIndex, imageNumber);
	return res;
}

void LabelResultsPool::DeleteResultObj(LabelResults* obj)
{
	instance->Delete(obj);
}

void LabelResultsPool::Initialize()
{
	instance = new LabelResultsPool();
	sprintf_s(instance->name, "LabelResultsPool");
	instance->grow();
}

void LabelResultsPool::Shutdown()
{
	delete instance;
}


LabelResultsPool::LabelResultsPool()
{

}

LabelResultsPool::~LabelResultsPool()
{
	deleteEverything();
}

void LabelResultsPool::deleteObject(Reusable* obj)
{
	delete (LabelResults*)obj;
}

Reusable* LabelResultsPool::create()
{
	return new LabelResults();
};
