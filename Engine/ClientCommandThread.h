
#ifndef CLIENT_COMMAND_THREAD_H
#define CLIENT_COMMAND_THREAD_H

class ClientCommandStatus
{
public:
	bool Running;
};

class ClientCommandThread
{
	
public:
	ClientCommandThread() = default;
	~ClientCommandThread() = default;
	ClientCommandThread& operator=(ClientCommandThread&) = delete;
	ClientCommandThread(ClientCommandThread&) = delete;

	void operator()(ClientCommandStatus* status);

};

#endif

