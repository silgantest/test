#include "SoftwareTriggerThread.h"
#include "ModeState.h"

void SoftwareTriggerThread::operator()(SvfEngineRunner* runner) const
{
	int count = 0;
	std::list <Camera*>::iterator it;
	int milliseconds = 200;

#ifdef MEASURETIMES
	ofstream myfile;
	myfile.open("C:\\out\\times.csv", std::ofstream::out | std::ofstream::trunc);
	myfile.close();
	Stats* s;
#endif
	while (runner->running)
	{
		count++;
		if(ModeState::WaitForOKtoTrigger(milliseconds))
		{
			for (it = runner->cams->begin(); it != runner->cams->end(); ++it)
			{
#ifdef MEASURETIMES
				s = new Stats();
				s->CamNumber = ((Camera*)*it)->GetIndex();
				s->Action = Activity::Trigger;
#endif
				((Camera*)*it)->FireSoftwareTrigger();

#ifdef MEASURETIMES
				s->timestampStart = GetTickCount();
				((Camera*)*it)->AddTriggerStats(s);
				s = nullptr;
#endif
			}
		}


		Sleep(milliseconds);
		if (count > 1000000000)
			return;
#ifdef MEASURETIMES
		if (count % 100 == 0)
		{
			runner->writefile(runner->cams, runner->ips);
			runner->clearStats(runner->cams, runner->ips);
		}
#endif
	}
}
