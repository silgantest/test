#include "Reusable.h"
#include "Point.h"
#include "IPC.h"

#ifndef RESULTS_DETAILS
#define RESULTS_DETAILS


class ResultDetails : public Reusable
{
public:
	int Values[MAX_DETAILS_BUFFER];
	virtual void Reset() override;
	virtual void Unload() = 0;

};


class BottleResultDetails : public ResultDetails
{
public:
	BottleResultDetails();
	virtual ~BottleResultDetails();
	virtual void Unload() override;

}; 

class CapResultDetails : public ResultDetails
{
public:
	CapResultDetails();
	virtual ~CapResultDetails();
	void AddColorInfo(int r, int g, int b);
	void AddCapLeftSide(Point side);
	void AddCapRightSide(Point side);
	void AddCapLeftTop(Point top);
	void AddCapRightTop(Point top);
	void AddNeckLeftLip(SpecialPoint lip);
	void AddNeckRightLip(SpecialPoint lip);
	void AddSportBand(TwoPoint sport);
	void AddCapMidTop(Point mid);
	void AddCapMidBot(Point mid);
	virtual void Unload() override;
};

#endif
