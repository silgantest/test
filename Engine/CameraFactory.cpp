#include "CameraFactory.h"
#ifndef MEM_EVAL
#include "BaslerCamera.h"
#endif

CameraFactory::CameraFactory()
{
}


CameraFactory::~CameraFactory()
{
}


std::list<Camera*>* CameraFactory::GetCameras()
{
	std::list<Camera*>* camList = new std::list<Camera*>();
	
#ifndef MEM_EVAL
	std::list<BaslerCamera*>* bascams = BaslerCamera::ScanAllCameras();
	camList->insert(camList->end(), bascams->begin(), bascams->end());
	delete bascams;
#endif
	return camList;
}
