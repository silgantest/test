#ifndef THREADS_H
#define THREADS_H

#ifdef _WIN32

#include <thread>
#include <windows.h>
const DWORD MS_VC_EXCEPTION = 0x406D1388;

typedef __int32 int32_t;
typedef unsigned __int32 uint32_t;
#pragma pack(push,8)

typedef struct tagTHREADNAME_INFO
{
    DWORD dwType; // Must be 0x1000.
    LPCSTR szName; // Pointer to name (in user addr space).
    DWORD dwThreadID; // Thread ID (-1=caller thread).
    DWORD dwFlags; // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)

void SetThreadName(uint32_t dwThreadID, const char* threadName);
void SetThreadName(const char* threadName);
void SetThreadName(std::thread* thread, const char* threadName);



#elif defined(__linux__)
#include <sys/prctl.h>
void SetThreadName(const char* threadName)
{
    prctl(PR_SET_NAME, threadName, 0, 0, 0);
}

#else
void SetThreadName(std::thread* thread, const char* threadName)
{
    auto handle = thread->native_handle();
    pthread_setname_np(handle, threadName);
}
#endif
#endif