#include "PointResult.h"
#include "PointResultsPool.h"

TwoPointResult::TwoPointResult()
{
	Reset();
}

TwoPointResult::~TwoPointResult()
{
}

void TwoPointResult::SetResult(TwoPoint res)
{
	this->pos = res;
}

TwoPoint TwoPointResult::GetResult()
{
	return pos;
}

void TwoPointResult::Reset()
{
	pos = TwoPoint();
}
void TwoPointResult::Unload()
{
	TwoPointResultsPool::DeleteResultObj(this);
}

SpecialPointResult::SpecialPointResult()
{
	Reset();
}

SpecialPointResult::~SpecialPointResult()
{
}

void SpecialPointResult::SetResult(SpecialPoint res)
{
	this->pos = res;
}

SpecialPoint SpecialPointResult::GetResult()
{
	return pos;
}

void SpecialPointResult::Reset()
{
	pos = SpecialPoint();
}
void SpecialPointResult::Unload()
{
	SpecialPointResultsPool::DeleteResultObj(this);
}