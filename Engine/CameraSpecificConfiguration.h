#ifndef CAMERA_SPECIFIC_CONFIGURATION_H
#define CAMERA_SPECIFIC_CONFIGURATION_H

class CameraSpecificConfiguration
{
protected:
	CameraSpecificConfiguration() {};
public:
	virtual ~CameraSpecificConfiguration() {};

	virtual void SetAOI(int x, int y, int width, int height) = 0;
	virtual void SetupTrigger(bool hardware) = 0;
	virtual void SetupPixelFormat() = 0;

};

#endif