#include "BaslerCapturedImagePool.h"

//static method
BaslerCapturedImagePool* BaslerCapturedImagePool::instance;
void BaslerCapturedImagePool::Initialize()
{
	instance = new BaslerCapturedImagePool();
	sprintf_s(instance->name, "BaslerCapturedImagePool");
	instance->grow();
}

void BaslerCapturedImagePool::Shutdown()
{
	delete instance;
}

BaslerCapturedImage* BaslerCapturedImagePool::GetCapturedImage(CGrabResultPtr capPtr, int imNum, int camNum)
{
	BaslerCapturedImage* b = (BaslerCapturedImage*)instance->Get();
	b->Set(capPtr, imNum, camNum);
	return b;
}

void BaslerCapturedImagePool::DeleteCapturedImage(BaslerCapturedImage* obj)
{
	instance->Delete(obj);
}

BaslerCapturedImagePool::BaslerCapturedImagePool()
{

}


BaslerCapturedImagePool::~BaslerCapturedImagePool()
{
	deleteEverything();
}

void BaslerCapturedImagePool::deleteObject(Reusable* obj)
{
	delete (BaslerCapturedImage*)obj;
}

Reusable* BaslerCapturedImagePool::create()
{
	return new BaslerCapturedImage();
};

