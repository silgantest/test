#include "ReusablePool.h"
#include <iostream>

ReusablePool::ReusablePool()
{
	inuse = new std::list<Reusable*>();
	available = new std::list<Reusable*>();
	memset(name, 0, 50);
}

void ReusablePool::deleteFromList(std::list<Reusable*>* list)
{
	while (list->size() > 0)
	{
		deleteObject(list->front());
		list->pop_front();
	}
}

void ReusablePool::deleteEverything()
{
	{
		std::lock_guard<std::mutex> alk(inuseLock);
		deleteFromList(inuse);
		delete inuse;
	}
	{
		std::lock_guard<std::mutex> alk(availLock);
		deleteFromList(available);
		delete available;
	}
}

ReusablePool::~ReusablePool()
{
	//deleteEverything should be called in derived
}

void ReusablePool::grow()
{
	char str[100];
	sprintf_s(str, "growing...%s\n", name);
	std::cout << str;
	for (int i = 0; i < GROWTH_RATE; i++)
	{
		Reusable* r = create();
		available->push_back(r);
	}
}

Reusable* ReusablePool::Get()
{
	Reusable* ret;
	{
		std::lock_guard<std::mutex> alk(availLock);
		if (available->size() == 0)
			grow();
		ret = available->front();
		available->pop_front();
	}
	
	{
		std::lock_guard<std::mutex> alk(inuseLock);
		inuse->push_back(ret);
	}
	return ret;
}

void ReusablePool::Delete(Reusable* obj)
{
	{
		std::lock_guard<std::mutex> alk(inuseLock);
		inuse->remove(obj);
	}
	obj->Reset();
	{
		std::lock_guard<std::mutex> alk(availLock);
		available->push_back(obj);
	}
}

