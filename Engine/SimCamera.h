#include "Camera.h"
#include <vector>
#include "ImageFileUtilities.h"

#ifndef SIM_CAMERA_H
#define SIM_CAMERA_H


struct LoadedImage
{
	LoadedImage(unsigned char* im, int w, int h, int s) : width(w), height(h), size(s), image(im)
	{

	}
	int width, height, size;
	unsigned char* image;
};


class SimCamera : public virtual Camera
{
private:
	std::string* simcamstring;
	CameraParameter* dummyparam;
	std::vector<LoadedImage> images;
	std::mutex triggerLock;
	std::condition_variable triggerCondition;

	int triggerCount;
	int imageCount;


public:
	SimCamera();
	virtual ~SimCamera();

	void LoadSetAllParameters();
	void SaveAllParameters();
	void SetParameter(CameraParameter* param);
	virtual CameraParameter* ReadParameter();

	virtual void Start(CounterLocks* lk) override;
	virtual void Stop();

	const std::string GetModel();
	const std::string GetSerial();

	virtual bool IsRunning();

	virtual void FireSoftwareTrigger();

	virtual int GetImageCount() { return imageCount; }
	virtual void  TriggerWait();

	virtual void incrementImageNumber() override;
	LoadedImage GetNextImage();

};

#endif //SIM_CAMERA_H
