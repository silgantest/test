// SvfEngine.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "SvfEngine.h"
#include "SvfEngineRunner.h"


int main()
{
	SvfEngineRunner* runner = new SvfEngineRunner();
	runner->Run();
	delete runner;
}