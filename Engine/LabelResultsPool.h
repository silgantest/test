#include "LabelResults.h"
#include "ReusablePool.h"

#ifndef LABEL_RESULTS_POOL_H
#define LABEL_RESULTS_POOL_H

class LabelResultsPool : public ReusablePool
{
private:
	static LabelResultsPool* instance;

protected:
	LabelResultsPool();

	virtual Reusable* create() override;
	void deleteObject(Reusable* obj) override;

public:
	LabelResultsPool(LabelResultsPool&) = delete;
	LabelResultsPool& operator=(LabelResultsPool& rhs) = delete;

	virtual ~LabelResultsPool();

	static LabelResults* GetResultObj(int camIndex, int imageNumber);
	static void Initialize();
	static void Shutdown();
	static void DeleteResultObj(LabelResults* obj);

};

#endif