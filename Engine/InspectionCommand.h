#include "InspectionResult.h"

#ifndef INSPECTION_COMMAND_H
#define INSPECTION_COMMAND_H

class InspectionCommand
{
public:
	InspectionCommand() {};
	virtual ~InspectionCommand() {};

	virtual void Execute(char* im_src, int x, int y) = 0;
	virtual void Teach(char* im_src, int x, int y) = 0;

};

#endif //INSPECTION_COMMAND_H