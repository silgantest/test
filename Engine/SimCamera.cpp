#include "SimCamera.h"
#include "SimCapturedImage.h"
#include "ImageFileUtilities.h"
#include "Directories.h"
#include "SimCameraThread.h"
#include <filesystem>
#include "Threads.h"

SimCamera::SimCamera()  : triggerCount(0), imageCount(0)
{
	simcamstring = new std::string("Sim Camera");
	dummyparam = new CameraParameter();

}


SimCamera::~SimCamera()
{
	delete simcamstring;
	delete dummyparam;
}

void SimCamera::LoadSetAllParameters()
{

}

void SimCamera::SaveAllParameters()
{

}

void SimCamera::SetParameter(CameraParameter* param)
{
};

CameraParameter* SimCamera::ReadParameter()
{
	return dummyparam;
}


const std::string SimCamera::GetModel()
{
	return *simcamstring;
}
const std::string SimCamera::GetSerial()
{
	return *simcamstring;
}

void SimCamera::Stop()
{
	running = false;
	triggerCondition.notify_one();
}

bool SimCamera::IsRunning()
{
	return running;
}

void SimCamera::Start(CounterLocks* lk)
{
	triggerCount = 0;
	imageCount = 0;
	int width = 0, height = 0, size = 0;

	char camdir[30];
	memset(camdir, '\0', 30);
	sprintf_s(camdir, "%s%d", SIM_DIR_CAM, this->GetIndex() + 1);
	for (auto& p : std::filesystem::directory_iterator(camdir))
	{
		unsigned char* im = ImageFileUtilities::ReadBMP(p.path().string().c_str(), false, &width, &height, &size);
		images.push_back(LoadedImage(im, width, height, size));
	}
	imageNumber = 0;
	running = true;
	SimCameraThread sct;
	std::thread scThread(std::ref(sct), this, lk);
	scThread.detach();
	char str[50];

	sprintf_s(str, "Camera %d\n", this->GetIndex());
	SetThreadName(&scThread, str);

}

void SimCamera::FireSoftwareTrigger()
{
	std::unique_lock<std::mutex > lk(triggerLock);
	triggerCount++;
	lk.unlock();
	triggerCondition.notify_one();

}

void SimCamera::TriggerWait()
{
	std::unique_lock<std::mutex> lk(triggerLock);
	triggerCondition.wait(lk, [this] {
		return triggerCount == GetImageCount() || !IsRunning(); 
		});

}

void SimCamera::incrementImageNumber()
{
	Camera::incrementImageNumber();
	imageCount++;
}

LoadedImage SimCamera::GetNextImage()
{
	int index = imageCount % images.size();
	return images.at(index);
}
