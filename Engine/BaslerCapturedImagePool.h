#include "BaslerCapturedImage.h"
#include "ReusablePool.h"


#ifndef BASLER_CAPTURED_IMAGE_POOL_H
#define BASLER_CAPTURED_IMAGE_POOL_H


class BaslerCapturedImagePool : public ReusablePool
{
private:
	static BaslerCapturedImagePool* instance;

protected:
	BaslerCapturedImagePool();
	virtual Reusable* create() override;
	void deleteObject(Reusable* obj) override;

public:
	BaslerCapturedImagePool(BaslerCapturedImagePool&) = delete;
	BaslerCapturedImagePool& operator=(BaslerCapturedImagePool& rhs) = delete;
	virtual ~BaslerCapturedImagePool();

	static BaslerCapturedImage* GetCapturedImage(CGrabResultPtr capPtr, int imNum, int camNum);
	static void DeleteCapturedImage(BaslerCapturedImage* obj);
	static void Initialize();
	static void Shutdown();
};

#endif