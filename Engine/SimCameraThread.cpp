#include "SimCameraThread.h"
#include "SimCapturedImagePool.h"
#include "ThreadCounter.h"
#include "ImageFileUtilities.h"

void SimCameraThread::operator()(SimCamera* cam, CounterLocks* lk) const
{
	ThreadCounter TC(lk); //Track if thread is still open
	int index = 0;
	LoadedImage im(nullptr, -1, -1, -1);
	while (cam->IsRunning())
	{
#ifdef MEASURETIMES
		Stats* s = new Stats();
		s->CamNumber = cam->GetIndex();
		s->Action = Activity::Grab;
		s->timestampStart = GetTickCount();
#endif
		cam->incrementImageNumber();
		{
			cam->TriggerWait();
			if (!cam->IsRunning())
				break;
			im = cam->GetNextImage();
		}

		cam->QueueImage(SimCapturedImagePool::GetSimImage(im.image, cam->GetImageNumber(), cam->GetIndex(), im.width, im.height));
#ifdef MEASURETIMES
		s->ImageNumber = cam->GetImageNumber();
			s->timestampEnd = GetTickCount();
			cam->AddStats(s);
#endif
			//wait for next trigger
	}

}
