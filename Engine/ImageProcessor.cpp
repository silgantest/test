#include "ImageProcessor.h"
#include "InspectionTrigger.h"
#include "Threads.h"
#include "JobManager.h"

ImageProcessor::ImageProcessor(ResultsHandler* resHandler, unsigned int camindex) : plc(nullptr), index(camindex), inspection(nullptr), resultsHandler(resHandler)
{	
	imageQueue = new SharedImageQueue();
	jobUpdateRequired = false;
#ifdef MEASURETIMES
	times = new std::vector<Stats*>(125);
	times->clear();
#endif
	JobManager::RegisterForUpdates(this);
}

ImageProcessor::~ImageProcessor()
{
	delete imageQueue;
	delete inspection;
#ifdef MEASURETIMES
	delete times;
#endif
}

void ImageProcessor::JobUpdated(Job* newJob)
{
	jobUpdateRequired = true;
}


void ImageProcessor::QueueImage(CapturedImage* capImg)
{
	imageQueue->Push(capImg);
}

void ImageProcessor::SetInspection(Inspector* ins)
{
	if (inspection)
		delete inspection;
	inspection = ins;
}

void ImageProcessor::ReadyForProcessing()
{
	stopRequested = false;
	imageQueue->Clear();
}

void ImageProcessor::DoneProcessing()
{
	imageQueue->StopWaiting();
	stopRequested = true;
}

void ImageProcessor::StopWaiting()
{
	imageQueue->StopWaiting();
}

CapturedImage* ImageProcessor::Pop()
{
	return imageQueue->Pop();
}

void ImageProcessor::Inspect(CapturedImage* im)
{
	inspection->Inspect(im, resultsHandler);
}

void ImageProcessor::ProcessJobUpdates()
{
	if (jobUpdateRequired)
	{
		jobUpdateRequired = false;
		JobManager::Lock();
		inspection->ResetForJob();
		JobManager::Unlock();
	}
}

bool ImageProcessor::StopRequested()
{
	return stopRequested;
}

#ifdef MEASURETIMES
void ImageProcessor::AddStats(Stats* s)
{
	std::lock_guard<std::mutex> lk(timesMtx);
	times->push_back(s);

}

void ImageProcessor::clearStats()
{
	std::lock_guard<std::mutex> lk(timesMtx);
	for (size_t j = 0; j < times->size(); j++)
	{
		delete times->at(j);
	}
	times->clear();
}
#endif