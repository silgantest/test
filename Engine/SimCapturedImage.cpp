#include "SimCapturedImage.h"
#include "SimCapturedImagePool.h"
#include "ImageFileUtilities.h"


void SimCapturedImage::Set(unsigned char* image, int imNum, int camNum, int width, int height)
{
	this->cameraIndex = camNum;
	this->imageNumber = imNum;
	this->imageData = image;
	this->width = width;
	this->height = height;
}

SimCapturedImage::SimCapturedImage() : imageData(nullptr)
{
}

SimCapturedImage::~SimCapturedImage()
{

}

void* SimCapturedImage::GetBuffer()
{
	return imageData;
}

int SimCapturedImage::GetWidth()
{
	return width;
}

int SimCapturedImage::GetHeight()
{
	return height;
}

void SimCapturedImage::Reset()
{
	this->cameraIndex = -1;
	this->imageNumber = -1;
	this->width = -1;
	this->height = -1;

}

void SimCapturedImage::Unload()
{
	SimCapturedImagePool::DeleteCapturedImage(this);

}



