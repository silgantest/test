#define MAX_INSPECTIONS 12

#ifndef RESULTS_STRUCT
#define RESULTS_STRUCT

struct ResultSet
{
	float Min;
	float Max;
	float Value;
	bool passed;
	bool enabled;
	bool isFloat;

	void Clear()
	{
		Min = Max = Value = 0.0f;
		enabled = passed = isFloat = false;
	}

};

class Results
{
public:
	ResultSet resultSet[MAX_INSPECTIONS];
	float		Result1l;
	float		Result1r;
	float		Result11[4];


	void Clear()
	{
		for (int i = 0; i < MAX_INSPECTIONS; i++)
			resultSet[i].Clear();


		Result1l = Result1r = 0.0f;
		Result11[1] = Result11[2] = Result11[3] = Result11[0] = 0.0f;
	}

	void GetResultsArray(unsigned char* outArray)
	{
		int position = 0;
		unsigned __int64 address = (unsigned __int64)outArray;


		memcpy(&outArray[position], &resultSet[0], sizeof(ResultSet));
		memcpy(&outArray[position + 8], &Result1l, sizeof(float));
		position += 32;
		memcpy(&outArray[position], &resultSet[1], sizeof(ResultSet));
		memcpy(&outArray[position + 8], &Result1r, sizeof(float));
		position += 32;

		for (int i = 2; i < MAX_INSPECTIONS; i++)
		{
			memcpy(&outArray[position], &resultSet[i], sizeof(ResultSet));
			position += 32;
		}

	}
};

#endif