#include "CapturedImage.h"
#include <assert.h>

#ifndef SHARED_QUEUE_H
#define SHARED_QUEUE_H

using namespace std::chrono_literals;

class SharedImageQueue 
{
private:
	std::condition_variable counterVar;
	std::mutex lock;
	CapturedImage* image;
public:
	SharedImageQueue()
	{ 
		image = nullptr;
	}
	virtual ~SharedImageQueue()
	{
	}

	SharedImageQueue(SharedImageQueue&) = default;
	SharedImageQueue& operator=(SharedImageQueue&) = delete;
	
	CapturedImage* Pop()
	{
		std::unique_lock<std::mutex> lk(lock);
		counterVar.wait(lk, [&] {return image != nullptr; });
		CapturedImage* ret = image;
		image = nullptr;
		return ret;
	}

	bool Push(CapturedImage* item)
	{
		std::lock_guard<std::mutex> lk(lock);
		if (!image)
		{
			image = item;
			if (image->GetImageNumber() == -1)
				assert(false);
		}
		else
		{
			if (item->GetImageNumber() > 0)
				assert(false); //missed handling an image, very bad
			image->Unload();
			image = item;
			//TODO:
			//already has a waiting post
		}
		counterVar.notify_one();
		return true;
	}

	//Should stay small
	bool Clear()
	{
		bool ret = false;
		std::lock_guard<std::mutex> lk(lock);
		if (image)
			image->Unload();
		image = nullptr;
		return ret;
	}

	void StopWaiting()
	{
		Push(new StopCapturedImage());
	}
};

#endif //SHARED_QUEUE_H