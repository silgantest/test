#include "FloatingInspectionResultsPool.h"
// static method
FloatingInspectionResultsPool* FloatingInspectionResultsPool::instance;

void FloatingInspectionResultsPool::Initialize()
{
	instance = new FloatingInspectionResultsPool();
	instance->grow();
}

void FloatingInspectionResultsPool::Shutdown()
{
	delete instance;
}

void FloatingInspectionResultsPool::deleteObject(Reusable* obj)
{
	delete (FloatRangeResult*)obj;
}

void FloatingInspectionResultsPool::DeleteResultObj(FloatRangeResult* obj)
{
	instance->Delete(obj);
}

FloatingInspectionResultsPool::FloatingInspectionResultsPool()
{
	sprintf_s(name, "Cap85IntermediateImagePackPool");
}

FloatingInspectionResultsPool::~FloatingInspectionResultsPool()
{
	deleteEverything();
}

Reusable* FloatingInspectionResultsPool::create()
{
	return new FloatRangeResult();
};

FloatRangeResult* FloatingInspectionResultsPool::GetResultObj()
{
	return (FloatRangeResult*)instance->Get();
}

