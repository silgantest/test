#include "PLCcommProxy.h"
#include "DummyPLCcomm.h"


PLCcommProxy::Holder PLCcommProxy::theCommHolder = PLCcommProxy::Holder();


PLCcommProxy::PLCcommProxy()
{
}

PLCcommProxy::Holder::Holder()
{
	if (SerialPLCcomm::IsPortAvailable())
	{
		instance = new SerialPLCcomm();
	}
	else
	{
		instance = new DummyPLCcomm();
	}
}

PLCcommProxy::Holder::~Holder()
{
	delete instance;
}

PLCcommProxy::~PLCcommProxy()
{
}

PLCcomm* PLCcommProxy::GetPLCcomm()
{
	return theCommHolder.instance;
}
