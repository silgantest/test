#include <mutex>
#include <list>
#include "Reusable.h"
#include "InspectionResult.h"
#include "CapturedImage.h"
#include "IntermediateImagePack.h"
#include "Cap85IntermediateImagePack.h"
#include "ResultsStruct.h"
#include "ResultDetails.h"

#ifndef RESULTS_GROUP_H
#define RESULTS_GROUP_H

#define BOTTLE_CAMS 4
#define TOTAL_CAMS 5

class ResultsGroup : public Reusable
{

private:
	int imageNumber;
	int totalInspections;
	int numberInspections;

protected:
	std::list<InspectionResult*>* inspRes;
	CapturedImage* images[TOTAL_CAMS];
	ResultDetails* resultdetails[TOTAL_CAMS];
	IntermediateImagePack* packs[BOTTLE_CAMS];
	Cap85IntermediateImagePack* capPack;
	ResultsGroup();
	Results* results;

public:
	ResultsGroup(int num, int reqInsp, int numCameras);
	virtual ~ResultsGroup();

	virtual void AddInspectionResults(InspectionResult* res[], int count, CapturedImage* im, Cap85IntermediateImagePack* pack);
	virtual void AddInspectionResults(InspectionResult* res, CapturedImage* im, IntermediateImagePack* pack);

	virtual void AddDetails(ResultDetails* details, int index);
	virtual ResultDetails* GetDetails(int index);

	virtual bool Complete();
	int GetImageNumber();
	CapturedImage** GetImages(); //Called after all inpsections complete
	IntermediateImagePack** GetPacks(); //Called after all inpsections complete
	Cap85IntermediateImagePack* GetCapPack();//Called after all inpsections complete

	virtual void Reset() override;
	virtual void Unload() override;
	void Set(int imageNumber, int reqInsp, int numCameras);

	virtual Results* GetResultsSet();

	std::list<InspectionResult*>* GetResultsList();
};


#endif // RESULTS_GROUP_H
