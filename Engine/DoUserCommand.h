#include "InspectionCommand.h"

#ifndef DO_USER_COMMAND
#define DO_USER_COMMAND

//#define RGB(r,g,b) ((unsigned int)(((char)(r)|((unsigned short)((char)(g))<<8))|(((unsigned short)(char)(b))<<16)))


class DoUserCommand : InspectionCommand
{
private:
	int LineLength;
	int MainDisplayImageHeight;
	int MainDisplayImageWidth;
	char* im_trouble;
	int resultColor;
public:
	DoUserCommand(int MainDisplayImageHeight, int MainDisplayImageWidth)
	{
		int LineLength = 4 * (MainDisplayImageHeight / 2);
		this->MainDisplayImageWidth = MainDisplayImageWidth;
		this->MainDisplayImageHeight = MainDisplayImageHeight;
		im_trouble = (char*) malloc(100 * 32 * 4);
	}
	virtual ~DoUserCommand()
	{
		free(im_trouble);
	}

	DoUserCommand(DoUserCommand&) = delete;

	int GetResult()
	{
		return resultColor;
	}

	void Teach(char *im_srcIn, int x, int y)
	{
		return Execute(im_srcIn, x, y);
	}

	void Execute(char *im_srcIn, int x, int y)
	{
		int width = 60;
		int UserStrength = 0;

		int pixB2, pixR2, pixG2, pixB, pixR, pixG;

		int	totR, totB, totG;
		totR = totB = totG = 0;

		int	count = 0;
		int LineLength2 = 4 * width;
		int ypos = 0;
		int xwidth = 4 * 32;
		int yheight = width;

			int xpos = x;
			int filter = 255;//theapp->jobinfo[pframe->CurrentJobNum].colorFilter;

			if (xpos % 4 != 0)
			{
				xpos += 1;

				if (xpos % 4 != 0)
				{
					xpos += 1;	if (xpos % 4 != 0) { xpos += 1; }
				}
			}

			int xpos3 = 0;	int ypos3 = 32;

			if (xpos >= 0 && xpos <= MainDisplayImageWidth - 30 && y >= 0 && y <= MainDisplayImageHeight - 30)
			{
				for (ypos = y; ypos < yheight + y; ypos++)
				{
					for (int xpos2 = 4 * xpos; xpos2 < xwidth + 4 * xpos; xpos2 += 4)
					{
						pixB = *(im_srcIn + xpos2 + LineLength * ypos);
						pixG = *(im_srcIn + xpos2 + 1 + LineLength * ypos);
						pixR = *(im_srcIn + xpos2 + 2 + LineLength * ypos);

						if ((pixB <= filter && pixG <= filter) || (pixB <= filter && pixR <= filter) ||
							(pixG <= filter && pixR <= filter))
						{
							pixB2 = pixB;
							pixG2 = pixG;
							pixR2 = pixR;

							*(im_trouble + xpos3 + LineLength2 * ypos3) = pixB;
							*(im_trouble + xpos3 + 1 + LineLength2 * ypos3) = pixG;
							*(im_trouble + xpos3 + 2 + LineLength2 * ypos3) = pixR;

							count += 1;
							totR += pixR2; totG += pixG2; totB += pixB2;
						}
						else
						{
							*(im_trouble + xpos3 + LineLength2 * ypos3) = 0;
							*(im_trouble + xpos3 + 1 + LineLength2 * ypos3) = 0;
							*(im_trouble + xpos3 + 2 + LineLength2 * ypos3) = 0;
						}

						if (ypos3 > 0) { ypos3 -= 1; }
					}

					xpos3 += 4;	ypos3 = 31;
				}

				if (count > 0)
				{
					totB = totB / count; totR = totR / count; totG = totG / count;
					resultColor = RGB(totR, totG, totB);
					
				}
			}
		}
};


//TODO:
/*

	COLORREF resultColor = 0;

	int pixB2, pixR2, pixG2, pixB, pixR, pixG;

	int	totR, totB, totG;
	totR=totB=totG=0;

	int	count		=	0;
	int LineLength	=	4*(MainDisplayImageHeight/2);
	int LineLength2	=	4*width;
	int ypos		=	0;
	int xwidth		=	4*32;
	int yheight		=	width;

	int xpos		=	x;
	int filter		=	255;//theapp->jobinfo[pframe->CurrentJobNum].colorFilter;

	if(xpos % 4 !=0)
	{
		xpos+=1;

		if(xpos % 4 !=0)
		{ xpos+=1;	if(xpos%4 !=0)	{xpos+=1;} }
	}

	int xpos3=0;	int ypos3=32;

	if(xpos>=0 && xpos<=MainDisplayImageWidth-30 && y>=0 && y <=MainDisplayImageHeight-30)
	{
		for(ypos=y; ypos<yheight+y; ypos++)
		{
			for(int xpos2=4*xpos; xpos2<xwidth+4*xpos; xpos2+=4)
			{
				pixB	= *(im_srcIn + xpos2 +		LineLength*ypos);
				pixG	= *(im_srcIn + xpos2 + 1 +	LineLength*ypos);
				pixR	= *(im_srcIn + xpos2 + 2 +	LineLength*ypos);

				if( (pixB<=filter && pixG<=filter) || (pixB<=filter && pixR<=filter) ||
					(pixG<=filter && pixR<=filter)	)
				{
					pixB2=pixB;
					pixG2=pixG;
					pixR2=pixR;

					*(im_trouble + xpos3 +		LineLength2*ypos3)	= pixB;
					*(im_trouble + xpos3 + 1 +	LineLength2*ypos3)	= pixG;
					*(im_trouble + xpos3 + 2 +	LineLength2*ypos3)	= pixR;

					count+=1;
					totR+=pixR2; totG+=pixG2; totB+=pixB2;
				}
				else
				{
					*(im_trouble + xpos3 +		LineLength2*ypos3)	=	0;
					*(im_trouble + xpos3 + 1 +	LineLength2*ypos3)	=	0;
					*(im_trouble + xpos3 + 2 +	LineLength2*ypos3)	=	0;
				}

				if(ypos3>0)	{ypos3-=1;}
			}

			xpos3+=4;	ypos3=31;
		}

		if(count>0)
		{
			totB=totB/count; totR=totR/count; totG=totG/count;
			resultColor = RGB(totR,totG,totB);
		}
	}

	return resultColor;

*/

#endif //DO_USER_COMMAND