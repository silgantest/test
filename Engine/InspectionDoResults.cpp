#include "InspectionDoResults.h"
#include "PointResult.h"
#include "ImageFileUtilities.h"
#include "FloatRangeResult.h"

Bottle84DoResults::Bottle84DoResults(AppConfig* config) : config(config)
{
	ScaleSize = config->GetMainDisplayImageHeight() * config->GetMainDisplayImageWidth() * config->GetBytesPerPixel();
	ScaleSize = ScaleSize / 9;
	imgStitchedLigh = new unsigned char[ScaleSize];
	imgStitchedDark = new unsigned char[ScaleSize];
	imgStitchedEdg = new unsigned char[ScaleSize];

	imgStitchedFilLigh = new unsigned char[ScaleSize];
	imgStitchedFilDark = new unsigned char[ScaleSize];
	imgStitchedFilEdge = new unsigned char[ScaleSize];
		
	imgStitchedFilFinal = new unsigned char[ScaleSize];
	imgStitchedBW = new unsigned char[ScaleSize];
	jobSettings = new BottleResultJobSettings();
	Reset();
}


void Bottle84DoResults::Reset()
{
	memset(imgStitchedLigh, 0, ScaleSize);
	memset(imgStitchedDark, 0, ScaleSize);
	memset(imgStitchedEdg, 0, ScaleSize);
	memset(imgStitchedFilLigh, 0, ScaleSize);
	memset(imgStitchedFilDark, 0, ScaleSize);
	memset(imgStitchedFilEdge, 0, ScaleSize);
	memset(imgStitchedFilFinal, 0, ScaleSize);
	memset(imgStitchedBW, 0, ScaleSize);

	memset(NoLbl, 0, sizeof(TwoPoint) * BOTTLE_CAMS);
	
	memset(xNoLbl, 0, sizeof(int) * BOTTLE_CAMS);
	memset(yNoLbl, 0, sizeof(int) * BOTTLE_CAMS);
	memset(wNoLbl, 0, sizeof(int) * BOTTLE_CAMS);
	memset(hNoLbl, 0, sizeof(int) * BOTTLE_CAMS);
	memset(avgNoLabel, 0, sizeof(int) * BOTTLE_CAMS);
	memset(bottleXspliceIni, 0, sizeof(int) * BOTTLE_CAMS);
	memset(bottleYspliceIni, 0, sizeof(int) * BOTTLE_CAMS);
	memset(minSpliceAvg, 0, sizeof(int) * BOTTLE_CAMS);
	memset(varianceSplice, 0, sizeof(int) * BOTTLE_CAMS);
	memset(avgPixInRow, 0, sizeof(int) * BOTTLE_CAMS);
	memset(yposSplice, 0, sizeof(int) * BOTTLE_CAMS);
	memset(sdeviatiSplice, 0, sizeof(int) * BOTTLE_CAMS);
	memset(shiftedMidd, 0, sizeof(int) * BOTTLE_CAMS);
	memset(shiftedMiddWPoints, 0, sizeof(int) * BOTTLE_CAMS);
	memset(stitchX, 0, sizeof(int) * BOTTLE_CAMS);
	memset(stitchY, 0, sizeof(int) * BOTTLE_CAMS);
	memset(xAvoidIni, 0, sizeof(int) * BOTTLE_CAMS);
	memset(xAvoidEnd, 0, sizeof(int) * BOTTLE_CAMS);

	im_match3 = nullptr;
	im_mat3 = nullptr;

	outh0s = 0, outh1s = 0, outh2s = 0, outh3s = 0, outh4s = 0;
	outh5s = 0, outh6s = 0, outh7s = 0;
	outh1ms = 0, outh2ms = 0, outh3ms = 0, outh4ms = 0;
	outh5ms = 0, outh6ms = 0, outh7ms = 0;
	outh0 = 0, outh1 = 0, outh2 = 0, outh3 = 0;
	outh4 = 0, outh5 = 0, outh6 = 0, outh7 = 0;
	outh1m = 0, outh2m = 0, outh3m = 0, outh4m = 0;
	outh5m = 0, outh6m = 0, outh7m = 0;
}

BottleCap85DoResults::BottleCap85DoResults(AppConfig* config) : Bottle84DoResults(config)
{

}

Bottle84DoResults::~Bottle84DoResults()
{
	delete[] imgStitchedLigh;
	delete[] imgStitchedDark;
	delete[] imgStitchedEdg;

	delete[] imgStitchedFilLigh;
	delete[] imgStitchedFilDark;
	delete[] imgStitchedFilEdge;
}

void BottleCap85DoResults::DoResults(ResultsGroup* group)
{
	results = group->GetResultsSet();
	Bottle84DoResults::DoResults(group);
}

void Bottle84DoResults::SetupMins()
{
	for (int i = 0; i < 12; i++)
	{
		this->results->resultSet[i].Min = jobSettings->inspections[i]->GetMin();
		this->results->resultSet[i].Max = jobSettings->inspections[i]->GetMax();
		this->results->resultSet[i].enabled = jobSettings->inspections[i]->IsEnabled();
		this->results->resultSet[i].isFloat = jobSettings->inspections[i]->IsFloat();
	}

	if (jobSettings->bottleStyle != 3)
	{
		this->results->resultSet[5].Min >= jobSettings->inspections[5]->GetMinB();
	}
	else if (jobSettings->bottleStyle == 10)
	{
		this->results->resultSet[5].Max = jobSettings->inspections[5]->GetMaxC();
	}
}

void Bottle84DoResults::UpdateJob(Job* newJob)
{
	std::lock_guard<std::mutex> lk(mtx);
	jobSettings->boltOffset = newJob->GetBoltOffset();
	jobSettings->boltOffset2 = newJob->GetBoltOffset2();
	jobSettings->bottleStyle= newJob->GetBottleStyle();
	jobSettings->ellipseSens = newJob->GetEllipseSens();
	jobSettings->filterSelNolabel = newJob->GetFilterSelNoLabel();
	jobSettings->filterSelSplice = newJob->GetFilterSelSplice();
	jobSettings->findMiddles = newJob->GetFindMiddles();
	jobSettings->orienArea = newJob->GetOrienArea();
	jobSettings->hsplice = newJob->GetHSplice();
	jobSettings->hsplicesmBx = newJob->GetHSpliceSmBx();
	jobSettings->method = newJob->GetLabelIntegrityMethod();
	jobSettings->noLabelOffsetH = newJob->GetNoLabelOffsetH();
	jobSettings->noLabelOffsetW = newJob->GetNoLabelOffsetW();
	jobSettings->noLabelSens = newJob->GetNoLabelSens();
	jobSettings->rotOffset = newJob->GetRotOffset();
	jobSettings->spliceVar = newJob->GetSpliceVar();
	jobSettings->stDarkImg = newJob->GetStDarkImg();
	jobSettings->stLightImg = newJob->GetStLightImg();
	jobSettings->stEdgeImg = newJob->GetStEdgeImg();
	jobSettings->stitchH = newJob->GetStitchH();
	jobSettings->stitchW = newJob->GetStitchW();
	jobSettings->useStDark = newJob->GetUseStDark();
	jobSettings->useStLigh = newJob->GetUseStLight();
	jobSettings->useStEdge = newJob->GetUseStEdge();
	jobSettings->wehaveTempX = newJob->GetWeHaveTempX();
	jobSettings->wsplice = newJob->GetWSplice();
	for (int i = 0; i < BOTTLE_CAMS; i++)
	{
		jobSettings->xOffset[i] = newJob->GetNoLabelOffsetX(i);
		jobSettings->YSplice[i] = newJob->GetYSplice(i);
		jobSettings->stitchY[i] = newJob->GetStitchY(i);
	}
	for (int i = 0; i < INSPECTIONS_85; i++)
	{
		jobSettings->inspections[i] = newJob->GetInspection(i);
	}
	
	jobSettings->yStart = newJob->GetNoLabelOffset();

	jobSettings->stLightSen = newJob->GetStLightSen();
	jobSettings->stDarkSen = newJob->GetStDarkSen();
	jobSettings->stEdgeSen = newJob->GetStEdgeSen();
	jobSettings->avoidOffsetX = newJob->GetAvoidOffsetX();

}


void Bottle84DoResults::DoResults(ResultsGroup* group)
{
	std::lock_guard<std::mutex> lk(mtx);
	Inspection85Results* specGroup = (Inspection85Results*)group;
	results = group->GetResultsSet();

	int cam = -1;
	int camToCheck = -1;
	int minAvg = 0;
	int maxQtyPt = 0;

	labelPosY = -1;
	SpecialPoint posPattBy6;
	TwoPoint bestLeftRes;
	float bestLeft;
	
	float bestcorPattBy6;	int bestcamPattBy6;		int bestPattTypBy6;
	int fact = 3;
	int tmpMax = 0;
	int tmpMin = 10000;
	int sumHs = 0;
	TwoPoint maxHeightRes, minHeightRes;
	int minHeight = 1000000;
	SetupMins();
	switch (jobSettings->method)
	{
	case LabelIntegrityMethod::Method1:

		TwoPoint res = specGroup->GetBestLabelPos();
		camToCheck = specGroup->GetBestLabelPosIndex();
		labelBox.y = res.y;
		labelBox.x = res.x;
		labelPosY = res.y;
		break;
	case LabelIntegrityMethod::Method2:
		if (jobSettings->wehaveTempX)
		{
			//Finding the camera that shows the best Pattern for Left
			TwoPoint bestLeft = specGroup->GetBestLeftLabelPos();
			camLeft = specGroup->GetBestLeftLabelPosIndex();

			bestcamPattBy6 = camLeft;  //Which camera gives the highest corelation
			bestcorPattBy6 = bestLeft.x1; //Highest corelation
			posPattBy6 = specGroup->GetPattBy6();
			bestPattTypBy6 = 1;

			resBest = bestLeft.x1; //Best correlation at higher Scale
			//resBest		=	pattLeftPosBy6[camLeft].c; //Best correlation at higher Scale
			cam = camLeft;
			camToCheck = camLeft;
			labelBox.y = bestLeft.y;
			labelBox.x = bestLeft.x;
			labelPosY = bestLeft.y;
		}
		break;
	case LabelIntegrityMethod::Method3:

		maxQtyPt = 0;
		TwoPoint minAvgRes = specGroup->GetMinAvgLabelPos();
		minAvg = minAvgRes.x1;
		cam = specGroup->GetMinAvgLabelPosIndex();
		camToCheck = cam;
		labelBox.x = minAvgRes.x;
		labelBox.y = minAvgRes.y;
		labelPosY = minAvgRes.y;

		//Finding camera with the best correlation
		resBest = 100; //updating best correlation
		if (labelBox.y == config->GetCroppedCameraWidth() / 3)
		{
			labelBox.y = 0;
		}
		break;
	case LabelIntegrityMethod::Method4:
		minHeightRes = specGroup->GetMinHeightLabelPos();
		minHeight = minHeightRes.y;
		resBest = 100; //updating best correlation
		cam = specGroup->GetMinHeightLabelPosIndex();
		camToCheck = cam;
		labelBox.y = minHeightRes.y;
		labelBox.x = minHeightRes.x;
		labelPosY = minHeightRes.y;

		break;
	case LabelIntegrityMethod::Method5:
		fact = 3;
		tmpMax = 0;
		tmpMin = 10000;
		sumHs = specGroup->GetSumHeights();

		minHeightRes = specGroup->GetMinHeightLabelPos();
		maxHeightRes = specGroup->GetMaxHeightLabelPos();

		resBest = 100;

		sumHs -= maxHeightRes.y; sumHs -= minHeightRes.y;
		labelPosY = (sumHs / 2) / fact;
		break;

	case LabelIntegrityMethod::Method6:
		//Finding the camera that shows the best Pattern for Left
		bestLeftRes = specGroup->GetBestLeftLabelPos();
		bestLeft = bestLeftRes.x1;

		camLeft = specGroup->GetBestLeftLabelPosIndex();

		bestcamPattBy6 = camLeft;  //Which camera gives the highest corelation
		bestcorPattBy6 = bestLeft; //Highest corelation
		posPattBy6.x = bestLeftRes.x * 3; //pattLeftPosBy6[camLeft];  //posPattBy6 is used in Ondraw to Draw Red Box
		posPattBy6.y = bestLeftRes.y * 3;
		bestPattTypBy6 = 1;

		resBest = bestLeft; //Best correlation at higher Scale
		//resBest		=	pattLeftPosBy6[camLeft].c; //Best correlation at higher Scale
		cam = camLeft;
		camToCheck = camLeft;
		labelBox.y = bestLeftRes.y;  //By 3 Resolution Best Corelation Y Cord.
		labelBox.x = bestLeftRes.x;  //By 3 Resolution Best Corelation X Cord.
		labelPosY = bestLeftRes.y;
		break;
	default:
		break;
	}

	if(jobSettings->bottleStyle == 10)
		DoLipton(specGroup);

	fact = 3; int  sumAvg = 0;

	for (int i = 0; i < 4; i++) //Four cameras, then run this four times
	{
		NoLbl[i] = CheckLabelNatural(specGroup->GetPacks()[i], fact); sumAvg += NoLbl[i].x;
	} //qty of pixels that exceed a threshold

	results->resultSet[7].Value = sumAvg / 4.0f;

	results->resultSet[8].Value = Result9(specGroup);
	Stitch(camToCheck, specGroup, labelBox);
	if (jobSettings->bottleStyle == 3)
		DoWolverine(cam, specGroup);
	else
		DoNotWolverine();
	Result11();
	CreateResultObj();
	SendEverywhere();
}

TwoPoint Bottle84DoResults::CheckLabelNatural(IntermediateImagePack* pack, int fact)
{
	unsigned char* noLabelDiv3 = pack->GetFilter(jobSettings->filterSelNolabel);

	TwoPoint result;
	result.x = result.y = result.x1 = result.y1 = 0;

	int Length = config->GetMainDisplayImageWidth() / fact;
	int Height = config->GetMainDisplayImageHeight() / fact;
	int nCam = pack->camIndex;
	int xOffset = jobSettings->xOffset[nCam] / fact;

	wNoLbl[nCam] = jobSettings->noLabelOffsetW / fact;
	hNoLbl[nCam] = jobSettings->noLabelOffsetH / fact;

	xNoLbl[nCam] = min(xOffset + 30, Length - wNoLbl[nCam] - 5);
	yNoLbl[nCam] = min(jobSettings->yStart, Height - hNoLbl[nCam] - 5);

	int d1 = 0;
	int pixa;
	int Thresh = jobSettings->noLabelSens;
	int sumPix = 0;
	int nPix = 0;

	for (int xpos = xNoLbl[nCam]; xpos <= (xNoLbl[nCam] + wNoLbl[nCam]); xpos++)
	{
		for (int ypos = yNoLbl[nCam]; ypos <= yNoLbl[nCam] + hNoLbl[nCam]; ypos++)
		{
			pixa = *(noLabelDiv3 + xpos + Length * ypos);

			if (pixa >= Thresh) { result.x += 1; }

			sumPix += pixa;	nPix++;
		}
	}

	if (nPix != 0) avgNoLabel[nCam] = sumPix / nPix;

	return result;
}

void Bottle84DoResults::DoNotWolverine()
{
	results->resultSet[4].Value = (jobSettings->orienArea - labelPosY) / 2;	//Height
		//if(Result5>0)	{Result5-=1;}
		//if(Result5<0)	{Result5+=1;}

	int value2 = int(results->resultSet[4].Value);
	int		outh0s = 0, outh1s = 0, outh2s = 0, outh3s = 0, outh4s = 0;
	int		outh5s = 0, outh6s = 0, outh7s = 0;
	int		outh1ms = 0, outh2ms = 0, outh3ms = 0, outh4ms = 0;
	int		outh5ms = 0, outh6ms = 0, outh7ms = 0;

	int		outh0 = 0, outh1 = 0, outh2 = 0, outh3 = 0;
	int		outh4 = 0, outh5 = 0, outh6 = 0, outh7 = 0;
	int		outh1m = 0, outh2m = 0, outh3m = 0, outh4m = 0;
	int		outh5m = 0, outh6m = 0, outh7m = 0;

	switch (value2)
	{
	case 0:	outh0 += 1;	outh0s += 1;	break;
	case 1: outh1 += 1;	outh1s += 1;	break;
	case 2: outh2 += 1;	outh2s += 1;	break;
	case 3: outh3 += 1;	outh3s += 1;	break;
	case 4: outh4 += 1;	outh4s += 1;	break;
	case 5: outh5 += 1;	outh5s += 1;	break;
	case 6: outh6 += 1;	outh6s += 1;	break;
	case -1:outh1m += 1;	outh1ms += 1;	break;
	case -2:outh2m += 1;	outh2ms += 1;	break;
	case -3:outh3m += 1;	outh3ms += 1;	break;
	case -4:outh4m += 1;	outh4ms += 1;	break;
	case -5:outh5m += 1;	outh5ms += 1;	break;
	case -6:outh6m += 1;	outh6ms += 1;	break;
	}

	if (results->resultSet[4].Value >= 7) { outh7 += 1;	outh7s += 1; }
	if (results->resultSet[4].Value <= -7) {outh7m += 1;	outh7ms += 1; }

	results->resultSet[6].Value = resBest;
}

TwoPoint Bottle84DoResults::FindPattern3(unsigned char* im_srcIn)
{
	TwoPoint res;
	res.x = res.y = res.x1 = 50;

	int Limit = 40;
	int d1 = 0;
	int r, q;

	int LineLength = config->GetCroppedCameraHeight() / 3;
	int Height = config->GetCroppedCameraWidth() / 3;

	bool debugging = false;

	if (debugging) {  ImageFileUtilities::SaveGrayBitmapToFile(im_srcIn, LineLength, Height, "findpattern3.bmp"); }

	int		LineLength2 = 12;
	float	n = LineLength2 * LineLength2;

	int		xpos = 1;
	int		xpos2 = 0;
	int		ypos = 1;
	int		ypos2 = 0;
	int		xpos3 = 0;

	float	sumxy = 0;
	float	sumx = 0;
	float	sumy = 0;
	float	sumxsq = 0;
	float	sumysq = 0;

	float	f0 = 0;
	float	f1 = 0;
	float	f2 = 0;

	float	corr = 0;

	float savedcorr = 0.3f;
	int savedpos = 0;
	int cnt = 0;
	int startY = jobSettings->boltOffset2 - jobSettings->boltOffset;

	if (startY <= 10) { startY = 40; }
	if (startY >= 300) { startY = 300; }

	for (int w = startY; w <= startY + 12; w++)//22
	{
		for (q = 40; q <= 90; q++)
		{
			for (ypos = -6; ypos < 6; ypos++)
			{
				for (xpos3 = -6; xpos3 < 6; xpos3++)
				{
					*(im_match3 + xpos2 + LineLength2 * (ypos2)) = *(im_srcIn + xpos3 + q + LineLength * (ypos + w));
					xpos2 += 1;
				}

				xpos2 = 0;
				ypos2 += 1;
			}

			ypos2 = 0;

			//calculate correlation
			//OnSaveBW(im_match2, 6);

			sumxy = 0.0; sumx = 0.0; sumy = 0.0; sumxsq = 0.0; sumysq = 0.0;

			for (r = 0; r < n; r++)
			{
				sumxy += (*(im_mat3 + r)) * (*(im_match3 + r));
				sumx += *(im_mat3 + r);
				sumy += *(im_match3 + r);
				sumxsq += (*(im_mat3 + r)) * (*(im_mat3 + r));
				sumysq += (*(im_match3 + r)) * (*(im_match3 + r));
			}

			f0 = sumxy - ((sumx * sumy) / n);
			f1 = sumxsq - ((sumx * sumx) / n);
			f2 = sumysq - ((sumy * sumy) / n);

			if (sqrtf(f1 * f2) != 0.0) { corr = f0 / sqrtf(f1 * f2); }
			else { corr = 0; }

			if (corr >= savedcorr)
			{
				savedcorr = corr;
				savedpos = q;
				res.x1 = int(savedcorr * 100);
				res.y = w + 6;
				res.x = q + 6;
			}
		}
	}

	return res;
}

void Bottle84DoResults::DoWolverine(int cam, Inspection85Results* specGroup)
{
	TwoPoint Ledge = FindLedge3(specGroup->GetPacks()[cam]->bw,
		specGroup->GetLabelResults(cam)->GetLabelPos(), cam);

	if (cam == 3)
	{
		//TODO we do nothing with this value
//		neckEdge = FindPattern3(specGroup->GetImagePack(cam)->bw);
		FindPattern3(specGroup->GetPacks()[cam]->bw);
	}

	int zerobot = jobSettings->orienArea + 13;
	results->resultSet[4].Value = (zerobot -labelPosY) / 2;	//Height

	if (results->resultSet[4].Value > 0) { results->resultSet[4].Value -= 1; }
	if (results->resultSet[4].Value < 0) { results->resultSet[4].Value += 1; }

	int value2 = int(results->resultSet[4].Value);

	switch (value2)
	{
	case 0:	outh0 += 1;	break;	case 1: outh1 += 1;	break;
	case 2: outh2 += 1;	break;	case 3: outh3 += 1;	break;
	case 4: outh4 += 1;	break;	case 5: outh5 += 1;	break;
	case 6: outh6 += 1;	break;	case -1:outh1m += 1;	break;
	case -2:outh2m += 1;	break;	case -3:outh3m += 1;	break;
	case -4:outh4m += 1;	break;	case -5:outh5m += 1;	break;
	case -6:outh6m += 1;	break;
	}

	results->resultSet[6].Value = resBest;
	bool integrityOK;
	if (results->resultSet[6].Value >= results->resultSet[6].Min && results->resultSet[6].Value <= results->resultSet[6].Max)
	{
		integrityOK = true;
	}
	else { integrityOK = false; }
	if (!integrityOK) { results->resultSet[4].Value = 7; }
	if (results->resultSet[4].Value >= 7) { outh7 += 1; }
	if (results->resultSet[4].Value <= -7) {outh7m += 1; }

	//Result6=(Ledge.y1);//rotate
	//TODO We do nothing with this value
	//theapp->bottlepos = Ledge.y1;

	int modRes = (135 - Ledge.x) / 2;
	//TODO We do nothing with this value
	//theapp->labelpos = modRes;

	results->resultSet[5].Value = (modRes - Ledge.y1) - jobSettings->rotOffset;
	results->resultSet[5].Value = results->resultSet[5].Value * 0.9f;
	int		out0 = 0, out1 = 0, out2 = 0, out3 = 0, out4 = 0, out5 = 0, out6 = 0, out7 = 0, out8 = 0, out9 = 0;
	int		out10 = 0, out11 = 0, out12 = 0, out13 = 0, out14 = 0, out15 = 0, out16 = 0, out17 = 0, out18 = 0, out19 = 0;
	int		out1m = 0, out2m = 0, out3m = 0, out4m = 0, out5m = 0, out6m = 0, out7m = 0, out8m = 0, out9m = 0;
	int		out10m = 0, out11m = 0, out12m = 0, out13m = 0, out14m = 0, out15m = 0, out16m = 0, out17m = 0, out18m = 0, out19m = 0;


	if (results->resultSet[5].Value >= 8) { results->resultSet[5].Value -= 2; }
	if (results->resultSet[5].Value >= 3) { results->resultSet[5].Value -= 1; }
	if (results->resultSet[5].Value <= -8) { results->resultSet[5].Value += 2; }
	if (results->resultSet[5].Value <= -3) { results->resultSet[5].Value += 1; }
	if (results->resultSet[5].Value >= 20) { results->resultSet[5].Value = 20; }
	if (results->resultSet[5].Value <= -20) { results->resultSet[5].Value = -20; }

	if (results->resultSet[5].Value <= -19) { out19m += 1; }

	int value = int(results->resultSet[5].Value);

	switch (value)
	{
	case 0:	out0 += 1;	break;	case 1:	out1 += 1;	break;
	case 2:	out2 += 1;	break;	case 3:	out3 += 1;	break;
	case 4:	out4 += 1;	break;	case 5:	out5 += 1;	break;
	case 6:	out6 += 1;	break;	case 7:	out7 += 1;	break;
	case 8:	out8 += 1;	break;	case 9:	out9 += 1;	break;
	case 10:out10 += 1;	break;	case 11:out11 += 1;	break;
	case 12:out12 += 1;	break;	case 13:out13 += 1;	break;
	case 14:out14 += 1;	break;	case 15:out15 += 1;	break;
	case 16:out16 += 1;	break;	case 17:out17 += 1;	break;
	case 18:out18 += 1;	break;

	case -1:out1m += 1;	break;
	case -2:out2m += 1;	break;	case -3:out3m += 1;	break;
	case -4:out4m += 1;	break;	case -5:out5m += 1;	break;
	case -6:out6m += 1;	break;	case -7:out7m += 1;	break;
	case -8:out8m += 1;	break;	case -9:out9m += 1;	break;
	case -10:out10m += 1;	break;	case -11:out11m += 1;	break;
	case -12:out12m += 1;	break;	case -13:out13m += 1;	break;
	case -14:out14m += 1;	break;	case -15:out15m += 1;	break;
	case -16:out16m += 1;	break;	case -17:out17m += 1;	break;
	case -18:out18m += 1;	break;
	}

	if (results->resultSet[5].Value >= 19) { out19 += 1; }
	if (results->resultSet[5].Value <= -19) { out19m += 1; }
}

TwoPoint Bottle84DoResults::FindLedge3(unsigned char* image, TwoPoint Bolt, int cam)
{
	TwoPoint result;
	result.x = result.y = result.x1 = result.y1 = 50;

	result.x = Bolt.x;

	int pixa, pixb, pixc, pixd, pixe, pixf;
	int pixa2, pixb2, pixc2, pixd2, pixe2, pixf2;
	int pixa3, pixb3, pixc3, pixd3, pixe3, pixf3;

	int smallValue = 0;

	SinglePoint posR, posL;
	SinglePoint posR2, posL2;

	int d1, d2, d3; d1 = d2 = d3 = 0;

	int combined = 0;
	int startX = 46;
	int endX = 83;


	int startY = jobSettings->boltOffset2;

	if (startY >= 340) { startY = 340; }
	if (startY <= 30) { startY = 30; }

	int endY = startY - jobSettings->boltOffset;

	int xposR = 83;
	int xposR2 = 73;
	int xposL2 = 56;
	int xposL = 46;

	int h, g;//,j,k,d,t;
	int smallest = 1;

	int LineLength2 = config->GetCroppedCameraHeight() / 3;

	int o = 0;
	int p = 0;
	int count = 5;
	int avg = 0;
	int finalShade = 0;
	int thresh = jobSettings->ellipseSens;
	int avgy = 0;
	int best = 0;
	int shift = 30;
	int left = 0;
	int right = 0;
	int direction = 0;

	for (h = startX; h < endX; h += 2)
	{
		for (g = startY; g > endY; g--)
		{
			pixa = *(image + h + 1 + (LineLength2 * (g)));
			pixb = *(image + h + (LineLength2 * (g)));
			pixc = *(image + h - 1 + (LineLength2 * (g)));

			pixd = *(image + h + 1 + (LineLength2 * (g + 4)));
			pixe = *(image + h + (LineLength2 * (g + 4)));
			pixf = *(image + h - 1 + (LineLength2 * (g + 4)));

			d1 = ((pixa + 2 * pixb + pixc) - (pixd + 2 * pixe + pixf));

			if (d1 >= thresh)
			{
				ledgePoint[count].x = h; ledgePoint[count].y = g; g = endY;
			}
			else
			{
				ledgePoint[count].x = 0; ledgePoint[count].y = 200;
			}
		}

		count += 1;
	}

	for (int jj = 7; jj < count; jj++)
	{
		direction = ledgePoint[jj].y - ledgePoint[jj - 1].y;

		if (direction < 0) { left += 1; }
		if (direction > 0) { right += 1; }
	}

	h = xposR;

	for (g = startY; g > endY; g--)
	{
		pixa = *(image + h + 1 + (LineLength2 * (g)));
		pixb = *(image + h + (LineLength2 * (g)));
		pixc = *(image + h - 1 + (LineLength2 * (g)));

		pixd = *(image + h + 1 + (LineLength2 * (g + 4)));
		pixe = *(image + h + (LineLength2 * (g + 4)));
		pixf = *(image + h - 1 + (LineLength2 * (g + 4)));

		pixa2 = *(image + h + 1 + 1 + (LineLength2 * (g - 1)));
		pixb2 = *(image + h + 1 + (LineLength2 * (g - 1)));
		pixc2 = *(image + h - 1 + 1 + (LineLength2 * (g - 1)));

		pixd2 = *(image + h + 1 + 1 + (LineLength2 * (g + 3)));
		pixe2 = *(image + h + 1 + (LineLength2 * (g + 3)));
		pixf2 = *(image + h - 1 + 1 + (LineLength2 * (g + 3)));

		pixa3 = *(image + h + 1 + 2 + (LineLength2 * (g - 2)));
		pixb3 = *(image + h + 2 + (LineLength2 * (g - 2)));
		pixc3 = *(image + h - 1 + 2 + (LineLength2 * (g - 2)));

		pixd3 = *(image + h + 1 + 2 + (LineLength2 * (g + 2)));
		pixe3 = *(image + h + 2 + (LineLength2 * (g + 2)));
		pixf3 = *(image + h - 1 + 2 + (LineLength2 * (g + 2)));

		d1 = ((pixa + 2 * pixb + pixc) - (pixd + 2 * pixe + pixf));
		d2 = ((pixa2 + 2 * pixb2 + pixc2) - (pixd2 + 2 * pixe2 + pixf2));
		d3 = ((pixa3 + 2 * pixb3 + pixc3) - (pixd3 + 2 * pixe3 + pixf3));

		if (d1 >= thresh && d2 >= thresh && d3 >= thresh)
		{
			ledgePoint[1].x = posR.x = h; ledgePoint[1].y = posR.y = g; g = endY;
		}
		else
		{
			ledgePoint[1].x = posR.x = 0; ledgePoint[1].y = posR.y = 200;
		}
	}

	h = xposR2;

	for (g = startY; g > endY; g--)
	{
		pixa = *(image + h + 1 + LineLength2 * (g));
		pixb = *(image + h + LineLength2 * (g));
		pixc = *(image + h - 1 + LineLength2 * (g));

		pixd = *(image + h + 1 + LineLength2 * (g + 4));
		pixe = *(image + h + LineLength2 * (g + 4));
		pixf = *(image + h - 1 + LineLength2 * (g + 4));

		pixa2 = *(image + h + 1 + 1 + LineLength2 * (g - 1));
		pixb2 = *(image + h + 1 + LineLength2 * (g - 1));
		pixc2 = *(image + h - 1 + 1 + LineLength2 * (g - 1));

		pixd2 = *(image + h + 1 + 1 + LineLength2 * (g + 3));
		pixe2 = *(image + h + 1 + LineLength2 * (g + 3));
		pixf2 = *(image + h - 1 + 1 + LineLength2 * (g + 3));

		pixa3 = *(image + h + 1 + 2 + LineLength2 * (g - 2));
		pixb3 = *(image + h + 2 + LineLength2 * (g - 2));
		pixc3 = *(image + h - 1 + 2 + LineLength2 * (g - 2));

		pixd3 = *(image + h + 1 + 2 + LineLength2 * (g + 2));
		pixe3 = *(image + h + 2 + LineLength2 * (g + 2));
		pixf3 = *(image + h - 1 + 2 + LineLength2 * (g + 2));

		d1 = ((pixa + 2 * pixb + pixc) - (pixd + 2 * pixe + pixf));
		d2 = ((pixa2 + 2 * pixb2 + pixc2) - (pixd2 + 2 * pixe2 + pixf2));
		d3 = ((pixa3 + 2 * pixb3 + pixc3) - (pixd3 + 2 * pixe3 + pixf3));

		if (d1 >= thresh && d2 >= thresh && d3 >= thresh)
		{
			ledgePoint[2].x = posR2.x = h; ledgePoint[2].y = posR2.y = g; g = endY;
		}
		else
		{
			ledgePoint[2].x = posR2.x = 0;	ledgePoint[2].y = posR2.y = 200;
		}
	}

	h = xposL;

	for (g = startY; g > endY; g--)
	{
		pixa = *(image + h + 1 + LineLength2 * (g));
		pixb = *(image + h + LineLength2 * (g));
		pixc = *(image + h - 1 + LineLength2 * (g));

		pixd = *(image + h + 1 + LineLength2 * (g + 4));
		pixe = *(image + h + LineLength2 * (g + 4));
		pixf = *(image + h - 1 + LineLength2 * (g + 4));

		pixa2 = *(image + h + 1 + 1 + LineLength2 * (g + 1));
		pixb2 = *(image + h + 1 + LineLength2 * (g + 1));
		pixc2 = *(image + h - 1 + 1 + LineLength2 * (g + 1));

		pixd2 = *(image + h + 1 + 1 + LineLength2 * (g + 5));
		pixe2 = *(image + h + 1 + LineLength2 * (g + 5));
		pixf2 = *(image + h - 1 + 1 + LineLength2 * (g + 5));

		pixa3 = *(image + h + 1 + 2 + LineLength2 * (g + 2));
		pixb3 = *(image + h + 2 + LineLength2 * (g + 2));
		pixc3 = *(image + h - 1 + 2 + LineLength2 * (g + 2));

		pixd3 = *(image + h + 1 + 2 + LineLength2 * (g + 6));
		pixe3 = *(image + h + 2 + LineLength2 * (g + 6));
		pixf3 = *(image + h - 1 + 2 + LineLength2 * (g + 6));

		d1 = ((pixa + 2 * pixb + pixc) - (pixd + 2 * pixe + pixf));
		d2 = ((pixa2 + 2 * pixb2 + pixc2) - (pixd2 + 2 * pixe2 + pixf2));
		d3 = ((pixa3 + 2 * pixb3 + pixc3) - (pixd3 + 2 * pixe3 + pixf3));

		if (d1 >= thresh && d2 >= thresh && d3 >= thresh)
		{
			ledgePoint[3].x = posL.x = h; ledgePoint[3].y = posL.y = g; g = endY;
		}
		else
		{
			ledgePoint[3].x = posL.x = 0; ledgePoint[3].y = posL.y = 200;
		}
	}

	h = xposL2;

	for (g = startY; g > endY; g--)
	{
		pixa = *(image + h + 1 + LineLength2 * (g));
		pixb = *(image + h + LineLength2 * (g));
		pixc = *(image + h - 1 + LineLength2 * (g));

		pixd = *(image + h + 1 + LineLength2 * (g + 4));
		pixe = *(image + h + LineLength2 * (g + 4));
		pixf = *(image + h - 1 + LineLength2 * (g + 4));

		pixa2 = *(image + h + 1 + 1 + LineLength2 * (g + 1));
		pixb2 = *(image + h + 1 + LineLength2 * (g + 1));
		pixc2 = *(image + h - 1 + 1 + LineLength2 * (g + 1));

		pixd2 = *(image + h + 1 + 1 + LineLength2 * (g + 5));
		pixe2 = *(image + h + 1 + LineLength2 * (g + 5));
		pixf2 = *(image + h - 1 + 1 + LineLength2 * (g + 5));

		pixa3 = *(image + h + 1 + 2 + LineLength2 * (g + 2));
		pixb3 = *(image + h + 2 + LineLength2 * (g + 2));
		pixc3 = *(image + h - 1 + 2 + LineLength2 * (g + 2));

		pixd3 = *(image + h + 1 + 2 + LineLength2 * (g + 6));
		pixe3 = *(image + h + 2 + LineLength2 * (g + 6));
		pixf3 = *(image + h - 1 + 2 + LineLength2 * (g + 6));

		d1 = ((pixa + 2 * pixb + pixc) - (pixd + 2 * pixe + pixf));
		d2 = ((pixa2 + 2 * pixb2 + pixc2) - (pixd2 + 2 * pixe2 + pixf2));
		d3 = ((pixa3 + 2 * pixb3 + pixc3) - (pixd3 + 2 * pixe3 + pixf3));

		if (d1 >= thresh && d2 >= thresh && d3 >= thresh)
		{
			ledgePoint[4].x = posL2.x = h; ledgePoint[4].y = posL2.y = g; g = endY;
		}
		else
		{
			ledgePoint[4].x = posL2.x = 0;	ledgePoint[4].y = posL2.y = 200;
		}
	}

	int pR = startY - posR.y;
	int pR2 = startY - posR2.y;
	int pL = startY - posL.y;
	int pL2 = startY - posL2.y;

	if (pR2 > pL2) { result.y1 = 38 + pR2; }
	else if (pL2 > pR2) { result.y1 = 42 - pL2; }

	if (pR2 > pR && pL2 < 0 && pL < 0) { result.y1 = 36 - pR2; }
	if (pL2 > pL && pR2 < 0 && pR < 0) { result.y1 = 38 + pL2; }

	if (pR2 < 15 && pL2 < 15)
	{
		if (pR >= pL) { result.y1 = 34 + pR; }
		else if (pL > pR) { result.y1 = 46 - pL; }
	}

	if (pR2 < 0 && pL2 < 0 && pL < 0) { result.y1 = 34 - pR; }
	if (pR2 < 0 && pL2 < 0 && pR < 0) { result.y1 = 38 + pL; }

	result.x1 = Bolt.x;

	int modRes = (135 - Bolt.x) / 2;

	if (modRes <= 25) { if (left > right) result.y1 = 200; }
	if (modRes >= 54) { if (left < right) result.y1 = 200; }

	float resTemp = float(result.y1) * 0.94;
	result.y1 = (int) resTemp;

	return result;
}

void Bottle84DoResults::calcTearCoordinates(Inspection85Results* specGroup)
{
	
	int stitchW = jobSettings->stitchW;
	int stitchH = jobSettings->stitchH;

	int offsetX = abs(jobSettings->avoidOffsetX);
	if (offsetX > stitchW / 4) { offsetX = stitchW / 4; }
	int i;

	for (i = 0; i < 4; i++)
	{
		if (jobSettings->findMiddles)
		{
			stitchX[i] = specGroup->GetLabelResults(i)->GetXMiddle()- stitchW / 2;
		}
		else
		{
			int LengthOrig = config->GetMainDisplayImageWidth();
			stitchX[i] = (LengthOrig - stitchW) / 2;
		}

		stitchY[i] = jobSettings->stitchY[i];

		/////////////////////
		//Finding the avoided areas in each camera
	}

	if (jobSettings->bottleStyle == 10)
	{
		for (i = 0; i < 4; i++)
		{
			xAvoidIni[i] = (i - 1) * stitchW + ((specGroup->GetLabelResults(i)->GetMiddleShiftWPoints() - offsetX) - stitchX[i]);
			xAvoidEnd[i] = xAvoidIni[i] + 2 * offsetX;
		}
	}
	else
	{
		for (i = 0; i < 4; i++)
		{
			xAvoidIni[i] = (i - 1) * stitchW + ((specGroup->GetLabelResults(i)->GetXMiddle() - offsetX) - stitchX[i]);
			xAvoidEnd[i] = xAvoidIni[i] + 2 * offsetX;
		}
	}

	//just to avoid out of range limits
	for (i = 0; i < 4; i++)
	{
		if (xAvoidIni[i]<0 || xAvoidEnd[i]>(i) * stitchW)
		{
			xAvoidIni[i] = (i - 1) * stitchW + ((specGroup->GetLabelResults(i)->GetXMiddle() - offsetX) - stitchX[i]);
			xAvoidEnd[i] = xAvoidIni[i] + 2 * offsetX;
		}

	}
}

void Bottle84DoResults::Stitch(int camToCheck, Inspection85Results* specGroup, TwoPoint labelBox)
{
	bool takeC1, takeC2, takeC3, takeC4;
	takeC1 = takeC2 = takeC3 = takeC4 = false;

	if (camToCheck == 1) { takeC4 = takeC1 = takeC2 = true; }
	else if (camToCheck == 2) { takeC1 = takeC2 = takeC3 = true; }
	else if (camToCheck == 3) { takeC2 = takeC3 = takeC4 = true; }
	else if (camToCheck == 4) { takeC3 = takeC4 = takeC1 = true; }

	int fct = 1; 

	for (int c = 1; c <= 4; c++) { stitchX[c] = 0; stitchY[c] = 0; }

	calcTearCoordinates(specGroup);

	float qtyPixels = Stitch4ImgX(specGroup, 
		camToCheck, labelBox,
		takeC1, takeC2, takeC3, takeC4, fct = 3);

	CopyStitchedImages();

	//Result6 = edgeProp;
	results->resultSet[9].Value = float(qtyPixels);
}

void Bottle84DoResults::CopyStitchedImages()
{
	//TODO:assert(FALSE);
}


int Bottle84DoResults::Stitch4ImgX(Inspection85Results* specGroup, int camPatt, TwoPoint pattPos,
	bool takecam1, bool takecam2, bool takecam3, bool takecam4, int fact)
{

	//int wBox, int hBox;
	/*It appeared that in the original code, these values were overwritten*/

	unsigned char* im_Ligh[4];
	unsigned char* im_Dark[4];
	unsigned char* im_Edge[4];

	unsigned char* im_LighSeq[4];
	unsigned char* im_DarkSeq[4];
	unsigned char* im_EdgeSeq[4];

	for (int i = 0; i < 4; i++)
	{
		im_Ligh[i] = specGroup->GetPacks()[i]->GetFilter(jobSettings->stLightImg);
		im_Dark[i] = specGroup->GetPacks()[i]->GetFilter(jobSettings->stDarkImg);
		im_Edge[i] = specGroup->GetPacks()[i]->GetFilter(jobSettings->stEdgeImg);
	}

	int wBx = jobSettings->stitchW / fact;	int hBx = jobSettings->stitchH / fact;

	int xBx1 = stitchX[0] / fact;	int yBx1 = stitchY[0] / fact;
	int xBx2 = stitchX[1] / fact;	int yBx2 = stitchY[1] / fact;
	int xBx3 = stitchX[2] / fact;	int yBx3 = stitchY[2] / fact;
	int xBx4 = stitchX[3] / fact;	int yBx4 = stitchY[3] / fact;

	int xbx1 = 0;	int ybx1 = 0;
	int xbx2 = 0;	int ybx2 = 0;
	int xbx3 = 0;	int ybx3 = 0;
	int xbx4 = 0;	int ybx4 = 0;

	int StitchedLength = 4 * wBx;
	int StitchedHeight = hBx;

	int cam;
	int camLength = config->GetMainDisplayImageWidth() / fact;
	int camHeight = config->GetMainDisplayImageHeight() / fact;

	int offX;// pixS, ;
	int y;

	bool debugging = false;

	//////////////////

	int thLight = jobSettings->stLightSen;
	int thDark = jobSettings->stDarkSen;
	int thEdges = jobSettings->stEdgeSen;

	//Always show pattern at the SECOND position
	if (camPatt == 0) { camPatt = 1; }
	camPatt = 2;

	if (camPatt == 1)
	{
		im_LighSeq[0] = im_Ligh[3]; im_LighSeq[1] = im_Ligh[0]; im_LighSeq[2] = im_Ligh[1]; im_LighSeq[3] = im_Ligh[2];
		im_DarkSeq[0] = im_Dark[3]; im_DarkSeq[1] = im_Dark[0]; im_DarkSeq[2] = im_Dark[1]; im_DarkSeq[3] = im_Dark[2];
		im_EdgeSeq[0] = im_Edge[3]; im_EdgeSeq[1] = im_Edge[0]; im_EdgeSeq[2] = im_Edge[1]; im_EdgeSeq[3] = im_Edge[2];
		xbx1 = xBx4;	ybx1 = yBx4;
		xbx2 = xBx1;	ybx2 = yBx1;
		xbx3 = xBx2;	ybx3 = yBx2;
		xbx4 = xBx3;	ybx4 = yBx3;
	}

	if (camPatt == 2)
	{
		im_LighSeq[0] = im_Ligh[0]; im_LighSeq[1] = im_Ligh[1]; im_LighSeq[2] = im_Ligh[2]; im_LighSeq[3] = im_Ligh[3];
		im_DarkSeq[0] = im_Dark[0]; im_DarkSeq[1] = im_Dark[1]; im_DarkSeq[2] = im_Dark[2]; im_DarkSeq[3] = im_Dark[3];
		im_EdgeSeq[0] = im_Edge[0]; im_EdgeSeq[1] = im_Edge[1]; im_EdgeSeq[2] = im_Edge[2]; im_EdgeSeq[3] = im_Edge[3];

		xbx1 = xBx1;	ybx1 = yBx1;
		xbx2 = xBx2;	ybx2 = yBx2;
		xbx3 = xBx3;	ybx3 = yBx3;
		xbx4 = xBx4;	ybx4 = yBx4;
	}

	if (camPatt == 3)
	{
		im_LighSeq[0] = im_Ligh[1]; im_LighSeq[1] = im_Ligh[2]; im_LighSeq[2] = im_Ligh[3]; im_LighSeq[3] = im_Ligh[0];
		im_DarkSeq[0] = im_Dark[1]; im_DarkSeq[1] = im_Dark[2]; im_DarkSeq[2] = im_Dark[3]; im_DarkSeq[3] = im_Dark[0];
		im_EdgeSeq[0] = im_Edge[1]; im_EdgeSeq[1] = im_Edge[2]; im_EdgeSeq[2] = im_Edge[3]; im_EdgeSeq[3] = im_Edge[0];

		xbx1 = xBx2;	ybx1 = yBx2;
		xbx2 = xBx3;	ybx2 = yBx3;
		xbx3 = xBx4;	ybx3 = yBx4;
		xbx4 = xBx1;	ybx4 = yBx1;
	}

	if (camPatt == 4)
	{
		im_LighSeq[0] = im_Ligh[2]; im_LighSeq[1] = im_Ligh[3]; im_LighSeq[2] = im_Ligh[0]; im_LighSeq[3] = im_Ligh[1];
		im_DarkSeq[0] = im_Dark[2]; im_DarkSeq[1] = im_Dark[3]; im_DarkSeq[2] = im_Dark[0]; im_DarkSeq[3] = im_Dark[1];
		im_EdgeSeq[0] = im_Edge[2]; im_EdgeSeq[1] = im_Edge[3]; im_EdgeSeq[2] = im_Edge[0]; im_EdgeSeq[3] = im_Edge[1];

		xbx1 = xBx3;	ybx1 = yBx3;
		xbx2 = xBx4;	ybx2 = yBx4;
		xbx3 = xBx1;	ybx3 = yBx1;
		xbx4 = xBx2;	ybx4 = yBx2;
	}


	for (y = 0; y < StitchedHeight; y++)
	{
		for (int x = 0; x < StitchedLength; x++)
		{
			*(imgStitchedDark + x + StitchedLength * y) = 255;
			*(imgStitchedLigh + x + StitchedLength * y) = 255;
			*(imgStitchedEdg + x + StitchedLength * y) = 255;

			*(imgStitchedFilDark + x + StitchedLength * y) = 255;
			*(imgStitchedFilLigh + x + StitchedLength * y) = 255;
			*(imgStitchedFilEdge + x + StitchedLength * y) = 255;

			*(imgStitchedFilFinal + x + StitchedLength * y) = 0;

			/////////////

			*(imgStitchedBW + x + StitchedLength * y) = 255;
		}
	}


	bool stitOutOfRange = false;

	if (stitchX[0] < 0 || stitchY[0] < 0 || stitchX[1] < 0 || stitchY[1] < 0 ||
		stitchX[2] < 0 || stitchY[2] < 0 || stitchX[3] < 0 || stitchY[3] < 0)
	{
		stitOutOfRange = true; //TODO needed??? edgeProp = 10000;
	}


	if (stitOutOfRange) { return 0; }

	//Why is this doing this???
//	if(camPatt==0)	{return 0;}

	int pattPosX = pattPos.x;

	///////////
	///////////

	cam = 1;
	offX = (cam - 1) * wBx;

	for (y = ybx1; y < (ybx1 + hBx); y++)
	{
		for (int x = xbx1; x < (xbx1 + wBx); x++)
		{
			*(imgStitchedLigh + (x - xbx1) + offX + StitchedLength * (y - ybx1)) = *(im_LighSeq[0] + x + camLength * y);
			*(imgStitchedDark + (x - xbx1) + offX + StitchedLength * (y - ybx1)) = *(im_DarkSeq[0] + x + camLength * y);
			*(imgStitchedEdg + (x - xbx1) + offX + StitchedLength * (y - ybx1)) = *(im_EdgeSeq[0] + x + camLength * y);
		}
	}

	///////////

	cam = 2;
	offX = (cam - 1) * wBx;

	for (y = ybx2; y < (ybx2 + hBx); y++)
	{
		for (int x = xbx2; x < (xbx2 + wBx); x++)
		{
			*(imgStitchedLigh + (x - xbx2) + offX + StitchedLength * (y - ybx2)) = *(im_LighSeq[1] + x + camLength * y);
			*(imgStitchedDark + (x - xbx2) + offX + StitchedLength * (y - ybx2)) = *(im_DarkSeq[1] + x + camLength * y);
			*(imgStitchedEdg + (x - xbx2) + offX + StitchedLength * (y - ybx2)) = *(im_EdgeSeq[1] + x + camLength * y);
		}
	}

	///////////

	cam = 3;
	offX = (cam - 1) * wBx;

	for (y = ybx3; y < (ybx3 + hBx); y++)
	{
		for (int x = xbx3; x < (xbx3 + wBx); x++)
		{
			*(imgStitchedLigh + (x - xbx3) + offX + StitchedLength * (y - ybx3)) = *(im_LighSeq[2] + x + camLength * y);
			*(imgStitchedDark + (x - xbx3) + offX + StitchedLength * (y - ybx3)) = *(im_DarkSeq[2] + x + camLength * y);
			*(imgStitchedEdg + (x - xbx3) + offX + StitchedLength * (y - ybx3)) = *(im_EdgeSeq[2] + x + camLength * y);
		}
	}

	///////////

	cam = 4;
	offX = (cam - 1) * wBx;

	for (y = ybx4; y < (ybx4 + hBx); y++)
	{
		for (int x = xbx4; x < (xbx4 + wBx); x++)
		{
			*(imgStitchedLigh + (x - xbx4) + offX + StitchedLength * (y - ybx4)) = *(im_LighSeq[3] + x + camLength * y);
			*(imgStitchedDark + (x - xbx4) + offX + StitchedLength * (y - ybx4)) = *(im_DarkSeq[3] + x + camLength * y);
			*(imgStitchedEdg + (x - xbx4) + offX + StitchedLength * (y - ybx4)) = *(im_EdgeSeq[3] + x + camLength * y);
		}
	}

	///////////

	if (debugging)
	{
		if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(imgStitchedBW, StitchedLength, StitchedHeight, "C:\\DebugFiles\\imgStitchedBW.bmp"); }
	}


	//		*(imgStitchedFilDark + x + StitchedLength*y) = 255;
	//		*(imgStitchedFilLigh + x + StitchedLength*y) = 255;

	/////////////////////////

	int pix, pixVal;

	for (y = 1; y < StitchedHeight - 1; y++)
	{
		for (int x = 1; x < StitchedLength - 1; x++)
		{
			pix = *(imgStitchedLigh + x + StitchedLength * y);

			if (pix > thLight) { pixVal = 255; }
			else { pixVal = 0; }

			*(imgStitchedFilLigh + x + StitchedLength * y) = pixVal;

			//////////////////////

			pix = *(imgStitchedDark + x + StitchedLength * y);

			if (pix < thDark) { pixVal = 255; }
			else { pixVal = 0; }

			*(imgStitchedFilDark + x + StitchedLength * y) = pixVal;
		}
	}


	int pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;


	// Creating the Sobel image
	for (y = 1; y < StitchedHeight - 1; y++)
	{
		for (int x = 1; x < StitchedLength - 1; x++)
		{
			pix1 = *(imgStitchedEdg + x - 1 + StitchedLength * (y - 1));
			pix2 = *(imgStitchedEdg + x + 0 + StitchedLength * (y - 1));
			pix3 = *(imgStitchedEdg + x + 1 + StitchedLength * (y - 1));

			pix4 = *(imgStitchedEdg + x - 1 + StitchedLength * (y + 0));
			pix5 = *(imgStitchedEdg + x + 0 + StitchedLength * (y + 0));
			pix6 = *(imgStitchedEdg + x + 1 + StitchedLength * (y + 0));

			pix7 = *(imgStitchedEdg + x - 1 + StitchedLength * (y + 1));
			pix8 = *(imgStitchedEdg + x + 0 + StitchedLength * (y + 1));
			pix9 = *(imgStitchedEdg + x + 1 + StitchedLength * (y + 1));

			//pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
			//		abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix = abs((pix1 + 2 * pix4 + pix7) - (pix3 + 2 * pix6 + pix9));

			pixVal = min(pix, 255);

			if (pixVal > thEdges)
			{
				*(imgStitchedFilEdge + x + StitchedLength * y) = 255;
			}
			else
			{
				*(imgStitchedFilEdge + x + StitchedLength * y) = 0;
			}
		}
	}


	bool useStLigh = jobSettings->useStLigh;
	bool useStDark = jobSettings->useStDark;
	bool useStEdge = jobSettings->useStEdge;

	if (useStLigh)
	{
		for (y = 1; y < StitchedHeight - 1; y++)
		{
			for (int x = 1; x < StitchedLength - 1; x++)
			{
				pix = *(imgStitchedFilLigh + x + StitchedLength * y);

				if (pix == 255) { *(imgStitchedFilFinal + x + StitchedLength * y) = 255; }
			}
		}
	}

	if (useStDark)
	{
		for (y = 1; y < StitchedHeight - 1; y++)
		{
			for (int x = 1; x < StitchedLength - 1; x++)
			{
				pix = *(imgStitchedFilDark + x + StitchedLength * y);

				if (pix == 255) { *(imgStitchedFilFinal + x + StitchedLength * y) = 255; }
			}
		}
	}

	if (useStEdge)
	{
		for (y = 1; y < StitchedHeight - 1; y++)
		{
			for (int x = 1; x < StitchedLength - 1; x++)
			{
				pix = *(imgStitchedFilEdge + x + StitchedLength * y);

				if (pix == 255) { *(imgStitchedFilFinal + x + StitchedLength * y) = 255; }
			}
		}
	}

	int ini1 = xAvoidIni[1] / fact; int end1 = xAvoidEnd[1] / fact;
	int ini2 = xAvoidIni[2] / fact; int end2 = xAvoidEnd[2] / fact;
	int ini3 = xAvoidIni[3] / fact; int end3 = xAvoidEnd[3] / fact;
	int ini4 = xAvoidIni[4] / fact; int end4 = xAvoidEnd[4] / fact;

	int countPix = 0;

	for (int x = 1; x < StitchedLength - 1; x++)
	{
		if ((x > ini1 && x < end1) || (x > ini2 && x < end2) ||
			(x > ini3 && x < end3) || (x > ini4 && x < end4))
		{
			continue;
		}
		else
		{
			for (y = 1; y < StitchedHeight - 1; y++)
			{
				pix = *(imgStitchedFilFinal + x + StitchedLength * y);

				if (pix == 0) { countPix++; }
			}
		}
	}


	return countPix;
}


float Bottle84DoResults::Result9(Inspection85Results* specGroup)
{
	int fact = 3;

	int wsplice = jobSettings->wsplice;
	int hsplice = jobSettings->hsplice;
	int hsplicesmBx = jobSettings->hsplicesmBx;

	for (int i = 0; i < 4; i++)
	{
		//or		bottleYspliceIni[i] = job->GetYSplice(1);
		bottleYspliceIni[i] = jobSettings->YSplice[i];

		if (jobSettings->findMiddles)
		{
			bottleXspliceIni[i] = specGroup->GetLabelResults(i)->GetXMiddle() - wsplice / 2;
		}
		else
		{
			bottleXspliceIni[i] = (config->GetCroppedCameraHeight() - wsplice) / 2;
		}

		findSplice(i, fact, bottleXspliceIni[i], bottleYspliceIni[i], wsplice, hsplice, hsplicesmBx, specGroup->GetPacks()[i]);

		//TRACE("%i vari:%i stdev:%i\n", i, varianceSplice[i], sdeviatiSplice[i]);
	}

	int valMin = 255;
	int thVariance = jobSettings->spliceVar;  // From the UI. Called " Dispersion Limit"

	for (int i = 1; i <= 4; i++)  // Even if 1 camera satisfies the condition
	{
		if (minSpliceAvg[i] < valMin && varianceSplice[i] < thVariance)
		{
			valMin = minSpliceAvg[i];
		}
	}

	return valMin;

}


//this methods finds the minimum average of pixel value found in a rectangle
//and it also calculates the standard deviation inside of that box
void Bottle84DoResults::findSplice(int nCam, int fact,
	int xSplice, int ySplice, int wSplice,
	int hSplice, int hsmSplice, IntermediateImagePack* pack)
{
	////////////////////////////
	unsigned char* imgForSpliceDiv3 = pack->GetFilter(jobSettings->filterSelSplice);
	//Img selection

	////////////////////////////

	int Length = config->GetMainDisplayImageWidth() / fact;
	int Height = config->GetMainDisplayImageHeight() / fact;

	int xsplice = xSplice / fact;
	int ysplice = ySplice / fact;
	int wsplice = wSplice / fact;
	int hsplice = hSplice / fact;
	int hsplicesmBx = hsmSplice / fact;

	////////////////////////////

	bool debugging = false;

	if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(imgForSpliceDiv3, Length, Height, "C:\\DebugFiles\\imgForSpliceDiv3.bmp"); }


	int sumPix = 0;
	int y = 0;
	int pix;

	int sumWhitePix = 0;


	minSpliceAvg[nCam] = 255;

	//If out of bounds, just exit
	if (wsplice <= 0 || hsplice <= 0 || hsplicesmBx <= 0 ||
		xsplice<0 || ysplice<0 ||
		(xsplice + wsplice)>Length ||
		(ysplice + hsplice)>Height)
	{
		return;
	}

	for (int i = 0; i < hsplice; i++)
	{
		y = i + ysplice; sumPix = 0;

		for (int x = xsplice; x < xsplice + wsplice; x++)
		{
			pix = *(imgForSpliceDiv3 + x + Length * y); sumPix += pix;
		}

		avgPixInRow[i] = sumPix / wsplice;  //Calculating the pixel intenstity of each row
	}

	int sumAvg = 0;
	int currentAvgInGreenBox = 0;
	int minAvg = 255;

	//Efficient method
	int i;
	for (i = 0; i < hsplicesmBx; i++)
	{
		sumAvg += avgPixInRow[i];
	}

	if (hsplicesmBx > 0)
	{
		minAvg = sumAvg / hsplicesmBx;
	}

	yposSplice[nCam] = ysplice * fact;

	for (i = 0; i < hsplice - hsplicesmBx; i++)
	{
		//substracting the previous row
		sumAvg -= avgPixInRow[i];
		//adding the next row
		sumAvg += avgPixInRow[i + hsplicesmBx];

		if (hsplicesmBx > 0)
		{
			currentAvgInGreenBox = sumAvg / hsplicesmBx;

			if (currentAvgInGreenBox < minAvg)
			{
				minAvg = currentAvgInGreenBox;
				yposSplice[nCam] = (i + ysplice + 1) * fact;
			}
		}
	}

	minSpliceAvg[nCam] = minAvg;

	//////////////////////////////////////////////
	sumPix = 0;
	int sumPixPix = 0;
	int npix = 0;

	//in case of the black splice finding the minimum is enough
	//but in the case of silver splice we also need to calculate the standard deviation

	for (y = yposSplice[nCam] / fact; y < (yposSplice[nCam] / fact + hsplicesmBx); y++)
	{
		for (int x = xsplice; x < xsplice + wsplice; x++)
		{
			pix = *(imgForSpliceDiv3 + x + Length * y);
			sumPix += pix;
			sumPixPix += (pix * pix);
			npix++;
		}
	}

	if (npix <= 0) { npix = 1; }

	int avgpix = sumPix / npix;
	int avgpixpix = sumPixPix / npix;

	varianceSplice[nCam] = avgpixpix - avgpix * avgpix;
	sdeviatiSplice[nCam] = (int)sqrt(abs(varianceSplice[nCam]));
}

void Bottle84DoResults::DoLipton(Inspection85Results* specGroup)
{
	for (int i = 1; i <= 4; i++)
	{
		shiftedMidd[i] = abs(specGroup->GetLabelResults(i)->GetMiddleShift() - specGroup->GetLabelResults(i)->GetXMiddle());
	}

	int	maxShift = shiftedMidd[1];
	int minShift = shiftedMidd[1];

	for (int i = 2; i <= 4; i++) //finding the biggest shifted distance among the four cameras
	{
		if (shiftedMidd[i] > maxShift) { maxShift = shiftedMidd[i]; }
	}

	results->resultSet[5].Value = maxShift;

		///////////////////////////////////////////

	for (int i = 1; i <= 4; i++)
	{
		shiftedMiddWPoints[i] = abs(specGroup->GetLabelResults(i)->GetMiddleShiftWPoints() - specGroup->GetLabelResults(i)->GetXMiddle());
	}

	int maxShiftWPoints = shiftedMiddWPoints[1];

	for (int i = 2; i <= 4; i++)
	{
		if (shiftedMiddWPoints[i] > maxShiftWPoints) { maxShiftWPoints = shiftedMiddWPoints[i]; }
	}

	results->resultSet[5].Value = maxShiftWPoints;

		///////////////////////////////////////////

	float maxIncDeg = std::max<float>(specGroup->GetLabelResults(0)->GetAngIncDegL(), specGroup->GetLabelResults(0)->GetAngIncDegR());
	float maxTmp;

	for (int i = 2; i <= 4; i++)
	{
		maxTmp = std::max<float>(specGroup->GetLabelResults(0)->GetAngIncDegL(), specGroup->GetLabelResults(0)->GetAngIncDegR());
		if (maxTmp > maxIncDeg) { maxIncDeg = maxTmp; }
	}
	
	if (results->resultSet[11].enabled)
	{
		results->resultSet[11].Value = maxIncDeg;
	}
}


void Bottle84DoResults::CreateResultObj()
{
//	assert(false);
}

void Bottle84DoResults::SendEverywhere()
{
//	assert(false);
}


void Bottle84DoResults::Result11()
{
	results->resultSet[10].Value = 2000;
	if (results->Result11[0] < results->resultSet[10].Value)
	{
		results->resultSet[10].Value = results->Result11[0];
	}
	if (results->Result11[1] < results->resultSet[10].Value)
	{
		results->resultSet[10].Value = results->Result11[1];
	}
	if (results->Result11[2] < results->resultSet[10].Value)
	{
		results->resultSet[10].Value = results->Result11[2];
	}
	if (results->Result11[3] < results->resultSet[10].Value)
	{
		results->resultSet[10].Value = results->Result11[3];
	}
}

void Bottle84DoResults::SetAll()
{
	if (results->Result1l >= results->resultSet[0].Min && results->Result1r >= results->resultSet[0].Min &&
		results->Result1l <= results->resultSet[0].Max && results->Result1r <= results->resultSet[0].Max)
	{
		results->resultSet[0].passed = true;
	}
	else
	{
		results->resultSet[0].passed = false;
	}

	for (int i = 1; i < 10; i++)
	{
		if (results->resultSet[i].Value >= results->resultSet[i].Min && 
			results->resultSet[i].Value <= results->resultSet[i].Max)
		{
			results->resultSet[i].passed = true;
		}
		else { results->resultSet[i].passed = false; }
	}
		

	if (config->GetWaistAvailable())
	{
		if (results->resultSet[10].Value > results->resultSet[10].Min && results->resultSet[10].Value <= results->resultSet[10].Max)
		{
			results->resultSet[10].passed = true;
		}
		else
		{
			results->resultSet[10].passed = false;
		}
	}
	else
	{
		results->resultSet[10].passed = true;
	}

	//align available or square bottle job
	if (config->GetAlignAvailable() || jobSettings->bottleStyle == 10)
	{
		if (results->resultSet[11].Value > results->resultSet[11].Min && results->resultSet[11].Value <= results->resultSet[11].Max)
		{
			results->resultSet[11].passed = true;
		}
		else
		{
			results->resultSet[11].passed = false;
		}
	}

}