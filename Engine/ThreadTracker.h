#include <future>
#include "CounterLocks.h"

#ifndef THREAD_TRACKER_H
#define THREAD_TRACKER_H

class ThreadTracker
{
public:
	ThreadTracker();
	~ThreadTracker();

	void operator()(CounterLocks* lk) const;
};

#endif