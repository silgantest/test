#include "Patterns.h"


void Patterns::SetPatternType(int type) { patternType = type; }

void Patterns::LoadPattern2(const char* jobPath)
{
	char path[100];
	memset(path, '\0', 100);
	sprintf_s(path, "%s\\pat\\image2XBMP.bmp", jobPath);//drive

	this->pattern2->Load(path);

}

void Patterns::LoadPatternX_method6(const char* jobPath)
{
	////////////
	char path3[100];
	char path6[100];

	int width, height, size;

	memset(path3, '\0', 100);
	memset(path6, '\0', 100);

	switch (patternType)
	{
	case 1:
	{
		sprintf_s(path3, "%s\\pat\\M6patBy3L.bmp", jobPath);//drive
		sprintf_s(path6, "%s\\pat\\M6patBy6L.bmp", jobPath);//drive
		break;
	}case 2:
	{
		sprintf_s(path3, "%s\\pat\\M6patBy3M.bmp", jobPath);//drive
		sprintf_s(path3, "%s\\pat\\M6patBy6M.bmp", jobPath);//drive
		break;
	}
	case 3:
	{
		sprintf_s(path3, "%s\\pat\\M6patBy3R_num3.bmp", jobPath);//drive
		sprintf_s(path3, "%s\\pat\\M6patBy6R_num3.bmp", jobPath);//drive
		break;
	}
	default:
	{
		sprintf_s(path3, "%s\\pat\\M6patBy3L.bmp", jobPath);//drive
		sprintf_s(path6, "%s\\pat\\M6patBy6L.bmp", jobPath);//drive
		break;
	}
	}
	this->bytePattBy3_method6_1->Load(path3);
	this->bytePattBy6_method6_1->Load(path6);

}

void Patterns::LoadCaps(const char* jobPath)
{
	char path[200];
	memset(path, 0, 200);
	sprintf_s(path, "%s\\pat\\capimagel.bmp", jobPath);//drive
	im_matchl->Load(path);

	memset(path, 0, 200);
	sprintf_s(path, "%s\\pat\\capimager.bmp", jobPath);//drive
	im_matchr->Load(path);
}


void Patterns::LoadPatternX(const char* jobPath)
{
	char path3[100];
	char path6[100];

	memset(path3, '\0', 100);
	memset(path6, '\0', 100);


	switch (patternType)
	{
	case 1:
	{
		sprintf_s(path3, "%s\\pat\\patBy3L.bmp", jobPath);//drive
		sprintf_s(path6, "%s\\pat\\patBy6L.bmp", jobPath);//drive
		break;
	}
	case 2:
	{
		sprintf_s(path3, "%s\\pat\\patBy3M.bmp", jobPath);//drive
		sprintf_s(path6, "%s\\pat\\patBy6M.bmp", jobPath);//drive
		break;
	}
	case 3:
	{
		sprintf_s(path3, "%s\\pat\\patBy3R.bmp", jobPath);//drive
		sprintf_s(path6, "%s\\pat\\patBy6R.bmp", jobPath);//drive
		break;
	}
	default:
	{
		sprintf_s(path3, "%s\\pat\\patBy3.bmp", jobPath);//drive
		sprintf_s(path6, "%s\\pat\\patBy6.bmp", jobPath);//drive
	}
	}
	this->bytePattBy3->Load(path3);
	this->bytePattBy6->Load(path6);


	bool debugging = false;
	if (debugging) {
		ImageFileUtilities::SaveGrayBitmapToFile(this->bytePattBy3->GetPattern(),
			this->bytePattBy3->GetWidth(), this->bytePattBy3->GetHeight(), "C:\\DebugFiles\\bytePattBy3.bmp");
	}
	if (debugging) {
		ImageFileUtilities::SaveGrayBitmapToFile(this->bytePattBy6->GetPattern(),
			this->bytePattBy3->GetWidth(), this->bytePattBy3->GetHeight(), "C:\\DebugFiles\\bytePattBy6.bmp");
	}
}

void Patterns::LoadPatternX_method6_num3(const char* jobPath)
{

	char path3[100];
	char path6[100];

	int width, height, size;

	memset(path3, '\0', 100);
	memset(path6, '\0', 100);


	switch (patternType)
	{
	case 1:
	{
		sprintf_s(path3, "%s\\pat\\M6patBy3L_num3.bmp", jobPath);
		sprintf_s(path6, "%s\\pat\\M6patBy6L_num3.bmp", jobPath);
		break;
	}
	case 2:
	{
		sprintf_s(path3, "%s\\pat\\M6imgBy3M_num3.bmp", jobPath);
		sprintf_s(path6, "%s\\pat\\M6patBy6M_num3.bmp", jobPath);
		break;
	}
	case 3:
	{
		sprintf_s(path3, "%s\\pat\\M6patBy3R_num3.bmp", jobPath);
		sprintf_s(path6, "%s\\pat\\M6patBy6R_num3.bmp", jobPath);
		break;
	}
	default:
	{
		sprintf_s(path3, "%s\\pat\\M6patBy3_num3.bmp", jobPath);
		sprintf_s(path6, "%s\\pat\\M6patBy6_num3.bmp", jobPath);
	}
	}

	this->bytePattBy3_method6_num3->Load(path3);
	this->bytePattBy6_method6_num3->Load(path6);

	bool debugging = false;
	if (debugging) {
		ImageFileUtilities::SaveGrayBitmapToFile(this->bytePattBy3_method6_num3->GetPattern(),
			this->bytePattBy3_method6_num3->GetWidth(), this->bytePattBy3_method6_num3->GetHeight(), "C:\\DebugFiles\\bytePattBy3.bmp");
	}
	if (debugging) {
		ImageFileUtilities::SaveGrayBitmapToFile(this->bytePattBy6_method6_num3->GetPattern(),
			this->bytePattBy6_method6_num3->GetWidth(), this->bytePattBy6_method6_num3->GetHeight(), "C:\\DebugFiles\\bytePattBy6.bmp");
	}

}

void Patterns::DeleteAll()
{
	delete pattern2;
	delete pattern2X;
	delete bytePattBy3_method6_1;
	delete bytePattBy6_method6_1;
	delete bytePattBy3_method6;
	delete bytePattBy6_method6;
	delete bytePattBy3_method6_num3;
	delete bytePattBy6_method6_num3;
	delete im_matchl;
	delete im_matchr;
}

Patterns::Patterns()
{
	patternType = -1;
	pattern2 = new ImagePattern();
	pattern2X = new ImagePattern();
	bytePattBy3_method6_1 = new ImagePattern();
	bytePattBy6_method6_1 = new ImagePattern();
	bytePattBy3_method6 = new ImagePattern();
	bytePattBy6_method6 = new ImagePattern();
	bytePattBy3_method6_num3 = new ImagePattern();
	bytePattBy6_method6_num3 = new ImagePattern();
	bytePattBy6 = new ImagePattern();
	bytePattBy3 = new ImagePattern();
	im_matchl = new ImagePattern();
	im_matchr = new ImagePattern();
}

Patterns::~Patterns()
{
	DeleteAll();
}

void Patterns::Load(const char* jobPath)
{
	LoadPatternX_method6(jobPath);
	LoadPattern2(jobPath);
	LoadPatternX(jobPath);
	LoadPatternX_method6_num3(jobPath);
	LoadCaps(jobPath);
}

void Patterns::CopyCapLeft(ImagePattern* ip)
{
	im_matchl->CopyTo(ip);
}

void Patterns::CopyCapRight(ImagePattern* ip)
{
	im_matchr->CopyTo(ip);
}

void Patterns::CopyBytePattBy3(ImagePattern* ip)
{
	bytePattBy3->CopyTo(ip);
}

void Patterns::CopyBytePattBy6(ImagePattern* ip)
{
	bytePattBy6->CopyTo(ip);
}

void Patterns::CopyBytePattBy6_method6_1(ImagePattern* ip)
{
	bytePattBy6_method6_1->CopyTo(ip);
}

void Patterns::CopyBytePattBy3_method6_1(ImagePattern* ip)
{
	bytePattBy3_method6_1->CopyTo(ip);
}

void Patterns::CopyBytePattBy6_method6(ImagePattern* ip)
{
	bytePattBy6_method6->CopyTo(ip);
}

void Patterns::CopyBytePattBy3_method6(ImagePattern* ip)
{
	bytePattBy3_method6->CopyTo(ip);
}

void Patterns::CopyBytePattBy6_method6_num3(ImagePattern* ip)
{
	bytePattBy6_method6_num3->CopyTo(ip);
}

void Patterns::CopyBytePattBy3_method6_num3(ImagePattern* ip)
{
	bytePattBy3_method6_num3->CopyTo(ip);
}

void Patterns::CopyPattern2(ImagePattern* ip)
{
	pattern2->CopyTo(ip);
}

void Patterns::CopyPattern2X(ImagePattern* ip)
{
	pattern2X->CopyTo(ip);
}