#include "ResultsGroupPool.h"

//static methods
Results85GroupPool* Results85GroupPool::instance;

ResultsGroupPool::ResultsGroupPool()
{

}
Reusable* ResultsGroupPool::create()
{
	return nullptr;
}

ResultsGroupPool::~ResultsGroupPool()
{

}
	
void ResultsGroupPool::deleteObject(Reusable* obj)
{

}

ResultsGroup* ResultsGroupPool::GetResultGroup(int num, int reqInsp, int numCameras)
{
	return nullptr;
}

ResultsGroup* Results85GroupPool::GetResultGroup(int num, int reqInsp, int numCameras)
{
	Inspection85Results* group = (Inspection85Results*)instance->Get();
	group->Set(num, reqInsp, numCameras);

	return group;
}

void Results85GroupPool::Initialize()
{
	instance = new Results85GroupPool();
	sprintf_s(instance->name, "Results85GroupPool");
	instance->grow();
}

void Results85GroupPool::Shutdown()
{
	delete instance;
}

void Results85GroupPool::DeleteResultGroup(Inspection85Results* node)
{
	instance->Delete(node);
}

ResultsGroupPool* Results85GroupPool::GetPool()
{
	return instance;
}

Results85GroupPool::Results85GroupPool()
{
}

Results85GroupPool::~Results85GroupPool()
{
	deleteEverything();
}

Reusable* Results85GroupPool::create()
{
	return new Inspection85Results();
};

void Results85GroupPool::deleteObject(Reusable* obj)
{
	delete (ResultsGroup*)obj;
}
