#include "CapResultsDetailsPool.h"

// static methods
CapResultsDetailsPool* CapResultsDetailsPool::instance;

CapResultDetails* CapResultsDetailsPool::GetDetails()
{
	return (CapResultDetails*)instance->Get();
}

void CapResultsDetailsPool::DeleteDetails(CapResultDetails* obj)
{
	instance->Delete((Reusable*)obj);
}

void CapResultsDetailsPool::Initialize()
{
	instance = new CapResultsDetailsPool();
	sprintf_s(instance->name, "CapResultsDetailsPool");
	instance->grow();
}

void CapResultsDetailsPool::Shutdown()
{
	delete instance;
}


CapResultsDetailsPool::CapResultsDetailsPool()
{

}

CapResultsDetailsPool::~CapResultsDetailsPool()
{
	deleteEverything();
}

void CapResultsDetailsPool::deleteObject(Reusable* obj)
{
	delete (CapResultDetails*)obj;
}

Reusable* CapResultsDetailsPool::create()
{
	return new CapResultDetails();
};
