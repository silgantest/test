#include "BaslerCapturedImage.h"
#include "BaslerCapturedImagePool.h"

BaslerCapturedImage::BaslerCapturedImage()
{
}

BaslerCapturedImage::~BaslerCapturedImage()
{
}

void BaslerCapturedImage::Set(CGrabResultPtr capPtr, int imNum, int camNum)
{
	CapturedImage::Set(imNum, camNum);

	captured = capPtr;
}

BaslerCapturedImage* BaslerCapturedImage::CreateBaslerCapImg(CGrabResultPtr capPtr, int imNum, int camNum)
{
	return BaslerCapturedImagePool::GetCapturedImage(capPtr, imNum, camNum);  
}

void* BaslerCapturedImage::GetBuffer()
{
	return captured->GetBuffer();
}

int BaslerCapturedImage::GetWidth()
{
	return captured->GetWidth();
}

int BaslerCapturedImage::GetHeight()
{
	return captured->GetHeight();
}

void BaslerCapturedImage::Reset()
{
}

void BaslerCapturedImage::Unload()
{
	BaslerCapturedImagePool::DeleteCapturedImage(this);
}

