#include "Camera.h"
#include "BaslerCameraSpecificConfiguration.h"
#include <pylon/PylonIncludes.h>
#include <pylon/gige/BaslerGigEInstantCamera.h>
#include <pylon/usb/BaslerUsbInstantCamera.h>
#include <atomic>
#include <mutex>
#include <vector>
#include "stats.h"

#ifndef BASLER_CAMERA_H
#define BASLER_CAMERA_H

class BaslerCameraThread;

class BaslerCamera : public Camera
{
	friend class BaslerCameraThread;
protected:
	BaslerCamera(CInstantCamera* cam, CameraSpecificConfiguration* config);
	static void CameraInit();

	static bool beenInit;
	CInstantCamera* camera;
	CameraSpecificConfiguration* camConfig;

	mutex startStop;

public:
	virtual ~BaslerCamera();

	static std::list<BaslerCamera*>* ScanAllCameras();
	static void Shutdown();

	virtual void LoadSetAllParameters();
	virtual void SaveAllParameters();
	virtual void SetParameter(CameraParameter* param);
	virtual CameraParameter* ReadParameter();

	virtual void Start(CounterLocks* lk) override;
	virtual void Stop();

	virtual const std::string GetModel();
	virtual const std::string GetSerial();

	virtual bool IsRunning();

	virtual void FireSoftwareTrigger();

	void GetNextImage(CGrabResultPtr& ptrGrabResult);
	void ResetImageNumber();
private:
	std::thread* grabThread;


};
 
#endif //BASLER_CAMERA_H
