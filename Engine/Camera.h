#include "CameraParameter.h"
#include "ImageProcessor.h"
#include "CounterLocks.h"
#include <string>
#include <fstream>

#ifndef CAMERA_H
#define CAMERA_H

using namespace std;

class Camera
{
protected:
	atomic<bool> running;
	const int mask = 0xFF;
	int imageNumber;
	int fileCount;
	ImageProcessor* processor;

	int xAOI;
	int yAOI;
	int widthAOI;
	int heightAOI;


public:

	Camera() 
	{
		std::vector<Stats*>::const_iterator st;
#ifdef MEASURETIMES
		timesTrigger = new std::vector<Stats*>(125);
		times = new std::vector<Stats*>(125);
		times->clear();
		timesTrigger->clear();
#endif
		processor = nullptr;


	}
	virtual ~Camera()
	{
#ifdef MEASURETIMES
		delete timesTrigger;
		delete times;
#endif
	}

	virtual void incrementImageNumber()
	{
		imageNumber++;
		imageNumber = imageNumber & mask;

	}

	virtual int GetImageNumber()
	{
		return imageNumber;
	}
	   	 
	void SetProcessor(ImageProcessor* p)
	{
		if(IsRunning())
			Stop();  //?? or return error
		processor = p;
	}
	virtual void LoadSetAllParameters() = 0;
	virtual void SaveAllParameters() = 0;
	virtual void SetParameter(CameraParameter* param) = 0;
	virtual CameraParameter* ReadParameter() = 0;

	virtual void Start(CounterLocks* lk) = 0;
	virtual void Stop() = 0;
	virtual void SetAOI(int x, int y, int width, int height)
	{
		xAOI = x;
		yAOI = y;
		widthAOI = width;
		heightAOI = height;
	}

	virtual const std::string GetModel() = 0;
	virtual const std::string GetSerial() = 0;

	virtual bool IsRunning() = 0;

	unsigned int GetIndex()
	{
		return index;
	}

	void SetIndex(unsigned int val)
	{
		index = val;
	}

	virtual void FireSoftwareTrigger() = 0;
	void QueueImage(CapturedImage* im)
	{
		processor->QueueImage(im);
	}


private:
	unsigned int index;

#ifdef MEASURETIMES
protected:
	std::mutex timesMtx;
	std::vector<Stats*>* times;
	std::vector<Stats*>* timesTrigger;

public:
	void AddStats(Stats* s)
	{
		std::lock_guard<std::mutex> lk(timesMtx);
		times->push_back(s);
		fileCount++;
	}

	void AddTriggerStats(Stats* s)
	{
		std::lock_guard<std::mutex> lk(timesMtx);
		timesTrigger->push_back(s);
	}
	void clearStats()
	{
		std::list <Camera*>::iterator itcams;
		{
			std::lock_guard<std::mutex> lk(timesMtx);
			for (size_t j = 0; j < times->size(); j++)
			{
				delete times->at(j);
				delete timesTrigger->at(j);
			}
			times->clear();
			timesTrigger->clear();
		}
	}

	void WriteTimes(std::ofstream& myfile)
	{
		std::vector<Stats*>::const_iterator st;
		Stats* stat;
		int p = 0, q = 0, r = 0, s = 0;
		unsigned long t = 0, u = 0, v = 0, w = 0;
		unsigned long diff1 = 0, diff2 = 0, diff = 0;
		char buffer[250];
		{
		
			std::lock(timesMtx, processor->timesMtx);
			std::lock_guard<std::mutex> lka(timesMtx, std::adopt_lock);
			std::lock_guard<std::mutex> lkb(processor->timesMtx, std::adopt_lock);

			for (size_t j = 0; j < times->size(); j++)
			{
				if (processor->times->size() >= j)
					break;
				stat = ((Stats*)processor->times->at(j));
				p = times->at(j)->CamNumber;
				q = stat->CamNumber;
				r = times->at(j)->ImageNumber;
				s = stat->ImageNumber;
				t = timesTrigger->at(j)->timestampStart;
				u = times->at(j)->timestampEnd;
				v = stat->timestampStart;
				w = stat->timestampEnd;

				if ((p == q) && (r == s))
				{
					memset(buffer, 0, 250);
					diff1 = u - t;
					diff2 = w - v;
					diff = w - t;
					sprintf_s(buffer, 250, "%d,%d,%d,%d,%d,%d,%d,%d,%d\n", p, r, t, u, v, w, diff1, diff2, diff);
					myfile.write(buffer, strlen(buffer));

				
				}

			}
			myfile.flush();
		}
		fileCount = 0;
	}

#endif

};

/* Model
void AquistionLoop()
{
	while (true)
	{
		//Grab Frame  
		//Add to buffer
		//if(failed) //do nothing
	}
}

void ProcessingLoop()
{
	while (true)
	{
		//Retrieve all items from queue and process last one 
		//run all inspections on all 3 images
		//Send pass fail result
	}
}


struct BufferGroup
{
	CamBuffer buf1;
	CamBuffer buf2;
	CamBuffer buf3;

	//add based on index
	//CopyOut and place in queue for processing
};

struct CamBuffer
{
	//lock per camera
	unsigned char* cam;
	bool empty;

	bool Add(unsigned char* input)
	{
		if (!empty)
			return false;
		//copy image
		empty = true;
	}

	void CopyOut(unsigned char* dest)
	{
		//copy image
		empty = false;

	}
};
*/
#endif //CAMERA_H