#include "CapturedImage.h"
#include "FloatingInspectionResultsPool.h"
#include "AppConfig.h"
#include <list>
#include "ResultsHandler.h"

#ifndef INSPECTOR_H
#define INSPECTOR_H

#define MAX_WIDTH 1456
#define MAX_HEIGHT 1088
#define TEMPLATE_SIZE 9

class Inspector
{
protected:
	Inspector() {};
public:
	virtual ~Inspector(){};

	virtual void Inspect(CapturedImage* capImg, ResultsHandler* handler) = 0;
	virtual void ResetForJob() = 0;
	virtual void SetAppConfig(AppConfig* config) = 0;
};

class DummyInspector : public virtual Inspector
{
	/*
	This is meant to be really slow image processing work for simulations
	*/

private:
	unsigned char thumb[TEMPLATE_SIZE][TEMPLATE_SIZE] = {{0xED, 0xEC, 0xED, 0xEC, 0xED, 0xEC, 0xEC, 0xED, 0xEC},
							{0xAD, 0xAC, 0xAD, 0xAC, 0xAD, 0xAC, 0xAC, 0xAD, 0xAC},
							{0xED, 0xEC, 0xED, 0xEC, 0xED, 0xEC, 0xEC, 0xED, 0xEC},
							{0xAD, 0xAC, 0xAD, 0xAC, 0xAD, 0xAC, 0xAC, 0xAD, 0xAC},
							{0xED, 0xEC, 0xED, 0xEC, 0xED, 0xEC, 0xEC, 0xED, 0xEC},
							{0xAD, 0xAC, 0xAD, 0xAC, 0xAD, 0xAC, 0xAC, 0xAD, 0xAC},
							{0xED, 0xEC, 0xED, 0xEC, 0xED, 0xEC, 0xEC, 0xED, 0xEC},
							{0xAD, 0xAC, 0xAD, 0xAC, 0xAD, 0xAC, 0xAC, 0xAD, 0xAC},
							{0xED, 0xEC, 0xED, 0xEC, 0xED, 0xEC, 0xEC, 0xED, 0xEC}};

	int results[MAX_HEIGHT-4][MAX_WIDTH-4];
	int intermediate[MAX_HEIGHT][MAX_WIDTH];
	int centerThumb[TEMPLATE_SIZE][TEMPLATE_SIZE];

public:
	DummyInspector() 
	{ 
		unsigned char templateMean = mean((unsigned char*)thumb, TEMPLATE_SIZE, TEMPLATE_SIZE);
		center((unsigned char*)thumb, (int*)centerThumb, templateMean, TEMPLATE_SIZE, TEMPLATE_SIZE);

	};
	virtual ~DummyInspector() { };

	virtual void ResetForJob(Job* job) {};
	virtual void SetAppConfig(AppConfig* config) {};

	virtual void Inspect(CapturedImage* capImg, ResultsHandler* handler)
	{
		InspectionResult* ir = FloatingInspectionResultsPool::GetResultObj();
		ir->SetImageNumber(capImg->GetImageNumber());
		ir->SetCode(InspectionCode::Dummy);
		unsigned char buffMean = mean((unsigned char*)capImg->GetBuffer(), capImg->GetWidth(), capImg->GetHeight());		
		center((unsigned char*)capImg->GetBuffer(), (int*)intermediate, buffMean, capImg->GetWidth(), capImg->GetHeight());
		
		for (int i = 0; i < (capImg->GetWidth() - 4); i++)
		{
			for (int j = 0; j < (capImg->GetHeight() - 4); j++)
			{
				results[i][j] = crossCorr(intermediate, centerThumb, i, j);
			}
		}

		handler->AddResult(ir, capImg, nullptr, nullptr);
	}

	unsigned char mean(unsigned char* buffer, int width, int height)
	{
		float f = 0;
		for (int j = 0; j < height; j++)
		{
			for (int i = 0; i < width; i++)
			{
				f += buffer[j * width + i];
			}
		}
		f = f / (width * height);
		return (unsigned char)f;
	}

	void center(unsigned char* source, int* dest, unsigned char mean, int width, int height)
	{
		for (int j = 0; j < height; j++)
		{
			for (int i = 0; i < width; i++)
			{
				dest[j * width + i] = source[j * width + i] - mean;
			}
		}
	}

	int crossCorr(int buffer[MAX_HEIGHT][MAX_WIDTH], int tmplt[TEMPLATE_SIZE][TEMPLATE_SIZE], int xOff, int yOff)
	{
		int valNum = 0;
		int valDenom = 0;
		for (int j = 0; j < TEMPLATE_SIZE; j++)
		{
			for (int i = 0; i < TEMPLATE_SIZE; i++)
			{	
				valNum += (buffer[(xOff + i)][(yOff + j)] * tmplt[TEMPLATE_SIZE - i - 1][TEMPLATE_SIZE - j - 1]);
				valDenom +=(buffer[(xOff + i)][(yOff + j)]* buffer[(xOff + i)][(yOff + j)]
					*tmplt[TEMPLATE_SIZE - i - 1][TEMPLATE_SIZE - j - 1]* tmplt[TEMPLATE_SIZE - i - 1][TEMPLATE_SIZE - j - 1]);
			}
		}
		return  int((float)valNum/ pow((double)valDenom, 0.5) * 100.0 );
	}

};

#endif