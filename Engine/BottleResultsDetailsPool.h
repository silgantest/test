
#include "ResultDetails.h"
#include "ReusablePool.h"

#ifndef BOTTLE_RESULTS_DETAILS_POOL_H
#define BOTTLE_RESULTS_DETAILS_POOL_H

class BottleResultsDetailsPool : ReusablePool
{
private:
	static BottleResultsDetailsPool* instance;

protected:
	BottleResultsDetailsPool();

	virtual Reusable* create() override;

	void deleteObject(Reusable* obj) override;

public:
	BottleResultsDetailsPool(BottleResultsDetailsPool&) = delete;
	BottleResultsDetailsPool& operator=(BottleResultsDetailsPool& rhs) = delete;

	virtual ~BottleResultsDetailsPool();

	static BottleResultDetails* GetDetails();
	static void DeleteDetails(BottleResultDetails* obj);
	static void Initialize();
	static void Shutdown();
};

#endif
