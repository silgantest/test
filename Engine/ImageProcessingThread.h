#include "ImageProcessor.h"
#include "InspectionTrigger.h"
#include "CounterLocks.h"
#include <future>

#ifndef IMAGE_PROCESSING_THREAD_H
#define IMAGE_PROCESSING_THREAD_H

class ImageProcessingThread
{

public:
	ImageProcessingThread() = default;
	~ImageProcessingThread() = default;
	ImageProcessingThread& operator=(ImageProcessingThread&) = delete;
	ImageProcessingThread(ImageProcessingThread&) = delete;

	void operator()(ImageProcessor* ip, CounterLocks* lk) const;
};

#endif