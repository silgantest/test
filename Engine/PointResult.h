#include "Point.h"

#ifndef POINT_RESULT
#define POINT_RESULT

#include "InspectionResult.h"

class TwoPointResult : public InspectionResult
{
private:
	TwoPoint pos;
	
public:
	TwoPointResult();
	virtual ~TwoPointResult();

	void SetResult(TwoPoint res);
	TwoPoint GetResult();

	void Reset();
	virtual void Unload();

};

class SpecialPointResult : public InspectionResult
{
private:
	SpecialPoint pos;

public:
	SpecialPointResult();
	virtual ~SpecialPointResult();

	void SetResult(SpecialPoint res);
	SpecialPoint GetResult();

	void Reset();
	virtual void Unload();

};


#endif //POINT_RESULT