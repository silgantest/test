#include "ReusablePool.h"
#include "ResultsGroup.h"
#include "Inspection85Results.h"

#ifndef RESULTS_GROUP_POOL_H
#define RESULTS_GROUP_POOL_H


class ResultsGroupPool : public ReusablePool
{
protected:
	ResultsGroupPool();
	virtual Reusable* create();

public:
	ResultsGroupPool(ResultsGroupPool&) = delete;
	ResultsGroupPool& operator=(ResultsGroupPool& rhs) = delete;

	virtual ~ResultsGroupPool();
	virtual void deleteObject(Reusable* obj) override;

	virtual ResultsGroup* GetResultGroup(int num, int reqInsp, int numCameras);
};


class Results85GroupPool : public ResultsGroupPool
{
private:
	static Results85GroupPool* instance;
	
protected:
	Results85GroupPool();
	virtual Reusable* create();

public:
	Results85GroupPool(Results85GroupPool&) = delete;
	Results85GroupPool& operator=(Results85GroupPool& rhs) = delete;

	virtual ~Results85GroupPool();
	virtual void deleteObject(Reusable* obj) override;

	virtual ResultsGroup* GetResultGroup(int num, int reqInsp, int numCameras) override;
	static ResultsGroupPool* GetPool();
	static void Initialize();
	static void Shutdown();
	static void DeleteResultGroup(Inspection85Results* node);
};
#endif //RESULTS_GROUP_POOL_H