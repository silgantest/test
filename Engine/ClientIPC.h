#include "ResultsGroup.h"
#include "Definitions.h"
#include "SystemConfigLoader.h"
#include "Windows.h"

#define WIN32_LEAN_AND_MEAN

#ifndef CLIENT_IPC
#define CLIENT_IPC

class ClientIPC
{
private:
    ClientIPC()	{}

    static DWORD BUF_SIZE;
    static LPCTSTR szName; 
    static HANDLE hMapFile;
    static HANDLE semSharedDataReady;
    static HANDLE mtxSharedDataAccess;

    static unsigned char* pBuf;

    static unsigned char* images[BOTTLE_CAMS];
    static unsigned char* capImage;
    static unsigned char* inspections;

public:

    static void Create();
    static void PushToClient(ResultsGroup* group);
    static void CloseBuffer();
};

#undef WIN32_LEAN_AND_MEAN

#endif 