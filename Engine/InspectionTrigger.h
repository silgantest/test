#include <mutex>

#ifndef INSPECTION_TRIGGER_H
#define INSPECTION_TRIGGER_H

class InspectionTrigger
{
private:
	std::condition_variable inspectionsComplete;
	std::mutex inspMtx;
	int lastCount;

	static int mask;
	static InspectionTrigger instance;

public:
	InspectionTrigger();
	static void Wait(unsigned int count);
	static void Notify();
	static void SetLastCount(int count);

};


#endif