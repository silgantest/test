#ifndef STATUS_UPDATE_THREAD_H
#define STATUS_UPDATE_THREAD_H

class UpdatorStatus
{
public:
	bool Running;
};


class StatusUpdateThread
{
public:
	StatusUpdateThread() = default;
	~StatusUpdateThread() = default;
	StatusUpdateThread& operator=(StatusUpdateThread&) = delete;
	StatusUpdateThread(StatusUpdateThread&) = delete;

	void operator()(UpdatorStatus* status);

};

#endif
