#include "FloatRangeResult.h"
#include "ReusablePool.h"

#ifndef INSPECTION_RESULTS_POOL_H
#define INSPECTION_RESULTS_POOL_H

class FloatingInspectionResultsPool : public ReusablePool
{
private:
	

	static FloatingInspectionResultsPool* instance;


protected:
	FloatingInspectionResultsPool();

	virtual Reusable* create() override;
	void deleteObject(Reusable* obj) override;

public:
	FloatingInspectionResultsPool(FloatingInspectionResultsPool&) = delete;
	FloatingInspectionResultsPool& operator=(FloatingInspectionResultsPool& rhs) = delete;
	virtual ~FloatingInspectionResultsPool();
	
	static FloatRangeResult* GetResultObj();
	static void DeleteResultObj(FloatRangeResult* obj);
	static void Initialize();
	static void Shutdown();


};

#endif