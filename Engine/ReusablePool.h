#include "Reusable.h"
#include <list>
#include <mutex>

#ifndef REUSABLE_POOL_H
#define REUSABLE_POOL_H

#define GROWTH_RATE 5

class ReusablePool
{
protected:
	std::list<Reusable*>* inuse;
	std::list<Reusable*>* available;

	std::mutex availLock;
	std::mutex inuseLock;
	char name[50];

	void grow();
	virtual Reusable* create() = 0;
	virtual void deleteObject(Reusable* obj) = 0;

	virtual void deleteEverything(); //Must be called by implementors  
	void deleteFromList(std::list<Reusable*>* list);

public:
	ReusablePool();
	virtual ~ReusablePool();

	Reusable* Get();
	void Delete(Reusable* obj);



};

#endif //REUSABLE_POOL_H