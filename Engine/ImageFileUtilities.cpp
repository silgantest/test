#include "ImageFileUtilities.h"
#include <stdio.h>
#include <fstream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;

unsigned char* ImageFileUtilities::ReadBMP(const char* filename, bool gray, int* width, int* height, int* size)
{
    cv::Mat m = imread(filename);
    *width = m.cols;
    *height = m.rows;
    unsigned char* Buffer = nullptr;
    if (gray)
    {
        *size = *width * *height;
        cv::Mat gray(*width, *height, CV_8U);
        if(m.channels() != 1)
            cv::cvtColor(m, gray, cv::COLOR_BGR2GRAY);
        Buffer = new unsigned char[*size];
        memcpy(Buffer, gray.data, *size);
    }
    else
    {
        *size = *width * *height * 4;
        cv::Mat clr(*width, *height, CV_8UC4);

        if (m.channels() != 4)
            cv::cvtColor(m, clr, cv::COLOR_BGR2BGRA);
        Buffer = new unsigned char[*size];
        memcpy(Buffer, clr.data, *size);

    }
    
    return Buffer;

}
 
std::unique_ptr<BYTE[]> ImageFileUtilities::CreateNewBuffer(unsigned long& padding, unsigned char* pmatrix, const int& width, const int& height, bool gray)
{
    padding = (4 - ((width * 3) % 4)) % 4;
    int scanlinebytes = width * 3;
    int total_scanlinebytes = scanlinebytes + padding;
    long newsize = height * total_scanlinebytes;
    std::unique_ptr<BYTE[]> newbuf(new BYTE[newsize]);

    // Fill new array with original buffer, pad remaining with zeros
    std::fill(&newbuf[0], &newbuf[newsize], 0);
    long bufpos = 0;
    long newpos = 0;
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < 3 * width; x += 3)
        {
            // Determine positions in original and padded buffers
            bufpos = y * 3 * width + (3 * width - x);
            newpos = (height - y - 1) * total_scanlinebytes + x;
            if (gray)
            {
                newbuf[newpos] = pmatrix[bufpos];
                newbuf[newpos + 1] = pmatrix[bufpos];
                newbuf[newpos + 2] = pmatrix[bufpos];

            }
            else
            {
                newbuf[newpos] = pmatrix[bufpos];
                newbuf[newpos + 1] = pmatrix[bufpos + 1];
                newbuf[newpos + 2] = pmatrix[bufpos + 2];
            }
        }
    }

    return newbuf;
}

void ImageFileUtilities::SaveAlphaBitmapToFile(unsigned char* bits, int lWidth, int lHeight, const char* cFileName)
{

    cv::Mat img(lHeight, lWidth, CV_8UC4);
    int baseIndex = 0;
    for (int i = 0; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {
            img.at<cv::Vec4b>(i, j) = cv::Vec4b(bits[baseIndex + 0],
                bits[baseIndex + 1],
                bits[baseIndex + 2],
                bits[baseIndex + 3]);
            baseIndex = baseIndex + 4;
        }
    }
    cv::imwrite(cFileName, img);
}

void ImageFileUtilities::ExportBytes(BYTE* image, int width, int height, int depth)
{
    std::ofstream fout;

    fout.open("c:\\out\\res3.txt");

    char str[100];

    width = 136;
    for (int i = 0; i < 136; i++)
    {
        for (int j = 0; j < 361; j += 3)
        {
            memset(str, 0, 100);
            sprintf_s(str, "%d, %d, %d\n", image[i * width + j], image[i * width + j + 1], image[i * width + j + 2]);
            fout.write(str, strlen(str));
        }
    }
    fout.close();
}


void ImageFileUtilities::Reduce(unsigned char* bits, int lWidth, int lHeight)
{
    cv::Mat img(lHeight, lWidth, CV_8UC4);
    cv::Mat imgR(lWidth, lHeight, CV_8UC4);
    cv::Mat img2(lHeight/3, lWidth/3, CV_8UC4);
    cv::Mat img3(lHeight / 3, lWidth / 3, CV_8UC3);
    memcpy(img.data, bits, lWidth*lHeight*4);
    cv::imwrite("c:\\out\\file7.bmp", img);
    rotate(img, imgR, ROTATE_90_COUNTERCLOCKWISE);
    cv::imwrite("c:\\out\\file7.bmp", imgR);
    resize(imgR, img2, cv::Size(lHeight / 3, lWidth / 3), 3, 3, cv::INTER_AREA);
    cv::imwrite("c:\\out\\file7.bmp", img2);
    cv::cvtColor(img2, img3, cv::COLOR_BGRA2BGR);
    cv::imwrite("c:\\out\\file7.bmp", img3);
    ExportBytes(img3.data, lWidth / 3, lHeight / 3, 3);
}

void ImageFileUtilities::SaveBitmapToFile(unsigned char* bits, int lWidth, int lHeight, const char* cFileName)
{
  
    cv::Mat img(lHeight, lWidth, CV_8UC3);
   // memcpy(img2.data, bits, lWidth * lHeight * 3);
    int baseIndex = 0;
    for (int i = 0; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {
            img.at<cv::Vec3b>(i, j) = cv::Vec3b(bits[baseIndex + 0],
                bits[baseIndex + 1],
                bits[baseIndex + 2]);
            baseIndex = baseIndex + 4;
        }
    }
    cv::imwrite(cFileName, img);
}

void ImageFileUtilities::SaveGrayBitmapToFile(unsigned char* bits, int lWidth, int lHeight, const char* cFileName)
{

    cv::Mat img(lHeight, lWidth, CV_8U);
    int baseIndex = 0;
    for (int i = 0; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {
            img.at<uchar>(i, j) = bits[baseIndex];
            baseIndex++;
        }
    }
    cv::imwrite(cFileName, img);
}


