#include "JobLoader.h"
#include <list>
#include <mutex>
#include "patterns.h"

#ifndef JOB_MANAGER_H
#define JOB_MANAGER_H

class NotifyJobUpdated
{
public:
	virtual void JobUpdated(Job* newJob) = 0;
};

class JobManager
{

private:
	JobLoader* loader;
	Job* currentJob;
	std::list<NotifyJobUpdated*>* updateList;
	static JobManager* instance;
	std::mutex lock;

	Patterns* patterns;
protected:
	JobManager()
	{
		updateList = new std::list<NotifyJobUpdated*>();
		loader = new JobLoader();
		currentJob = nullptr;
		patterns = new Patterns();
	}



	void Notify()
	{
		std::list<NotifyJobUpdated*>::iterator it;
		for (it = updateList->begin(); it != updateList->end(); ++it) {
			(*it)->JobUpdated(currentJob);
		}
	}

public:
	virtual ~JobManager()
	{
		delete updateList;
		delete loader;
		delete patterns;
		if (currentJob)
			delete currentJob;
	}

	static Job* GetCurrentJob()
	{
		return instance->currentJob;
	}


	static Patterns* GetCurrentPatterns()
	{
		return instance->patterns;
	}

	static void Initialize()
	{
		instance = new JobManager();
		instance->LoadDefaultJob();
		instance->patterns->Load(instance->currentJob->GetPath());
	}

	static void Shutdown()
	{
		delete instance;
	}

	//Not Ideal, but better than alternatives to allowing job read
	static void Lock()
	{
		instance->lock.lock();
	}

	static void Unlock()
	{
		instance->lock.unlock();
	}

	static void LoadDefaultJob()
	{
		instance->currentJob = instance->loader->LoadDefaultJob();
	}


	static void RegisterForUpdates(NotifyJobUpdated* obj)
	{
		instance->updateList->push_back(obj);
	}

	static void ChangeJob(const char* newJobName)
	{
		std::lock_guard<std::mutex> lk(instance->lock);
		delete instance->currentJob;
		instance->currentJob = instance->loader->LoadJob(newJobName);
		instance->patterns->Load(instance->currentJob->GetPath());
		instance->Notify();
	}

};

#endif