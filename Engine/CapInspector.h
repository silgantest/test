#include "Inspector.h"
#include "Point.h"
#include "AppConfig.h"
#include "DoUserCommand.h"
#include "Patterns.h"
#include <list>

#ifndef CAP_INSPECTOR_H
#define CAP_INSPECTOR_H

#define NUM_CAP_INSP 4

class CapInspector : public Inspector
{
private:
	char* im_match2;
	ImagePattern* im_matchl;
	ImagePattern* im_matchr;
	int camIndex;
	DoUserCommand* doUser;
	
	//TODO:
	//Check best data type
	int	lineLength;
	int	lineLength2;
	int	neckBarY;
	int	height;
	float length;
	int neckLimit;
	int capLimit;
	int capLimit2;
	int lDist;

	int mainLineLength;
	int mainHeight;
	int croppedHeight;
	int croppedLineLength;

	unsigned char* byte5ForPattMatchDiv3;

	int sportSenLimit;
	int sroppedLineLength;
	int sportPos;
	bool useSport;

	int colorTarget2;
	int capColorOffset;
	int targetR;
	int targetG;
	int targetB;

	ImageFilters method;
	FloatRangeResult* res[NUM_CAP_INSP];

	int MainDisplayImageHeight;
	int MainDisplayImageWidth;

public:
	CapInspector();
	virtual ~CapInspector();
	   
	// Inherited via Inspector
	virtual void Inspect(CapturedImage * capImg, ResultsHandler* handler) override;
	virtual void ResetForJob() override;
	virtual void SetAppConfig(AppConfig* config) override;
	SpecialPoint FindNeckLipPat(unsigned char* im_srcIn, Point side, bool left);
	Point FindCapSide(unsigned char* im_srcIn, int MidCap, bool left);
	Point FindCapTop(unsigned char* im_srcIn, int SideDist, int side);
	TwoPoint CheckSportBand(unsigned char* im_srcIn, Point CapLTop, Point CapRTop);
	float FindCapMid(unsigned char* im_srcIn, float MidTop);


};

#endif