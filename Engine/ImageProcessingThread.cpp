#include "ImageProcessingThread.h"
#include "ThreadCounter.h"

void ImageProcessingThread::operator()(ImageProcessor* ip, CounterLocks* lk) const
{
	ThreadCounter TC(lk);  //Track if thread is still open
	CapturedImage* im;
	while (im = (CapturedImage*)ip->Pop())
	{
		if (!im->GetBuffer())
		{
			im->Unload();
			break;
		}

#ifdef MEASURETIMES
		Stats* s = new Stats();
		s->timestampStart = GetTickCount();
#endif
		InspectionTrigger::Wait(im->GetImageNumber());
		ip->Inspect(im);
		ip->ProcessJobUpdates();
#ifdef MEASURETIMES
		s->CamNumber = ip->index;
		s->Action = Activity::ProcessImage;
		s->ImageNumber = im->GetImageNumber();
		s->timestampEnd = GetTickCount();
		ip->AddStats(s);
#endif
		if (ip->StopRequested())
			break;
		
	}

}