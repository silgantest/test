#ifndef REUSABLE_H
#define REUSABLE_H

class Reusable 
{
friend class ReusablePool;

protected:
	static int count;
	int id;

public:
	Reusable();

	virtual ~Reusable();

	int GetId();

	virtual void Reset() = 0;
	virtual void Unload() = 0;
};


#endif