
#ifndef STATS_H
#define STATS_H

using namespace std;

enum class Activity
{
	Grab = 0,
	ProcessImage = 1,
	Trigger = 2,
	Unknown
};

class Stats
{
public:
	unsigned int CamNumber;
	int ImageNumber;
	Activity Action;
	unsigned long timestampStart;
	unsigned long timestampEnd;

	Stats() : CamNumber(-1), ImageNumber(-1), Action(Activity::Unknown), timestampStart(-1), timestampEnd(-1) {};
	virtual ~Stats() {};
};

#endif