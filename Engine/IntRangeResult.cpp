#include "IntRangeResult.h"
#include "IntInspectionResultsPool.h"

IntRangeResult::IntRangeResult()
{
	Reset();
}

IntRangeResult::~IntRangeResult()
{
}

void IntRangeResult::SetResult(int res)
{
	x = res;
}

void IntRangeResult::Reset()
{
	x = 0;
}
void IntRangeResult::Unload()
{
	IntInspectionResultsPool::DeleteResultObj(this);
}