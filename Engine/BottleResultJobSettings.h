#include "LabelIntegrityMethod.h"
#include <mutex>

#ifndef BOTTLE_RESULT_JOB_SETTINGS
#define BOTTLE_RESULT_JOB_SETTINGS

class BottleResultJobSettings
{
public:
	LabelIntegrityMethod method;
	ImageFilters filterSelNolabel;
	ImageFilters filterSelSplice;
	Inspection* inspections[INSPECTIONS_85];

	int bottleStyle;
	int wehaveTempX;
	int yStart;
	int xOffset[BOTTLE_CAMS];
	int noLabelOffsetW;
	int noLabelOffsetH;
	int noLabelSens;
	int orienArea;
	int boltOffset;
	int boltOffset2;
	int rotOffset;
	int stLightSen;
	int stDarkSen;
	int stEdgeSen;
	int avoidOffsetX;

	int ellipseSens;
	int stitchW;
	int stitchH;
	int stitchY[BOTTLE_CAMS];
	bool findMiddles;

	ImageFilters stLightImg;
	ImageFilters stDarkImg;
	ImageFilters stEdgeImg;

	bool useStLigh;
	bool useStDark;
	bool useStEdge;

	int wsplice;
	int hsplice;
	int hsplicesmBx;

	int YSplice[BOTTLE_CAMS];
	int spliceVar;


};

#endif