#include "Cap85IntermediateImagePackPool.h"

#include "SystemConfigLoader.h"
#include "IntermediateImagePackPool.h"

//static methods
Cap85IntermediateImagePackPool* Cap85IntermediateImagePackPool::instance = nullptr;
bool Cap85IntermediateImagePackPool::loadedApp = false;

Cap85IntermediateImagePack* Cap85IntermediateImagePackPool::GetImage(CapturedImage* image)
{
	Cap85IntermediateImagePack* b = (Cap85IntermediateImagePack*)instance->Get();
	b->Set(image->GetImageNumber());

	return b;
}

void Cap85IntermediateImagePackPool::DeleteImages(Cap85IntermediateImagePack* obj)
{
	instance->Delete(obj);
}

void Cap85IntermediateImagePackPool::Initialize()
{
	instance = new Cap85IntermediateImagePackPool();
	instance->grow();
}

void Cap85IntermediateImagePackPool::Shutdown()
{
	delete instance;
}

Cap85IntermediateImagePackPool::Cap85IntermediateImagePackPool()
{
	sprintf_s(name, "Cap85IntermediateImagePackPool");
}

Cap85IntermediateImagePackPool::~Cap85IntermediateImagePackPool()
{
	deleteEverything();
}

void Cap85IntermediateImagePackPool::deleteObject(Reusable* obj)
{
	delete (IntermediateImagePack*)obj;
}

Reusable* Cap85IntermediateImagePackPool::create()
{

	SystemConfig* cfg = SystemConfigLoader::GetConfig();
	AppConfig* acfg = cfg->GetAppConfig();
	int dispHeight = acfg->GetMainDisplayImageHeight();
	int dispWidth = acfg->GetMainDisplayImageWidth();
	int bytesPerPixel = acfg->GetBytesPerPixel();

	return new Cap85IntermediateImagePack(dispHeight, dispWidth, bytesPerPixel);
};

void Cap85IntermediateImagePackPool::loadApp()
{

}
