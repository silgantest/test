#include "IntermediateImagePack.h"
#include "IntermediateImagePackPool.h"

void IntermediateImagePack::Reset()
{
	camIndex = -1;
	imageNumber =-1;
	memset(reduced, 255, this->scaledHeight* this->scaledWidth*4);
}

void IntermediateImagePack::Unload()
{
	IntermediateImagePackPool::DeleteImages(this);
}

void IntermediateImagePack::Set(int imNum, int camNum)
{
	camIndex = camNum;
	imageNumber = imNum;

}

unsigned char* IntermediateImagePack::GetFilter(ImageFilters filter)
{
	switch (filter)
	{
	case ImageFilters::BlackAndWhite:
		return this->bw;
	case ImageFilters::Saturation:
		return this->satDiv3;
	case ImageFilters::Magenta:
		return this->magDiv3;
	case ImageFilters::Yellow:
		return this->yelDiv3;
	case ImageFilters::Cyan:
		return this->cyaDiv3;
	case ImageFilters::Red:
		return this->redbw;
	case ImageFilters::Green:
		return this->greenbw;
	case ImageFilters::Blue:
		return this->bluebw;
	}
	return nullptr;
}

/*

	unsigned char* filterForPattMatchDiv3;  //reference only, Set filter
	unsigned char* filterForPattMatchDiv6;  //reference only, Set filter
	unsigned char* imgMiddBottle;  //reference only, SetMiddBottle
	unsigned char* filterForShiftedDiv3; //reference only
*/

IntermediateImagePack::IntermediateImagePack(size_t height, size_t width, size_t scaledheight, size_t scaledwidth, size_t bytesPerPixel) : 
	camIndex(-1), filterForPattMatchDiv3(0), filterForPattMatchDiv6(0), filterForShiftedDiv3(0), imgMiddBottle(0)
{

	this->scaledHeight = scaledwidth ;
	this->scaledWidth = scaledheight;


	this->reduced = new unsigned char[width * height * 3];
	this->satDiv3 = new unsigned char [width * height];
	this->sa2Div3 = new unsigned char[width * height];
	this->bw = new unsigned char[width * height];
	this->magDiv3 = new unsigned char[width * height];
	this->yelDiv3 = new unsigned char[width * height];
	this->cyaDiv3 = new unsigned char[width * height];
	this->bluebwDiv6 = new unsigned char[width * height];
	this->bluebw = new unsigned char[width * height];
	this->greenbw = new unsigned char[width * height];
	this->redbw = new unsigned char[width * height];

	this->edgH = new unsigned char[width * height];
	this->edgV = new unsigned char[width * height];
	this->bin = new unsigned char[width * height];
	this->byteBinEdges = new unsigned char[width * height];
	this->edge = new unsigned char[width * height];

	this->magDiv6 = new unsigned char[width * height];
	this->yelDiv6 = new unsigned char[width * height];
	this->cyaDiv6 = new unsigned char[width * height];
	this->redbwDiv6 = new unsigned char[width * height];
	this->greenbwDiv6 = new unsigned char[width * height];
	this->satDiv6 = new unsigned char[width * height];
	this->bwDiv6 = new unsigned char[width * height];

	this->scaled = new unsigned char[scaledheight * scaledwidth * bytesPerPixel];
	this->rotated = new unsigned char[scaledheight * scaledwidth * bytesPerPixel];
	this->gray = new unsigned char[scaledheight * scaledwidth * bytesPerPixel];

	this->byteBinMet5 = new unsigned char[width * height];

	int count = width * height * bytesPerPixel;
	count += width * height * 23;
	count += scaledwidth * scaledheight * bytesPerPixel * 2;
	count += scaledwidth * scaledheight;
}

IntermediateImagePack::~IntermediateImagePack()
{
	delete[] this->reduced;
	delete[] this->satDiv3;
	delete[] this->sa2Div3;
	delete[] this->bw;
	delete[] this->magDiv3;
	delete[] this->yelDiv3;
	delete[] this->cyaDiv3;
	delete[] this->bluebw;
	delete[] this->greenbw;
	delete[] this->redbw;
	delete[] this->edgH;
	delete[] this->edgV;
	delete[] this->edge;
	delete[] this->bin;
	delete[] this->byteBinEdges;

	delete[] this->magDiv6;
	delete[] this->yelDiv6;
	delete[] this->cyaDiv6;
	delete[] this->redbwDiv6;
	delete[] this->greenbwDiv6;
	delete[] this->bluebwDiv6;
	delete[] this->satDiv6;
	delete[] this->bwDiv6;

	delete[] this->rotated;
	delete[] this->gray;

	delete[] this->byteBinMet5;
	delete[] this->scaled;
}

void IntermediateImagePack::SetFilterForPattMatch(ImageFilters filterSelPatt)
{
	if (filterSelPatt == ImageFilters::BlackAndWhite) { filterForPattMatchDiv3 = bw; filterForPattMatchDiv6 = bwDiv6; }
	else if (filterSelPatt == ImageFilters::Saturation) { filterForPattMatchDiv3 = satDiv3;  filterForPattMatchDiv6 = satDiv6;}
	else if (filterSelPatt == ImageFilters::Magenta) { filterForPattMatchDiv3 = magDiv3;  filterForPattMatchDiv6 = magDiv6;}
	else if (filterSelPatt == ImageFilters::Yellow) { filterForPattMatchDiv3 = yelDiv3; filterForPattMatchDiv6 = yelDiv6;}
	else if (filterSelPatt == ImageFilters::Cyan) { filterForPattMatchDiv3 = cyaDiv3; filterForPattMatchDiv6 = cyaDiv6;}
	else if (filterSelPatt == ImageFilters::Red) { filterForPattMatchDiv3 = redbw; filterForPattMatchDiv6 = redbwDiv6;}
	else if (filterSelPatt == ImageFilters::Green) { filterForPattMatchDiv3 = greenbw; filterForPattMatchDiv6 = greenbwDiv6;}
	else if (filterSelPatt == ImageFilters::Blue) { filterForPattMatchDiv3 = bluebw; filterForPattMatchDiv6 = bluebwDiv6;}
}

void IntermediateImagePack::SetMiddBottle(int midImg)
{
	if (midImg == 1) { imgMiddBottle = bw; }
	else if (midImg == 2) { imgMiddBottle = satDiv3; }
	else if (midImg == 3) { imgMiddBottle = magDiv3; }
	else if (midImg == 4) { imgMiddBottle = yelDiv3; }
	else if (midImg == 5) { imgMiddBottle = cyaDiv3; }
	else if (midImg == 6) { imgMiddBottle = redbw; }
	else if (midImg == 7) { imgMiddBottle = greenbw; }
	else if (midImg == 8) { imgMiddBottle = bluebw; }
}

void  IntermediateImagePack::SetFilterForShifted(ImageFilters filterSelShifted)
{

	if (filterSelShifted == ImageFilters::BlackAndWhite) { filterForShiftedDiv3 = bw; }
	else if (filterSelShifted == ImageFilters::Saturation) { filterForShiftedDiv3 = satDiv3; }
	else if (filterSelShifted == ImageFilters::Magenta) { filterForShiftedDiv3 = magDiv3; }
	else if (filterSelShifted == ImageFilters::Yellow) { filterForShiftedDiv3 = yelDiv3; }
	else if (filterSelShifted == ImageFilters::Cyan) { filterForShiftedDiv3 = cyaDiv3; }
	else if (filterSelShifted == ImageFilters::Red) { filterForShiftedDiv3 = redbw; }
	else if (filterSelShifted == ImageFilters::Yellow) { filterForShiftedDiv3 = greenbw; }
	else if (filterSelShifted == ImageFilters::Blue) { filterForShiftedDiv3 = bluebw; }
}


