#include "CapturedImage.h"
#include "ReusablePool.h"
#include "Cap85IntermediateImagePack.h"

#ifndef CAP_85_INTERMEDIATE_IMAGE_PACK_POOL_H
#define CAP_85_INTERMEDIATE_IMAGE_PACK_POOL_H

class Cap85IntermediateImagePackPool : ReusablePool
{

private:


	static bool loadedApp;
	static void loadApp();
	static Cap85IntermediateImagePackPool* instance;

protected:
	Cap85IntermediateImagePackPool();

	virtual Reusable* create() override;

	void deleteObject(Reusable* obj) override;
	//void deleteObject(void* obj) override;

public:
	Cap85IntermediateImagePackPool(Cap85IntermediateImagePackPool&) = delete;
	Cap85IntermediateImagePackPool& operator=(Cap85IntermediateImagePackPool& rhs) = delete;

	virtual ~Cap85IntermediateImagePackPool();

	static Cap85IntermediateImagePack* GetImage(CapturedImage* image);
	static void DeleteImages(Cap85IntermediateImagePack* obj);
	static void Initialize();
	static void Shutdown();
};

#endif
