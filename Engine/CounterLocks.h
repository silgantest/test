#include <mutex>

#ifndef COUNTER_LOCKS_H
#define COUNTER_LOCKS_H

class CounterLocks
{

private:
	std::mutex				 mtx_thread_count;
	std::condition_variable  cv_thread_count;

public:
	CounterLocks() = default;
	~CounterLocks() = default;
	CounterLocks& operator=(CounterLocks& rhs) = delete;
	CounterLocks(CounterLocks&) = delete;

	void ThreadCountNotify();
	void ThreadCountWait();

};

#endif COUNTER_LOCKS_H