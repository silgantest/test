#include "CapturedImage.h"
#include <mutex>
#ifndef SIM_CAPTURED_IMAGE_H
#define SIM_CAPTURED_IMAGE_H

class SimCapturedImage : public CapturedImage
{
	friend class SimCapturedImagePool;

protected:
	unsigned char* imageData;
	int width;
	int height;

	void Set(unsigned char* image, int imNum, int camNum, int width, int height);
	SimCapturedImage();
public:

	virtual ~SimCapturedImage();
	virtual void* GetBuffer();
	virtual int GetWidth();
	virtual int GetHeight();
	virtual void Reset() override;
	virtual void Unload() override;


};
#endif
