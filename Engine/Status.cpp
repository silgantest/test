#include "Status.h"

Status* Status::currentStatus;

Status::Status()
{
	memset(&ReasonForFailure, 0, MAX_INSPECTIONS * sizeof(int));
	memset(&currentIndices, 0, MAX_INSPECTIONS * sizeof(int));
	memset(&Trends, 0, CIRCULAR_BUFFER_SIZE + MAX_INSPECTIONS * sizeof(bool));
}

Status::~Status()
{

}

void Status::Shutdown()
{
	delete currentStatus;
}

void Status::Initialize()
{
	currentStatus = new Status();
}

Status* Status::GetStatus()
{
	return currentStatus;
}

void Status::UpdateFromResultSet(Results* results)
{
	for (int i = 0; i < MAX_INSPECTIONS; i++)
		UpdateFromResultSet(i, results->resultSet[i].passed);
}

void Status::UpdateFromResultSet(int index, bool pass)
{
	inspected++;
	if (!pass)
	{
		ReasonForFailure[index]++;
		rejected++;
	}
	Trends[index][currentIndices[index]] = pass;
	currentIndices[index]++;
	currentIndices[index] = currentIndices[index] & (CIRCULAR_BUFFER_SIZE - 1);
}

void Status::GetStatusArray(unsigned char* buffer)
{
	memcpy(buffer, ReasonForFailure, MAX_INSPECTIONS * sizeof(int));
	int index = MAX_INSPECTIONS * sizeof(int) - 1;
	memcpy(&buffer[index], Trends, MAX_INSPECTIONS * CIRCULAR_BUFFER_SIZE * sizeof(bool));
	index += MAX_INSPECTIONS * CIRCULAR_BUFFER_SIZE * sizeof(bool);
	memcpy(&buffer[index], (void*)inspected, sizeof(int));
	memcpy(&buffer[index], (void*)rejected, sizeof(int));
	memcpy(&buffer[index], (void*)mode, sizeof(int));
}
