#include "InspectionUtilities.h"
#include "Definitions.h"
#include <algorithm> 
#include "ImageFileUtilities.h"

using namespace std;

InspectionUtilities::InspectionUtilities()
{
}


double InspectionUtilities::FindAverageXValue(int* coordinates, int numPoints)
{
	double average = 0;

	for (int i = 0; i < numPoints; i++)
	{
		average = average + coordinates[i*2];
	}

	average = average / numPoints;

	return average;
}

int InspectionUtilities::AveragePixels(unsigned char* img, int imageWidth, int imageHeight,
	int averagingWindowLength,
	int averagingCenterYCoordinate, int averagingCenterXCoordinate,
	bool isVerticalAverage)
{
	int pixelAvg = 0;
	int xCoordinate = 0;
	int yCoordinate = 0;
	for (int offSetIndex = -averagingWindowLength; offSetIndex <= averagingWindowLength; offSetIndex++)
	{
		xCoordinate = averagingCenterXCoordinate + offSetIndex * (!isVerticalAverage);
		yCoordinate = averagingCenterYCoordinate + offSetIndex * isVerticalAverage;

		if (xCoordinate >= 0 && xCoordinate < imageWidth &&
			yCoordinate >= 0 && yCoordinate < imageHeight)
		{
			pixelAvg = pixelAvg + img[xCoordinate + yCoordinate * imageWidth];
		}
		else
		{
			pixelAvg = 0;
			break;
		}
	}
	pixelAvg = pixelAvg / ((2 * averagingWindowLength) + 1);

	return pixelAvg;
}

void InspectionUtilities::FindEdge(unsigned char* img, int imageWidth, int imageHeight,
	int topBound, int bottomBound,
	int leftBound, int rightBound,
	int verticalSearchLength, int horizontalSearchLength,
	int edgeThreshold,
	int searchLeftToRight,
	int* coordinatesOfEdgePoints, int numPoints)
{
	//coordinatesOfEdgePoints.clear();					//Resulting edge coordinates.

	//Check if input parameters are valid;
	bool inputParametersValid = true;

	if (img == NULL)
	{
		inputParametersValid = false;
	}
	if (imageWidth <= 0 || imageHeight <= 0)
	{
		inputParametersValid = false;
	}

	if (topBound < verticalSearchLength || topBound >= bottomBound)
	{
		inputParametersValid = false;
	}
	if (bottomBound > (imageHeight - 1) - verticalSearchLength)
	{
		inputParametersValid = false;
	}

	if (leftBound < horizontalSearchLength || leftBound >= rightBound)
	{
		inputParametersValid = false;
	}
	if (rightBound > (imageWidth - 1) - horizontalSearchLength)
	{
		inputParametersValid = false;
	}

	if (verticalSearchLength < 1 || verticalSearchLength > 20)
	{
		inputParametersValid = false;
	}
	if (horizontalSearchLength < 1 || horizontalSearchLength > 20)
	{
		inputParametersValid = false;
	}

	if (inputParametersValid)
	{
		int verticalNumberOfPixelsToCheck = (bottomBound - topBound) + 1;

		int verticalStartCoordinate = topBound;
		int verticalEndCoordinate = bottomBound;
		int verticalIncrement = 1;

		int horizontalStartCoordinate;
		int horizontalEndCoordinate;
		int horizontalIncrement;

		if (searchLeftToRight)
		{
			horizontalStartCoordinate = leftBound;
			horizontalEndCoordinate = rightBound;
			horizontalIncrement = 1;
		}
		else
		{
			horizontalStartCoordinate = rightBound;
			horizontalEndCoordinate = leftBound;
			horizontalIncrement = -1;
		}

		double basePixelsAvg = 0;									//Average value of base pixel and its vertical neighbors.
		double offsetPixelsAvg = 0;									//Average value of currently inspected pixel and its vertical neighbors.
		int edgeVectorIndex = 0;
		int xBeingChecked = 0;
		double absValue = 0;
		bool edgeFound = false;

		bool isVerticalAverage = true;

		for (int y = verticalStartCoordinate; y <= verticalEndCoordinate; y += verticalIncrement)
		{
			edgeFound = false;
			for (int x = horizontalStartCoordinate; x >= leftBound && x <= rightBound; x += horizontalIncrement)
			{
				//Compute vertical average for horizontal base pixel. Takes verticalSearchLength
				//before and after the current pixel.
				basePixelsAvg = AveragePixels(img, imageWidth, imageHeight, verticalSearchLength, y, x, isVerticalAverage);

				//Go to next horizontal offset.
				for (int xOffsetIndex = 1 * horizontalIncrement; xOffsetIndex * horizontalIncrement <= horizontalSearchLength; xOffsetIndex += horizontalIncrement)
				{
					//Compute vertical average for horizontally offset pixel.
					xBeingChecked = x + xOffsetIndex;
					offsetPixelsAvg = AveragePixels(img, imageWidth, imageHeight, verticalSearchLength, y, xBeingChecked, isVerticalAverage);
					//Compute difference between base pixel and offset pixel.
					absValue = abs(basePixelsAvg - offsetPixelsAvg);

					//If difference is large enough consider edge found and stop searching.
					if (absValue > edgeThreshold)
					{
						SinglePoint tempSinglePoint;
						tempSinglePoint.x = xBeingChecked;
						tempSinglePoint.y = y;

						edgeVectorIndex = y - topBound;
						if (tempSinglePoint.x >= 0 && tempSinglePoint.x < imageWidth &&
							tempSinglePoint.y >= 0 && tempSinglePoint.y < imageHeight &&
							edgeVectorIndex >= 0 && edgeVectorIndex < numPoints)
						{
							coordinatesOfEdgePoints[edgeVectorIndex*2] = tempSinglePoint.x;
							coordinatesOfEdgePoints[edgeVectorIndex * 2 + 1] = tempSinglePoint.y;
						}
						edgeFound = true;
						break;
					}
				}

				if (edgeFound)
				{
					break;
				}
			}
		}
	}
}


void InspectionUtilities::FillEdge(int* edgeCoordinates,
	int avgWindowSize, int edgeCoordinateDeviation,
	bool fillTopToBottom, std::deque<SinglePoint>* coordinatesQueue,
	int* edgeCoordinatesFilled, int numPoints, int vectorSize)
{
	coordinatesQueue->clear();
	//Initialize filled edge coordinates with 0 x values, y values are copied over from the edge coordinates vector.

	memset(edgeCoordinatesFilled, 0, sizeof(int) * vectorSize);

	int index = 0;
	if (numPoints > 0)
	{
		for (int i = 0; i < numPoints; i++)
		{
			edgeCoordinatesFilled[index + 1] = edgeCoordinates[index + 1];
			index += 2;
		}

		int fillStartingIndex;
		int fillEndingIndex;
		int fillIncrement;

		if (fillTopToBottom)
		{
			fillStartingIndex = 0;
			fillEndingIndex = numPoints - 1;
			fillIncrement = 1;
		}
		else
		{
			fillStartingIndex = numPoints - 1;
			fillEndingIndex = 0;
			fillIncrement = -1;
		}

		//Find x coordinates of top segment
		int initialAverage = 0;
		for (int edgeVectorIndex = fillStartingIndex; edgeVectorIndex != (fillStartingIndex + avgWindowSize * fillIncrement); edgeVectorIndex = edgeVectorIndex + fillIncrement)
		{
			initialAverage = initialAverage + edgeCoordinates[edgeVectorIndex*2];
			index += 2;
		}
		initialAverage = initialAverage / avgWindowSize;

		int initialBetterAverage = 0;
		int numberOfInitialGoodPoints = 0;
		index = fillStartingIndex * 2;
		for (int edgeVectorIndex = fillStartingIndex; edgeVectorIndex < numPoints && edgeVectorIndex <(fillEndingIndex + avgWindowSize * fillIncrement); edgeVectorIndex += fillIncrement)
			//for (int edgeVectorIndex = fillStartingIndex; edgeVectorIndex != (fillEndingIndex + avgWindowSize * fillIncrement); edgeVectorIndex = edgeVectorIndex + fillIncrement)
			{
			if (abs(edgeCoordinates[edgeVectorIndex*2] - initialAverage) < edgeCoordinateDeviation)
			{
				initialBetterAverage = initialBetterAverage + edgeCoordinates[edgeVectorIndex*2];
				numberOfInitialGoodPoints++;
			}
		}

		if (numberOfInitialGoodPoints > 0)
		{
			initialBetterAverage = initialBetterAverage / numberOfInitialGoodPoints;
		}
		else
		{
			initialBetterAverage = initialAverage;
		}

		SinglePoint tempSinglePoint;
		for (int edgeVectorIndex = fillStartingIndex; edgeVectorIndex < numPoints && edgeVectorIndex != (fillEndingIndex + avgWindowSize * fillIncrement); edgeVectorIndex = edgeVectorIndex + fillIncrement)
		{
			tempSinglePoint.x = initialBetterAverage;
			tempSinglePoint.y = edgeCoordinates[edgeVectorIndex*2+1];
			coordinatesQueue->push_back(tempSinglePoint);
		}

		//Remove outliers
		for (int edgeVectorIndex = fillStartingIndex; edgeVectorIndex <numPoints && edgeVectorIndex != (fillEndingIndex + fillIncrement); edgeVectorIndex = edgeVectorIndex + fillIncrement)
		{
			int coordinateAverage = 0;
			for (size_t coordinatesQueueIndex = 0; coordinatesQueueIndex <= coordinatesQueue->size() - 1; coordinatesQueueIndex++)
			{
				coordinateAverage = coordinateAverage + (*coordinatesQueue)[coordinatesQueueIndex].x;
			}
			coordinateAverage = coordinateAverage / (coordinatesQueue->size());

			if ((edgeCoordinates[edgeVectorIndex*2] - coordinateAverage) > edgeCoordinateDeviation)
			{
				tempSinglePoint.x = coordinateAverage;
				tempSinglePoint.y = edgeCoordinates[edgeVectorIndex*2+1];

				edgeCoordinatesFilled[edgeVectorIndex*2] = tempSinglePoint.x;
				edgeCoordinatesFilled[edgeVectorIndex * 2 + 1] = tempSinglePoint.y;
				coordinatesQueue->pop_front();
				coordinatesQueue->push_back(tempSinglePoint);
			}
			else if ((edgeCoordinates[edgeVectorIndex*2] - coordinateAverage) < -edgeCoordinateDeviation)
			{
				tempSinglePoint.x = coordinateAverage;
				tempSinglePoint.y = edgeCoordinates[edgeVectorIndex*2 + 1];

				edgeCoordinatesFilled[edgeVectorIndex*2] = tempSinglePoint.x;
				edgeCoordinatesFilled[edgeVectorIndex * 2] = tempSinglePoint.y;
				coordinatesQueue->pop_front();
				coordinatesQueue->push_back(tempSinglePoint);
			}
			else
			{
				tempSinglePoint.x = edgeCoordinates[edgeVectorIndex*2];
				tempSinglePoint.y = edgeCoordinates[edgeVectorIndex*2+1];

				edgeCoordinatesFilled[edgeVectorIndex*2] = tempSinglePoint.x;
				edgeCoordinatesFilled[edgeVectorIndex*2+1] = tempSinglePoint.y;
				coordinatesQueue->pop_front();
				coordinatesQueue->push_back(tempSinglePoint);
			}

			//If edge was never found assign average edge x coordinate.
			if (edgeCoordinates[edgeVectorIndex*2] == 0)
			{
				tempSinglePoint.x = coordinateAverage;
				tempSinglePoint.y = edgeCoordinates[edgeVectorIndex*2+1];

				edgeCoordinatesFilled[edgeVectorIndex*2] = tempSinglePoint.x;
				edgeCoordinatesFilled[edgeVectorIndex * 2+1] = tempSinglePoint.y;
				coordinatesQueue->pop_front();
				coordinatesQueue->push_back(tempSinglePoint);
			}
		}
	}
}

linearRegData InspectionUtilities::linearRegY(std::vector<SinglePoint>& points)
{
	linearRegData result;

	result.avgx = 0;
	result.avgy = 0;
	result.incl = 0;
	result.angIncRad = 0;
	result.angIncDeg = 0;

	int npoints = points.size();

	if (npoints > 2)
	{
		long int sumX = 0;	long int sumY = 0;
		long int sumXY = 0;	long int sumYY = 0;

		float avgX = 0;	float avgY = 0;
		float avgXY = 0;	float avgYY = 0;
		float incli = 0;

		SinglePoint tmp;

		for (int i = 0; i < npoints; i++)
		{
			tmp = points[i];

			sumX += tmp.x;
			sumY += tmp.y;
			sumXY += tmp.x * tmp.y;
			sumYY += tmp.y * tmp.y;
		}

		avgX = float(sumX) / npoints;
		avgY = float(sumY) / npoints;
		avgXY = float(sumXY) / npoints;
		avgYY = float(sumYY) / npoints;

		float denomin = avgYY - avgY * avgY;
		if (denomin <= 0) { denomin = 1; }

		incli = (avgXY - avgX * avgY) / denomin;

		result.avgx = avgX;
		result.avgy = avgY;
		result.incl = incli;
		result.angIncRad = atan(incli);
		result.angIncDeg = (atan(incli)) * 180.0f / 3.1416f;
	}

	return result;
}


/**Good**/
void InspectionUtilities::ScaleImageDownBySubsampling(unsigned char* full, IntermediateImagePack* pack, int imageWidth, int imageHeight)
{


	int imageWidthOrg = imageWidth;
	int imageHeightOrg = imageHeight;
	int imageStrideOrg = imageWidthOrg * BYTES_PER_PIXEL;

	int xPositionInArrayOrg = 0;

	int imageWidthNew = imageWidthOrg / FullImageScaleDownFactor;
	int imageHeightNew = imageHeightOrg / FullImageScaleDownFactor;
	int imageStrideNew = imageWidthNew * BYTES_PER_PIXEL;

	int xCoordinateNew = 0;
	int yCoordinateNew = 0;

	int xPositionInArrayNew = 0;
	int yPositionInArrayNew = 0;

	int newXcoordinate = 0;
	int newYcoordinate = 0;

	for (int x = 0; x < imageWidth; x += FullImageScaleDownFactor)
	{
		for (int y = 0; y < imageHeight; y += FullImageScaleDownFactor)
		{

			xPositionInArrayOrg = x * BYTES_PER_PIXEL;

			xCoordinateNew = x / FullImageScaleDownFactor;
			yCoordinateNew = y / FullImageScaleDownFactor;

			xPositionInArrayNew = xCoordinateNew * BYTES_PER_PIXEL;
			yPositionInArrayNew = yCoordinateNew;

			pack->scaled[xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = full[xPositionInArrayOrg + y * imageStrideOrg];
			pack->scaled[1 + xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = full[1 + xPositionInArrayOrg + y * imageStrideOrg];
			pack->scaled[2 + xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = full[2 + xPositionInArrayOrg + y * imageStrideOrg];
			pack->scaled[3 + xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = full[3 + xPositionInArrayOrg + y * imageStrideOrg];
		}
	}
}

void InspectionUtilities::ConvertBGRAToGray(IntermediateImagePack* pack)
{

	int bytesPerPixelOld = 4;
	int bytesPerPixelNew = 1;

	int imageWidthOrg = pack->scaledWidth;
	int imageHeightOrg = pack->scaledHeight;
	int imageStrideOrg = imageWidthOrg * bytesPerPixelOld;

	int xCoordinateOrg = 0;
	int yCoordinateOrg = 0;

	int xPositionInArrayOrg = 0;
	int yPositionInArrayOrg = 0;

	int imageWidthNew = imageWidthOrg;
	int imageHeightNew = imageHeightOrg;
	int imageStrideNew = imageWidthNew * bytesPerPixelNew;

	int xCoordinateNew = 0;
	int yCoordinateNew = 0;

	int xPositionInArrayNew = 0;
	int yPositionInArrayNew = 0;

	int newXcoordinate = 0;
	int newYcoordinate = 0;

	double greyScaleValue = 0;

	for (size_t x = 0; x < pack->scaledWidth; x++)
	{
		for (size_t y = 0; y < pack->scaledHeight; y++)
		{
			xCoordinateOrg = x;
			yCoordinateOrg = y;

			xPositionInArrayOrg = xCoordinateOrg * bytesPerPixelOld;
			yPositionInArrayOrg = yCoordinateOrg;

			xCoordinateNew = xCoordinateOrg;
			yCoordinateNew = yCoordinateOrg;

			xPositionInArrayNew = xCoordinateNew;
			yPositionInArrayNew = yCoordinateNew;

			greyScaleValue = 0.07 * pack->rotated[1 + xPositionInArrayOrg + yPositionInArrayOrg * imageStrideOrg] +
				0.72 * pack->rotated[2 + xPositionInArrayOrg + yPositionInArrayOrg * imageStrideOrg] +
				0.21 * pack->rotated[3 + xPositionInArrayOrg + yPositionInArrayOrg * imageStrideOrg];

			pack->gray[xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = greyScaleValue + 0.5;
		}
	}
}

void InspectionUtilities::Rotate(IntermediateImagePack* pack, bool rot90deg)
{
	if (rot90deg)
		Rotate90Right(pack);
	else
		Rotate270Right(pack);

}

void InspectionUtilities::Rotate270Right(IntermediateImagePack* pack)
{
	int imageWidthOrg = pack->scaledWidth;
	int imageHeightOrg = pack->scaledHeight;

	int imageWidthNew = pack->scaledHeight;
	int imageHeightNew = pack->scaledWidth;

	int imageStrideOrg = imageWidthOrg * BYTES_PER_PIXEL;
	int imageStrideNew = pack->scaledHeight * BYTES_PER_PIXEL;

	int xCoordinateOrg = 0;
	int yCoordinateOrg = 0;

	int xCoordinateNew = 0;
	int yCoordinateNew = 0;

	int xPositionInArrayOrg = 0;
	int yPositionInArrayOrg = 0;

	int xPositionInArrayNew = 0;
	int yPositionInArrayNew = 0;


	int newXcoordinate = 0;
	int newYcoordinate = 0;

	for (size_t x = 0; x < pack->scaledWidth; x++)
	{
		for (size_t y = 0; y < pack->scaledHeight; y++)
		{
			xCoordinateOrg = x;
			yCoordinateOrg = y;

			xPositionInArrayOrg = xCoordinateOrg * BYTES_PER_PIXEL;
			yPositionInArrayOrg = yCoordinateOrg;



			xCoordinateNew = yCoordinateOrg;
			yCoordinateNew = (pack->scaledWidth - xCoordinateOrg) - 1;

			xPositionInArrayNew = xCoordinateNew * BYTES_PER_PIXEL;
			yPositionInArrayNew = yCoordinateNew;


			pack->rotated[xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = pack->scaled[xPositionInArrayOrg + yPositionInArrayOrg * imageStrideOrg];
			pack->rotated[1 + xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = pack->scaled[1 + xPositionInArrayOrg + yPositionInArrayOrg * imageStrideOrg];
			pack->rotated[2 + xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = pack->scaled[2 + xPositionInArrayOrg + yPositionInArrayOrg * imageStrideOrg];
			pack->rotated[3 + xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = pack->scaled[3 + xPositionInArrayOrg + yPositionInArrayOrg * imageStrideOrg];
		}
	}
}

void InspectionUtilities::Rotate90Right(IntermediateImagePack* pack)
{
	int imageWidthOrg = pack->scaledWidth;
	int imageHeightOrg = pack->scaledHeight;

	int imageWidthNew = pack->scaledHeight;
	int imageHeightNew = pack->scaledWidth;

	int imageStrideOrg = imageWidthOrg * BYTES_PER_PIXEL;
	int imageStrideNew = pack->scaledHeight * BYTES_PER_PIXEL;

	int xCoordinateOrg = 0;
	int yCoordinateOrg = 0;

	int xCoordinateNew = 0;
	int yCoordinateNew = 0;

	int xPositionInArrayOrg = 0;
	int yPositionInArrayOrg = 0;

	int xPositionInArrayNew = 0;
	int yPositionInArrayNew = 0;


	int newXcoordinate = 0;
	int newYcoordinate = 0;

	for (size_t x = 0; x < pack->scaledWidth; x++)
	{
		for (size_t y = 0; y < pack->scaledHeight; y++)
		{
			xCoordinateOrg = x;
			yCoordinateOrg = y;

			xPositionInArrayOrg = xCoordinateOrg * BYTES_PER_PIXEL;
			yPositionInArrayOrg = yCoordinateOrg;


			xCoordinateNew = (pack->scaledHeight - yCoordinateOrg) - 1;
			yCoordinateNew = x;

			xPositionInArrayNew = xCoordinateNew * BYTES_PER_PIXEL;
			yPositionInArrayNew = yCoordinateNew;

			pack->rotated[xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = pack->scaled[xPositionInArrayOrg + yPositionInArrayOrg * imageStrideOrg];
			pack->rotated[1 + xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = pack->scaled[1 + xPositionInArrayOrg + yPositionInArrayOrg * imageStrideOrg];
			pack->rotated[2 + xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = pack->scaled[2 + xPositionInArrayOrg + yPositionInArrayOrg * imageStrideOrg];
			pack->rotated[3 + xPositionInArrayNew + yPositionInArrayNew * imageStrideNew] = pack->scaled[3 + xPositionInArrayOrg + yPositionInArrayOrg * imageStrideOrg];
		}
	}
}




void InspectionUtilities::Reduce270(unsigned char* Image, IntermediateImagePack* imgPak, int mainDisWidth, int mainDisHeight)
{

	int nCam = 1;
	int cWidth = mainDisWidth;
	int cHeight = mainDisHeight;
	int cOffset = 0;

	int pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pix1x, pix2x, pix3x, pix4x, pix5x, pix6x, pix7x, pix8x, pix9x;
	int pix1y, pix2y, pix3y, pix4y, pix5y, pix6y, pix7y, pix8y, pix9y;

	int pixR, pixG, pixB;
	int pixC; int pixM; int pixY;

	int _max, _min, I;//, S, 
	int SI, SI2;

	int origLineLength = 4 * cWidth;

	//this is a rotated image so height will become new length
	int newLineLengthColDiv3 = 4 * cHeight / 3;
	int newLengthBWDiv3 = cHeight / 3;
	int newHeightBWDiv3 = cWidth / 3;

	bool debugging = false;

	int startx = 0;	int endx = 4 * cWidth;
	int starty = 0;	int endy = cHeight;

	int j = 0;
	int g = 0;

	int l = newHeightBWDiv3 + cOffset;

	int shift = 0;
	//int	origLineLengthTimesiplus0;
	//int	origLineLengthTimesiplus1;
	//int	origLineLengthTimesiplus2;

	for (int i = starty; i < endy - 2; i += 3)
	{
		//origLineLengthTimesiplus0=origLineLength*(i+0);
		//origLineLengthTimesiplus1=origLineLength*(i+1);
		//origLineLengthTimesiplus2=origLineLength*(i+2);

		l = (newHeightBWDiv3 - 1) + cOffset;
		//l=newLengthBWDiv3+cOffset;


		int off0 = origLineLength * (i + 0);
		int off1 = origLineLength * (i + 1);
		int off2 = origLineLength * (i + 2);
		for (int k = startx; k < endx - 12; k += 12)
		{
			pix1 = *(Image + k + 0 + off0);
			pix2 = *(Image + k + 1 + off0);
			pix3 = *(Image + k + 2 + off0);
			pix4 = *(Image + k + 4 + off0);
			pix5 = *(Image + k + 5 + off0);
			pix6 = *(Image + k + 6 + off0);
			pix7 = *(Image + k + 8 + off0);
			pix8 = *(Image + k + 9 + off0);
			pix9 = *(Image + k + 10 + off0);

			pix1x = *(Image + k + 0 + off1);
			pix2x = *(Image + k + 1 + off1);
			pix3x = *(Image + k + 2 + off1);
			pix4x = *(Image + k + 4 + off1);
			pix5x = *(Image + k + 5 + off1);
			pix6x = *(Image + k + 6 + off1);
			pix7x = *(Image + k + 8 + off1);
			pix8x = *(Image + k + 9 + off1);
			pix9x = *(Image + k + 10 + off1);

			pix1y = *(Image + k + 0 + off2);
			pix2y = *(Image + k + 1 + off2);
			pix3y = *(Image + k + 2 + off2);
			pix4y = *(Image + k + 4 + off2);
			pix5y = *(Image + k + 5 + off2);
			pix6y = *(Image + k + 6 + off2);
			pix7y = *(Image + k + 8 + off2);
			pix8y = *(Image + k + 9 + off2);
			pix9y = *(Image + k + 10 + off2);

			pixB = (pix1 + pix4 + pix7 + pix1x + pix4x + pix7x + pix1y + pix4y + pix7y) / 9;
			pixG = (pix2 + pix5 + pix8 + pix2x + pix5x + pix8x + pix2y + pix5y + pix8y) / 9;
			pixR = (pix3 + pix6 + pix9 + pix3x + pix6x + pix9x + pix3y + pix6y + pix9y) / 9;

			*(imgPak->reduced + j + 0 + newLineLengthColDiv3 * l) = pixB;
			*(imgPak->reduced + j + 1 + newLineLengthColDiv3 * l) = pixG;
			*(imgPak->reduced + j + 2 + newLineLengthColDiv3 * l) = pixR;

			*(imgPak->bw + g + newLengthBWDiv3 * l) = (pixR + pixG + pixB) / 3;

			////////////////

			*(imgPak->bluebw + g + newLengthBWDiv3 * l) = pixB;
			*(imgPak->greenbw + g + newLengthBWDiv3 * l) = pixG;
			*(imgPak->redbw + g + newLengthBWDiv3 * l) = pixR;

			////////////////

			_min = min(pixB, pixG);	_min = min(_min, pixR);
			_max = max(pixB, pixG);	_max = max(_max, pixR);

			I = _max;

			if (I < 10) { SI = 0; }
			else { SI = 255 * (I - _min) / I; }

			SI = max(SI, 0);	SI = min(SI, 255);

			*(imgPak->sa2Div3 + g + newLengthBWDiv3 * l) = SI;

			///////////////////////

			SI2 = _max - _min;
			*(imgPak->satDiv3 + g + newLengthBWDiv3 * l) = SI2;

			///////////////////////

			pixC = I - pixR; pixM = I - pixG; pixY = I - pixB;

			*(imgPak->magDiv3 + g + newLengthBWDiv3 * l) = pixM;
			*(imgPak->yelDiv3 + g + newLengthBWDiv3 * l) = pixY;
			*(imgPak->cyaDiv3 + g + newLengthBWDiv3 * l) = pixC;

			////////////////

			if (l > 0) { l -= 1; }
		}
	
		j += 4;
		g += 1;
	}

	//////////

	int newLengthBWDiv6 = newLengthBWDiv3 / 2;
	int newHeightBWDiv6 = newHeightBWDiv3 / 2;

	int newLengthBWDiv3yplus0;
	int newLengthBWDiv3yplus1;

	int p = 0; int q = 0; int pix;

	for (int y = 0; y < newHeightBWDiv3 - 1; y += 2)
	{
		newLengthBWDiv3yplus0 = newLengthBWDiv3 * (y + 0);
		newLengthBWDiv3yplus1 = newLengthBWDiv3 * (y + 1);

		p = 0;

		for (int x = 0; x < newLengthBWDiv3 - 1; x += 2)
		{
			pix1 = *((imgPak->bw + x + 0 + newLengthBWDiv3yplus0));
			pix2 = *((imgPak->bw + x + 1 + newLengthBWDiv3yplus0));
			pix3 = *((imgPak->bw + x + 0 + newLengthBWDiv3yplus1));
			pix4 = *((imgPak->bw + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->bwDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////
			pix1 = *((imgPak->satDiv3 + x + 0 + newLengthBWDiv3yplus0));
			pix2 = *((imgPak->satDiv3 + x + 1 + newLengthBWDiv3yplus0));
			pix3 = *((imgPak->satDiv3 + x + 0 + newLengthBWDiv3yplus1));
			pix4 = *((imgPak->satDiv3 + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->satDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////

			pix1 = *((imgPak->magDiv3 + x + 0 + newLengthBWDiv3yplus0));
			pix2 = *((imgPak->magDiv3 + x + 1 + newLengthBWDiv3yplus0));
			pix3 = *((imgPak->magDiv3 + x + 0 + newLengthBWDiv3yplus1));
			pix4 = *((imgPak->magDiv3 + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->magDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////

			pix1 = *((imgPak->yelDiv3 + x + 0 + newLengthBWDiv3yplus0));
			pix2 = *((imgPak->yelDiv3 + x + 1 + newLengthBWDiv3yplus0));
			pix3 = *((imgPak->yelDiv3 + x + 0 + newLengthBWDiv3yplus1));
			pix4 = *((imgPak->yelDiv3 + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->yelDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////

			pix1 = *((imgPak->cyaDiv3 + x + 0 + newLengthBWDiv3yplus0));
			pix2 = *((imgPak->cyaDiv3 + x + 1 + newLengthBWDiv3yplus0));
			pix3 = *((imgPak->cyaDiv3 + x + 0 + newLengthBWDiv3yplus1));
			pix4 = *((imgPak->cyaDiv3 + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->cyaDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////

			pix1 = *((imgPak->redbw + x + 0 + newLengthBWDiv3yplus0));
			pix2 = *((imgPak->redbw + x + 1 + newLengthBWDiv3yplus0));
			pix3 = *((imgPak->redbw + x + 0 + newLengthBWDiv3yplus1));
			pix4 = *((imgPak->redbw + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->redbwDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////

			pix1 = *((imgPak->greenbw + x + 0 + newLengthBWDiv3yplus0));
			pix2 = *((imgPak->greenbw + x + 1 + newLengthBWDiv3yplus0));
			pix3 = *((imgPak->greenbw + x + 0 + newLengthBWDiv3yplus1));
			pix4 = *((imgPak->greenbw + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->greenbwDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////

			pix1 = *((imgPak->bluebw + x + 0 + newLengthBWDiv3yplus0));
			pix2 = *((imgPak->bluebw + x + 1 + newLengthBWDiv3yplus0));
			pix3 = *((imgPak->bluebw + x + 0 + newLengthBWDiv3yplus1));
			pix4 = *((imgPak->bluebw + x + 1 + newLengthBWDiv3yplus1));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->bluebwDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////

			p++;
		}

		q++;
	}

}


void InspectionUtilities::Reduce90(unsigned char* Image, IntermediateImagePack* imgPak, int mainDisWidth, int mainDisHeight)
{
	int nCam = 2;
	int cWidth = mainDisWidth;
	int cHeight = mainDisHeight;
	int cOffset = 0;

	int pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pix1x, pix2x, pix3x, pix4x, pix5x, pix6x, pix7x, pix8x, pix9x;
	int pix1y, pix2y, pix3y, pix4y, pix5y, pix6y, pix7y, pix8y, pix9y;
	int pixR, pixG, pixB;
	int pixC; int pixM; int pixY;
	int _max, _min, I;//, S, 
	int SI, SI2;

	int origLineLength = 4 * cWidth;
	int newLineLengthColDiv3 = 4 * cHeight / 3;

	int newLengthBWDiv3 = cHeight / 3;
	int newHeightBWDiv3 = cWidth / 3;

	bool debugging = false;

	int startx = 0;	int endx = 4 * cWidth;
	int starty = 0;	int endy = cHeight;

	//why on earth did I come up with this?
	//int j =	4*cHeight-4;
	int j = 4 * (newLengthBWDiv3 - 1);

	//int g =	cHeight-1;
	int g = newLengthBWDiv3 - 1;

	int h = cOffset;
	int l = cOffset;


	for (int i = starty; i < endy - 3; i += 3)
	{
		l = cOffset;
		h = cOffset;

		for (int k = startx; k < endx - 12; k += 12)
		{
			pix1 = *((Image + k + origLineLength * (i)));
			pix2 = *((Image + k + 1 + origLineLength * (i)));
			pix3 = *((Image + k + 2 + origLineLength * (i)));
			pix4 = *((Image + k + 4 + origLineLength * (i)));
			pix5 = *((Image + k + 5 + origLineLength * (i)));
			pix6 = *((Image + k + 6 + origLineLength * (i)));

			pix7 = *((Image + k + 8 + origLineLength * (i)));
			pix8 = *((Image + k + 9 + origLineLength * (i)));
			pix9 = *((Image + k + 10 + origLineLength * (i)));

			pix1x = *((Image + k + origLineLength * (i + 1)));
			pix2x = *((Image + k + 1 + origLineLength * (i + 1)));
			pix3x = *((Image + k + 2 + origLineLength * (i + 1)));
			pix4x = *((Image + k + 4 + origLineLength * (i + 1)));
			pix5x = *((Image + k + 5 + origLineLength * (i + 1)));
			pix6x = *((Image + k + 6 + origLineLength * (i + 1)));

			pix7x = *((Image + k + 8 + origLineLength * (i + 1)));
			pix8x = *((Image + k + 9 + origLineLength * (i + 1)));
			pix9x = *((Image + k + 10 + origLineLength * (i + 1)));

			pix1y = *((Image + k + origLineLength * (i + 2)));
			pix2y = *((Image + k + 1 + origLineLength * (i + 2)));
			pix3y = *((Image + k + 2 + origLineLength * (i + 2)));

			pix4y = *((Image + k + 4 + origLineLength * (i + 2)));
			pix5y = *((Image + k + 5 + origLineLength * (i + 2)));
			pix6y = *((Image + k + 6 + origLineLength * (i + 2)));

			pix7y = *((Image + k + 8 + origLineLength * (i + 2)));
			pix8y = *((Image + k + 9 + origLineLength * (i + 2)));
			pix9y = *((Image + k + 10 + origLineLength * (i + 2)));

			pixB = (pix1 + pix4 + pix7 + pix1x + pix4x + pix7x + pix1y + pix4y + pix7y) / 9;
			pixG = (pix2 + pix5 + pix8 + pix2x + pix5x + pix8x + pix2y + pix5y + pix8y) / 9;
			pixR = (pix3 + pix6 + pix9 + pix3x + pix6x + pix9x + pix3y + pix6y + pix9y) / 9;

			*(imgPak->reduced + j + newLineLengthColDiv3 * l) = pixB;
			*(imgPak->reduced + j + 1 + newLineLengthColDiv3 * l) = pixG;
			*(imgPak->reduced + j + 2 + newLineLengthColDiv3 * l) = pixR;

			*(imgPak->bw + g + newLengthBWDiv3 * h) = (pixR + pixG + pixB) / 3;

			////////////////

			*(imgPak->bluebw + g + newLengthBWDiv3 * h) = pixB;
			*(imgPak->greenbw + g + newLengthBWDiv3 * h) = pixG;
			*(imgPak->redbw + g + newLengthBWDiv3 * h) = pixR;

			////////////////

			_min = min(pixB, pixG);	_min = min(_min, pixR);
			_max = max(pixB, pixG);	_max = max(_max, pixR);

			I = _max;

			if (I < 10) { SI = 0; }
			else { SI = 255 * (I - _min) / I; }

			SI = max(SI, 0);	SI = min(SI, 255);

			*(imgPak->sa2Div3+ g + newLengthBWDiv3 * h) = SI;

			//////////////////

			SI2 = _max - _min;
			*(imgPak->sa2Div3 + g + newLengthBWDiv3 * h) = SI2;

			///////////////////////

			pixC = I - pixR; pixM = I - pixG; pixY = I - pixB;

			*(imgPak->magDiv3 + g + newLengthBWDiv3 * h) = pixM;
			*(imgPak->yelDiv3 + g + newLengthBWDiv3 * h) = pixY;
			*(imgPak->cyaDiv3 + g + newLengthBWDiv3 * h) = pixC;

			////////////////

			l += 1;
			h += 1;
		}

		if (j >= 4) { j -= 4; }
		if (g >= 1) { g -= 1; }
	}


	//////////

	int newLengthBWDiv6 = newLengthBWDiv3 / 2;
	int newHeightBWDiv6 = newHeightBWDiv3 / 2;

	int p = 0; int q = 0; int pix;

	for (int y = 0; y < newHeightBWDiv3 - 1; y += 2)
	{
		p = 0;

		for (int x = 0; x < newLengthBWDiv3 - 1; x += 2)
		{
			pix1 = *((imgPak->bw + x + 0 + newLengthBWDiv3 * (y + 0)));
			pix2 = *((imgPak->bw + x + 1 + newLengthBWDiv3 * (y + 0)));
			pix3 = *((imgPak->bw + x + 0 + newLengthBWDiv3 * (y + 1)));
			pix4 = *((imgPak->bw + x + 1 + newLengthBWDiv3 * (y + 1)));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->bwDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1 = *((imgPak->sa2Div3+ x + 0 + newLengthBWDiv3 * (y + 0)));
			pix2 = *((imgPak->sa2Div3 + x + 1 + newLengthBWDiv3 * (y + 0)));
			pix3 = *((imgPak->sa2Div3 + x + 0 + newLengthBWDiv3 * (y + 1)));
			pix4 = *((imgPak->sa2Div3 + x + 1 + newLengthBWDiv3 * (y + 1)));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->satDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////////
			pix1 = *((imgPak->magDiv3 + x + 0 + newLengthBWDiv3 * (y + 0)));
			pix2 = *((imgPak->magDiv3 + x + 1 + newLengthBWDiv3 * (y + 0)));
			pix3 = *((imgPak->magDiv3 + x + 0 + newLengthBWDiv3 * (y + 1)));
			pix4 = *((imgPak->magDiv3 + x + 1 + newLengthBWDiv3 * (y + 1)));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->magDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1 = *((imgPak->yelDiv3 + x + 0 + newLengthBWDiv3 * (y + 0)));
			pix2 = *((imgPak->yelDiv3 + x + 1 + newLengthBWDiv3 * (y + 0)));
			pix3 = *((imgPak->yelDiv3 + x + 0 + newLengthBWDiv3 * (y + 1)));
			pix4 = *((imgPak->yelDiv3 + x + 1 + newLengthBWDiv3 * (y + 1)));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->yelDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1 = *((imgPak->cyaDiv3 + x + 0 + newLengthBWDiv3 * (y + 0)));
			pix2 = *((imgPak->cyaDiv3 + x + 1 + newLengthBWDiv3 * (y + 0)));
			pix3 = *((imgPak->cyaDiv3 + x + 0 + newLengthBWDiv3 * (y + 1)));
			pix4 = *((imgPak->cyaDiv3 + x + 1 + newLengthBWDiv3 * (y + 1)));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->cyaDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1 = *((imgPak->redbw + x + 0 + newLengthBWDiv3 * (y + 0)));
			pix2 = *((imgPak->redbw + x + 1 + newLengthBWDiv3 * (y + 0)));
			pix3 = *((imgPak->redbw + x + 0 + newLengthBWDiv3 * (y + 1)));
			pix4 = *((imgPak->redbw + x + 1 + newLengthBWDiv3 * (y + 1)));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->redbwDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1 = *((imgPak->greenbw + x + 0 + newLengthBWDiv3 * (y + 0)));
			pix2 = *((imgPak->greenbw + x + 1 + newLengthBWDiv3 * (y + 0)));
			pix3 = *((imgPak->greenbw + x + 0 + newLengthBWDiv3 * (y + 1)));
			pix4 = *((imgPak->greenbw + x + 1 + newLengthBWDiv3 * (y + 1)));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->greenbwDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////////

			pix1 = *((imgPak->bluebw + x + 0 + newLengthBWDiv3 * (y + 0)));
			pix2 = *((imgPak->bluebw + x + 1 + newLengthBWDiv3 * (y + 0)));
			pix3 = *((imgPak->bluebw + x + 0 + newLengthBWDiv3 * (y + 1)));
			pix4 = *((imgPak->bluebw + x + 1 + newLengthBWDiv3 * (y + 1)));

			pix = (pix1 + pix2 + pix3 + pix4) / 4;

			*(imgPak->bluebwDiv6 + p + newLengthBWDiv6 * q) = pix;

			////////////////

			p++;
		}

		q++;
	}

}


void InspectionUtilities::Reduce(unsigned char* Image, IntermediateImagePack* imgPak, int mainDisWidth, int mainDisHeight, int rotateCounter)
{
	if (rotateCounter)
		Reduce270(Image, imgPak, mainDisWidth, mainDisHeight);
	else
		Reduce90(Image, imgPak, mainDisWidth, mainDisHeight);
}



