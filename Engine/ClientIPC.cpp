#include "ClientIPC.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include "IPC.h"
#include "ImageFileUtilities.h"
#include <iostream>


///PLATFORM SPECIFIC


DWORD ClientIPC::BUF_SIZE = 0; //main image size * 4 + cap image size
LPCTSTR ClientIPC::szName = SHARED_MEMORY_IMAGES;
HANDLE ClientIPC::hMapFile = nullptr;
HANDLE ClientIPC::semSharedDataReady = nullptr;
HANDLE ClientIPC::mtxSharedDataAccess = nullptr;
unsigned char* ClientIPC::pBuf;
unsigned char* ClientIPC::images[];
unsigned char* ClientIPC::inspections;

void ClientIPC::CloseBuffer()
{
    UnmapViewOfFile(pBuf);

    CloseHandle(hMapFile);
    delete[] inspections;
}

void ClientIPC::PushToClient(ResultsGroup* group)
{
    CapturedImage** list = group->GetImages();
    IntermediateImagePack** packs = group->GetPacks();
    int index, height, width;

    WaitForSingleObject(mtxSharedDataAccess, INFINITE);
    for (int i=0; i<BOTTLE_CAMS; i++)
    {
        index = list[i]->GetCameraIndex();
        height = packs[i]->scaledHeight;
        width = packs[i]->scaledWidth;

        CopyMemory((PVOID)(pBuf +index*IMAGE_SIZE), packs[i]->reduced, height * width * BYTES_PER_PIXEL);
    }

    int capImgSize = group->GetCapPack()->scaledHeight * group->GetCapPack()->scaledWidth;
    CopyMemory((PVOID)(pBuf + BOTTLE_CAMS * IMAGE_SIZE), group->GetCapPack()->reduced, capImgSize * BYTES_PER_PIXEL);
    /*Copy cap filters*/
    int loc = BOTTLE_CAMS * IMAGE_SIZE + capImgSize * BYTES_PER_PIXEL;
    CopyMemory((PVOID)(pBuf + loc), group->GetCapPack()->bw, capImgSize);
    CopyMemory((PVOID)(pBuf + loc + capImgSize), group->GetCapPack()->redbw, capImgSize);
    CopyMemory((PVOID)(pBuf + loc + capImgSize * 2), group->GetCapPack()->greenbw, capImgSize);
    CopyMemory((PVOID)(pBuf + loc + capImgSize * 3), group->GetCapPack()->bluebw, capImgSize);

    group->GetResultsSet()->GetResultsArray(inspections);

    loc = loc + capImgSize * 3;
    CopyMemory((PVOID)(pBuf + loc), inspections, MAX_RESULTS_SIZE);
    loc = loc + MAX_RESULTS_SIZE;

    for (int i = 0; i < TOTAL_CAMS; i++)
    {
        CopyMemory((PVOID)(pBuf + loc), group->GetDetails(i)->Values, MAX_DETAILS_BUFFER);
        loc = loc + MAX_DETAILS_BUFFER;
    }


    ReleaseMutex(mtxSharedDataAccess);

    ReleaseSemaphore(semSharedDataReady, 1, 0);

}

void ClientIPC::Create()
{
    BUF_SIZE = SHARED_MEM_BUFFER_SIZE;

/*    hMapFile = OpenFileMapping(
        FILE_MAP_WRITE,
        false,
        szName);    */             // name of mapping object


    hMapFile = CreateFileMapping(
        INVALID_HANDLE_VALUE,    // use paging file
        NULL,                    // default security
        PAGE_READWRITE,          // read/write access
        0,                       // maximum object size (high-order DWORD)
        BUF_SIZE,                // maximum object size (low-order DWORD)
        szName);                 // name of mapping object

    if (hMapFile == NULL)
    {
        _tprintf(TEXT("Could not create file mapping object (%d).\n"),
            GetLastError());
        return;
    }
    pBuf = (unsigned char*)MapViewOfFile(hMapFile,   // handle to map object
        FILE_MAP_ALL_ACCESS, // read/write permission
        0,
        0,
        BUF_SIZE);

    if (pBuf == NULL)
    {
        _tprintf(TEXT("Could not map view of file (%d).\n"),
            GetLastError());

        CloseHandle(hMapFile);

        return;
    }

    int loc = 0;

    for (int i = 0; i < BOTTLE_CAMS; i++)
    {
        images[i] = &pBuf[loc];
        loc += IMAGE_SIZE;
    }

    inspections = new unsigned char[MAX_RESULTS_SIZE];
    memset(inspections, 0, MAX_RESULTS_SIZE);

    mtxSharedDataAccess = OpenMutex(MUTEX_ALL_ACCESS, TRUE, SHARED_MEMORY_IMAGES_MTX);
    semSharedDataReady = OpenSemaphore(SEMAPHORE_MODIFY_STATE, TRUE, SHARED_MEMORY_IMAGES_SEM);
    ReleaseSemaphore(semSharedDataReady, 1, 0);
}