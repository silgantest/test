#include "PointResultsPool.h"
// static method
TwoPointResultsPool* TwoPointResultsPool::instance;

void TwoPointResultsPool::Initialize()
{
	instance = new TwoPointResultsPool();
	instance->grow();
}

void TwoPointResultsPool::Shutdown()
{
	delete instance;
}

void TwoPointResultsPool::deleteObject(Reusable* obj)
{
	delete (TwoPointResult*)obj;
}

void TwoPointResultsPool::DeleteResultObj(TwoPointResult* obj)
{
	instance->Delete(obj);
}

TwoPointResultsPool::TwoPointResultsPool()
{

}

TwoPointResultsPool::~TwoPointResultsPool()
{
	deleteEverything();
}

Reusable* TwoPointResultsPool::create()
{
	return new TwoPointResult();
};

TwoPointResult* TwoPointResultsPool::GetResultObj(TwoPoint pos)
{
	TwoPointResult* res =  (TwoPointResult*)instance->Get();
	res->SetResult(pos);
	return res;
}



// static method
SpecialPointResultsPool* SpecialPointResultsPool::instance;

void SpecialPointResultsPool::Initialize()
{
	instance = new SpecialPointResultsPool();
	instance->grow();
}

void SpecialPointResultsPool::Shutdown()
{
	delete instance;
}

void SpecialPointResultsPool::deleteObject(Reusable* obj)
{
	delete (SpecialPointResult*)obj;
}

void SpecialPointResultsPool::DeleteResultObj(SpecialPointResult* obj)
{
	instance->Delete(obj);
}

SpecialPointResultsPool::SpecialPointResultsPool()
{

}

SpecialPointResultsPool::~SpecialPointResultsPool()
{
	deleteEverything();
}

Reusable* SpecialPointResultsPool::create()
{
	return new SpecialPointResult();
};

SpecialPointResult* SpecialPointResultsPool::GetResultObj(SpecialPoint pos)
{
	SpecialPointResult* res = (SpecialPointResult*)instance->Get();
	res->SetResult(pos);
	return res;
}


