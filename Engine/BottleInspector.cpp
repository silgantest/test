#include "BottleInspector.h"
#include "InspectionUtilities.h"
#include "LabelResults.h"
#include "IntermediateImagePackPool.h"
#include "PointResultsPool.h"
#include "ImageFileUtilities.h"
#include <math.h>
#include <algorithm>
#include "ImageFileUtilities.h"
#include <iostream>
#include "LabelResultsPool.h"
#include <fstream>
#include "JobManager.h"
#include "BottleResultsDetailsPool.h"

BottleInspector::BottleInspector(int camIndex)
{
	nCam = camIndex;
	//TODO
	coordinatesOfContainerLeftTopEdgePointsRaw = nullptr;
	coordinatesOfContainerLeftTopEdgePointsFilled = nullptr;

	coordinatesOfContainerLeftWaistEdgePointsRaw = nullptr;
	coordinatesOfContainerLeftWaistEdgePointsFilled = nullptr;
	coordQueue = nullptr;

	bytePattBy6_method6_1 = new ImagePattern();
	bytePattBy3_method6_1 = new ImagePattern();
	bytePattBy3 = new ImagePattern();
	bytePattBy6_method6 = new ImagePattern();
	bytePattBy3_method6 = new ImagePattern();
	bytePattBy6_method6_num3 = new ImagePattern();
	pattern2 = new ImagePattern();

}


BottleInspector::~BottleInspector()
{
	delete[] coordinatesOfContainerLeftTopEdgePointsRaw;
	delete[] coordinatesOfContainerLeftTopEdgePointsFilled;

	delete[] coordinatesOfContainerLeftWaistEdgePointsRaw;
	delete[] coordinatesOfContainerLeftWaistEdgePointsFilled;
	delete coordQueue;

	delete bytePattBy6_method6_1;
	delete bytePattBy3_method6_1;
	delete bytePattBy3;
	delete pattern2;
	delete bytePattBy3_method6;
	delete bytePattBy6_method6;
	delete bytePattBy6_method6_num3;
}

void BottleInspector::ResetForJob()
{
	Job* job = JobManager::GetCurrentJob();
	sensEdge = job->GetSensEdge();
	filterPattern = job->GetFilterSelPatt();
	findMiddles = job->GetFindMiddles();
	method = job->GetLabelIntegrityMethod();
		
	zerobot = job->GetOrienArea();
	edgeTransition = job->GetEdgeTransition();  //whether Dark-> Light or ViceVersa

	heigMet3 = job->GetHeigMet3();
	widtMet3 = job->GetWidtMet3();
	senTaught = job->GetSensMet3();

	ytopLimPatt = job->GetYTopLimPatt();
	ybotLimPatt = job->GetYBotLimPatt();

	boxWt = job->GetBoxWt();
	boxHt = job->GetBoxHt();
	boxYt = job->GetBoxYt();
	boxHb = job->GetBoxHb();
	boxYb = job->GetBoxYb();

	shiftTopLim = job->GetShiftTopLim(nCam);
	shiftBotLim = job->GetShiftBottomLim(nCam);
	widthMet5 = job->GetWidtMet5();
	heigMet5 = job->GetHeigMet5();
	bottleStyle = job->GetBottleStyle();

	senMidd = job->GetSenMidd();
	midImg = job->GetMiddImg();
	taughtMiddleRefY = job->GetTaughtMiddleRefY(nCam);

	filterSelShifted = job->GetFilterSelShifted();
	shiftedLabSen = job->GetShiftedLabSen();


	shiftWidth = job->GetShiftWidth();
	shiftHorEndLim = job->GetShiftHorEndLim();
	shiftHorIniLim = job->GetShiftHorIniLim();

	edgeLabSen = job->GetEdgeLabSen();


	waistVerticalSearchLength = job->GetWaistVerticalSearchLength(nCam);
	waistHorizontalSearchLength = job->GetWaistHorizontalSearchLength(nCam);
	waistEdgeThreshold = job->GetWaistEdgeThreshold(nCam);

	waistMeasurementTopBound = job->GetWaistMeasurementTopBound(nCam);
	waistMeasurementBottomBound = job->GetWaistMeasurementBottomBound(nCam);
	waistMeasurementLeftBound = job->GetWaistMeasurementLeftBound(nCam);
	waistMeasurementRightBound = job->GetWaistMeasurementRightBound(nCam);

	waistMeasurementUpperTopBound = job->GetWaistMeasurementUpperTopBound(nCam);
	waistMeasurementUpperBottomBound = job->GetWaistMeasurementUpperBottomBound(nCam);
	waistMeasurementUpperLeftBound = job->GetWaistMeasurementUpperLeftBound(nCam);
	waistMeasurementUpperRightBound = job->GetWaistMeasurementUpperRightBound(nCam);

	//prevent resizing of vectors while running
	int size = waistMeasurementBottomBound - waistMeasurementTopBound + 3;

	if (size > sizeWaistArray)
	{
		sizeWaistArray = size;

		if (coordinatesOfContainerLeftTopEdgePointsRaw)
			delete[] coordinatesOfContainerLeftTopEdgePointsRaw;
		coordinatesOfContainerLeftTopEdgePointsRaw = new int[2 * sizeWaistArray];

		if (coordinatesOfContainerLeftTopEdgePointsFilled)
			delete coordinatesOfContainerLeftTopEdgePointsFilled;
		coordinatesOfContainerLeftTopEdgePointsFilled = new int[2 * sizeWaistArray];

		if (coordinatesOfContainerLeftWaistEdgePointsRaw)
			delete coordinatesOfContainerLeftWaistEdgePointsRaw;
		coordinatesOfContainerLeftWaistEdgePointsRaw = new int[2 * sizeWaistArray];

		if (coordinatesOfContainerLeftWaistEdgePointsFilled)
			delete coordinatesOfContainerLeftWaistEdgePointsFilled;
		coordinatesOfContainerLeftWaistEdgePointsFilled = new int[2 * sizeWaistArray];

		vectorSize = 2 * sizeWaistArray;
		if (coordQueue)
			delete coordQueue;
		coordQueue = new std::deque<SinglePoint>(sizeWaistArray);
	}

	Patterns* patterns = JobManager::GetCurrentPatterns();

	patterns->CopyBytePattBy6_method6_1(bytePattBy6_method6_1);
	patterns->CopyBytePattBy3_method6_1(bytePattBy3_method6_1);
	patterns->CopyBytePattBy3(bytePattBy3);
	patterns->CopyBytePattBy6_method6(bytePattBy6_method6);
	patterns->CopyBytePattBy3_method6(bytePattBy3_method6);
	patterns->CopyBytePattBy6_method6_num3(bytePattBy6_method6_num3);
	patterns->CopyPattern2(pattern2);

}

void BottleInspector::SetAppConfig(AppConfig* config)
{
	MainDisplayImageHeight = config->GetMainDisplayImageHeight();
	MainDisplayImageWidth = config->GetMainDisplayImageWidth();

	MainCameraDisplayYOffset = config->GetMainCameraDisplayYOffset();
	MainCameraDisplayXOffset = config->GetMainCameraDisplayXOffset();


	//Are the height and width set correctly?????
	mainHeight = config->GetMainDisplayImageHeight() / 2;
	mainLineLength = config->GetMainDisplayImageWidth() / 2;

	//This is actually the same as above
	croppedWidth = config->GetCroppedCameraWidth() / 2;
	croppedHeight = config->GetCroppedCameraHeight() / 2;

	sizepattBy3_2W = config->GetSizePattBy3_2W();
	sizepattBy3_2H = config->GetSizePattBy3_2H();

	sizepattBy6 = config->GetSizePattBy6();
	sizepattBy12 = config->GetSizePattBy12();

	FullImageScaledRotatedWidth = MainDisplayImageWidth / config->GetScaleFactor();
	FullImageScaledRotatedHeight = MainDisplayImageHeight / config->GetScaleFactor();
	CroppedImageYOffset = config->GetCroppedCameraOffsetY();

	waistAvailable = config->GetWaistAvailable();
	camWidth = config->GetCamWidth();
	camHeight = config->GetCamHeight();

}

void BottleInspector::Inspect(CapturedImage* capImg, ResultsHandler* handler) 
{
	BottleResultDetails* details = BottleResultsDetailsPool::GetDetails();

	int newHeight = MainDisplayImageHeight;
	int newWidth = MainDisplayImageWidth;

	int offsetX = MainCameraDisplayXOffset;
	int offsetY = (camWidth == MainDisplayImageHeight) ? MainCameraDisplayYOffset : CroppedImageYOffset;
	IntermediateImagePack* pack = IntermediateImagePackPool::GetImage(capImg);
	resultwaist = NAN;



	InspectionUtilities::Reduce((unsigned char*)capImg->GetBuffer(), pack, capImg->GetWidth(), capImg->GetHeight(), (capImg->GetCameraIndex() + 1)%2);

	if (waistAvailable)
	{
		InspectionUtilities::ScaleImageDownBySubsampling((unsigned char*)capImg->GetBuffer(), pack, MainDisplayImageWidth, MainDisplayImageHeight);
		InspectionUtilities::Rotate(pack, nCam % 2 == 0);
		InspectionUtilities::ConvertBGRAToGray(pack);
	}
	pack->SetFilterForPattMatch(filterPattern);
	pack->SetMiddBottle(midImg);

	edgeImgCreation(pack, 3);

	TwoPoint pos = getLabelPos(pack);

	LabelResults* l = LabelResultsPool::GetResultObj(nCam, capImg->GetImageNumber());
	l->SetCam(nCam);
	l->SetLabelPos(pos);
	//TwoPoint p;
	//p.x = 0;
	//l->SetLabelPos(p);
	l->SetWaistResult(resultwaist);
	l->SetXMiddle(xMidd);
	l->SetMiddleShift(middlShift);
	l->SetMiddleShiftWPoints(middlShiftWPoints);
	l->SetAngIncDeg(angIncDegL, angIncDegR);
	l->SetPosBy6(pattLeftPosBy6);

	handler->AddResult(l, capImg, pack, details);

}


void BottleInspector::edgeImgCreation(IntermediateImagePack* pack, int fact /*Not sure why 3*/)
{
	int LineLength = MainDisplayImageWidth / fact;
	int Height = MainDisplayImageHeight / fact;

	bool debugging = false;
	if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(pack->filterForPattMatchDiv3, LineLength, Height, "C:\\DebugFiles\\filterForPattMatchDiv3.bmp"); }

		/////////////////

	int pix, pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pixH, pixV, pixValH, pixValV;

	int th = sensEdge;
	int thV = th / 2;

	// Creating the Sobel image
	for (int y = 3; y < Height - 3; y++)
	{
		for (int x = 3; x < LineLength - 3; x++)
		{
			pix1 = *(pack->filterForPattMatchDiv3 + x - 1 + LineLength * (y - 1));
			pix2 = *(pack->filterForPattMatchDiv3 + x + 0 + LineLength * (y - 1));
			pix3 = *(pack->filterForPattMatchDiv3 + x + 1 + LineLength * (y - 1));

			pix4 = *(pack->filterForPattMatchDiv3 + x - 1 + LineLength * (y + 0));
			pix5 = *(pack->filterForPattMatchDiv3 + x + 0 + LineLength * (y + 0));
			pix6 = *(pack->filterForPattMatchDiv3 + x + 1 + LineLength * (y + 0));

			pix7 = *(pack->filterForPattMatchDiv3 + x - 1 + LineLength * (y + 1));
			pix8 = *(pack->filterForPattMatchDiv3 + x + 0 + LineLength * (y + 1));
			pix9 = *(pack->filterForPattMatchDiv3 + x + 1 + LineLength * (y + 1));

			pixH = abs((pix1 + 2 * pix2 + pix3) - (pix7 + 2 * pix8 + pix9));
			pixV = abs((pix1 + 2 * pix4 + pix7) - (pix3 + 2 * pix6 + pix9));

			pix = pixH - 2 * pixV;

			//pixValH = MAX(0, MIN(pixH,255));
			pixValH = max(0, min(pix, 255));
			pixValV = max(0, min(pixV, 255));

			*(pack->edgH + x + LineLength * y) = pixValH;
			*(pack->edgV + x + LineLength * y) = pixValV;


			pix = 0;

			if (pixValV > th) { pix = 150; }
			if (pixValH > th) { pix = 255; }
			if (pixValV > th && pixValH > th) { pix = 0; }

			*(pack->bin + x + LineLength * y) = pix;
		}
	}

	//	if (debugging) { SaveBWCustom1(im_cam1EdgH, LineLength, Height); }

		//////////////////
}


void BottleInspector::edgeImgBin(IntermediateImagePack* pack, int fact)
{
	int LineLength = MainDisplayImageWidth / fact;
	int Height = MainDisplayImageHeight / fact;

	bool debugging = false;
	if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(pack->redbw, LineLength, Height, "C:\\DebugFiles\\redbw.bmp"); }
	if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(pack->redbw, LineLength, Height, "C:\\DebugFiles\\greenbw.bmp"); }
	if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(pack->redbw, LineLength, Height, "C:\\DebugFiles\\bluebw.bmp"); }

	/////////////////

	int pix, pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pixVal;

	int th = 120;
	int y;
	for (y = 3; y < Height - 3; y++)
	{
		for (int x = 3; x < LineLength - 3; x++)
		{
			*(pack->byteBinEdges + x + LineLength * y) = 0;
		}
	}

	if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(pack->byteBinEdges, LineLength, Height, "C:\\DebugFiles\\byteBinEdges.bmp"); }

	//////////////////

	// Creating the Sobel image
	for (y = 3; y < Height - 3; y++)
	{
		for (int x = 3; x < LineLength - 3; x++)
		{
			pix1 = *(pack->redbw + x - 1 + LineLength * (y - 1));
			pix2 = *(pack->redbw + x + 0 + LineLength * (y - 1));
			pix3 = *(pack->redbw + x + 1 + LineLength * (y - 1));

			pix4 = *(pack->redbw + x - 1 + LineLength * (y + 0));
			pix5 = *(pack->redbw + x + 0 + LineLength * (y + 0));
			pix6 = *(pack->redbw + x + 1 + LineLength * (y + 0));

			pix7 = *(pack->redbw + x - 1 + LineLength * (y + 1));
			pix8 = *(pack->redbw + x + 0 + LineLength * (y + 1));
			pix9 = *(pack->redbw + x + 1 + LineLength * (y + 1));

			pix = abs((pix1 + 2 * pix2 + pix3) - (pix7 + 2 * pix8 + pix9)) +
				abs((pix1 + 2 * pix4 + pix7) - (pix3 + 2 * pix6 + pix9));

			pixVal = max(0, min(pix, 255));

			if (pixVal > th)
			{
				*(pack->byteBinEdges + x + LineLength * y) = 255;
			}
		}
	}

	if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(pack->byteBinEdges, LineLength, Height, "C:\\DebugFiles\\byteBinEdges.bmp"); }

	//////////////////

	// Creating the Sobel image
	for (y = 3; y < Height - 3; y++)
	{
		for (int x = 3; x < LineLength - 3; x++)
		{
			pix1 = *(pack->greenbw + x - 1 + LineLength * (y - 1));
			pix2 = *(pack->greenbw + x + 0 + LineLength * (y - 1));
			pix3 = *(pack->greenbw + x + 1 + LineLength * (y - 1));

			pix4 = *(pack->greenbw + x - 1 + LineLength * (y + 0));
			pix5 = *(pack->greenbw + x + 0 + LineLength * (y + 0));
			pix6 = *(pack->greenbw + x + 1 + LineLength * (y + 0));

			pix7 = *(pack->greenbw + x - 1 + LineLength * (y + 1));
			pix8 = *(pack->greenbw + x + 0 + LineLength * (y + 1));
			pix9 = *(pack->greenbw + x + 1 + LineLength * (y + 1));

			pix = abs((pix1 + 2 * pix2 + pix3) - (pix7 + 2 * pix8 + pix9)) +
				abs((pix1 + 2 * pix4 + pix7) - (pix3 + 2 * pix6 + pix9));

			pixVal = max(0, min(pix, 255));

			if (pixVal > th)
			{
				*(pack->byteBinEdges + x + LineLength * y) = 255;
			}
		}
	}

	if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(pack->byteBinEdges, LineLength, Height, "C:\\DebugFiles\\byteBinEdges.bmp"); }

	//////////////////

	// Creating the Sobel image
	for (y = 3; y < Height - 3; y++)
	{
		for (int x = 3; x < LineLength - 3; x++)
		{
			pix1 = *(pack->bluebw + x - 1 + LineLength * (y - 1));
			pix2 = *(pack->bluebw + x + 0 + LineLength * (y - 1));
			pix3 = *(pack->bluebw + x + 1 + LineLength * (y - 1));

			pix4 = *(pack->bluebw + x - 1 + LineLength * (y + 0));
			pix5 = *(pack->bluebw + x + 0 + LineLength * (y + 0));
			pix6 = *(pack->bluebw + x + 1 + LineLength * (y + 0));

			pix7 = *(pack->bluebw + x - 1 + LineLength * (y + 1));
			pix8 = *(pack->bluebw + x + 0 + LineLength * (y + 1));
			pix9 = *(pack->bluebw + x + 1 + LineLength * (y + 1));

			pix = abs((pix1 + 2 * pix2 + pix3) - (pix7 + 2 * pix8 + pix9)) +
				abs((pix1 + 2 * pix4 + pix7) - (pix3 + 2 * pix6 + pix9));

			pixVal = max(0, min(pix, 255));

			if (pixVal > th)
			{
				*(pack->byteBinEdges + x + LineLength * y) = 255;
			}
		}
	}

	if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(pack->byteBinEdges, LineLength, Height, "C:\\DebugFiles\\byteBinEdges.bmp"); }

	//////////////////
}


TwoPoint BottleInspector::getLabelPos(IntermediateImagePack* pack)
{
	TwoPoint BotPos;
	///////////////////////////////////////////
	//Finding Middle of the Bottle
	if (findMiddles)
	{
		int fact = 3;
		findBottleInPic(pack, fact);
	}
	else
	{
		bottleXL = 0 + 30;
		bottleXR = mainLineLength - 30;
		xMidd = mainLineLength / 2;
	}

	///////////////////////////////////////////

	unsigned char* imgForPattMatchDiv3 = pack->filterForPattMatchDiv3;
	unsigned char* imgForPattMatchDiv6 = pack->filterForPattMatchDiv6;

	if (method == LabelIntegrityMethod::Method1)
	{
		BotPos = findPatternX1(imgForPattMatchDiv3);
	}
	// Pattern Matching at different Scales
	else if (method == LabelIntegrityMethod::Method2)
	{
		//TDO
		//DO WE NEED THIS!!! NOT ADDRESS PATTERN LOADING FAILURES
		wehaveTempX = true;
		if (wehaveTempX)  //what is this???
		{
			int imgFac;	int pattType = 1;

			imgFac = 6;

			pattLeftPosBy6 = findPattA1(imgForPattMatchDiv6, bytePattBy6_method6_1->GetPattern(), imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3 = findPattHigherRes(imgForPattMatchDiv3, bytePattBy3_method6_1->GetPattern(), imgFac, pattLeftPosBy6);

			BotPos.x = pattLeftPosBy3.x / 3;
			BotPos.y = pattLeftPosBy3.y / 3;
			BotPos.x1 = pattLeftPosBy6.c;
		}
	}
	else if (method == LabelIntegrityMethod::Method3)
	{
		int imgFac;

		imgFac = 3;
		pattLeftPosBy3 = findLabelEdge_b(imgForPattMatchDiv3, imgFac, pattLeftPosBy6);

		BotPos.x = pattLeftPosBy3.x / 3;
		BotPos.y = pattLeftPosBy3.y / 3;
		BotPos.x1 = pattLeftPosBy3.c;
	}
	else if (method == LabelIntegrityMethod::Method4)
	{
		int imgFac;

		imgFac = 3;
		pattLeftPosBy3 = findWhiteBand(pack, imgForPattMatchDiv3, bytePattBy3->GetPattern(), imgFac, pattLeftPosBy6);

		BotPos.x = pattLeftPosBy3.x / 3;
		BotPos.y = pattLeftPosBy3.y / 3;
		BotPos.x1 = pattLeftPosBy3.c;
	}
	else if (method == LabelIntegrityMethod::Method5)
	{
		int fact = 3;
		//TODO
		//Is this right???
		int width = croppedHeight / 3; int height = croppedWidth / 3;

		int tmpW = boxWt;

		int tmpHt = boxHt;
		int tmpYt = boxYt;

		int tmpHb = boxHb;
		int tmpYb = boxYb;

		bxMethod5_Xt = bxMethod5_Xb = xMidd - tmpW / 2;
		if (bxMethod5_Xt < 0) { bxMethod5_Xt = bxMethod5_Xb = 0; }

		//Binarized automatically, images come from filter selected on Insp dlg box
		createHistAndBinarize(pack,
			imgForPattMatchDiv3,
			bxMethod5_Xt, tmpYt, tmpHt,
			bxMethod5_Xb, tmpYb, tmpHb,
			tmpW, fact);

		width = croppedHeight / 3; height = croppedWidth / 3;
		//Save Drawing info????
//			CVisByteImage imgbinEdg(width, height, 1, -1, byteBinMet5C2); 
//			imageBinMeth5[ncam] = imgbinEdg;

		int limYtop = ytopLimPatt;
		int limYbot = ybotLimPatt;
		int limX = bxMethod5_Xt;
		int limW = tmpW;

		//Now find the edge with the binary image
		labelPos.x = 0; labelPos.y = 0; labelPos.x1 = 100;
		BotPos = findLabelEdge_Method5(pack->byteBinMet5, fact, limYtop, limYbot, limX, limW);
		BotPos.x1 = 100;
	}
	else if (method == LabelIntegrityMethod::Method6)
	{
		if (wehaveTempX)
		{
			int imgFac;	int pattType = 1;

			//##Pattern 1
			imgFac = 6;
			pattLeftPosBy6_method6_1 = findPattA1(imgForPattMatchDiv6, bytePattBy6_method6_1->GetPattern(), imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6_1 = findPattHigherRes(imgForPattMatchDiv3, bytePattBy3_method6_1->GetPattern(), imgFac, pattLeftPosBy6_method6_1);

			/*
			imgFac = 6;
			pattLeftPosBy6[ncam] = FindPattA1_C2(img2ForPattMatchDiv6, bytePattBy6[pattType], ncam, imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3[ncam] = FindPattHigherRes_C2(img2ForPattMatchDiv3, bytePattBy3[pattType], ncam, imgFac, pattLeftPosBy6[ncam]);

			TRACE( "I2: Old Patt cor %f \n",pattLeftPosBy6[ncam].c);
			*/

			//#Pattern 2 
			pattType = 2;
			imgFac = 6;
			pattLeftPosBy6_method6 = findPattA1(imgForPattMatchDiv6, bytePattBy6_method6->GetPattern(), imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6 = findPattHigherRes(imgForPattMatchDiv3, bytePattBy3_method6->GetPattern(), imgFac, pattLeftPosBy6_method6);

			//#Pattern 3 
			pattType = 3;
			imgFac = 6;
			pattLeftPosBy6_method6_num3 = findPattA1(imgForPattMatchDiv6, bytePattBy6_method6_num3->GetPattern(), imgFac, pattType);

			imgFac = 3;
			pattLeftPosBy3_method6_num3 = findPattHigherRes(imgForPattMatchDiv3, bytePattBy6_method6_num3->GetPattern(), imgFac, pattLeftPosBy6_method6_num3);

			//if orig pattern is returning a better corelation
			//if((pattLeftPosBy6[ncam].c >= pattLeftPosBy6_method6[ncam].c) &&(pattLeftPosBy6[ncam].c >= pattLeftPosBy6_method6_num3[ncam].c))
			if ((pattLeftPosBy3_method6_1.c >= pattLeftPosBy6_method6.c) &&
				(pattLeftPosBy3_method6_1.c >= pattLeftPosBy6_method6_num3.c))
			{
				BotPos.x = pattLeftPosBy3_method6_1.x / 3;
				BotPos.y = pattLeftPosBy3_method6_1.y / 3;
				BotPos.x1 = pattLeftPosBy6_method6_1.c;
				/*
				BotPos.x = pattLeftPosBy3[ncam].x/3;
				BotPos.y = pattLeftPosBy3[ncam].y/3;
				BotPos.x1= pattLeftPosBy6[ncam].c;
				*/
			}
			else if ((pattLeftPosBy6_method6.c >= pattLeftPosBy3_method6_1.c) &&
				(pattLeftPosBy6_method6.c >= pattLeftPosBy6_method6_num3.c))//if new pattern is returning a better correlation
			{
				BotPos.x = pattLeftPosBy3_method6.x / 3;
				BotPos.y = pattLeftPosBy3_method6.y / 3;
				BotPos.x1 = pattLeftPosBy6_method6.c;
			}
			else if ((pattLeftPosBy6_method6_num3.c >= pattLeftPosBy3_method6_1.c) &&
				(pattLeftPosBy6_method6_num3.c >= pattLeftPosBy6_method6.c))
			{
				BotPos.x = pattLeftPosBy3_method6_num3.x / 3;
				BotPos.y = pattLeftPosBy3_method6_num3.y / 3;
				BotPos.x1 = pattLeftPosBy6_method6_num3.c;
			}
		}
	}

	///////////////////////////////////////////

	xShift = 0;
	yShift = shiftTopLim + 15;
	hShift = (shiftBotLim - shiftTopLim) - 30;
	int fact = 3;

	if (bottleStyle == 10) //Lipton - Square
	{
		findShiftedEdges7(pack, fact);
	}

	composeWaistInspection(pack);

	return BotPos;



}


void BottleInspector::findShiftedEdges7(IntermediateImagePack* pack, int fact)
{
	/////////////////////////////////////
	xLShiftPts.clear();
	xRShiftPts.clear();

	int LineLength = croppedHeight / fact;
	int Height = croppedWidth/ fact;

	/////////////////////////////////////

	int x;//; int pix, 
	int pix1, pix2, pix3, pix4, pix5, pix6;//, pix7, pix8, pix9, pixVal;

	/////////////////////////////////////

	int	yStart = shiftTopLim / fact;
	int	yEnd = shiftBotLim / fact;
	int thEdge = shiftedLabSen;

	if (yEnd < yStart || yEnd < 0 || yStart < 5) { return; }

	/////////////////////////////////////
	int x0 = shiftHorIniLim;
	int x1 = shiftHorEndLim;
	int y0 = shiftTopLim;
	int h0 = shiftBotLim - shiftTopLim;
	int w1 = shiftWidth;
	int h1 = h0;

	//First find the area and x,y position with the most amount of black points
	//*****Step 1 is finding area with most number of Black points
	SpecialPoint tmp = checkNoLabel1(fact, pack->byteBinEdges, x0, y0, x1, h0, w1, h1);
	xShift = tmp.x;

	/////////////////////////////////////

	int thEdge2 = edgeLabSen;
	int diff1, diff2, diff3;

	SinglePoint tmpSinglePtLeft;
	SinglePoint tmpSinglePtRigh;

	tmpSinglePtLeft.x = 0;
	tmpSinglePtRigh.x = 0;

	middlShift = tmp.x + w1 / 2; //positioning in the middle of the opening, tmp.x is on the left
	if (middlShift < 0) middlShift = w1 / 2;

	//Find all the possible points to the LEFT
	for (int y = yStart; y < yEnd; y++)
	{
		for (x = middlShift / fact; x > 20; x--) //edge will not go under 20
		{
			pix1 = *(pack->filterForShiftedDiv3 + x + 1 + y * LineLength);
			pix2 = *(pack->filterForShiftedDiv3 + x - 1 + y * LineLength);

			pix3 = *(pack->filterForShiftedDiv3 + x + 2 + y * LineLength);
			pix4 = *(pack->filterForShiftedDiv3 + x - 2 + y * LineLength);

			pix5 = *(pack->filterForShiftedDiv3 + x + 3 + y * LineLength);
			pix6 = *(pack->filterForShiftedDiv3 + x - 3 + y * LineLength);

			diff1 = pix1 - pix2;
			diff2 = pix3 - pix4;
			diff3 = pix5 - pix6;

			//////////////////////////////////////////////////

			if ((pix2 < thEdge2 && pix4 < thEdge2 && pix6 < thEdge2) ||
				(diff1 > thEdge2 && diff2 > thEdge2 && diff3 > thEdge2))
			{
				tmpSinglePtLeft.x = x * fact;
				tmpSinglePtLeft.y = y * fact;

				xLShiftPts.push_back(tmpSinglePtLeft);
				break;
			}
		}
	}

	///////////

	//Find all the possible points to the RIGHT
	for (int y = yStart; y < yEnd; y++)
	{
		for (x = middlShift / fact; x < LineLength - 20; x++) //edge will not go higher than length -20
		{
			pix1 = *(pack->filterForShiftedDiv3 + x + 1 + y * LineLength);
			pix2 = *(pack->filterForShiftedDiv3 + x - 1 + y * LineLength);

			pix3 = *(pack->filterForShiftedDiv3 + x + 2 + y * LineLength);
			pix4 = *(pack->filterForShiftedDiv3 + x - 2 + y * LineLength);

			pix5 = *(pack->filterForShiftedDiv3 + x + 3 + y * LineLength);
			pix6 = *(pack->filterForShiftedDiv3 + x - 3 + y * LineLength);

			diff1 = pix2 - pix1;
			diff2 = pix4 - pix3;
			diff3 = pix6 - pix5;

			//////////////////////////////////////////////////

			if ((pix1 < thEdge2 && pix3 < thEdge2 && pix5 < thEdge2) ||
				(diff1 > thEdge2 && diff2 > thEdge2 && diff3 > thEdge2))
			{
				tmpSinglePtRigh.x = x * fact;
				tmpSinglePtRigh.y = y * fact;

				xRShiftPts.push_back(tmpSinglePtRigh);
				break;
			}
		}
	}

	int yRedGreen = y0 + h0 / 2;

	////////////////////////////
	//Remove outliers
	verAxisPtsRemOutliers(xLShiftPts);

	//Remove outliers
	verAxisPtsRemOutliers(xRShiftPts);

	qtyShiftLeft = xLShiftPts.size();
	qtyShiftRigh = xRShiftPts.size();

	////////////////////////////

	//Calculate Skewness
	float incl;

	//Once we have clean set of points calculate linear regression with respect to Y - LEFT	
	linearRegData lineEqLeft = InspectionUtilities::linearRegY(xLShiftPts);
	incl = lineEqLeft.incl;
	inclinL = incl;
	angIncDegL = fabs(lineEqLeft.angIncDeg);

	//Once we have clean set of points calculate linear regression with respect to Y - RIGHT	
	linearRegData lineEqRigh = InspectionUtilities::linearRegY(xRShiftPts);
	incl = lineEqRigh.incl;
	inclinR = incl;
	angIncDegR = fabs(lineEqRigh.angIncDeg);

	xLBorder = lineEqLeft.avgx + lineEqLeft.incl * (yRedGreen - lineEqLeft.avgy);
	xRBorder = lineEqRigh.avgx + lineEqRigh.incl * (yRedGreen - lineEqRigh.avgy);
	middlShiftWPoints = (xLBorder + xRBorder) / 2;
}



void BottleInspector::findBottleInPic(IntermediateImagePack* pack, int fact)
{
	int Length = croppedHeight / fact;
	int Height = croppedWidth / fact;

	int pix0, pix1, pix2, pix3, pix4, pix5, pix6;
	int diff1, diff2, diff3;

	int th = senMidd;

	int y = taughtMiddleRefY / fact;

	if (y<0 || y>Height) return;

	if (midImg == 1 || midImg == 6 || midImg == 7 || midImg == 8) //using BW image with White Plastic as background
	{
		for (int x = 10; x < Length / 2; x++)
		{
			pix1 = *(pack->imgMiddBottle + x - 1 + Length * y);
			pix2 = *(pack->imgMiddBottle + x - 2 + Length * y);
			pix3 = *(pack->imgMiddBottle + x - 3 + Length * y);
			pix4 = *(pack->imgMiddBottle + x + 1 + Length * y);
			pix5 = *(pack->imgMiddBottle + x + 2 + Length * y);
			pix6 = *(pack->imgMiddBottle + x + 3 + Length * y);

			diff1 = pix1 - pix4; diff2 = pix2 - pix5; diff3 = pix3 - pix6;

			if (diff1 > th && diff2 > th && diff3 > th)
			{
				bottleXL = x * fact;
				bottleYL = y * fact;
				break;
			}
		}

		for (int x = Length - 10; x > Length / 2; x--)
		{
			pix1 = *(pack->imgMiddBottle + x - 1 + Length * y);
			pix2 = *(pack->imgMiddBottle + x - 2 + Length * y);
			pix3 = *(pack->imgMiddBottle + x - 3 + Length * y);
			pix4 = *(pack->imgMiddBottle + x + 1 + Length * y);
			pix5 = *(pack->imgMiddBottle + x + 2 + Length * y);
			pix6 = *(pack->imgMiddBottle + x + 3 + Length * y);

			diff1 = pix4 - pix1; diff2 = pix5 - pix2; diff3 = pix6 - pix3;

			if (diff1 > th && diff2 > th && diff3 > th)
			{
				bottleXR = x * fact;
				bottleYR = y * fact;
				break;
			}
		}
	}
	else
	{
		for (int x = 0; x < Length / 2; x++)
		{
			pix0 = *(pack->imgMiddBottle + x + 0 + Length * y);
			pix1 = *(pack->imgMiddBottle + x + 1 + Length * y);
			pix2 = *(pack->imgMiddBottle + x + 2 + Length * y);
			pix3 = *(pack->imgMiddBottle + x + 3 + Length * y);

			if (pix0 > th && pix1 > th && pix2 > th && pix3 > th)
			{
				bottleXL = x * fact;
				bottleYL = y * fact;	break;
			}
		}

		for (int x = Length; x > Length / 2; x--)
		{
			pix0 = *(pack->imgMiddBottle + x - 0 + Length * y);
			pix1 = *(pack->imgMiddBottle + x - 1 + Length * y);
			pix2 = *(pack->imgMiddBottle + x - 2 + Length * y);
			pix3 = *(pack->imgMiddBottle + x - 3 + Length * y);

			if (pix0 > th && pix1 > th && pix2 > th && pix3 > th)
			{
				bottleXR = x * fact;
				bottleYR = y * fact;
				break;
			}
		}
	}

	xMidd = (bottleXL + bottleXR) / 2;
}

TwoPoint BottleInspector::findPatternX1(unsigned char* im_srcIn)
{
	TwoPoint result;
	result.x = result.y = result.x1 = 50;

	int Limit = 40;
	int d1 = 0;
	int r;//,q;

	//This is correct
	int LineLength = croppedHeight / 3;
	int Height = croppedWidth / 3;
	/*
	Original was 	
	int LineLength		=	CroppedCameraHeight/3;
	int Height			=	CroppedCameraWidth/3;
	
	*/


	int		LineLength2 = 12;
	float	n = LineLength2 * LineLength2;

	int		xpos2 = 0;
	int		ypos2 = 0;

	float	sumXY = 0;
	float	sumX = 0;
	float	sumXX = 0;
	float	sumY = 0;
	float	sumYY = 0;

	float	f0 = 0;
	float	f1 = 0;
	float	f2 = 0;
	float	corr = 0;

	float	savedBestCorr = 0.3f;
	int		savedPosX = 0;
	int		cnt = 0;
	int		startY = ytopLimPatt;

	/////////////////

	sumX = 0.0;	sumXX = 0.0;

	for (r = 0; r < n; r++)
	{
		sumX += *(pattern2->GetPattern() + r);
		sumXX += (*(pattern2->GetPattern() + r)) * (*(pattern2->GetPattern() + r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;
	bool atInitLoop = false;


	int szpattHalf = LineLength2 / 2;
	int yTopLim = ytopLimPatt * 3 / 3;
	int yBotLim = ybotLimPatt * 3 / 3;
	int yIni = max(yTopLim + szpattHalf, szpattHalf);
	int yEnd = min(Height - szpattHalf, yBotLim - szpattHalf);

	//for(int y=startY; y<=startY+searchHeight; y++)//22
	for (int y = yIni; y <= yEnd; y++)
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop = true;

		for (x = 10; x <= 126; x++)
		{
			sumXY = 0.0;

			for (ypos = -6; ypos < 6; ypos++)
			{
				if (atInitLoop)
				{
					for (xpos = -6; xpos < 6; xpos++)
					{
						pixT = *(pattern2->GetPattern() + xpos + 6 + 12 * (ypos + 6));
						pix = *(im_srcIn + xpos + x + LineLength * (ypos + y));

						sumY += pix;
						sumYY += pix * pix;
						sumXY += pixT * pix;
					}
				}
				else
				{
					pixOld = *(im_srcIn - 7 + x + LineLength * (ypos + y));
					pixNew = *(im_srcIn + 5 + x + LineLength * (ypos + y));

					for (xpos = -6; xpos < 6; xpos++)
					{
						pixT = *(pattern2->GetPattern() + xpos + 6 + 12 * (ypos + 6));
						pix = *(im_srcIn + xpos + x + LineLength * (ypos + y));

						sumXY += pixT * pix;
					}

					sumY += pixNew;
					sumY -= pixOld;
					sumYY += pixNew * pixNew;
					sumYY -= pixOld * pixOld;
				}
			}

			atInitLoop = false;

			f0 = sumXY - ((sumX * sumY) / n);
			f1 = sumXX - ((sumX * sumX) / n);
			f2 = sumYY - ((sumY * sumY) / n);

			if (sqrtf(f1 * f2) != 0.0) { corr = f0 / sqrtf(f1 * f2); }
			else { corr = 0; }

			if (corr >= savedBestCorr)
			{
				savedBestCorr = corr;
				savedPosX = x;
				result.x1 = int(savedBestCorr * 100);
				result.y = y;
			}
		}
	}

	result.x = savedPosX;

	return result;
}

SpecialPoint BottleInspector::findPattA1(unsigned char* imgIn, unsigned char* imgPatt, int factor, int pattTyp)
{
	SpecialPoint result;
	result.x = result.y = result.c = 0;

	int LineLength = MainDisplayImageWidth / factor;
	int Height = MainDisplayImageHeight / factor;

	int szpatt;
	if (factor == 12) { szpatt = sizepattBy12; }
	if (factor == 6) { szpatt = sizepattBy6; }

	int szpattHalf = szpatt / 2;
	int LineLengthT = szpatt;
	float n = szpatt * szpatt;

	int xpos2 = 0;
	int ypos2 = 0;

	float	sumXY = 0;
	float	sumX = 0;
	float	sumXX = 0;
	float	sumY = 0;
	float	sumYY = 0;

	float	f0 = 0;
	float	f1 = 0;
	float	f2 = 0;
	float	corr = 0;

	float	savedBestCorr = 0.0;
	int		savedPosX = 0;
	int		cnt = 0;

	int fct2 = 1;
	if (factor == 12) { fct2 = 4; }
	if (factor == 6) { fct2 = 2; }

	bool debugging = false;
	if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(imgIn, LineLength, Height, "C:\\DebugFiles\\findpattA1.bmp"); }
	if (debugging) { ImageFileUtilities::SaveGrayBitmapToFile(imgPatt, LineLength, Height, "C:\\DebugFiles\\imgPatt.bmp"); }

	/////////////////

	sumX = 0.0;	sumXX = 0.0;

	for (int r = 0; r < n; r++)
	{
		sumX += *(imgPatt + r);
		sumXX += (*(imgPatt + r)) * (*(imgPatt + r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;

	bool atInitLoop = false;

	//////////////////

	int yTopLim = ytopLimPatt * 3 / factor;  ///Marked from the UI
	int yBotLim = ybotLimPatt * 3 / factor;
	int yIni = max(yTopLim + szpattHalf, yTopLim);  //Initial Y of the search area
	int yEnd = min(Height - szpattHalf, yBotLim - szpattHalf);

	/*
		int yTopLim = theapp->jobinfo[pframe->CurrentJobNum].ytopLimPatt*3/factor;
		int yBotLim = theapp->jobinfo[pframe->CurrentJobNum].ybotLimPatt*3/factor;
		int yIni	= MAX(szpattHalf, yTopLim);
		int yEnd	= MIN(Height-szpattHalf,yBotLim);
	*/

	//for(int y=startY; y<=startY+searchHeight; y++)//22
	for (int y = yIni; y <= yEnd; y++)
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop = true;

		for (x = szpattHalf; x < LineLength - szpattHalf; x++)
		{
			sumXY = 0.0;

			for (ypos = -szpattHalf; ypos < szpattHalf; ypos++)
			{
				if (atInitLoop)
				{
					for (xpos = -szpattHalf; xpos < szpattHalf; xpos++)
					{
						pixT = *(imgPatt + xpos + szpattHalf + LineLengthT * (ypos + szpattHalf));
						pix = *(imgIn + xpos + x + LineLength * (ypos + y));

						sumY += pix;
						sumYY += pix * pix;
						sumXY += pixT * pix;
					}
				}
				else
				{
					pixOld = *(imgIn - szpattHalf - 1 + x + LineLength * (ypos + y));
					pixNew = *(imgIn + szpattHalf - 1 + x + LineLength * (ypos + y));

					for (xpos = -szpattHalf; xpos < szpattHalf; xpos++)
					{
						pixT = *(imgPatt + xpos + szpattHalf + LineLengthT * (ypos + szpattHalf));
						pix = *(imgIn + xpos + x + LineLength * (ypos + y));

						sumXY += pixT * pix;
					}

					sumY += pixNew;
					sumY -= pixOld;
					sumYY += pixNew * pixNew;
					sumYY -= pixOld * pixOld;
				}
			}

			atInitLoop = false;

			f0 = sumXY - ((sumX * sumY) / n);
			f1 = sumXX - ((sumX * sumX) / n);
			f2 = sumYY - ((sumY * sumY) / n);

			if (sqrtf(f1 * f2) != 0.0) { corr = f0 / sqrtf(f1 * f2); }
			else { corr = 0; }

			if (corr >= savedBestCorr)
			{
				savedBestCorr = corr;
				savedPosX = x * factor;
				result.c = 100 * savedBestCorr;
				result.y = y * factor;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x = savedPosX;

	return result;
}

SpecialPoint BottleInspector::findPattHigherRes(unsigned char* imgIn, unsigned char* imgPatt, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x = result.y = result.c = 0;

	int LineLength = mainLineLength / factor;
	int Height = mainHeight / factor;
	int szpattW = sizepattBy3_2W;
	int szpattH = sizepattBy3_2H;
	int szpattHalfW = szpattW / 2;
	int szpattHalfH = szpattH / 2;
	int LineLengthT = szpattW;
	float n = (float) szpattW * szpattH;

	int xpos2 = 0;
	int ypos2 = 0;

	float	sumXY = 0;
	float	sumX = 0;
	float	sumXX = 0;
	float	sumY = 0;
	float	sumYY = 0;

	float	f0 = 0;
	float	f1 = 0;
	float	f2 = 0;
	float	corr = 0;

	float	savedBestCorr = 0.0;
	int		savedPosX = 0;
	int		cnt = 0;
	int		startY = refPt.y / factor;
	int		startX = refPt.x / factor;
	int		searchHeight = 26 / 2;

	/////////////////

	sumX = 0.0;	sumXX = 0.0;

	for (int r = 0; r < n; r++)
	{
		sumX += *(imgPatt + r);
		sumXX += (*(imgPatt + r)) * (*(imgPatt + r));
	}

	int x;
	int pix, pixT;
	int xpos, ypos;
	int pixNew, pixOld;

	bool atInitLoop = false;

	//////////////////

	if (startY <= szpattHalfH) { startY = szpattHalfH; }
	if (startX <= szpattHalfW) { startX = szpattHalfW; }

	for (int y = startY; y < startY + searchHeight + szpattHalfH; y++)//22
	{
		sumY = 0.0;  sumYY = 0.0;

		atInitLoop = true;

		for (x = startX; x < startX + szpattHalfW; x++)
		{
			sumXY = 0.0;

			for (ypos = -szpattHalfH; ypos < szpattHalfH; ypos++)
			{
				if (atInitLoop)
				{
					for (xpos = -szpattHalfW; xpos < szpattHalfW; xpos++)
					{
						pixT = *(imgPatt + xpos + szpattHalfW + LineLengthT * (ypos + szpattHalfH));
						pix = *(imgIn + xpos + x + LineLength * (ypos + y));

						sumY += pix;
						sumYY += pix * pix;
						sumXY += pixT * pix;
					}
				}
				else
				{
					pixOld = *(imgIn - szpattHalfW - 1 + x + LineLength * (ypos + y));
					pixNew = *(imgIn + szpattHalfW - 1 + x + LineLength * (ypos + y));

					for (xpos = -szpattHalfW; xpos < szpattHalfW; xpos++)
					{
						pixT = *(imgPatt + xpos + szpattHalfW + LineLengthT * (ypos + szpattHalfH));
						pix = *(imgIn + xpos + x + LineLength * (ypos + y));

						sumXY += pixT * pix;
					}

					sumY += pixNew;
					sumY -= pixOld;
					sumYY += pixNew * pixNew;
					sumYY -= pixOld * pixOld;
				}
			}

			atInitLoop = false;

			f0 = sumXY - ((sumX * sumY) / n);
			f1 = sumXX - ((sumX * sumX) / n);
			f2 = sumYY - ((sumY * sumY) / n);

			if (sqrtf(f1 * f2) != 0.0) { corr = f0 / sqrtf(f1 * f2); }
			else { corr = 0; }

			if (corr >= savedBestCorr)
			{
				savedBestCorr = corr;
				savedPosX = x * factor;
				result.c = int(savedBestCorr * 100);
				result.y = y * factor;
			}

			//calculate correlation
			//OnSaveBW(im_match2, 6);
		}
	}

	result.x = savedPosX;

	return result;
}

TwoPoint BottleInspector::findLabelEdge_Method5(unsigned char* imgIn, int fact, int limYt, int limYb, int limX, int limW)
{
	int	transLabel[1050];
	int	transLabelX[1050];
	int	transLabelY[1050];

	TwoPoint result;
	result.x = 0;
	result.y = 0;
	result.x1 = 0;

	int LineLength = camHeight / fact;
	int Height = camWidth / fact;

	/////////////////

	int pix;

	int Wslider = widthMet5 / fact;
	int hSlider = heigMet5 / fact;

	int yIni = limYt;
	int yEnd = (limYb)-hSlider;

	int xIni = limX / fact;
	int xEnd = (limX + limW) / fact;

	int npts = 0;
	int nptsLabel = 0;
	int nptsProdu = 0;
	int alphaInertia = 0;

	for (int i = 0; i < Height; i++)
	{
		transLabel[i] = 0;
		transLabelX[i] = 0;
		transLabelY[i] = 0;
	}

	bool atInitLoop;
	int x1;

	for (int y = yIni; y < yEnd; y++)
	{
		transLabel[y] = 0;
		atInitLoop = true;

		nptsLabel = 0;
		nptsProdu = 0;

		for (int x = xIni; x < xEnd - Wslider; x++)
		{
			//atInitLoop = true;

			if (atInitLoop)
			{
				for (x1 = x; x1 < x + Wslider; x1++)
				{
					for (int y1 = y; y1 < y + hSlider; y1++)
					{
						pix = *(imgIn + x1 + LineLength * y1);
						if (pix == 0) { nptsLabel++; }
						else { nptsProdu++; }
					}
				}
			}
			else
			{
				x1 = x - 1;

				for (int y1 = y; y1 < y + hSlider; y1++)
				{
					pix = *(imgIn + x1 + LineLength * y1);
					if (pix == 0) { nptsLabel--; }
					else { nptsProdu--; }
				}

				x1 = x + Wslider - 1;

				for (int y1 = y; y1 < y + hSlider; y1++)
				{
					pix = *(imgIn + x1 + LineLength * y1);
					if (pix == 0) { nptsLabel++; }
					else { nptsProdu++; }
				}
			}

			atInitLoop = false;

			int diff = nptsLabel - nptsProdu;

			if (diff > transLabel[y])
			{
				transLabel[y] = diff;
				transLabelX[y] = x * fact;
				transLabelY[y] = y * fact;
			}
		}

		//If the labels are bigger than product then make it label
		if (transLabel[y] > 0)
		{
			transLabel[y] = hSlider * (limW / fact);
		}

	}

	///////////////
	//With the transition information decide where the edge is
	//inertia a way to keep memory of the previous transitions
	//0.0 - 1.0

	//0.0 : no inertia
	//1.0 : It never forgets, it always remembers all the transitions
	//0.5 : to small
	//0.8 : appropiate ... 4/5

	for (int i = yIni; i < yEnd; i++)
	{
		alphaInertia = transLabel[i] + 4 * alphaInertia / 5;
		//TRACE("%i Trans[%i]-Alpha[%i]\n", y, transLabel[nCam][i], alphaInertia);

		if (alphaInertia > 0)
		{
			result.x = transLabelX[i];
			result.y = transLabelY[i];;
			result.x1 = 0;

			break;
		}
	}

	return result;
}

SpecialPoint BottleInspector::findLabelEdge_b(unsigned char* imgIn, int factor, SpecialPoint refPt)
{

	int LineLength = croppedHeight / factor;
	int Height = croppedWidth / factor;

	bool debugging = false;


	/////////////////

	int x;
	int pix, pix1, pix2;
	int diff;

	//////////////////

	int hTaught = heigMet3 / factor;  //Height of window used in Method 3
	int wTaught = widtMet3 / factor;  //Width of window used in Method 3
	int th = sensEdge;         //Sensitivity````

	int	startY = ytopLimPatt * 3 / factor;
	int	endY = ybotLimPatt * 3 / factor - hTaught;

	SpecialPoint result;
	result.x = 0;
	result.c = 255;
	result.y = startY * factor;      //ini Result var with default values.

	int counterBlock = 0;
	int sumpix = 0;
	bool foundEdge = false;
	bool stopNow = false;

	int minAvg = 255;
	int avgNow;

	int cntEdges = 0;
	int numEdgeReq = wTaught * 3 / 4;       //Three Fourth of the Width windw shoudl satisfy the threshold criteria

	for (x = 15; x < 120 - wTaught; x++)
	{
		for (int y = startY; (y < endY); y++)
		{
			foundEdge = false;
			cntEdges = 0;

			//**** First look for an edge
			for (int xW = x; xW < x + wTaught; xW++)   //At a time, going as far as the width window
			{
				pix1 = *(imgIn + xW + LineLength * (y + 0));
				pix = *(imgIn + xW + LineLength * (y + 2));

				if (edgeTransition == Job::EdgeTransition::Dark2Light) { diff = pix1 - pix; }  //Based on the type of transition selected.
				else { diff = pix - pix1; }

				if (diff > th) { cntEdges++; }
			}

			if (cntEdges > numEdgeReq)
			{
				cntEdges = 0;

				for (int xW = x; xW < x + wTaught; xW++)
				{
					pix1 = *(imgIn + xW + LineLength * (y - 2));
					pix = *(imgIn + xW + LineLength * (y + 2));

					if (edgeTransition == Job::EdgeTransition::Dark2Light) { diff = pix1 - pix; }
					else { diff = pix - pix1; }

					if (diff > th) { cntEdges++; }
				}

				if (cntEdges > numEdgeReq)
				{
					foundEdge = true;
				}
			}

			if (foundEdge)
			{
				stopNow = false;
				counterBlock = 0;
				sumpix = 0;

				//**** Second look for a consistent block
				//for(int y2=y+4; (y2<y+hTaught && stopNow==false); y2++)
				for (int y2 = y + 4; (y2 < y + hTaught); y2++)
				{
					//for(int x2=x; (x2<x+wTaught && stopNow==false); x2++)
					for (int x2 = x; (x2 < x + wTaught); x2++)
					{
						pix2 = *(imgIn + x2 + LineLength * y2);
						counterBlock++; sumpix += pix2;
					}
				}

				//*** Only go inside if block consistently found
				if (counterBlock != 0) { avgNow = sumpix / counterBlock; }
				else { avgNow = 255; }

				//if((y*factor)<result.y && avgBlock[lookinCam]<minAvg )
				if (avgNow < minAvg)
				{
/*					avgBlock = avgNow;
					minAvg = avgBlock;
					result.x = x * factor;
					result.y = y * factor;
					result.c = avgBlock;
*/
					minAvg = avgNow;
					result.x = x * factor;
					result.y = y * factor;
					result.c = avgNow;
				}
			}
		}
	}

	return result;
}

SpecialPoint BottleInspector::findWhiteBand(IntermediateImagePack* pack, unsigned char* imgIn, unsigned char* imgPatt, int factor, SpecialPoint refPt)
{
	SpecialPoint result;
	result.x = result.c = 0;
	result.y = mainHeight;

	int LineLength = croppedHeight / factor;
	int Height = croppedWidth / factor;

	int pix1, pix2, pix3, pix4, pix5, pix6, pix7, pix8, pix9;
	int pix, pixVal;

	// Creating the Sobel image
	for (int y = 3; y < Height - 3; y++)
	{
		for (int x = 3; x < LineLength - 3; x++)
		{
			pix1 = *(imgIn + x - 1 + LineLength * (y - 1));
			pix2 = *(imgIn + x + 0 + LineLength * (y - 1));
			pix3 = *(imgIn + x + 1 + LineLength * (y - 1));

			pix4 = *(imgIn + x - 1 + LineLength * (y + 0));
			pix5 = *(imgIn + x + 0 + LineLength * (y + 0));
			pix6 = *(imgIn + x + 1 + LineLength * (y + 0));

			pix7 = *(imgIn + x - 1 + LineLength * (y + 1));
			pix8 = *(imgIn + x + 0 + LineLength * (y + 1));
			pix9 = *(imgIn + x + 1 + LineLength * (y + 1));

			//pix =	abs( (pix1+2*pix2+pix3) - (pix7+2*pix8+pix9) ) +
			//		abs( (pix1+2*pix4+pix7) - (pix3+2*pix6+pix9) );

			pix = abs((pix1 + 2 * pix4 + pix7) - (pix3 + 2 * pix6 + pix9));

			pixVal = min(pix, 255);

			*(pack->edge + x + LineLength * y) = pixVal;
		}
	}

	/////////////////

	int x;
	//int diff;
	//int xpos, ypos;
	bool atInitLoop = false;

	//////////////////

	int	startY = ytopLimPatt * 3 / factor;
	int th = 80;

	int	counterV = 0;
	int counterBlock = 0;
	int sumpix = 0;
	bool foundEdge = false;
	bool stopNow = false;

	result.y = croppedWidth;

	int endY = ybotLimPatt * 3 / factor;

	int countEdges = 0;
	int	maxEdges = 0;

	for (int y = startY; y < endY; y++)
	{
		countEdges = 0;

		for (x = 15; x < 120; x++)
		{
			pix1 = *(pack->edge + x + LineLength * y);

			if (pix1 > th) { countEdges++; }
		}

		if (countEdges > maxEdges)
		{
			maxEdges = countEdges;
			result.x = (LineLength / 2) * factor;
			result.y = (y)*factor;
		}
	}

	return result;
}
	
void BottleInspector::createHistAndBinarize(IntermediateImagePack* pack, unsigned char* imgIn1, int tX, int tY, int tH, int bX, int bY, int bH, int W, int fact)
{
	unsigned char* imgR1 = pack->redbw;
	unsigned char* imgG1 = pack->greenbw;
	unsigned char* imgB1 = pack->bluebw;
	int pix1;//, pix2, pix3, pix4;
	int pixR, pixG, pixB;

	/////

	bool debugging = false;

	int newLengthBWDiv3 = croppedHeight / fact;
	int newHeightBWDiv3 = croppedWidth/ fact;

	/////////////////////
	int xIniT = max(0, tX / fact);
	int xEndT = min(newLengthBWDiv3, (tX + W) / fact);

	int yIniT = max(0, tY / fact);
	int yEndT = min(newHeightBWDiv3, (tY + tH) / fact);



	int xIniB = max(0, bX / fact);
	int xEndB = min(newLengthBWDiv3, (bX + W) / fact);

	int yIniB = max(0, bY / fact);
	int yEndB = min(newHeightBWDiv3, (bY + bH) / fact);

	for (int i = 0; i <= 255; i++) { histMeth5[i] = 0; }

	//////////
	//We need to double check that we are not out of range....
	//
	//
	//
	//////////
	int tRsum = 0; int tGsum = 0; int tBsum = 0; int tNpts = 0;
	int bRsum = 0; int bGsum = 0; int bBsum = 0; int bNpts = 0;
	int npts = 0;

	for (int y = yIniT; y < yEndT; y++)
	{
		for (int x = xIniT; x < xEndT; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3 * y);

			histMeth5[pix1]++;

			npts++;
		}
	}

	for (int y = yIniB; y < yEndB; y++)
	{
		for (int x = xIniB; x < xEndB; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3 * y);

			histMeth5[pix1]++;

			npts++;
		}
	}

	//////////////////////////////
	int greyVal05perc = 0;
	int greyVal10perc = 0;
	int greyVal15perc = 0;
	int greyVal25perc = 0;
	int greyVal50perc = 0;
	int greyVal75perc = 0;
	int greyVal85perc = 0;
	int greyVal95perc = 0;

	bool th05Found = false;
	bool th10Found = false;
	bool th15Found = false;
	bool th25Found = false;
	bool th50Found = false;
	bool th75Found = false;
	bool th85Found = false;
	bool th95Found = false;

	int thVal05perc = npts * 05 / 100;
	int thVal10perc = npts * 10 / 100;
	int thVal15perc = npts * 15 / 100;
	int thVal25perc = npts * 25 / 100;
	int thVal50perc = npts * 50 / 100;
	int thVal75perc = npts * 75 / 100;
	int thVal85perc = npts * 85 / 100;
	int thVal95perc = npts * 95 / 100;

	int accPix = 0;

	for (int i = 0; i <= 255; i++)
	{
		accPix += histMeth5[i];

		if (accPix >= thVal05perc && !th05Found) { greyVal05perc = i; th05Found = true; }
		if (accPix >= thVal10perc && !th10Found) { greyVal10perc = i; th10Found = true; }
		if (accPix >= thVal15perc && !th15Found) { greyVal15perc = i; th15Found = true; }
		if (accPix >= thVal25perc && !th25Found) { greyVal25perc = i; th25Found = true; }
		if (accPix >= thVal50perc && !th50Found) { greyVal50perc = i; th50Found = true; }
		if (accPix >= thVal75perc && !th75Found) { greyVal75perc = i; th75Found = true; }
		if (accPix >= thVal85perc && !th85Found) { greyVal85perc = i; th85Found = true; }
		if (accPix >= thVal95perc && !th95Found) { greyVal95perc = i; th95Found = true; }
	}

	int valLow = greyVal25perc;
	int valHig = greyVal75perc;
	int th = valLow + (valHig - valLow) * 40 / 100;

	int binVal;
	//////////////////////////////	
	//Create binary images
	for (int y = 0; y < newHeightBWDiv3 - 1; y++)
	{
		for (int x = 0; x < newLengthBWDiv3 - 1; x++)
		{
			pix1 = *(imgIn1 + x + newLengthBWDiv3 * y);

			if (pix1 > th) { binVal = 255; }
			else { binVal = 0; }
			*(pack->byteBinMet5 + x + newLengthBWDiv3 * y) = binVal;
		}
	}

	//////////
	int thGlare = 230;

	for (int y = 0; y < newHeightBWDiv3 - 1; y++)
	{
		for (int x = 0; x < newLengthBWDiv3 - 1; x++)
		{
			//Let's get rid of glare
			pixR = *(imgR1 + x + newLengthBWDiv3 * y);
			pixG = *(imgG1 + x + newLengthBWDiv3 * y);
			pixB = *(imgB1 + x + newLengthBWDiv3 * y);

			if (pixR > thGlare && pixG > thGlare && pixB > thGlare) { *(pack->byteBinMet5 + x + newLengthBWDiv3 * y) = 255; }
		}
	}
}


SpecialPoint BottleInspector::checkNoLabel1(int fac, unsigned char* imgIn, int x0, int y0, int x1, int h0, int w1, int h1)
{
	SpecialPoint result;
	result.x = result.y = result.c = 0;

	int camLength = croppedHeight / fac;
	int camHeight = croppedWidth / fac;
	int w1Sm = w1 / fac;
	int h1Sm = h1 / fac;
	int area = w1Sm * h1Sm;

	if (area <= 0) { area = 1; }

	int maxDensity = 0;
	int curDensity = 0;
	int pix;

	int xini = max(x0 / fac, 10);
	int xend = (x1 - w1) / fac - 10;
	int yini = max(y0 / fac, 10);
	int yend = min((y0 + h0) / fac, camHeight);

	if (xini<0 || xend>camLength || yini<0 || yend>camHeight) { return result; }

	int xDens = xini;
	int yDens = yini;

	result.x = xini * fac;
	result.y = yini * fac;
	result.c = 0;

	//this looks for the area with the most concentration of black points
	//if(pix==0) ... or if the pixel is black then add the concentration points
	//this will be the beginning of Inspection6
	//and it returns the position x, y of what is found
	int y;
	for (int x = xini; x < xend; x++)
	{
		for (y = yini; y < yend; y++)
		{
			if (x == xini)
			{
				curDensity = 0;

				//for each point calculate the concentration point
				for (int m = 0; m < w1Sm; m++)//horizontal
				{
					pix = *(imgIn + x + m + y * camLength);
					if (pix == 0) { curDensity++; }
				}
			}
			else
			{
				//for each point calculate the concentration point
				int m = -1;

				pix = *(imgIn + x + m + y * camLength);
				if (pix == 0) { curDensity--; }

				m = w1Sm - 1;

				pix = *(imgIn + x + m + y * camLength);
				if (pix == 0) { curDensity++; }
			}
		}

		if (curDensity > maxDensity)
		{
			maxDensity = curDensity;
			xDens = x * fac;
			yDens = y * fac;
		}
	}

	result.x = xDens;
	result.y = yDens;
	result.c = 100 * maxDensity / area;

	return result;
}


void BottleInspector::verAxisPtsRemOutliers(std::vector<SinglePoint>& v)
{

	if (v.size() > 0)
	{
		std::sort(v.begin(), v.end(), SingPtXAsc());

		int medianIndex = v.size() / 2;
		int medianX = v[medianIndex].x;
		int diff;

		for (size_t i = 0; i < v.size(); i++)
		{
			int xval = v[i].x;
			diff = abs(xval - medianX);

			if (diff > 10) //10
			{
				v.erase(v.begin() + i);
				i = i - 1;
			}
		}

	}
}

void BottleInspector::composeWaistInspection(IntermediateImagePack* pack)
{
	int verticalSearchLength = waistVerticalSearchLength;
	int horizontalSearchLength = waistHorizontalSearchLength;
	int edgeThreshold = waistEdgeThreshold;
	bool leftEdge = true;
	int imageWidth = FullImageScaledRotatedWidth;
	int imageHeight = FullImageScaledRotatedHeight;

	int topBound = waistMeasurementTopBound;
	int bottomBound = waistMeasurementBottomBound;
	int leftBound = waistMeasurementLeftBound;
	int rightBound = waistMeasurementRightBound;

	int numPoints;
	InitializeSinglePointCoordinates(topBound, bottomBound, imageWidth - 1, coordinatesOfContainerLeftTopEdgePointsRaw, numPoints);

	InspectionUtilities::FindEdge(pack->gray, imageWidth, imageHeight,
		topBound, bottomBound,
		leftBound, rightBound,
		verticalSearchLength, horizontalSearchLength,
		edgeThreshold,
		leftEdge,
		coordinatesOfContainerLeftTopEdgePointsRaw, numPoints);

	int avgWindowSize = 5;				//theapp->jobinfo[pframe->CurrentJobNum].cam1WaistAvgWindowSize;
	int edgeCoordinateDeviation = 15;	//theapp->jobinfo[pframe->CurrentJobNum].cam1WaistEdgeCoordinateDeviation;
	bool fillTopToBottom = true;

	InspectionUtilities::FillEdge(coordinatesOfContainerLeftTopEdgePointsRaw,
		avgWindowSize, edgeCoordinateDeviation, fillTopToBottom, coordQueue,
		coordinatesOfContainerLeftTopEdgePointsFilled, numPoints, vectorSize);

	leftEdge = true;
	topBound = waistMeasurementUpperTopBound;
	bottomBound = waistMeasurementUpperBottomBound;
	leftBound = waistMeasurementUpperLeftBound;
	rightBound = waistMeasurementUpperRightBound;

	InitializeSinglePointCoordinates(topBound, bottomBound, 0, coordinatesOfContainerLeftWaistEdgePointsRaw, numPoints);

	InspectionUtilities::FindEdge(pack->gray, imageWidth, imageHeight,
		topBound, bottomBound,
		leftBound, rightBound,
		verticalSearchLength, horizontalSearchLength,
		edgeThreshold,
		leftEdge,
		coordinatesOfContainerLeftWaistEdgePointsRaw, numPoints);

	InspectionUtilities::FillEdge(coordinatesOfContainerLeftWaistEdgePointsRaw,
		avgWindowSize, edgeCoordinateDeviation, fillTopToBottom, coordQueue,
		coordinatesOfContainerLeftWaistEdgePointsFilled, numPoints, vectorSize);

	int averageOfTopVector = InspectionUtilities::FindAverageXValue(coordinatesOfContainerLeftTopEdgePointsFilled, numPoints);
	int averageOfWaistVector = InspectionUtilities::FindAverageXValue(coordinatesOfContainerLeftWaistEdgePointsFilled, numPoints);

	resultwaist = averageOfTopVector - averageOfWaistVector;

}

void BottleInspector::InitializeSinglePointCoordinates(int startY, int endY, int defualtX,
	int* outSinglePointCoordinates, int& outSize)
{

	memset(outSinglePointCoordinates, 0, vectorSize * sizeof(int));

 	int verticalNumberOfPixelsToCheck = (endY - startY) + 1;
	outSize = verticalNumberOfPixelsToCheck;
	SinglePoint temSinglePoint;
	temSinglePoint.x = defualtX;

	int index = 0;
	for (int i = 0; i < verticalNumberOfPixelsToCheck; i++)
	{

		temSinglePoint.y = startY + index;
		outSinglePointCoordinates[index] = temSinglePoint.x;
		outSinglePointCoordinates[index +1] = temSinglePoint.y;
		index += 2;
	}

}