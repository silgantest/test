#include <vector>
#include "Point.h"
#include "LinearRegData.h"
#include "IntermediateImagePack.h"
#include <deque>
#include <mutex>

#ifndef INSPECTION_UTILITIES_H
#define INSPECTION_UTILITIES_H
#define FullImageScaleDownFactor 3


class InspectionUtilities
{
private:
	InspectionUtilities();
	static void Rotate270Right(IntermediateImagePack* pack);
	static void Rotate90Right(IntermediateImagePack* pack);
public:
	static double FindAverageXValue(int* coordinates, int numPoints);
	static void FindEdge(unsigned char* img, int imageWidth, int imageHeight,
		int verticalTopBound, int verticalBottomBound, int horizontalLeftBound, int horizontalRightBound,
		int verticalSearchLength, int horizontalSearchLength, int edgeThreshold, int searchLeftToRight,
		int* outCoordinatesOfEdgePoints, int numPoints);
	static linearRegData linearRegY(std::vector<SinglePoint>& points);
	static int AveragePixels(unsigned char* img, int imageWidth, int imageHeight, int averagingWindowLength,
		int averagingCenterYCoordinate, int averagingCenterXCoordinate, bool isVerticalAverage);
	static void FillEdge(int* edgeCoordinates, int avgWindowSize, int edgeCoordinateDeviation, bool fillTopToBottom, 
		std::deque<SinglePoint>* coordinatesQueue, int* edgeCoordinatesFilled, int numPoints, int vectorSize);


	static void Reduce(unsigned char* Image, IntermediateImagePack* imgPak, int mainDisWidth, int mainDisHeight, int rotateCounter = true);
	static void Reduce90(unsigned char* Image, IntermediateImagePack* imgPak, int mainDisWidth, int mainDisHeight);
	static void Reduce270(unsigned char* Image, IntermediateImagePack* imgPak, int mainDisWidth, int mainDisHeight);

	static void ScaleImageDownBySubsampling(unsigned char* full, IntermediateImagePack* pack, int imageWidth, int imageHeight);
	static void Rotate(IntermediateImagePack* pack, bool rot90deg);
	static void ConvertBGRAToGray(IntermediateImagePack* pack);

	
	static unsigned char* LoadPattern2(const char* jobPath);
	static unsigned char* LoadPattern2X(const char* jobPath);
	static void OnLoadPatternX_method6(const char* jobpath, int pattType);

};

#endif