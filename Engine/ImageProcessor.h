#include "CapturedImage.h"
#include "Inspector.h"
#include <vector>
#include <atomic>
#include "SharedImageQueue.h"
#include "Stats.h"
#include "PLCcomm.h"
#include <thread>
#include "JobManager.h"

#ifndef IMAGE_PROCESSOR_H
#define IMAGE_PROCESSOR_H

/*Must be thread-safe*/
class ImageProcessor : public NotifyJobUpdated
{
protected:
	SharedImageQueue* imageQueue;
	Inspector* inspection;
	std::atomic<bool> stopRequested;
	ResultsHandler* resultsHandler;
	bool jobUpdateRequired;
	PLCcomm* plc;
public:

#ifdef MEASURETIMES
	std::vector<Stats*>* times;
	std::mutex timesMtx;
	void AddStats(Stats* s);
#endif
	ImageProcessor(ResultsHandler* resHandler, unsigned int camindex);
	virtual ~ImageProcessor();

	void QueueImage(CapturedImage* capImg);
	void SetInspection(Inspector* ins);
	void ReadyForProcessing();
	void DoneProcessing();
	void StopWaiting();
	unsigned int index;
	void ProcessJobUpdates();
	CapturedImage* Pop();
	void Inspect(CapturedImage* im);
	bool StopRequested();

	void clearStats();
	virtual void JobUpdated(Job* newJob) override;

};

#endif //IMAGE_PROCESSOR_H