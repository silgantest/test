#include "StatusUpdateThread.h"
#include <windows.h>
#include "IPC.h"
#include "ModeState.h"

void StatusUpdateThread::operator()(UpdatorStatus* status)
{
	char str[100];
	HANDLE semSharedDataReady = OpenSemaphore(EVENT_ALL_ACCESS, TRUE, SHARED_MEMORY_STATUS_BUFFER_SEM);
	HANDLE mtxSharedDataAccess = OpenMutex(MUTEX_ALL_ACCESS, TRUE, SHARED_MEMORY_STATUS_BUFFER_MTX);

	if (!semSharedDataReady || !mtxSharedDataAccess)
		return;

	HANDLE hMapFile = OpenFileMapping(PAGE_READWRITE, false, STATUS_BUFFER);
	BYTE* pBuf = (BYTE*)MapViewOfFile(hMapFile, // handle to map object
		FILE_MAP_READ,  // read/write permission
		0,
		0,
		COMMAND_BUFFER_SIZE);

	int i = GetLastError();

	while (status->Running)
	{
	
		ModeState::GetCurrentStatus()->GetStatusArray((unsigned char*)pBuf);

		ReleaseSemaphore(semSharedDataReady, 1, 0);
	}
	UnmapViewOfFile(pBuf);
	CloseHandle(hMapFile);
}
