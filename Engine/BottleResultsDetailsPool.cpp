#include "BottleResultsDetailsPool.h"

BottleResultsDetailsPool* BottleResultsDetailsPool::instance;

BottleResultDetails* BottleResultsDetailsPool::GetDetails()
{
	return (BottleResultDetails*)instance->Get();
}

void BottleResultsDetailsPool::DeleteDetails(BottleResultDetails* obj)
{
	instance->Delete((Reusable*)obj);
}

void BottleResultsDetailsPool::Initialize()
{
	instance = new BottleResultsDetailsPool();
	sprintf_s(instance->name, "BottleResultsDetailsPool");
	instance->grow();
}

void BottleResultsDetailsPool::Shutdown()
{
	delete instance;
}


BottleResultsDetailsPool::BottleResultsDetailsPool()
{

}

BottleResultsDetailsPool::~BottleResultsDetailsPool()
{
	deleteEverything();
}

void BottleResultsDetailsPool::deleteObject(Reusable* obj)
{
	delete (BottleResultDetails*)obj;
}

Reusable* BottleResultsDetailsPool::create()
{
	return new BottleResultDetails();
};
