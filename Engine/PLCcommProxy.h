#include "PLCcomm.h"
#include "SerialPLCcomm.h"

#ifndef PLC_COMM_PROXY_H
#define PLC_COMM_PROXY_H


class PLCcommProxy
{
private:
	PLCcommProxy();

	class Holder
	{
	public:
		PLCcomm* instance;
		Holder();
		~Holder();

	};

public:
	virtual ~PLCcommProxy();

	static Holder theCommHolder;
	static PLCcomm* GetPLCcomm();
};

#endif //PLC_COMM_PROXY_H
