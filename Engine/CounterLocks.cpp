#include "CounterLocks.h"


void CounterLocks::ThreadCountNotify()
{
	this->cv_thread_count.notify_one();
}

void CounterLocks::ThreadCountWait()
{
	std::unique_lock<std::mutex> lock_ct(this->mtx_thread_count);
	this->cv_thread_count.wait(lock_ct);
}

