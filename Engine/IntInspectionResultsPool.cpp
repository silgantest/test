#include "IntInspectionResultsPool.h"

//static methods
IntInspectionResultsPool* IntInspectionResultsPool::instance;

IntRangeResult* IntInspectionResultsPool::GetResultObj()
{
	return (IntRangeResult*)instance->Get();
}

void IntInspectionResultsPool::DeleteResultObj(IntRangeResult* obj)
{
	instance->Delete(obj);
}

void IntInspectionResultsPool::Initialize()
{
	instance = new IntInspectionResultsPool();
	sprintf_s(instance->name, "IntInspectionResultsPool");
	instance->grow();
}

void IntInspectionResultsPool::Shutdown()
{
	delete instance;
}


IntInspectionResultsPool::IntInspectionResultsPool()
{

}

IntInspectionResultsPool::~IntInspectionResultsPool()
{
	deleteEverything();
}

void IntInspectionResultsPool::deleteObject(Reusable* obj)
{
	delete (IntRangeResult*)obj;
}

Reusable* IntInspectionResultsPool::create()
{
	return new IntRangeResult();
};
