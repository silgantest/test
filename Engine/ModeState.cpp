#include "ModeState.h"
#include "Status.h"
#include "PLCcommProxy.h"
#include "LogData.h"
#include <chrono>

ModeState* ModeState::instance;

void ModeState::Initialize()
{
	Status::Initialize();
	instance = new ModeState();
}

ModeState::ModeState()
{
	online = new OnlineMode();
	offline = new OfflineMode();
	current = offline;
	OKToTrigger = true;

}

ModeState::~ModeState()
{
}

void ModeState::Shutdown()
{
	delete instance;
	Status::Shutdown();
}

Status* ModeState::GetCurrentStatus()
{
	return Status::GetStatus();
}
//Refactor this somewhere else at some point
void ModeState::PauseTrigger()
{
	instance->GoOffline();
	instance->OKToTrigger = false;
	PLCcommProxy::GetPLCcomm()->PauseTrigger();
}

void ModeState::ResumeTrigger()
{
	PLCcommProxy::GetPLCcomm()->ResumeTrigger();
	instance->OKToTrigger = true;
	instance->cvTrigger.notify_all();
}

bool ModeState::WaitForOKtoTrigger(int mill)
{
	std::unique_lock<std::mutex> lk(instance->triggerMutex);
	std::chrono::microseconds d(mill);
	return instance->cvTrigger.wait_for(lk, d, [&]
	{
		return instance->OKToTrigger;
	});

}

void ModeState::GoOnline()
{
	instance->current = instance->online;
}

void ModeState::GoOffline()
{
	instance->current = instance->offline;
}