#include "SerialPLCComm.h"
#include "assert.h"

SerialPLCcomm::SerialPLCcomm() : bytesWritten(0)
{
	port = CreateFile("COM1", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

	DCB dcb;
	if (port == INVALID_HANDLE_VALUE)
	{
		DWORD err = GetLastError();
		//  Handle the error.
	}
	SecureZeroMemory(&dcb, sizeof(DCB));
	dcb.DCBlength = sizeof(DCB);

	bool f = GetCommState(port, &dcb);
	dcb.BaudRate = CBR_38400;
	dcb.ByteSize = 8;
	dcb.Parity = NOPARITY;
	dcb.StopBits = ONESTOPBIT;

	assert(SetCommState(port, &dcb));
	numBytes = 4;
}
	   
bool SerialPLCcomm::PauseTrigger()
{
	 
	return true;
}

bool SerialPLCcomm::ResumeTrigger()
{

	return true;
}

SerialPLCcomm::~SerialPLCcomm()
{
	CloseHandle(port);
}

bool SerialPLCcomm::SendPassFail(bool pass)
{
	if (pass)
		WriteFile(port, PLC_CODE_GoodBottle, numBytes, &bytesWritten, nullptr);
	else
		WriteFile(port, PLC_CODE_BadBottle, numBytes, &bytesWritten, nullptr);
	return bytesWritten > 0;
}

bool SerialPLCcomm::SendPassFail(char* code)
{
	return WriteFile(port, code, numBytes, &bytesWritten, nullptr);
}

bool SerialPLCcomm::IsPortAvailable()
{
	HANDLE port = CreateFile("COM2", GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, nullptr);
	DCB dcb;
	BOOL res = GetCommState(port, &dcb);
	if (res)
		CloseHandle(port);
	return res;

}
