#include "IntermediateImagePack.h"
#include "CapturedImage.h"
#include "ReusablePool.h"

#ifndef INTERMEDIATE_IMAGE_PACK_POOL_H
#define INTERMEDIATE_IMAGE_PACK_POOL_H


class IntermediateImagePackPool : public ReusablePool
{
private:


	static bool loadedApp;
	static void loadApp();
	static IntermediateImagePackPool* instance;

protected:
	IntermediateImagePackPool();

	virtual Reusable* create() override;

	void deleteObject(Reusable* obj) override;
	//void deleteObject(void* obj) override;

public:
	IntermediateImagePackPool(IntermediateImagePackPool&) = delete;
	IntermediateImagePackPool& operator=(IntermediateImagePackPool& rhs) = delete;

	virtual ~IntermediateImagePackPool();

	static IntermediateImagePack* GetImage(CapturedImage* image);
	static void DeleteImages(IntermediateImagePack* obj);
	static void Initialize();
	static void Shutdown();
};

#endif