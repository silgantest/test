#include <stdio.h>
#include "Reusable.h"
#include "CapturedImage.h"
#include "LabelIntegrityMethod.h"

#ifndef INTERMEDIATE_IMAGE_PACK
#define INTERMEDIATE_IMAGE_PACK

class IntermediateImagePack : public Reusable
{

public:
	IntermediateImagePack(size_t height, size_t width, size_t scaledheight, size_t scaledwidth, size_t bytesPerPixel);
	~IntermediateImagePack();


	int camIndex;
	int imageNumber;
	size_t scaledHeight;
	size_t scaledWidth;
	/*Created by reduce*/
	unsigned char* reduced;
	unsigned char* bw;
	unsigned char* bluebw; 
	unsigned char* greenbw; 
	unsigned char* redbw; 
	unsigned char* sa2Div3;
	unsigned char* satDiv3;
	unsigned char* magDiv3;
	unsigned char* yelDiv3;
	unsigned char* cyaDiv3;
	unsigned char* bluebwDiv6;
	unsigned char* redbwDiv6;
	unsigned char* greenbwDiv6;
	unsigned char* cyaDiv6;
	unsigned char* yelDiv6;
	unsigned char* magDiv6;
	unsigned char* bwDiv6;
	unsigned char* satDiv6;

	unsigned char* scaled; //ScaleImageDownBySubsampling
	unsigned char* rotated; //Rotate
	unsigned char* gray; //ConvertBGRAToGray
	unsigned char* byteBinMet5; //createHistAndBinarize
	unsigned char* byteBinEdges; //checkNoLabel1


	unsigned char* filterForPattMatchDiv3;  //reference only, Set filter
	unsigned char* filterForPattMatchDiv6;  //reference only, Set filter
	unsigned char* imgMiddBottle;  //reference only, SetMiddBottle
	unsigned char* filterForShiftedDiv3; //reference only 

	/*Created by edgeImgCreation*/
	unsigned char* edgH;
	unsigned char* edgV;
	unsigned char* bin;

	
	unsigned char* edge; //FindWhiteBand

	void SetFilterForPattMatch(ImageFilters filterSelPatt);
	void SetMiddBottle(int midImg);
	void SetFilterForShifted(ImageFilters filterSelPatt);

	unsigned char* GetFilter(ImageFilters filter);

	void Set(int imNum, int camNum);

	virtual void Reset() override;
	virtual void Unload() override;
};

#endif