#include "ResultsGroup.h"
#include "LabelResults.h"
#include <iostream>
#include "ResultsStruct.h"

#ifndef INSPECTION_85_RESULTS_H
#define INSPECTION_85_RESULTS_H

class Inspection85Results : public ResultsGroup
{

private:
	bool addedCap;
	TwoPoint bestPos;
	TwoPoint bestLeft;
	SpecialPoint posPattBy6;

	int bestPosIndex;
	int bestLeftIndex;

	TwoPoint minAvg;
	int minAvgIndex;

	TwoPoint minHeight;
	int minHeightIndex;
	TwoPoint maxHeight;
	int maxHeightIndex;

	TwoPoint maxAvg;
	int maxAvgIndex;

	int sumHs;
	LabelResults* labelRes[BOTTLE_CAMS];
	int inspResultOK[MAX_INSPECTIONS];

	void addLabelResults(LabelResults* res);
	void resetPoint(TwoPoint p);

public:
	Inspection85Results();
	virtual ~Inspection85Results();
	void AddResults(InspectionResult* res);
	LabelResults* GetLabelResults(int index);

	TwoPoint GetBestLabelPos();

	TwoPoint GetBestLeftLabelPos();
	int GetBestLeftLabelPosIndex();
	int GetBestLabelPosIndex();
	SpecialPoint GetPattBy6();

	TwoPoint GetMinAvgLabelPos();
	int GetMinAvgLabelPosIndex();
	TwoPoint GetMinHeightLabelPos();
	int GetMinHeightLabelPosIndex();

	TwoPoint GetMaxHeightLabelPos();
	int GetMaxHeightLabelPosIndex();

	int GetSumHeights();

	virtual void AddInspectionResults(InspectionResult* res, CapturedImage* im, IntermediateImagePack* pack) override;
	virtual void AddInspectionResults(InspectionResult* res[], int count, CapturedImage* im, Cap85IntermediateImagePack* pack);
	virtual void GetResultsSet(unsigned char* fullResults);

	virtual void Reset() override;
	virtual void Unload() override;
	virtual bool Complete() override;

};

#endif
