#include "BaslerCameraThread.h"
#include "BaslerCapturedImage.h"
#include "ThreadCounter.h"

void BaslerCameraThread::operator()(BaslerCamera* cam, ImageProcessor* processor, CounterLocks* lk) const
{
	ThreadCounter TC(lk);  //Track if thread is still open

	cam->ResetImageNumber();
	std::lock_guard<std::mutex> sslk(cam->startStop);
	while (cam->IsRunning())
	{
		CGrabResultPtr ptrGrabResult;
#ifdef MEASURETIMES
		Stats* s = new Stats();
		s->CamNumber = cam->GetIndex();
		s->Action = Activity::Grab;
		s->timestampStart = GetTickCount();
#endif
		cam->GetNextImage(ptrGrabResult);
		cam->incrementImageNumber();
		processor->QueueImage(BaslerCapturedImage::CreateBaslerCapImg(ptrGrabResult, cam->imageNumber, cam->GetIndex()));

#ifdef MEASURETIMES
		s->ImageNumber = cam->imageNumber;
		s->timestampEnd = GetTickCount();
		std::lock_guard<std::mutex> lk(cam->timesMtx);
		cam->times->push_back(s);
#endif
	}
}
