#include "Inspection85Results.h"
#include "ResultsGroupPool.h"
#include "FloatRangeResult.h"

Inspection85Results::Inspection85Results()
{
	Reset();
}

Inspection85Results::~Inspection85Results()
{
}

void Inspection85Results::AddInspectionResults(InspectionResult* res, CapturedImage* im, IntermediateImagePack* pack)
{
	images[im->GetCameraIndex()] = im;
	packs[im->GetCameraIndex()] = pack;

	if (res->GetCode() == InspectionCode::Label)
		addLabelResults((LabelResults*)res);
}

void Inspection85Results::AddInspectionResults(InspectionResult* res[], int count, CapturedImage* im, Cap85IntermediateImagePack* pack)
{
	images[im->GetCameraIndex()] = im;
	capPack = pack;

	for (int i = 0; i < count; i++)
	{
		if (res[i]->GetCode() == InspectionCode::Cap85)
		{
			results->resultSet[i].Value = ((FloatRangeResult*)res[i])->GetResult();
			if (res[i]->GetIndex() == 0)
			{
				results->Result1l = ((FloatRangeResult*)res[i])->GetLeftResult();
				results->Result1r = ((FloatRangeResult*)res[i])->GetRightResult();
			}
			addedCap = true;
			res[i]->Unload();
		}
	}
}



LabelResults* Inspection85Results::GetLabelResults(int index)
{
	return labelRes[index];
}

void Inspection85Results::addLabelResults(LabelResults* res)
{
	sumHs += res->GetLabelPos().y;
	labelRes[res->GetCam()] = res;
	if (res->GetLabelPos().x1 >= this->bestPos.x1)
	{
		this->bestPos = res->GetLabelPos();
		this->bestPosIndex = res->GetCam();
	}

	if (res->GetLabelPos().x1 >= this->bestLeft.x1)
	{
		this->bestLeft = res->GetLabelPos();
		this->bestLeftIndex = res->GetCam();
		this->posPattBy6 = res->GetPosBy6();
	}

	if (res->GetLabelPos().x1 < this->minAvg.x1)
	{
		this->minAvg = res->GetLabelPos();
		this->minAvgIndex = res->GetCam();

	}

	if (res->GetLabelPos().y < this->minHeight.y)
	{
		this->minHeight = res->GetLabelPos();
		this->minHeightIndex = res->GetCam();
	}
	if (res->GetLabelPos().y > this->maxHeight.y)
	{
		this->maxHeight = res->GetLabelPos();
		this->maxHeightIndex = res->GetCam();
	}
}

SpecialPoint Inspection85Results::GetPattBy6()
{
	return this->posPattBy6;
}

void Inspection85Results::AddResults(InspectionResult* res)
{
	switch (res->GetCode())
	{
	LabelResult:
		this->addLabelResults((LabelResults*)res);
		break;
	default:
		break; //throw somethings wrong
	}
}

TwoPoint Inspection85Results::GetBestLabelPos()
{
	return this->bestPos;
}

TwoPoint Inspection85Results::GetBestLeftLabelPos()
{
	return this->bestLeft;
}

int Inspection85Results::GetBestLeftLabelPosIndex()
{
	return this->bestLeftIndex;
}

int Inspection85Results::GetBestLabelPosIndex()
{
	return this->bestPosIndex;
}

TwoPoint Inspection85Results::GetMinAvgLabelPos()
{
	return this->minAvg;
}

int Inspection85Results::GetMinAvgLabelPosIndex()
{
	return this->minAvgIndex;
}

TwoPoint Inspection85Results::GetMinHeightLabelPos()
{
	return this->minHeight;
}

int Inspection85Results::GetMinHeightLabelPosIndex()
{
	return this->minHeightIndex;
}

TwoPoint Inspection85Results::GetMaxHeightLabelPos()
{
	return this->maxHeight;
}

int Inspection85Results::GetMaxHeightLabelPosIndex()
{
	return this->maxHeightIndex;
}

void Inspection85Results::resetPoint(TwoPoint p)
{
	p.x1 = p.x = p.y1 = p.y = 0;
}

bool Inspection85Results::Complete() 
{
	for (int i = 0; i <= 3; i++)
		if (this->labelRes[i] == nullptr)
			return false;
	return addedCap;
}

void Inspection85Results::Reset()
{
	this->resetPoint(this->bestPos);
	this->resetPoint(this->bestLeft);
	this->resetPoint(this->minAvg);
	this->resetPoint(this->minHeight);
	this->resetPoint(this->maxHeight);
	this->resetPoint(this->maxAvg);
	this->posPattBy6.c = this->posPattBy6.x = this->posPattBy6.y = 0;
	memset(this->labelRes, 0, sizeof(LabelResults*) * BOTTLE_CAMS);
	sumHs = 0;
	addedCap = false;
	results->Clear();
}

void Inspection85Results::Unload()
{
	ResultsGroup::Unload();
	//Move to handler
	for(int i=0; i< BOTTLE_CAMS; i++)
		if(labelRes[i])
			labelRes[i]->Unload();
	results->Clear();

	Results85GroupPool::DeleteResultGroup(this);
}

int Inspection85Results::GetSumHeights()
{
	return this->sumHs;
}


void Inspection85Results::GetResultsSet(unsigned char* fullResults)
{
	this->results->GetResultsArray(fullResults);
}
