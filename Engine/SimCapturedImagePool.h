#include "SimCapturedImage.h"
#include "ReusablePool.h"
#include <string>


#ifndef SIM_CAPTURED_IMAGE_POOL_H
#define SIM_CAPTURED_IMAGE_POOL_H


class SimCapturedImagePool : public ReusablePool
{
private:
	static SimCapturedImagePool* instance;

protected:
	SimCapturedImagePool();
	virtual Reusable* create() override;
	void deleteObject(Reusable* obj) override;

public:
	SimCapturedImagePool(SimCapturedImagePool&) = delete;
	SimCapturedImagePool& operator=(SimCapturedImagePool& rhs) = delete;
	virtual ~SimCapturedImagePool();

	static SimCapturedImage* GetSimImage(unsigned char* image, int imNum, int camNum, int width, int height);
	static void DeleteCapturedImage(SimCapturedImage* obj);
	static void Initialize();
	static void Shutdown();
};

#endif