#include "CapInspector.h"
#include "FloatingInspectionResultsPool.h"
#include "math.h"
#include "Cap85IntermediateImagePackPool.h"
#include "InspectionUtilities.h"
#include "ImageFileUtilities.h"
#include "JobManager.h"
#include "CapResultsDetailsPool.h"

CapInspector::CapInspector()
{
	im_match2 = (char*)malloc(50 * 60);
	im_matchl = new ImagePattern();
	im_matchr = new ImagePattern();
	lineLength2 = 28;
	length = lineLength2 * lineLength2;
	camIndex = 5;
	doUser = nullptr;
}


CapInspector::~CapInspector()
{
	delete im_matchr;
	delete im_matchl;
	free(im_match2);
	if (doUser)
		delete doUser;
}


void CapInspector::SetAppConfig(AppConfig* config)
{
	//TODO:
	//Are the height and width set correctly?????
	mainLineLength = config->GetMainDisplayImageHeight() / 2;
	mainHeight = config->GetMainDisplayImageWidth() / 2;
	MainDisplayImageHeight = config->GetMainDisplayImageHeight();
	MainDisplayImageWidth = config->GetMainDisplayImageWidth();

	//This is actually the same as above
	croppedLineLength = config->GetCroppedCameraWidth() / 2;
	croppedHeight = config->GetCroppedCameraHeight() / 2;

	doUser = new DoUserCommand(config->GetMainDisplayImageHeight(), config->GetMainDisplayImageWidth());
	lineLength = config->GetCamWidth() / 2;
	height = config->GetCamHeight() / 2;
	if (doUser)
		delete doUser;
	doUser = new DoUserCommand(config->GetMainDisplayImageHeight(), config->GetMainDisplayImageWidth());


}


void CapInspector::ResetForJob()
{
	Job* job = JobManager::GetCurrentJob();
	neckBarY = job->GetNeckBarY();

	if (neckBarY + 14 + 5 > height) { neckBarY = height - 19; }

	neckLimit = job->GetNeckStrength();
	capLimit = job->GetCapStrength();
	capLimit2 = job->GetCapStrength2();
	lDist = job->GetLDist();

	method = job->GetFilterSelPattCap();

	sportSenLimit = job->GetSportSen();
	sportPos = job->GetSportPos();
	useSport = job->SportCap();

	capColorOffset = job->GetCapColorOffset();

	targetR = job->GetColorTargR();
	targetG = job->GetColorTargG();
	targetB = job->GetColorTargB();
	Patterns* patterns = JobManager::GetCurrentPatterns();

	patterns->CopyCapLeft(im_matchl);
	patterns->CopyCapRight(im_matchr);

}

void CapInspector::Inspect(CapturedImage * capImg, ResultsHandler* handler)
{


	Cap85IntermediateImagePack* pack = Cap85IntermediateImagePackPool::GetImage(capImg);
	pack->ReduceCapCam((unsigned char *)capImg->GetBuffer(), MainDisplayImageHeight, MainDisplayImageWidth);
	pack->SetFilterForCap(method);
	byte5ForPattMatchDiv3 = pack->filterForPattMatchDiv3;


	CapResultDetails* details = CapResultsDetailsPool::GetDetails();
	for (int i = 0; i < 4; i++)
	{
		FloatRangeResult* fr = FloatingInspectionResultsPool::GetResultObj();
		fr->SetCode(InspectionCode::Cap85);
		fr->SetIndex(i);
		res[i] = fr;
	}


	SpecialPoint neckLLip, neckRLip;
	Point neckLSide, neckRSide;

	Point capLTop, capRTop;
	Point capLSide, capRSide;

	Point midBot, midTop;
	midTop.x = midTop.y = 0;

	//Edge detection Left, Right, Middle edges... before im_src5
	capLSide = FindCapSide(byte5ForPattMatchDiv3, neckBarY, true);
	capRSide = FindCapSide(byte5ForPattMatchDiv3, neckBarY, false);
	details->AddCapLeftSide(capLSide);
	details->AddCapRightSide(capRSide);
	int ldist = capLSide.x + lDist;

	//TODO
	int rdist = capRSide.x - lDist;

	capLTop = FindCapTop(byte5ForPattMatchDiv3, ldist, 0);
	capRTop = FindCapTop(byte5ForPattMatchDiv3, rdist, 1);
	details->AddCapLeftTop(capLTop);
	details->AddCapRightTop(capRTop);

	neckLSide.x = capLSide.x;
	neckLSide.y = neckBarY;

	neckRSide.x = capRSide.x;
	neckRSide.y = neckBarY;

	neckLLip = FindNeckLipPat(byte5ForPattMatchDiv3, neckLSide, true);
	neckRLip = FindNeckLipPat(byte5ForPattMatchDiv3, neckRSide, false);
	res[0]->SetResultLeft(sqrtf((neckLLip.y - capLTop.y)*(neckLLip.y - capLTop.y) + (neckLLip.x - capLTop.x)*(neckLLip.x - capLTop.x)));
	res[0]->SetResultRight(sqrtf((neckRLip.y - capRTop.y)*(neckRLip.y - capRTop.y) + (neckRLip.x - capRTop.x)*(neckRLip.x - capRTop.x)));
	details->AddNeckLeftLip(neckLLip);
	details->AddNeckRightLip(neckRLip);

	if (useSport)
	{
		TwoPoint sportBand = CheckSportBand(byte5ForPattMatchDiv3, capLTop, capRTop);
		res[1]->SetResult(sportBand.x1 - sportBand.x);
		details->AddSportBand(sportBand);
	}
	else
	{
		midBot.y = (neckRLip.y + neckLLip.y) / 2;
		midTop.y = FindCapMid(byte5ForPattMatchDiv3, midTop.x);
		details->AddCapMidTop(midBot);
		details->AddCapMidBot(midTop);

		res[1]->SetResult(midBot.y - midTop.y);
	}


	if (neckLLip.c <= neckRLip.c)
	{
		res[3]->SetResult(neckLLip.c);
	}
	else
	{
		res[3]->SetResult(neckRLip.c);
	}

	doUser->Execute((char*)capImg->GetBuffer(), neckLLip.x + 80, neckLLip.y - capColorOffset);
	int ColorResult2 = doUser->GetResult();
		
		//TODO: Reduce implicit conversions
		int resR = GetRValue(ColorResult2);
		int resG = GetGValue(ColorResult2);
		int resB = GetBValue(ColorResult2);


		details->AddColorInfo(resR, resG, resB);

		res[2]->SetResult(sqrtf(fabs((resB - targetB)*(resB - targetB)) +
							fabs((resG - targetG)*(resG - targetG)) +
							fabs((resR - targetR)*(resR - targetR))));

		handler->AddResults((InspectionResult**)res, NUM_CAP_INSP, capImg, pack, details);
}


TwoPoint CapInspector::CheckSportBand(unsigned char* im_srcIn, Point CapLTop, Point CapRTop)
{
	TwoPoint result;
	result.x = result.y = result.x1 = result.y1 = 0;

	int xpos = 0;
	int ypos = sportPos;//CapLTop.y-90;

	if (ypos <= 3) { ypos = 3; }

	int count = 0;
	int pixa, pixb, pixc, pixd, pixe, pixf;

	int d1 = 0;

	if (CapLTop.x > 10 && CapLTop.x < 500 && CapRTop.x>10 && CapRTop.x < 500 && ypos >= 3 && ypos < 100)
	{
		for (xpos = CapLTop.x - 5; xpos < 400; xpos++)
		{
			pixa = *(im_srcIn + xpos - 2 + croppedLineLength * (ypos - 1));
			pixb = *(im_srcIn + xpos - 2 + croppedLineLength * (ypos));
			pixc = *(im_srcIn + xpos - 2 + croppedLineLength * (ypos + 1));

			pixd = *(im_srcIn + xpos + 2 + croppedLineLength * (ypos - 1));
			pixe = *(im_srcIn + xpos + 2 + croppedLineLength * (ypos));
			pixf = *(im_srcIn + xpos + 2 + croppedLineLength * (ypos + 1));

			d1 = abs((pixa + 2 * pixb + pixc) - (pixd + 2 * pixe + pixf));

			if (d1 > sportSenLimit)//&& (d2 > Limit) && (d3 > Limit )
			{
				result.y = ypos; result.x = xpos; xpos = 400;
			}
		}

		for (xpos = CapRTop.x + 5; xpos > 100; xpos--)
		{
			pixa = *(im_srcIn + xpos - 2 + croppedLineLength * (ypos - 1));
			pixb = *(im_srcIn + xpos - 2 + croppedLineLength * (ypos));
			pixc = *(im_srcIn + xpos - 2 + croppedLineLength * (ypos + 1));

			pixd = *(im_srcIn + xpos + 2 + croppedLineLength * (ypos - 1));
			pixe = *(im_srcIn + xpos + 2 + croppedLineLength * (ypos));
			pixf = *(im_srcIn + xpos + 2 + croppedLineLength * (ypos + 1));

			d1 = abs((pixa + 2 * pixb + pixc) - (pixd + 2 * pixe + pixf));

			if (d1 > sportSenLimit)//&& (d2 > Limit) && (d3 > Limit )
			{
				result.y1 = ypos; result.x1 = xpos; xpos = 100;
			}
		}
	}

	return result;
}


Point CapInspector::FindCapTop(unsigned char* im_srcIn, int SideDist, int side)
{
	int nCam = 5;
	Point result;
	result.x = result.y = 0;

	int pixa1, pixb1, pixc1, pixd1, pixe1, pixf1;
	int pixa2, pixb2, pixc2, pixd2, pixe2, pixf2;
	int pixa3, pixb3, pixc3, pixd3, pixe3, pixf3;

	int d1, d2, d3;
	
	int xpos = SideDist;
	int ypos = 1;

	int XStart = 10;
	int XEnd = mainLineLength - 10;

	int maxdif = 0;

	if (xpos >= XStart && xpos <= XEnd)
	{
		for (ypos = 4; ypos < mainHeight - 30; ypos++)
		{
			pixa1 = *(im_srcIn + xpos - 4 + mainLineLength * (ypos - 2));
			pixb1 = *(im_srcIn + xpos - 3 + mainLineLength * (ypos - 2));
			pixc1 = *(im_srcIn + xpos - 2 + mainLineLength * (ypos - 2));
			pixd1 = *(im_srcIn + xpos - 4 + mainLineLength * (ypos + 2));
			pixe1 = *(im_srcIn + xpos - 3 + mainLineLength * (ypos + 2));
			pixf1 = *(im_srcIn + xpos - 2 + mainLineLength * (ypos + 2));

			pixa2 = *(im_srcIn + xpos - 1 + mainLineLength * (ypos - 2));
			pixb2 = *(im_srcIn + xpos + 0 + mainLineLength * (ypos - 2));
			pixc2 = *(im_srcIn + xpos + 1 + mainLineLength * (ypos - 2));
			pixd2 = *(im_srcIn + xpos - 1 + mainLineLength * (ypos + 2));
			pixe2 = *(im_srcIn + xpos + 0 + mainLineLength * (ypos + 2));
			pixf2 = *(im_srcIn + xpos + 1 + mainLineLength * (ypos + 2));

			pixa3 = *(im_srcIn + xpos + 2 + mainLineLength * (ypos - 2));
			pixb3 = *(im_srcIn + xpos + 3 + mainLineLength * (ypos - 2));
			pixc3 = *(im_srcIn + xpos + 4 + mainLineLength * (ypos - 2));
			pixd3 = *(im_srcIn + xpos + 2 + mainLineLength * (ypos + 2));
			pixe3 = *(im_srcIn + xpos + 3 + mainLineLength * (ypos + 2));
			pixf3 = *(im_srcIn + xpos + 4 + mainLineLength * (ypos + 2));

			d1 = abs((pixa1 + 2 * pixb1 + pixc1) - (pixd1 + 2 * pixe1 + pixf1));
			d2 = abs((pixa2 + 2 * pixb2 + pixc2) - (pixd2 + 2 * pixe2 + pixf2));
			d3 = abs((pixa3 + 2 * pixb3 + pixc3) - (pixd3 + 2 * pixe3 + pixf3));

			if (d1 > capLimit || d2 > capLimit || d3 > capLimit)
				//if( d1>Limit && d2>Limit && d3>Limit )
			{
				result.y = ypos;
				result.x = xpos;

				maxdif = max(d1, d2);
				maxdif = max(d3, maxdif);
				ypos = mainHeight - 30;
			}
		}
	}

	//TODO
	//Used in configuration 
//	if (side == 0) { currEdgeL = maxdif; }
//	else if (side == 1) { currEdgeR = maxdif; }



	return result;
}

Point CapInspector::FindCapSide(unsigned char* im_srcIn, int MidCap, bool left)
{
	int nCam = 5;
	Point result; result.x = result.y = 0;

	int pixa, pixb, pixc, pixd, pixe, pixf;//, pixg, pixh;

	int d1 = 0;

	int xpos = 10;
	int ypos = MidCap;
	int startx = 20;
	int endx;

	if (ypos <= height - 5 && ypos >= 1)
	{
		if (left)
		{
			//TODO - Displayed in statistics tab -- 
			//capLeftVal = 0;

			startx = 30; endx = mainLineLength * 3 / 4;

			for (xpos = startx; xpos < endx; xpos++)
			{
				pixa = *(im_srcIn + xpos - 2 + mainLineLength * (ypos - 1));
				pixd = *(im_srcIn + xpos + 2 + mainLineLength * (ypos - 1));

				pixb = *(im_srcIn + xpos - 2 + mainLineLength * (ypos));
				pixe = *(im_srcIn + xpos + 2 + mainLineLength * (ypos));

				pixc = *(im_srcIn + xpos - 2 + mainLineLength * (ypos + 1));
				pixf = *(im_srcIn + xpos + 2 + mainLineLength * (ypos + 1));

				d1 = abs((pixa + 2 * pixb + pixc) - (pixd + 2 * pixe + pixf));

				if (d1 > neckLimit)//&& (d2 > Limit) && (d3 > Limit )
				{
					result.y = ypos; result.x = xpos; 
					//TODO
					//capLeftVal = d1; 
					break;
				}
			}
		}
		else
		{
			//TODO
			//capRighVal = 0;

			startx = 475; endx = mainLineLength / 4;

			for (xpos = startx; xpos > endx; xpos--)
			{
				pixa = *(im_srcIn + xpos - 2 + mainLineLength * (ypos - 1));
				pixd = *(im_srcIn + xpos + 2 + mainLineLength * (ypos - 1));

				pixb = *(im_srcIn + xpos - 2 + mainLineLength * (ypos));
				pixe = *(im_srcIn + xpos + 2 + mainLineLength * (ypos));

				pixc = *(im_srcIn + xpos - 2 + mainLineLength * (ypos + 1));
				pixf = *(im_srcIn + xpos + 2 + mainLineLength * (ypos + 1));

				d1 = abs((pixa + 2 * pixb + pixc) - (pixd + 2 * pixe + pixf));

				if (d1 > neckLimit)//&& (d2 > Limit) && (d3 > Limit )
				{
					result.y = ypos; result.x = xpos - 2; 
					//TODO
					//capRighVal = d1; 
					break;
				}
			}
		}
	}

	return result;
}

SpecialPoint CapInspector::FindNeckLipPat(unsigned char* im_srcIn, Point side, bool left)
{
	int nCam = 5;
	SpecialPoint result;
	result.x = result.y = result.c = 0;

	int r, q;

	int xpos1 = 1;	int ypos1 = 1;
	int xpos2 = 0;	int ypos2 = 0;
	int xpos3 = 0;

	float sumxy = 0;
	float sumx = 0;
	float sumy = 0;
	float sumxsq = 0;
	float sumysq = 0;

	float f0 = 0;
	float f1 = 0;
	float f2 = 0;

	float corr = 0;
	float savedcorr = 0.3f;

	int	savedpos = -50;

	if (side.x > 24 && side.x < lineLength - 24 && neckBarY - 44>0)
	{
		xpos1 = side.x;

		if (left)
		{
			for (q = -30; q <= 5; q++)
			{
				for (ypos1 = neckBarY - 14; ypos1 < neckBarY + 14; ypos1++)
				{
					for (xpos3 = xpos1 - 24; xpos3 < xpos1 + 4; xpos3++)
					{
						*(im_match2 + xpos2 + lineLength2 * ypos2) = *(im_srcIn + xpos3 + lineLength * (ypos1 + q));
						xpos2 += 1;
					}

					xpos2 = 0; ypos2 += 1;
				}

				ypos2 = 0;

				sumxy = 0.0; sumx = 0.0; sumy = 0.0; sumxsq = 0.0; sumysq = 0.0;


				//TODO
				//So many implicit conversions 
				for (r = 0; r < length; r++)
				{
					sumxy += (*(im_matchl->GetPattern() + r)) * (*(im_match2 + r));
					sumx += *(im_matchl->GetPattern() + r);
					sumy += *(im_match2 + r);
					sumxsq += (*(im_matchl->GetPattern() + r)) * (*(im_matchl->GetPattern() + r));
					sumysq += (*(im_match2 + r)) * (*(im_match2 + r));
				}

				f0 = sumxy - ((sumx*sumy) / length);
				f1 = sumxsq - ((sumx*sumx) / length);
				f2 = sumysq - ((sumy*sumy) / length);

				if (sqrtf(f1*f2) != 0.0) { corr = f0 / sqrtf(f1*f2); }
				else { corr = 0; }

				if (corr >= savedcorr)
				{
					result.c = savedcorr = corr; savedpos = q;
				}

				result.x = side.x;
				result.y = savedpos + neckBarY;
			}
		}

		else
		{
			for (q = -30; q <= 5; q++)
			{
				for (ypos1 = neckBarY - 14; ypos1 < neckBarY + 14; ypos1++)
				{
					for (xpos3 = xpos1 - 4; xpos3 < xpos1 + 24; xpos3++)
					{
						*(im_match2 + xpos2 + lineLength2 * ypos2) = *(im_srcIn + xpos3 + lineLength * (ypos1 + q));
						xpos2 += 1;
					}

					xpos2 = 0; ypos2 += 1;
				}

				ypos2 = 0;

				//Calculate Correlation
				sumxy = 0.0; sumx = 0.0; sumy = 0.0; sumxsq = 0.0; sumysq = 0.0;

				for (r = 0; r < length; r++)
				{
					sumxy += (*(im_matchr->GetPattern() + r)) * (*(im_match2 + r));
					sumx += *(im_matchr->GetPattern() + r);
					sumy += *(im_match2 + r);
					sumxsq += (*(im_matchr->GetPattern() + r)) * (*(im_matchr->GetPattern() + r));
					sumysq += (*(im_match2 + r)) * (*(im_match2 + r));
				}

				f0 = sumxy - ((sumx*sumy) / length);
				f1 = sumxsq - ((sumx*sumx) / length);
				f2 = sumysq - ((sumy*sumy) / length);

				if (sqrtf(f1*f2) != 0.0) { corr = f0 / sqrtf(f1*f2); }
				else { corr = 0; }

				if (corr >= savedcorr) { result.c = savedcorr = corr;	savedpos = q; }

				result.x = side.x;
				result.y = savedpos + neckBarY;
			}
		}
	}

	result.c = 100 * result.c;

	return result;
}

float CapInspector::FindCapMid(unsigned char* im_srcIn, float MidTop)
{
	int nCam = 5;
	float result = 0;

	int pixa, pixb, pixc, pixd, pixe, pixf;
	int pixax, pixbx, pixcx, pixdx, pixex, pixfx;
	int pixam, pixbm, pixcm, pixdm, pixem, pixfm;
	int pixal, pixbl, pixcl, pixdl, pixel, pixfl;
	int pixar, pixbr, pixcr, pixdr, pixer, pixfr;

	int dl, dr, dm;//, dmx, 
	int dx, dz;

	bool debugging = false;

	int xpos = (int) MidTop;
	int ypos = 1;

	int xshift1 = 3;
	int xshift2 = -3;
	int xshift3 = 6;
	int xshift4 = -6;
	int maxdif = 0;

	int r = 2;

	if (xpos <= 500 && xpos >= 1)
	{
		for (ypos = 3; ypos < 250; ypos++)
		{
			pixa = *(im_srcIn + xpos - 1 + croppedLineLength * (ypos - r));
			pixb = *(im_srcIn + xpos + croppedLineLength * (ypos - r));
			pixc = *(im_srcIn + xpos + 1 + croppedLineLength * (ypos - r));

			pixd = *(im_srcIn + xpos - 1 + croppedLineLength * (ypos + r));
			pixe = *(im_srcIn + xpos + croppedLineLength * (ypos + r));
			pixf = *(im_srcIn + xpos + 1 + croppedLineLength * (ypos + r));

			pixal = *(im_srcIn + xpos + xshift1 - 1 + croppedLineLength * (ypos - r));
			pixbl = *(im_srcIn + xpos + xshift1 + croppedLineLength * (ypos - r));
			pixcl = *(im_srcIn + xpos + xshift1 + 1 + croppedLineLength * (ypos - r));

			pixdl = *(im_srcIn + xpos + xshift1 - 1 + croppedLineLength * (ypos + r));
			pixel = *(im_srcIn + xpos + xshift1 + croppedLineLength * (ypos + r));
			pixfl = *(im_srcIn + xpos + xshift1 + 1 + croppedLineLength * (ypos + r));

			pixar = *(im_srcIn + xpos + xshift2 - 1 + croppedLineLength * (ypos - r));
			pixbr = *(im_srcIn + xpos + xshift2 + croppedLineLength * (ypos - r));
			pixcr = *(im_srcIn + xpos + xshift2 + 1 + croppedLineLength * (ypos - r));

			pixdr = *(im_srcIn + xpos + xshift2 - 1 + croppedLineLength * (ypos + r));
			pixer = *(im_srcIn + xpos + xshift2 + croppedLineLength * (ypos + r));
			pixfr = *(im_srcIn + xpos + xshift2 + 1 + croppedLineLength * (ypos + r));

			pixam = *(im_srcIn + xpos + xshift3 - 1 + croppedLineLength * (ypos - r));
			pixbm = *(im_srcIn + xpos + xshift3 + croppedLineLength * (ypos - r));
			pixcm = *(im_srcIn + xpos + xshift3 + 1 + croppedLineLength * (ypos - r));

			pixdm = *(im_srcIn + xpos + xshift3 - 1 + croppedLineLength * (ypos + r));
			pixem = *(im_srcIn + xpos + xshift3 + croppedLineLength * (ypos + r));
			pixfm = *(im_srcIn + xpos + xshift3 + 1 + croppedLineLength * (ypos + r));

			pixax = *(im_srcIn + xpos + xshift4 - 1 + croppedLineLength * (ypos - r));
			pixbx = *(im_srcIn + xpos + xshift4 + croppedLineLength * (ypos - r));
			pixcx = *(im_srcIn + xpos + xshift4 + 1 + croppedLineLength * (ypos - r));

			pixdx = *(im_srcIn + xpos + xshift4 - 1 + croppedLineLength * (ypos + r));
			pixex = *(im_srcIn + xpos + xshift4 + croppedLineLength * (ypos + r));
			pixfx = *(im_srcIn + xpos + xshift4 + 1 + croppedLineLength * (ypos + r));

			dm = abs((pixa + 2 * pixb + pixc) - (pixd + 2 * pixe + pixf));
			dz = abs((pixam + 2 * pixbm + pixcm) - (pixdm + 2 * pixem + pixfm));
			dl = abs((pixal + 2 * pixbl + pixcl) - (pixdl + 2 * pixel + pixfl));
			dr = abs((pixar + 2 * pixbr + pixcr) - (pixdr + 2 * pixer + pixfr));
			dx = abs((pixax + 2 * pixbx + pixcx) - (pixdx + 2 * pixex + pixfx));

			if (dm > capLimit2 && dl > capLimit2 && dr > capLimit2 && dz > capLimit2 && dx > capLimit2)//&& (d2 > Limit) && (d3 > Limit )
			{
				maxdif = dm;
				if (dl > maxdif) maxdif = dl;
				if (dr > maxdif) maxdif = dr;
				if (dz > maxdif) maxdif = dz;
				if (dx > maxdif) maxdif = dx;
				result = (float) ypos;
				break;
			}
		}
	}

	//TODO:
	//Display result
	//currEdgeM = maxdif;


	return result;
}

