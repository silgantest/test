#include "PLCcomm.h"
#include "InspectionDoResults.h"
#include <map>
#include "PLCcommProxy.h"
#include "ResultsGroupPool.h"
#include "ClientIPC.h"
#include "JobManager.h"
#include "CounterLocks.h"
#include "ResultsGroupBuffer.h"
#include "ResultDetails.h"

#ifndef RESULTS_HANDLER_H
#define RESULTS_HANDLER_H

class ResultsHandler: public NotifyJobUpdated
{
private:
	ResultsGroup* resultsGroup[2];

	int requiredInspections;
	int numCameras;

	std::mutex lock;
	ResultsGroupBuffer* buffer;
	InspectionDoResults* doResults;
	ResultsGroup* getGroup(int imageNum);
	void handleComplete(ResultsGroup* node);
	int entryCount;

public:
	ResultsHandler(int reqIns, int numCams, AppConfig* config);
	virtual ~ResultsHandler();
	virtual void JobUpdated(Job* newJob) override;

	void Start(CounterLocks* lk);
	void Stop();

	void AddResults(InspectionResult* res[], int count, CapturedImage* im, Cap85IntermediateImagePack* pack, ResultDetails* details);
	void AddResult(InspectionResult* ir, CapturedImage* im, IntermediateImagePack* pack, ResultDetails* details);
};

#endif //RESULTS_HANDLER_H