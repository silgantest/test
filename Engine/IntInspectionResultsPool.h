#include "IntRangeResult.h"
#include "ReusablePool.h"

#ifndef INT_INSPECTION_RESULTS_POOL_H
#define INT_INSPECTION_RESULTS_POOL_H

class IntInspectionResultsPool : public ReusablePool
{
private:
	static IntInspectionResultsPool* instance;

protected:
	IntInspectionResultsPool();

	virtual Reusable* create() override;
	void deleteObject(Reusable* obj) override;

public:
	IntInspectionResultsPool(IntInspectionResultsPool&) = delete;
	IntInspectionResultsPool& operator=(IntInspectionResultsPool& rhs) = delete;

	virtual ~IntInspectionResultsPool();

	static IntRangeResult* GetResultObj();
	static void Initialize();
	static void Shutdown();
	static void DeleteResultObj(IntRangeResult* obj);

};

#endif