#include "FloatRangeResult.h"
#include "FloatingInspectionResultsPool.h"

FloatRangeResult::FloatRangeResult() 
{
	Reset();
}

FloatRangeResult::~FloatRangeResult() 
{
}

void FloatRangeResult::SetResultLeft(float res) 
{ 
	xLeft = res; 
}

void FloatRangeResult::SetResultRight(float res) 
{
	xRight = res; 
}

void FloatRangeResult::SetResult(float res) 
{ 
	x = res; 
}

float FloatRangeResult::GetResult()
{
	return x;
}

void FloatRangeResult::Reset() 
{ 
	x = xLeft = xRight = NAN; 
}
void FloatRangeResult::Unload() 
{ 
	FloatingInspectionResultsPool::DeleteResultObj(this); 
}

float FloatRangeResult::GetLeftResult()
{
	return this->xLeft;
}

float FloatRangeResult::GetRightResult()
{
	return this->xRight;
}

