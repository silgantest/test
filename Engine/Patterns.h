#include "ImageFileUtilities.h"

#ifndef PATTERNS_H
#define PATTERNS_H

class ImagePattern
{
private:
	unsigned char* pattern;
	int width;
	int height;
	int size;

public:
	ImagePattern()
	{
		pattern = nullptr;
		height = 0;
		width = 0;
		size = 0;
	}

	virtual ~ImagePattern()
	{
		if (pattern)
			delete[] pattern;
	}

	unsigned char* GetPattern()
	{
		return pattern;
	}

	int GetHeight()
	{
		return this->height;
	}

	int GetWidth()
	{
		return this->width;
	}

	int GetSize()
	{
		return this->size;
	}

	void Reallocate(int wid, int hgt, int sz)
	{
		if(pattern)
			delete[] pattern;
		pattern = new unsigned char[sz];
		this->width = wid;
		this->height = hgt;
		this->size = sz;
	}

	void CopyTo(ImagePattern* ip)
	{
		if (this->size != ip->size ||
			this->width != ip->width ||
			this->height != ip->height)
		{
			ip->Reallocate(this->width, this->height, this->size);
		}
		else if (this->size == 0)
		{
			ip->Reallocate(60, 60, 60*60*4);
		}

		memcpy(ip->GetPattern(), this->pattern, this->size);
	}

	void Load(const char* path)
	{
		this->pattern = ImageFileUtilities::ReadBMP(path, true, &this->width, &this->height, &this->size);
	}
};

class Patterns
{
private: 
	int patternType;

	ImagePattern* bytePattBy3_method6_1;
	ImagePattern* bytePattBy6_method6_1;

	ImagePattern* bytePattBy3_method6;
	ImagePattern* bytePattBy6_method6;

	ImagePattern* bytePattBy3_method6_num3;
	ImagePattern* bytePattBy6_method6_num3;

	ImagePattern* bytePattBy3;
	ImagePattern* bytePattBy6;

	ImagePattern* pattern2;
	ImagePattern* pattern2X;

	ImagePattern* im_matchl;
	ImagePattern* im_matchr;

	//unsigned char* bytePattBy3;

	void SetPatternType(int type);
	void LoadPattern2(const char* jobPath);
	void LoadPatternX_method6(const char* jobPath);
	void LoadPatternX(const char* jobPath);
	void LoadPatternX_method6_num3(const char* jobPath);
	void DeleteAll();
	void LoadCaps(const char* jobPath);

public:
	Patterns();
	~Patterns();

	void Load(const char* jobPath);
	void CopyBytePattBy3(ImagePattern* ip);
	void CopyBytePattBy6(ImagePattern* ip);
	void CopyBytePattBy6_method6_1(ImagePattern* ip);
	void CopyBytePattBy3_method6_1(ImagePattern* ip);
	void CopyBytePattBy6_method6(ImagePattern* ip);
	void CopyBytePattBy3_method6(ImagePattern* ip);
	void CopyBytePattBy6_method6_num3(ImagePattern* ip);
	void CopyBytePattBy3_method6_num3(ImagePattern* ip);
	void CopyPattern2(ImagePattern* ip);
	void CopyPattern2X(ImagePattern* ip);
	void CopyCapLeft(ImagePattern* ip);
	void CopyCapRight(ImagePattern* ip);

};

#endif 