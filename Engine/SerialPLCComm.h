#include "PLCComm.h"
#include "Windows.h"
///PLATFORM SPECIFIC

#ifndef SERIAL_PLC_COMM_H
#define SERIAL_PLC_COMM_H

class SerialPLCcomm : virtual public PLCcomm
{
public:
	SerialPLCcomm();
	virtual ~SerialPLCcomm();

	virtual bool SendPassFail(bool pass);
	virtual bool SendPassFail(char* code);

	static bool IsPortAvailable();
	virtual bool PauseTrigger();
	virtual bool ResumeTrigger();

private:
	HANDLE port;
	DWORD bytesWritten;

	DWORD numBytes;
};

#endif