#include <pylon/PylonIncludes.h>
#include <pylon/gige/BaslerGigEInstantCamera.h>
#include <pylon/usb/BaslerUsbInstantCamera.h>
#include <pylon/gige/ActionTriggerConfiguration.h>

#include <string>
#include "CameraSpecificConfiguration.h"

#ifndef BALSER_CAMERA_SPECIFIC_CONFIGURATION_H
#define BALSER_CAMERA_SPECIFIC_CONFIGURATION_H

using namespace std;
using namespace Pylon;

/*Basler used typedef to avoid duplicate code.  Never imagined GigE and supported by same system*/
class BaslerGigeSpecificConfiguration : public virtual CameraSpecificConfiguration
{
private:
	CBaslerGigEInstantCamera* camera;
public:
	BaslerGigeSpecificConfiguration(CBaslerGigEInstantCamera* cam) { camera = cam; };
	virtual ~BaslerGigeSpecificConfiguration() {};
	
	virtual void SetupTrigger(bool hardware)
	{
		if (!camera->IsGrabbing())
		{
			camera->TriggerMode = Basler_GigECamera::TriggerModeEnums::TriggerMode_On;
			if(hardware)
				camera->TriggerSource = Basler_GigECamera::TriggerSourceEnums::TriggerSource_Line1;
			else
				camera->TriggerSource = Basler_GigECamera::TriggerSourceEnums::TriggerSource_Software;
		}
	}

	virtual void SetAOI(int x, int y, int width, int height)
	{
		if (!camera->IsGrabbing())
		{
			/*Width and Height must be set first*/
			if (IsWritable(camera->Width))
				camera->Width = width;
			if (IsWritable(camera->Height))
				camera->Height = height;
			if (IsWritable(camera->OffsetX))
				camera->OffsetX.SetValue(x, true);
			if (IsWritable(camera->OffsetY))
				camera->OffsetY = y;
		}
	}

	virtual void SetupPixelFormat()
	{
		camera->PixelFormat = Basler_GigECamera::PixelFormat_BGRA8Packed;
	}

};

class BaslerUsbSpecificConfiguration : public virtual CameraSpecificConfiguration
{
private:
		CBaslerUsbInstantCamera* camera;
public:
	BaslerUsbSpecificConfiguration(CBaslerUsbInstantCamera* cam) { camera = cam; };
	virtual ~BaslerUsbSpecificConfiguration() {};

	virtual void SetAOI(int x, int y, int width, int height)
	{

		if (!camera->IsGrabbing())
		{
			/*Width and Height must be set first*/
			if (IsWritable(camera->Width))
				camera->Width = width;
			if (IsWritable(camera->Height))
				camera->Height = height;
			if(IsWritable(camera->OffsetX))
				camera->OffsetX.SetValue(x, true);
			if (IsWritable(camera->OffsetY))
				camera->OffsetY = y;
		}
	}

	virtual void SetupTrigger(bool hardware)
	{
		if (!camera->IsGrabbing())
		{
			camera->TriggerMode = Basler_UsbCameraParams::TriggerModeEnums::TriggerMode_On;
			if (hardware)
				camera->TriggerSource = Basler_UsbCameraParams::TriggerSourceEnums::TriggerSource_Line1;
			else
				camera->TriggerSource = Basler_UsbCameraParams::TriggerSourceEnums::TriggerSource_Software;
		}
	}

	virtual void SetupPixelFormat()
	{
		throw new exception("USB Pixel Format NOT Supported.  Must write conversion.");
//		camera->PixelFormat = Basler_UsbCameraParams::PixelFormat_BGRA8Packed;
	}


};


#endif //CAMERA_SPECIFIC_CONFIGURATION_H
